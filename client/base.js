import React from 'react';
import Base from './base.js';
import Home from './pages/home';
import Streams from './pages/stream';
import Programs from './pages/program';
import Sources from './pages/source';
import Channels from './pages/channel';
import Logs from './pages/logs';
import { RouteHandler, default as Router, Route, DefaultRoute, Link }  from 'react-router';
import Sockets from './lib/sockets';
let openSocket; 
import Console from 'react-console';
import _ from 'lodash';
import classnames from 'classnames';

import async from 'async';
import debugging from 'debug';
let	debug = debugging('snowstreams:client:base');

import { Collapse, Navbar, CollapsibleNav, Nav, NavItem, MenuItem, DropdownButton, NavDropdown } from 'react-bootstrap';
import { MenuItemLink, NavItemLink, ButtonLink, ListGroupItemLink } from 'react-router-bootstrap';


let App = React.createClass({
	
	contextTypes: {
		router: React.PropTypes.func
	},
	
	getInitialState() {
		
		//debug.log = this.logger;
		let _this = this;
		
		// cache log entries if they come in too fast
		this._cargo = async.cargo(function (logs,callback) {
			let send = {
				error: [],
				doc: [],
				message: []
			}
			for(let i=0; i<logs.length; i++){
				send.alive = logs[i].alive;
				if(logs[i].error !== '') send.error.push(logs[i].error);
				if(logs[i].doc !== '') send.doc.push(logs[i].doc);
				if(logs[i].message !== '') send.message.push(logs[i].message);
			}
			_this.setState({log : send});
			debug('log set sent ', send);
			callback();
		}, 25);
		return {
			status: {
				html: 'loading status...',
				updated: false,
				showStatus: false,
			},
			sockets: {
				auth: false,
				open: false,
			},
			log: {
				alive: false
			},
		}
	},
	
	componentWillReceiveProps(props) {
		debug('props', props, this.props)
		if(props.path !== this.props.path) {
			// new route
			let {status} = { ...this.state };
			status.updated = false;
			status.showStatus = false;
			this.setState({ status });
		}
		return false;
	},
	
	componentDidMount() {
		debug('emit status', this.state, this.props, this.context);
		Sockets.init(this.context.router, () => {
			openSocket = Sockets.openSocket;
			this.setState({
				sockets: {
					auth: Sockets.authSocket,
					open: Sockets.openSocket,
				}
			});
			['Source-log', 'File-log', 'Channel-log', 'Program-log', 'ProgramArgument-log', 'Stream-log', 'commander']
				.forEach((event) => {
					Sockets.authSocket.on(event, this.logger);
				});
			
			// status and dynamic properties update
			Sockets.openSocket.emit('status');
			Sockets.openSocket.on('status', (data) => {
				debug('status',data);
				let { status } = { ...this.state };
				
				// reset status class
				status.updated = false;
				this.setState({ status });
				
				// reset the proxy uri folder
				if(data.proxy) {
					Sockets.proxy = data.proxy;
				}
				
				// add the new data and set up[datred bit
				status.html = data.status;
				status.updated = true;
				
				this.setState({ status });
			});
		});
			
	},
	
	render() {    
		let _this = this;
		
		let className = classnames( {
			// flash is status updated
			'row--status--update': this.state.status.updated
		});
		let statusUpdated = classnames('status-link', {
			// glow if status updated recently
			'updated': this.state.status.updated
		});
		let loggedin = !this.state.sockets.auth ? 
			(<NavItemLink eventKey={7.1} to='signin'><span className="glyphicon glyphicon-log-in" /></NavItemLink>)
		:
			(<NavDropdown eventKey={6} title={<span className="glyphicon glyphicon-globe" />}  id="collapsible-navbar-dropdown">				  
				<MenuItem eventKey={7} href='/testbed'>Testbed</MenuItem> 
				 
				<MenuItem eventKey={8}  href='/keystone'>Keystone UI</MenuItem>
				<MenuItem divider />
				<MenuItem eventKey={9} href='/keystone/signout'>Logout</MenuItem>
			</NavDropdown>);
		let navbarInstance = (
		    <Navbar className={className} brand={<a href="/client/home" onClick={this.go} data-go="home"><span className='glyphicon glyphicon-home' aria-hidden="true"></span></a>} toggleNavKey={0}>
		   
			    <Nav navbar eventKey={0} >
					<NavItemLink eventKey={2} to='sources'>Sources</NavItemLink>
					<NavItemLink eventKey={3} to='streams'>Streams</NavItemLink>
					<NavItemLink eventKey={4} to='channels'>Channels</NavItemLink>
				
					<Nav navbar right >
						<NavItemLink eventKey={5} to='programs'>Programs</NavItemLink>
						<NavItem className={statusUpdated} eventKey={5.1} onClick={this.showStatus}>Status</NavItem> 
						{loggedin}
						
					</Nav> 
				</Nav>
				<Collapse in={this.state.status.showStatus} className="">
					<div className="col-sm-12 no-padding status" >
						<div className="col3 col-sm-12">
							<div dangerouslySetInnerHTML={{__html:this.state.status.html}} />
							<div className="clearfix" />
						</div>
						<div className="clearfix col-sm-12 close-status" onClick={this.showStatus}>close</div>
					</div>
					
				</Collapse>
			
			</Navbar>
		);
		
		let show;
		if(this.state.sockets.auth) {
			show = <RouteHandler sockets={Sockets} logger={this.logger} openConsole={this.openConsole} toggleConsole={this.toggleConsole} {...this.props} />
		} else {
			show = <span >Waiting for sockets</span>
		}
				
		return ( 
			<div  className="body">
				<Console command={_this.command} toggle={this.toggleConsole} alive={this.state.log.alive} log={this.state.log}  />
				{navbarInstance}
				<div className="navbar-faker" />	
				<div  className={"row1  shakeme "}>
					
					
					<div className=" col-sm-12 page-box no-padding">
						{show}
					</div>
					
				</div>
				
			</div>
		);
	},
	
	showStatus() {
		let { status } = { ...this.state };
		status.updated = false;
		status.showStatus = !this.state.status.showStatus;
		this.setState({ status });
	},
	
	go(e) {
		e.preventDefault();
		debug(e)
		let target;
		if(e.currentTarget) {
			target = e.currentTarget;
		} else {
			target = e.target;
		}
		this.context.router.transitionTo(target.dataset.go);
	}, 
	
	logger( ...data ) {
		let _this = this; 
		let log = { alive: this.state.log.alive };
		data.forEach((v) => {
			addLog(v);
		});
		
		function addLog(data) {
			log.message = '';
			log.doc = '';
			log.error = '';
			if(_.isObject(data)) {
				if(data.message) {
					log.message = data.message
				}
				if(data.doc) {
					log.doc = data.doc
				}
				if(data.data) {
					log.doc = data.data
				}
				if(data.error) {
					log.error = data.error
				}
				
			} else if(data) {
				log.message = data
				
			}
			_this._cargo.push(log);
		}
	},
	
	toggleConsole(e) {
		e.preventDefault();
		this.setState({ log: {alive: !this.state.log.alive} });
	},
	
	openConsole() {
		this.setState({ log: {alive: true} });
	},
	
	command(value) {
		
		let doc, find;
		let aliases = {
			F : 'File',
			C: 'Channel',
			P: 'Program',
			A: 'ProgramArgument',
			I: 'Source',
			O: 'Stream',
			c: 'Channel',
			f: 'File',
			p: 'Program',
			i: 'Source',
			o: 'Stream',
			h: 'help',
		}
		
		let args;
		let first;
		// search for doc and split there first
		if(value.search('doc') > -1) {
			first = value.split('::doc::');
			args = first[0].split('::');
			doc = first[1].split('::')
		} else if(value.search('search') > -1) {
			first = value.split('::search::');
			args = first[0].split('::');
			find = first[1].split('::')
		} else {
			args = value.split('::');
		}
		debug('command args', args);
		let main;
		if(!_.isArray(args)) {
			main = value.length === 1 ? aliases[value] : value || 'help';
			args = [main,main];
		} else {
			main = args[0].length === 1 ? aliases[args[0]] : args[0];
			if(!args[1]) args[1] = main;
		}		
		debug('command args again', args)
		let opts = {
			iden: 'commander' ,
			emit: 'help'
		}
		
		// for List args[0] = list and emitter = args[1]
		if(main && main[0] === main[0].toUpperCase()) {
			opts.list = main;
			opts.emit = args[1];
		} else {
			opts.manage = args[1].length === 1 ? aliases[args[1]] : args[1];
			if(main) opts.emit = main;
		}
		
		let arg = args.slice(2);
		_.each(arg, function(v) {
			let vv = v.split(':');
			if(_.isArray(vv)) {
				opts[vv[0]] = vv[1];
			}
		});
		if(_.isArray(doc)) {
			let sends = {}
			_.each(doc, function(v) {
				let vv = v.split(':');
				if(_.isArray(vv)) {
					sends[vv[0]] = vv[1];
				}
			});
			opts.doc = sends;
		}
		if(_.isArray(find)) {
			let sends = {}
			_.each(find, function(v) {
				let vv = v.split(':');
				if(_.isArray(vv)) {
					sends[vv[0]] = vv[1];
				}
			});
			opts.find = sends;
		}
		
		debug('emit', opts.emit, 'data', opts);
		this.logger({ message: 'request sent', data: opts});
		if(opts.list === 'wan') {
			this.state.sockets.auth.io.emit(opts.emit, opts);
		} else {
			this.state.sockets.auth.emit(opts.emit, opts);
		}
	}
});

export default App;

