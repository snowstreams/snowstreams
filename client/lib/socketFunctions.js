import React from 'react';
import path from 'path';
import _ from 'lodash';
import randomNumber from 'hat';

import debugging from 'debug';
let	debug = debugging('snowstreams:client:lib:socketFunctions');



function options() {
	 
	var exports = {};
	
	exports.trapResponse = function(socket, callback) {
		
		var unique = randomNumber();
		
		socket.once(unique, callback);
		
		return unique;
	}
	exports.trap = exports.trapResponse;
	
	exports.list = function(list, options, callback) {
		
		if(_.isFunction(options)) {
			callback = options;
			options = {};
		}
		
		if(!_.isObject(options)) options = {};
		
		if(!_.isFunction(callback)) return;

		this.authSocket.emit('list',{ 
			list: list, 
			exclude: options.exclude,
			include: options.include,
			limit: options.limit || 100,
			skip: options.skip,
			populate: options.populate,
			query: options.query,
			sort: options.sort,
			iden: this.trapResponse(this.authSocket, callback)
		});
	}
	exports.getDocument = function(list, options, callback) {
		
		if(!_.isFunction(callback)) return;

		this.authSocket.emit('get',{ 
			list: list, 
			exclude: options.exclude || '__v',
			find: { slug: options.slug },
			id: options.id || false,
			query: options.query,
			populate: options.populate,
			iden: this.trapResponse(this.auth, callback) 
		});
	}
	exports.submitForm = function(context, e, emit, page, callback) {
		var _this = context;
		
		_this.dismissAlert();
		
		//return setTimeout(()=>{callback('fake error');},2000);
		
		var form = _this.state.form;
		
		if(!_.isFunction(callback)) callback = function(){};
		
		var cb = (err, data, state, fn) => {
			setTimeout(()=>{
				if(state) {
					_this.setState(state);
				}
				if(_.isFunction) {
					fn();
				}
				callback(err, data);
			},1000);
			
		}
		var myFn = function(data) {
			if(data.error) {
				debug('form received error', data);
				
				cb(data, data, { 
					newalert: {
						style: 'danger',
						data: data,
						show: true
					}
				}, () => {
					_this.refs.manage.error();
				});
				
			} else {
				debug('form received response', data);
				var msg = form._id ? page + ' updated successfully... refreshing information' : page + ' added successfully... refreshing information'
				
				cb(null, data, { 
					newalert: {
						style: 'success',
						html: msg,
						show: true
					},
					// form:  _this.defaultForm()
				}, () => {
					
				});
			}
			
		}
		debug('send socket request');
		
		this.authSocket.emit(emit,{
			list: page, 
			action: form._id ? 'update' : 'create',
			doc: form,
			id: form._id || false,
			iden: this.trapResponse(this.authSocket, myFn) 
		});
		
	}
	
	exports.reallyDelete = function(context, e, page, callback) {
		debug('delete', context.state.form.name, context.state.form._id)
		var _this = context;
		var sock = this;
		if(!_.isFunction(callback)) callback = function(){};
		
		var myFn = function(data) {
			if(data.error) {
				_this.setState(
					{ 
						newalert: {
							style: 'danger',
							data: data,
							show: true
						}
					},
					() => {
						callback(data);
					}
				);
				
			} else {
				debug('form received response', data);
				var msg =  page + ' deleted'
				_this.setState(
					{ 
						newalert: {
							style: 'warning',
							html: msg,
							show: true
						},
						form:  _this.defaultForm()
					},
					() => {
						callback(null, data);
					}
				);
			} 
		}
		
		setTimeout(()=>{
			sock.authSocket.emit('remove',{ 
				list: page, 
				id: _this.state.form._id,
				iden: sock.trapResponse(sock.auth, myFn) 
			});
		}, 750);
	}
	
	exports.manager = function manager(options) {
		
		debug('manager', options)
		options.iden = 'commander';
		sock.authSocket.emit('manager',options);
		
	}
	
	exports.play = function play(startORstop, context, asset, callback) {
		debug(startORstop, context.state)
		
		if(!_.isFunction(callback)) callback = function(){};
		
		let slug = context.state.form.slug;
		
		if(!asset) {
			asset = context.state.asset;
		}
		
		var cb = (err, data) => {
			setTimeout(()=>{
				callback(err, data);
			},750);
		}
		
		if(!slug || !asset) {
			cb('Slug & Asset Required');
			return;
		}
		
		var myFn = function(data) {
			if(data.error) {
				
				cb(data.error);
				
			} else {
				
				debug(asset + ' received response', data);
				context.props.logger(data);
				cb(null, data);
				
			} 
		}
		
		setTimeout(()=>{
			this.authSocket.emit(startORstop, { 
				asset, 
				slug,
				iden: this.trapResponse(this.authSocket, myFn) 
			});
		}, 750);
	}
	
	exports.runProgram = function runProgram(context, program, query, callback) {
		debug('run program', program, query, context.state)
		
		if(!_.isFunction(callback)) callback = function(){};
		
		if(!program) {
			context.setState({ 
				newalert: {
					style: 'danger',
					data: { error: 'Program Required' },
					show: true
				}
			});
			return callback('Program Required');
		}
		
		var myFn = function(data) {
			if(data.error) {
				context.setState({ 
					newalert: {
						style: 'danger',
						data: data,
						show: true
					}
				});
				callback(data);
			} else {
				debug(program + ' received response', data);
				context.props.logger(data);
				callback(null, data);
			} 
		}
		
		this.authSocket.emit('runProgram', { 
			program, 
			query,
			iden: this.trapResponse(this.authSocket, myFn) 
		});
		
	}
	
	return exports;
	
}

export default options;
