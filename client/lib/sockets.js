import _ from 'lodash';
import debugging from 'debug';
import SF from  './socketFunctions';
import io from 'socket.io-client';
import {Navigation} from 'react-router';

let	debug = debugging('snowstreams:client:lib:sockets');
let port = window.socketPort;
let host = window.socketHost;

let Sockets = function() {
	
	// connected
	this.connected = {
		auth: false,
		open: false
	}
	this.authSocket = io('//' + host + ':' + port + '/lists');
	this.auth = this.authSocket;
	this.openSocket = io('//' + host + ':' + port + '/snowstreams');
	this.open = this.openSocket;
	debug('new sockets', '//' + host + ':' + port + '/lists');
	
	this.proxy = 'proxy';
	
	
}

Sockets.prototype.connectAuth = function(callback) {
	this.authSocket = io('//' + host + ':' + port + '/lists', { 'force new connection': true });
	this.auth = this.authSocket;
	debug('reconnect auth', this.auth);
	
	this.authSocket.on('connect',(data) => {
		debug('auth connected', 'lists');
		this.connected.auth = true;
		
	});
	this.authSocket.on('connect-error',(err) => {
		debug('auth connect-error',err);
		//location.href = '/client/signin';
		router.transitionTo('signin');
	});
	this.authSocket.on('error', (err) => {
		debug('auth error',err);
		//location.href = '/client/signin';
		this.router.transitionTo('signin');
	});
	
	if(_.isFunction(callback)) {
		callback(null,true);
	}
}

Sockets.prototype.init = function(router, callback) {
	
	this.router = router;
	
	let _this = this;
	
	this.authSocket.on('connect',(data) => {
		debug('auth connected', 'lists');
		this.connected.auth = true;
		
	});
	this.authSocket.on('connect-error',(err) => {
		updateConsole('auth connect-error',err);
		//location.href = '/client/signin';
		router.transitionTo('signin');
	});
	this.authSocket.on('error', (err) => {
		updateConsole('auth error',err);
		//location.href = '/client/signin';
		this.router.transitionTo('signin');
	});
	this.openSocket.on('connect',(data) => {
		updateConsole('open connected','snowstreams');
		this.connected.open = true;
		callback(); 
	});
	this.openSocket.on('connect-error',(err) => {
		updateConsole('open connect-error',err)
	});
	this.openSocket.on('error',(err) => {
		updateConsole('open error',err)
	});
	function updateConsole(...a) {
		debug(...a); 
	}
	
	
}

_.extend(Sockets.prototype, SF());


export default new Sockets();
