import React from 'react';
import { Link } from 'react-router';
import _ from 'lodash';

let module = { exports: {} }

/* db stuff */
module.exports.Store = require('./store');

/* alerts */
module.exports.Alerts = require('./flash');

/* man component
 * simple example
 * */
var GMan = React.createClass({
	getDefaultProps: function() {
		return ({divstyle:{float:'right',}});
	},
	render: function() {
	    return (
		<div style={this.props.divstyle} dangerouslySetInnerHTML={{__html: '' || ''}} />
	    );
	}
});

module.exports.GMan = GMan;

/* 
 * we use this for the countdown timer before we redirect a logged 
 * in user.  you can disable it 
 * by sending a redirect time of 0
 * */
var GInterval = {
	  intervals: [],
	  setInterval: function() {
		return this.intervals.push(setInterval.apply(null, arguments));
	  },
	  clearIntervals: function(who) {
		who = who - 1;
		if(GInterval.intervals.length === 1) {
			//console.log('clear all intervals',this.intervals)
			GInterval.intervals.map(clearInterval);
			GInterval.intervals = [];
		} else if(who && GInterval.intervals[who]) {
			//console.log('clear intervals',who,this.intervals[who])
			clearInterval(GInterval.intervals[who]);
		} else {
			//console.log('map intervals',this.intervals)
			GInterval.intervals.map(clearInterval);
			GInterval.intervals = [];
		}
	  }
};

module.exports.GInterval = GInterval;


var Button = React.createClass({
	contextTypes: {
		router: React.PropTypes.func
	},
	getInitialState: function() {
		return {};
	},
	getDefaultProps: function() {
		return {
			team: ''
		};
	},
	link: function(to) {
		this.context.router.transitionTo(to, {inning: this.props.inning, half: this.props.half});
	},
	render: function() {
		var click = this.props.click;
		var link = this.props.text;
		if(this.props.pitcher) {
			click = this.link.bind(this, 'pitcher');
		} else if(this.props.inning) {
			click = this.link.bind(this, 'inning');
		} else if(this.props.settings) {
			click = this.link.bind(this, 'home');
		}
		
		return (
			<div className={"button tall click " + this.props.class}>
				<div className={"number"} onClick={_.isFunction(click) ? click : false} >
					{link}
					<div className={"tag mlb-" + this.props.team.toLowerCase()} >
						{this.props.team}
					</div>
				</div>
			</div>
		);	
	},
	
});

module.exports.Button = Button;

export module.exports
