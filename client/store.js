import _ from 'lodash';

var store = function() {

}

store.prototype.setItem = function(key, value) {
	console.log('setItem',key, value);
	localStorage.setItem(key, JSON.stringify(value)); 
}

store.prototype.getItem = function(key) {
	var getitem = localStorage.getItem(key);
	//console.log('getitem', getitem);
	return JSON.parse(getitem);
}

store.prototype.removeItem = function(key) {
	console.log('removeItem',key);
	localStorage.removeItem(key); 
}

store.prototype.getState = function() {
	var Game = this.getGame() || {};
	return {
		game: Game,
		innings: Game.innings || false,
		inning: Game.inning || false,
		half: Game.half || false,
		home: Game.home || false,
		visitor: Game.visitor || false,
		reports: Game.reports || false,
		alert: Game.inning == 7 && Game.half === 2
	}
}

store.prototype.setGame = function(game) {
	this.setItem(game.model, game);
	this.setItem('current', game.model);
	return this;
}

store.prototype.saveGame = function(game) {
	console.log('save game', game)
	if(!isGame(game)) return false;
	this.setItem(game.model, game);
	return this;
}

store.prototype.getGame = function(id) {
	var current = id || this.getItem('current');
	//console.log('current',current);
	return this.getItem(current);
}

store.prototype.changeGame = function(id) {
	var game = this.getGame(id);
	this.setItem('current', game.model);
	return game;
}

store.prototype.newGame = function(options) {
	console.log('new game')
	var dname = moment(options.date);
	var game = {
		name: options.home + ' vs ' + options.visitor,
		home: options.home,
		date: dname.format('L'),
		visitor: options.visitor,
		model: dname.valueOf(),
		innings: options.innings || 9,
		inning: options.inning || 1,
		half: options.half || 2,
		reports: {
			inning: [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39],
		}
	}
	game.reports.inning.map(function(val,key) {
		game.reports.inning[key] = {
			top: {},
			bottom: {},
			pitchers: [],
		}
	});
		
	//var nname = dname.getFullYear() + dname.getMonth() + dname.getDate() + 
	this.setGame(game);
	
	return game;
}

store.prototype.allStorage = function(){

    var archive = {},  //notice change here
        keys = Object.keys(localStorage),
        i = 0;

    for (; i < keys.length; i++) {
        archive[ keys[i] ] = this.getItem( keys[i] );
    }

    return archive;
}
store.prototype.report = function(report, cb) {
	var game = this.getGame();
	var inn = game.reports.inning[report.inning];
	console.log('report',inn, report);
	if(_.isObject(inn)) {
		if(report.pitcher) {
			if(report.params.id) {
				if(report.prev !== report.inning) {
					// pitcher report changed innings so remove previous file and push new one
					delete game.reports.inning[report.prev].pitchers[report.params.id];
					inn.pitchers.push(report);
				} else {
					// same inning so overwrite file
					inn.pitchers[report.params.id] = report;
				}
			} else {
				inn.pitchers.push(report);	
			}
		} else if(report.half == 2) {
			inn.bottom = report;
		} else {
			inn.top = report;
		}
		this.saveGame(game);
		if(_.isFunction(cb)) {
			cb(game);
		}
	}
	
}

export default new store();

function isGame(game) {
	if(_.isObject(game)) {
		return true;
	} else {
		return false;
	}
	//if(!isGame()) return callback('select a game first');
}

function isCallback(callback) {
	if(_.isFunction(callback)) {
		return callback;
	} else {
		return function(err, data) {};
	}
}


