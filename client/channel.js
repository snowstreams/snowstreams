import React from 'react';
import { Link } from 'react-router';
import moment from 'moment';
import _ from 'lodash';
import Select from 'react-select';
import randomNumber from 'hat';
import { authSocket } from 'sockets';

var Stream = React.createClass({
	contextTypes: {
		router: React.PropTypes.func
	},
	getInitialState: function() {
		return { connected: false };
	},
	componentDidMount: function() {
		
	},
	componentWillMount: function() {
		
	},
	componentWillReceiveProps: function(props) {
		return false;
	},
	logChange: function (val) {
		console.log("Selected: " + val);
	},
	getOptions: function(list, lists, input, callback) {
		var _this = this;
		var myFn = function(data) {
			console.log(data);
			var options = []
			_.each(data.data[lists], function(v) {
				options.push({ value: v._id, label: v.name });
			});
			callback(null, {
				options: options,
				complete: true
			}); 
		}

		authSocket.emit('list',{ 
			list: list, 
			include: 'name',
			iden: _this.trapResponse(authSocket, myFn) 
		});
	},
	getSourceOptions: function(input, callback) {
		console.log('get options for source');
		this.getOptions('Source', 'sources', input, callback);
	},
	getStreamOptions: function(input, callback) {
		console.log('get options for streams');
		this.getOptions('Stream', 'streams', input, callback);
	},
	trapResponse : function(socket, callback) {
		
		var unique = randomNumber();

		var cb = function(data) {
			socket.removeListener(unique, cb);
			callback(data);
		}
		console.log('trap response', unique, cb);
		socket.on(unique, cb);

		return unique;
	},
	render: function() {
			console.log('channel state', this.state);
			var stream = <Select
				name="stream"
				asyncOptions={this.getStreamOptions}
				onChange={this.logChange}
				autoload={true}
			/>	
			var sources = <Select
				name="sources" 
				multi={true}
				autoload={true}
				asyncOptions={this.getSourceOptions}
				onChange={this.logChange}
			/>	
			return (
			
			<div className="">
				<div className="header3 bg-info">Channels</div>
				<div className="clearfix" />
				<div className="col1 col-md-2">
					<div className="col1">
						Add Channel
						
					</div>
				</div>
				<div className="col2 col-sm-10 col-md-10" >
					<form className="form-horizontal">	
						<fieldset>			
							
							<div className="form-group">
								<label className="col-md-2 control-label" htmlFor="name">name</label>  
								<div className="col-md-6">
									<input id="name" name="name" type="text" placeholder="cable" className="form-control input-md" required="" />
									<span className="help-block">must be unique</span>  
								</div>
							</div>
							<div className="clearfix" />
							<div className="form-group">
								<label className="col-md-2 control-label" htmlFor="type">Stream</label>
								<div className="col-md-6">
									{stream}
								</div>
							</div>
							<div className="clearfix" />
							<div className="form-group">
								<label className="col-md-2 control-label" htmlFor="type">Sources</label>
								<div className="col-md-6">
									{sources}
								</div>
								
							</div>
							
							<div className="clearfix" />
							<div className="form-group">
								<label className="col-md-2 control-label" htmlFor="program">program</label>  
								<div className="col-md-6">
									<input id="program" name="program" type="text" placeholder="dvgrab" className="form-control input-md" />
									<span className="help-block">full path to program</span>  
								</div>
							</div>
							<div className="clearfix" />
							<div className="form-group">
								<div className="col-md-offset-2 col-md-5">
									<button className="btn btn-info">Add Channel</button>  
								</div>
								
							</div>				

						</fieldset>
					</form>
				</div>
				
			</div>
			
		);
			
	},
	
	
	
});

export default Stream;
