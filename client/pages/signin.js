import React from 'react';
import { Link } from 'react-router';
import _ from 'lodash';
import Alert from '../common/alert.js';
import request from 'superagent';

var SignIn = React.createClass({
	contextTypes: {
		router: React.PropTypes.func
	},
	getInitialState: function() {
		return { 
			newalert: {},
		};
	},
	componentDidMount: function() {
		//this.getStreams();
	},
	componentWillMount: function() {
		
	},
	componentWillReceiveProps: function(props) {
		//console.log('receive props',props)
		//this.getStreams();
		return false;
	},
	
	submit: function(e) {
		
		e.preventDefault();
		console.log('submit form', this.inputEmail.getDOMNode().value);		
		request
			.post('/keystone/api/session/signin')
			.send({ email: this.inputEmail.getDOMNode().value, password: this.inputPassword.getDOMNode().value })
			.set('Accept', 'application/json')
			.end((err, res) => {
				// Calling the end function will send the request
				if(err || res.body.error) {
					return this.setState({ 
						newalert: {
							style: 'danger',
							data: { error: res.body.error },
							show: true
						}
					});
				}
				this.props.sockets.connectAuth();
				this.context.router.transitionTo('home');
			});

	},
	
	render: function() {
		console.log('log state', this.state);
		let _this = this;
		if(this.props.sockets.connected.auth) {
			return (
				<div id="signindiv" className=" ">
					<div className="header-channels lg-header ">
						Members
					</div>
					
					<div className="clearfix" />
					<div className="col-sm-12 no-padding" >
					Welcome back...
					</div>	
				</div>
			);
		}
		
		return (
			<div id="signindiv" className=" ">
					<div className="header-channels lg-header ">
						Members
					</div>
					
					<div className="clearfix" />
					<div className="col-sm-12 no-padding" >
						{this.state.newalert.show ? <Alert style={this.state.newalert.style} html={this.state.newalert.html} data={this.state.newalert.data} component={this.state.newalert.component} dismiss={_this.dismissAlert} /> : ''}
						 <form className="form-signin" onSubmit={this.submit}>
							<h2 className="form-signin-heading">Please sign in</h2>
							<label for="inputEmail" className="sr-only">Email address</label>
								<input type="email" ref={(ref) => this.inputEmail = ref} className="form-control" placeholder="Email address" required autofocus />
							<label for="inputPassword" className="sr-only">Password</label>
								<input type="password" ref={(ref) => this.inputPassword = ref} className="form-control" placeholder="Password" required />
							<div className="checkbox">
								<label>
								<input ref="remember" type="checkbox" value="remember-me" /> Remember me
								</label>
							</div>
							<button className="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>
						</form>
					</div>	
			</div>
		);
			
			
	},
	
	
	
});

export default SignIn;
