import React from 'react';
import _ from 'lodash';
import Select from 'react-select';
import debugging from 'debug';
let	debug = debugging('snowstreams:client:pages:flow:flow');

var Flow = React.createClass({
	contextTypes: {
		router: React.PropTypes.func
	},
	getInitialState: function() {
		let all = {};
		if(this.props.form.source) {
			_.each(this.props.form.source, (id) => {
				var s = this.props.sourceMap[id];
				all[id] = s;
			});
		}
		return { 
			inputList: [],
			inputs: all, 
			output: this.props.streamMap[this.props.form.stream] || {}
		};
	},
	componentDidMount: function() {
	},
	componentWillMount: function() {
	},
	componentWillReceiveProps: function(props) {
		//debug('receive props',props)
		let line = [];
		let all = {};
		if(props.form.source) {
			_.each(props.form.source, (id) => {
				let s = props.sourceMap[id];
				all[id] = s;
			});
		}
		
		this.setState({inputs: all, output: props.streamMap[props.form.stream] || {} });
		return false;
	},
	render: function() {
		debug('flow state', this.state, this.props);
		let _this = this;
		let inputs = [];
		let outputs = [];
		let args = {};
		let hasArg = false;
		
		if(this.state.output.program) {
			hasArg = this.state.output.argument;
			_.each(this.state.output.program.arguments, (arg, k) => {
				if(_.isObject(this.props.programArgumentMap[arg])) {
					args[this.props.programArgumentMap[arg].anchor] = this.props.programArgumentMap[arg];
				}
			});			
		}
		if(this.state.inputs) {
			_.each(this.state.inputs, (s, id) => {
				if(!_.isObject(s)) {
					
				} else if( s.type === 'file') {
					inputs.push(<div key={id}>
						<div className="col-xs-12" ><b >{s.name}</b></div>
						<div className="col-xs-12" style={{paddingBottom:10}}>{s.type + ' : ' + s.file}</div>
						<div className="col-xs-4 bg-info">isDir</div>
						<div className="col-xs-4 bg-info"># of files</div>
						<div className="col-xs-4 bg-info">extensions</div>
						<div className="clearfix" />
						<div className="col-xs-4">{s.directory ? 'YES' : 'NO'}</div>
						
						<div className="col-xs-4">{_.isArray(s.files) ? s.files.length : 1}</div>
						<div className="col-xs-4">{s.ext}</div>
						<hr />
					</div>);
					if(!hasArg && s.ext) {
						let pattern = s.ext.split(' ');
						_.each(pattern, (ext) => {
							inputs.push(<div key={ext + id}><div className="clearfix">
								<div className="col-sm-2">{ext}</div>
								<div className="col-sm-10">{args[ext] ? args[ext].command : ''}</div>
								
							</div><hr /></div>);
						});
					} else if(!hasArg) {
						if(_.isObject(s.files[0]) && args[s.files[0].ext.replace('.','')]) {
							let aa = args[s.files[0].ext.replace('.','')];
							inputs.push(<div key={s.files[0]._id+'45'}><div className="clearfix">
								<div className="col-sm-2">{aa.anchor}</div>
								<div className="col-sm-10">{aa.command}</div>
								
							</div><hr /></div>);
						} else { 
							_.each(args, (arg, k) => {
								inputs.push(<div key={arg._id+'45'}><div className="clearfix">
									<div className="col-sm-2">{arg.anchor}</div>
									<div className="col-sm-10">{arg.command}</div>
									
								</div><hr /></div>);
							});
						}
					} else {
						inputs.push(<div key={'ext' + id}><div className="clearfix">
							<div className="col-sm-2">cmd</div>
							<div className="col-sm-10">{_this.state.output.command}</div>
							
						</div><hr /></div>);
					}
				} else if(s.type === 'program') {
						
					inputs.push(<div key={id+s.name}>
						<div className="col-xs-12" ><b >{s.name}</b></div>
						<div className="col-xs-12" style={{paddingBottom:10}}>{s.type }</div>
						<hr />
					</div>);
					inputs.push(<div key={'e23xt' + s._id}><div className="clearfix">
						<div className="col-sm-2">cmd</div>
						<div className="col-sm-10">{s.command}</div>		
					</div><hr /></div>);
						
				} else {
					inputs.push(<div key={id+s.name}>
						<div className="col-xs-12" ><b >{s.name}</b></div>
						<div className="col-xs-12" style={{paddingBottom:10}}>{s.type }</div>
						<hr />
					</div>);
				}
					
			});
			if(_.isObject(this.state.output)) {
				let o = this.state.output;
				
				if(o.type === 'program') {
					// we have a program... it may have an %OUTPUT% to replace or be middleware
					let piped = _.isObject(o.stream) ? <span> {'>'} {o.stream.type.toUpperCase()} <b>{o.stream.name}</b></span> : o.host ? <span> {'>'} <b>{o.host}</b></span>: <span />;
					let out = <h5>{'>'} Program <b>{o.program.program} </b> {piped}</h5>;
					
					outputs.push(<div key={o._id+'name'}>
						<div className="col-xs-12" >{out}</div>
						<div className="clearfix" />
						<hr />
					</div>);
					
					if(_.isObject(o.stream)) {
						outputs.push(displayStream.call(_this, o.stream)); 
					}
				}
				if(o.type === 'udp') {
					// we have a udp stream
					let piped =  <b>{o.name}</b>;
					let out = <h5>{'>'} UDP {piped}</h5>;
					
					outputs.push(<div key={o._id+'name'}>
						<div className="col-xs-12" >{out}</div>
						<div className="clearfix" />
						<hr />
					</div>);
					
				}
				
			}
		}
		if(this.state.inputs) {
			
			return (
				
				<div className=" ">
					
					<div className="header-home">Flow</div>
					<div className="clearfix" />
					
					<div className="col3 col-sm-12 col-md-12 no-padding" >
						{displayStream.call(this, this.state.output)}
					</div>
					<div className="clearfix" />
					
					<div className="col3 col-sm-12 col-md-12 no-padding" >
						{inputs}	
					</div>
					<div className="clearfix" />
					
					<div className="col3 col-sm-12 col-md-12 no-padding" >
						{outputs}
					</div>	
				</div>
				
			);
		} else {
			return <span />
		}	
			
	},
	
	
	
});

export default Flow;

function displayStream(s) {
	if(s.type === 'udp') {
		let ret = (<div className="clearfix" key={s._id+s.type}>
			<div className="col-sm-2">UDP</div>
			<div className="col-sm-10"><a href={'udp://' + s.host + ':' + s.port}>{'udp://' + s.host + ':' + s.port}</a></div>
			<div className="clearfix" />
			<hr />
			<div className="col-sm-2">Proxy</div>
			<div className="col-sm-10"><a href={'/' + this.props.sockets.proxy + '/' + this.props.form.slug}>{'/' + this.props.sockets.proxy + '/' + this.props.form.slug}</a></div>
			<div className="clearfix" />
			<hr />
		</div>);
		return ret;
	}
}
