import React from 'react';
import { Link } from 'react-router';
import moment from 'moment';
 
let Home = React.createClass({
	contextTypes: {
		router: React.PropTypes.func
	},
	getInitialState: function() {
		return { status: <span />};
	},
	componentDidMount: function() {
		
	},
	componentWillMount: function() {
		
	},
	componentWillReceiveProps: function(props) {
		return false;
	},
	render: function() {
			console.log('home state', this.state);
			
			return (
				<div className=" ">
					<div className="header-home">Home</div>
					<div className="clearfix" />
					
					<div className="col0 col-md-4">
						<div className="jumbotron">
							<h1>Streams</h1>
							<p>list or something</p>
							<p><a className="btn btn-success btn-lg" id="streams" onClick={this.go}>Manage</a></p>
						</div>
					</div>
					<div className="col0 col-md-4">
						<div className="jumbotron">
							<h1>Sources</h1>
							<p>list or something</p>
							<p><a className="btn btn-info btn-lg" id="sources" onClick={this.go}>Manage</a></p>
						</div>
					</div>
					<div className="col0 col-md-4">
						<div className="jumbotron">
							<h1>Channels</h1>
							<p>list or something</p>
							<p><a className="btn btn-default btn-lg bg-info" id="channels" onClick={this.go}>Manage</a></p>
						</div>
					</div>
				</div>
			);
			
	},
	go: function(e) {
		this.context.router.transitionTo(e.target.id);
	}
	
	
	
});

export default Home;
