import React from 'react';
import { Link } from 'react-router';
import moment from 'moment';
import _ from 'lodash';
import Select from 'react-select';

var Logs = React.createClass({
	contextTypes: {
		router: React.PropTypes.func
	},
	getInitialState: function() {
		return { 
			status: <span />
		};
	},
	componentDidMount: function() {
		//this.getStreams();
	},
	componentWillMount: function() {
		
	},
	componentWillReceiveProps: function(props) {
		//console.log('receive props',props)
		//this.getStreams();
		return false;
	},
	getLogs: function() {
		console.log('get logs',this.state)
		var _this = this;
		var myFn = function(data) {
			console.log(data);
			
			_this.setState({streams: data.data.streams});
		}

		this.props.sockets.auth.emit('list',{ 
			list: 'Program', 
			exclude: '__v',
			populate: 0,
			iden: this.props.sockets.trap(this.props.sockets.auth, myFn) 
		});
	},
	render: function() {
		console.log('log state', this.state);
		
		return (
			
			<div className=" ">
				
				<div className="header-logs">Logs</div>
				
				<div className="clearfix" />
				<div className="col1 col-md-2">
					Available Logs
					
					<div className="col1">
						
					</div>
				</div>
				<div className="col2 col-sm-10 col-md-10 no-padding" style={{minHeight:250}}>
					
				</div>
					
			</div>
			
		);
			
			
	},
	
	
	
});

export default Logs;
