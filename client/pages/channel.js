import React from 'react';
import { Link } from 'react-router';
import moment from 'moment';
import _ from 'lodash';
import Select from 'react-select';
import Alert from '../common/alert.js';
import { ListGroupItem, ListGroup } from 'react-bootstrap';
import Loader from '../common/loader';
import { ListGroupItemLink, ButtonLink } from 'react-router-bootstrap';
import Flow from './flow/flow';
import * as utils from '../common/utils.js';


import debugging from 'debug';
let	debug = debugging('snowstreams:client:pages:channel');

let buildProgramQuery = utils.buildProgramQuery;
let Button = utils.Button;

let Channel = React.createClass({
	
	contextTypes: {
		router: React.PropTypes.func
	},
	
	getInitialState() {
		let obj = { 
			allowReload: true,
			loading: true,
			asset: 'Channel',
			channels: [],
			channelMap: {},
			streams: false,
			streamMap: {},
			active: 'program',
			sources: false,
			sourceMap: {},
			programs: false,
			programMap: {},
			programArgumentMap: {},
			newalert: {},
			durationError:2000,
			durationSuccess:1500,
			form: this.defaultForm(),
			currentField: false,
			formInfo: {
				name: ' <p>You will reference this channel by name everywhere.</p> <p> The name should be unique among your channels and should be easily identifiable.</p> ',
				input: '<p>Select the sources to use for inputs.</p>  <p>  You can override variables after your selection</p> ',
				output: ' <p>Select a stream to use as our output.</p>  <p>  You can override variables after your selection</p> ',
				loop: ' <p>Should we run this channel non-stop?</p> ',
				programs: ' <p>Add any programs you want listed to manage this channel.</p> ',
				expose: ' <p>Create an API route to manage this channel.  </p><p>Available commands are stop, start, restart and status.</p><p>Status will give you program information.  Currently that is limited to the filename, codec information if available, start time and expected stop time </p>',
			},	
		}
		return obj;
	},
	
	defaultForm() {
		return {
			name: '',
			program: '',
			type: '',
			sources: [],
			stream: false,
			
		}
	},
	
	listenForUpdate(data) {
		
		if( ( !this.state.forming && data.id === this.state.form._id )) {
			// update the current doc
			debug('got a doc update from emitter', data.id, this.state.form._id, this.state);
			let al = { loading: false };
			al.newalert = {
				style: 'warning',
				component: <span>Asset was updated by another user. <a href="#" onClick={(e) => {
					e.preventDefault();
					this.getChannel(data.data.slug);
				}} >Click to reload.</a></span>,
				show: true
			};
			
			this.setState( al );
		}
	},
	
	componentDidMount() {
		let _this = this;
		this.getChannels();
		this.checkProps(this.props);
		$(document).on('focus','#channeldiv .showinfo', function(e) {
			_this.showInfo(e);
		});
		this.props.sockets.authSocket.on('doc:post:save', this.listenForUpdate);
	},
	
	componentWillUnMount: function() {
		this.props.sockets.authSocket.removeListener('doc:save', this.listenForUpdate);
	},
	
	checkProps(props) {
		let _this = this;
		if(props.params.slug === 'new') {
			this.setState({ 
				form: _this.defaultForm(),
				newalert: props.query.alert || { show: false },
				currentField: false
			});
			return;
		} else if(props.params.slug) {
			this.getChannel(props.params.slug);
			return;
		}
	},
	
	componentWillReceiveProps(nextProps) {
		let sameQuery = this.props.params.slug === nextProps.params.slug;
		if(!sameQuery && this.props.params.slug !== '') {
			this.checkProps(nextProps);
		}
		return;
	},
	
	logChange: function (val) {
		debug("Selected: " + val);
	},
	
	getChannels() {
		let _this = this;
		let channelFn = function(data) {
			let send = _.isArray(data.data.channels) ? data.data.channels : [];
			let channelMap = {};
			_.each(send, function(v) {
				channelMap[v._id] = v;
			});
			_this.setState({channels: send, channelMap: channelMap});
		}
		let sourceFn = function(data) {
			let send2 = _.isArray(data.data.sources) ? data.data.sources : [];
			let sourceMap = {};
			_.each(send2, function(v) {
				sourceMap[v._id] = v;
			});
			_this.setState({sources: send2, sourceMap: sourceMap, loading: false});
		}
		let streamFn = function(data) {
			let send3 = _.isArray(data.data.streams) ? data.data.streams : [];
			let streamMap = {};
			_.each(send3, function(v) {
				streamMap[v._id] = v;
			});
			_this.setState({streams: send3, streamMap: streamMap});
		}
		let programFn = function(data) {
			let send = _.isArray(data.data.programs) ? data.data.programs : [];
			let programMap = {};
			let programArgumentMap = {};
			_.each(send, function(v) {
				programMap[v._id] = v;
				_.each(v.arguments, function(a) {
					programArgumentMap[a._id] = a;
				});
			});
			
			_this.setState({programs: send, programMap: programMap, programArgumentMap: programArgumentMap});
		}
		this.props.sockets.list('Channel', {sort:{name:1}}, channelFn);
		this.props.sockets.list('Stream', {populate: 'program argument stderr stream', sort:{name:1}}, streamFn);
		this.props.sockets.list('Program', {sort:{name:1}, populate: 'arguments'}, programFn);
		this.props.sockets.list('Source', {populate: 'program argument source stream'}, sourceFn);
	},
	
	getChannel(slug, callback) {
		let _this = this;
		
		// _this.setState({ form: _this.defaultForm(), loading: true });
		
		if(!_.isFunction(callback)) {
			callback = ()=>{};
		}
		
		let myFn = function(data) {
			
			// We got an error
			if(data.error) {
								
				_this.setState({ 
					newalert: {
						style: 'danger',
						data: data,
						show: true
					},
					currentField: 'error'
				});
				
			// we got a result that may contain a list
			} else if(_.isObject(data.data)) {
				
				let send = _.isArray(data.data.channels) ? data.data.channels[0] : _.isObject(data.data.channels) ? data.data.channels : {};
				
				let name = _.isObject(send.updatedBy) && _.isObject(send.updatedBy.name) ?  send.updatedBy.name.first + '  ' + send.updatedBy.name.last: ' Unknown';
				
				_this.setState({ 
					form: send,
					currentField: false,
					forming: false,
					newalert: {
						style: 'info',
						component: (<div>Updated last on {moment(send.updatedAt).format('MMMM Do YYYY, h:mm:ss a')} by {name}</div>),
						show: true
					}
				}, callback);
				
			// something is wrong
			} else {
				
				_this.refs.manage.error();
				
				_this.setState({ 
					currentField: 'error',
					forming: false,
					newalert: {
						style: 'danger',
						html: 'Error. Please check console',
						show: true
					},
				}, callback);
			
			}
		} // end myFn

		let opts = {  
			exclude: '__v',
			slug: slug,
			sort: 'order',
			populate: 'arguments updatedBy',
		};
		this.props.sockets.getDocument('Channel', opts, myFn);
	},
	buttonLoading(who) {
		this.refs[who].loading();
	},
	buttonSuccess(who) {
		this.refs[who].success();
	},
	buttonEnable(who) {
		this.refs[who].enable();
	},
	buttonFail(who) {
		this.refs[who].error();
	},
	startChannel() {
		debug('start channel');
	
		let _this = this;
		
		this.refs.start.loading();
		
		_this.props.sockets.play('start', this, this.state.asset, (err, data) => {
			debug('start callback', data, err);
			if(err) {
				this.buttonFail('start');
				_this.setState({ 
					newalert: {
						style: 'danger',
						html: err,
						show: true
					}
				});
			} else {
				_this.refs.start.success();
				_this.getChannel(_this.state.form.slug, () => {
					_this.setState({ 
						newalert: {
							style: 'success',
							html: data.message,
							show: true
						}
					});
					this.buttonSuccess('stop');
				});
				
			}
		});
	},
	
	stopChannel() {
		debug('stop channel');
		
		let _this = this;
		
		this.refs.stop.loading();
		
		_this.props.sockets.play('stop', this, this.state.asset, (err, data) => {
			debug('stop callback', data, err);
			if(err) {
				this.buttonFail('stop');
				_this.setState({ 
					newalert: {
						style: 'danger',
						html: err,
						show: true
					}
				});
			} else {
				_this.refs.stop.success(); 
				_this.getChannel(_this.state.form.slug, () => {
					_this.setState({ 
						newalert: {
							style: 'warning',
							html: data.message,
							show: true
						}
					});
					this.buttonSuccess('start');
				});
			}
		});
	},
	
	dismissAlert() {
		this.setState({ 
			newalert: {
				show: false
			}
		});
	},
	
	dismissInfoAlert() {
		this.setState({ 
			currentField: false
		});
	},
	
	submitForm(e) {
		e.preventDefault();
		this.refs.manage.loading();
		this.setState({ forming: true });
		let _this = this;
		let reload = (_this.state.form._id) ? true : false;
		let go = (_this.state.form._id) ? 'update' : 'create';
		delete _this.state.form.createdBy;
		delete _this.state.form.updatedBy;
		delete _this.state.form.createdAt;
		delete _this.state.form.updatedAt;
		this.props.sockets.submitForm(this, e, go, 'Channel', function(err, data) {
			if(!err) {
				_this.getChannels();
				if(go === 'create') {
					_this.refs.manage.success();
					_this.context.router.transitionTo('manage-channel', { slug: data.data.channels.slug } );
				} else {
					_this.getChannel(data.data.channels.slug, _this.refs.manage.success);

				}
			} else {
				_this.refs.manage.error();
				_this.setState({ 
					newalert: {
						style: 'warning',
						html: err,
						show: true
					},
					loading: false,
					forming: false
				});
			}
			
		});
	},
	
	handleChange(val, who) {
		let send = {form: this.state.form}
		let form = send.form;
		let name;
		let value;
		debug(val, who, form)
		if(who){
			name = who;
			value = _.isArray(val) ? val : _.isObject(val) ? val.value : '';
			if(who === 'program') {
				send.programPos = parseFloat(val.pos);
				form.argument = ''
			}
		} else {
			name = val.target.name;
			value = val.target.value
		}
		form[name] = value;
		this.setState(send);
    },
    
    handleCheck(val) {
		let send = { form: this.state.form };
		let form = send.form;
		let name;
		let value;
		name = val.target.name;
		value = val.target.value === 'true' ? true : false;
		form[name] = value;
		this.setState(send);
    },
    
    showInfo(e) {
		let name = e.currentTarget.dataset.name;
		if(name.search(':') !== -1) {
			name = 'arguments'
		}
		this.setState({currentField: name});
    },
    
    deleteChannel(e) {
		e.preventDefault();
		this.refs.remove.loading();
		utils.deleteDocument(this, e, 'Channel', ()=>{
			this.refs.remove.error();
		});
	},
	
	reallyDelete() {
		let _this = this;
		let form = { ...this.state.form };
		this.props.sockets.reallyDelete(this, null, 'Channel', function(err, data) {
			if(!err) {
				_this.getChannels();
				if(_this.state.form._id) {
					_this.getChannel(data.data.channels.slug);
				} else {
					_this.context.router.transitionTo('channels', {}, { 
						alert: {
							style: 'danger',
							html: 'Channel ' + form.slug + ' Deleted',
							show: true
						}
					});
				}
			} else {
				_this.refs.remove.error();
			}
		});
		
	},
	
	runProgram(e, id) {
		
		e.preventDefault();
		
		let _this = this;
		
		let program = this.state.programArgumentMap[id];
		
		let query = buildProgramQuery(program, this.state.form)
		
		debug('query', query);
		
		this.props.sockets.runProgram(this, this.state.programMap[program.program].slug, query.query, (err, data) => {
			debug('runProgram callback', data, err);
			this.setState({ 
				newalert: {
					style: 'success',
					html: data.message,
					show: true
				}
			});
		});
		
	},
	
	renderOption(option) {
		return <span style={{ color: option.hex }}>{option.label}</span>;

	},
	
	renderValue(option) {
		return <span style={{ color: option.hex, fontSize:'small' }}><strong>{option.label}</strong> {option.len ? '('+option.len+')' : ''}</span>
	},
	
	renderPrograms() {
				
		let checkVars = (arg) => {
			return arg.argument.split(' ')
				.filter((v) => {
					return v[0] === '%';
				})
				.map((v) => {
					return (<div key={v+'aa'} ><input id={arg._id + '_' + v} name={arg._id + '_' + v} type="text" placeholder={v}  className="form-control input-md"  onChange={this.handleChange} value={this.state.form[arg._id + '_' + v]} /></div>);
				});
		};
		
		return this.state.form.programs.map((p) => {
			let v = this.state.programMap[p];
			
			let aMap = v.arguments.map((arg) => {
				return (<div key={arg._id}  style={{marginBottom:15}}>
					<div className="col-sm-12 no-padding">
						<strong>{v.program}</strong>
					</div>
					<div className="col-sm-12 no-padding">
						{arg.argument}
					</div>
					<div className="col-sm-4 no-padding">
						<button style={{width:'100%'}} className="btn btn-info" onClick={(e) => { return this.runProgram(e, arg._id); }}><span className="glyphicon glyphicon-play" aria-hidden="true" /> {arg.anchor}</button>
					</div>
					<div className="col-sm-8 no-padding">
						{checkVars(arg)}
					</div>
					
					<div className="clearfix" />
				</div>);
			});
			return (
				<div key={v._id} className="col-sm-12 no-gutter">
					<div className="col-sm-12 no-padding">
						{aMap}
					</div>
				
				</div>
			);
			
			
		});
		
	},
	
	render: function() {
		
		//debug('render', this.state);	
		
		let _this = this;
		let state = this.state;
		
		let listItems = this.state.channels.map(function(v) {
			return (<ListGroupItemLink bsSize="small" to='manage-channel' params={{ slug: v.slug }} key={v.slug+'-key'}>{v.name}</ListGroupItemLink>);
		});
		if(listItems.length === 0) {
			listItems.push(<ListGroupItem  bsSize="xlarge"  key={'loading-key'}><div className="clearfix"><div className="" style={{textAlign:'center'}}><Loader /></div></div></ListGroupItem>);
		}
		let list = (
			<ListGroup>
				{listItems}
			</ListGroup>
		);
		
		// show loading if waiting for initial sockets
		if(!state.streams || !state.sources || !state.programs) {
			return (<div id="channeldiv" className=" ">
				
				<div className="header-loading"> Loading Channels</div>
				
				<div className="clearfix" />
				<div className="col1 col-md-2 menuitem ">
					{list}
				</div>
				<div className="col2 col-sm-10 col-md-10" >
					<div><Alert style={!state.channels ? 'warning' : 'success'} html={state.channels ? 'Channels ready' : 'Loading channels...'}  /></div>
					<div><Alert style={!state.programs ? 'warning' : 'success'} html={state.programs ? 'Programs ready' : 'Loading programs...'}  /></div>
					<div><Alert style={!state.sources ? 'warning' : 'success'} html={state.sources ? 'Sources ready' : 'Loading sources...'}  /></div>
					<div><Alert style={!state.streams ? 'warning' : 'success'} html={state.streams ? 'Streams ready' : 'Loading streams...'}  /></div>
				</div>
			</div>)
		}
		
		// show loading if waiting for submission
		if(state.loading1) {
			return (<div id="channeldiv" className=" ">
				
				<div className="header-channels"> Loading Channel </div>
				
				<div className="clearfix" />
				<div className="col1 col-md-2 menuitem ">
					{list}
				</div>
				<div className="col2 col-sm-10 col-md-10" >
					<div><Alert style='warning' html='Grabbing the lastest channel data.  Please wait a sec...'  /></div>
				</div>
			</div>)
		}
		
		let typeOptions = [
			{ value: 'file', label: 'file' },
			{ value: 'http', label: 'http' },
			{ value: 'program', label: 'program' },
			{ value: 'udp', label: 'udp' }
		];
		 
		let type = <Select
			name="type"
			options={typeOptions}
			onChange={function(val, obj) {
				_this.handleChange(obj[0], 'type')
			}}
			value={this.state.form.type}
		/>
		
		
		
		
		// inputs (sources)
		let source = <span />;
		let colors = {
			program: '#E6771C',
			file: '#6B38EA',
			http: '#46714B',
			udp: '#7E8F8F',
		};
		let sourceOptions = this.state.sources.map(function(v, k) {
			let len = 0
			len = v.type === 'file' && _.isArray(v.files) && v.files.length > 0 ? v.files.length : v.type === 'file' ? 1 : 0;
			return ({ value: v._id, label: v.name , hex: colors[v.type], len: len});
		});
		source = <Select
			name="source"
			allowCreate={false}
			options={sourceOptions}
			multi={true}
			onChange={function(val, obj) {
				//debug(val)
				_this.handleChange(val.split(','), 'source')
			}}
			optionRenderer={this.renderOption}
			valueRenderer={this.renderValue}
			value={this.state.form.source}
		/>;
		
		// output (stream)
		let stream = <span />;
		let streamOptions = this.state.streams.map(function(v, k) {
			return ({ value: v._id, label: v.name });
		});
		stream = <Select
			name="stream"
			allowCreate={false}
			options={streamOptions}
			onChange={function(val, obj) {
				_this.handleChange(obj[0], 'stream')
			}}
			value={this.state.form.stream}
		/>;
		
		// available programs
		let programOptions = this.state.programs.map(function(v, k) {
			return ({ value: v._id, label: v.name });
		});
		let programs = <Select
			name="programs"
			allowCreate={false}
			options={programOptions}
			multi={true}
			onChange={function(val, obj) {
				//debug(val)
				_this.handleChange(val.split(','), 'programs')
			}}
			optionRenderer={this.renderOption}
			valueRenderer={this.renderValue}
			value={this.state.form.programs}
		/>;
		
		let inputVars = false;
		let outputVars = false;
		
		return (
			
			<div id="channeldiv" className=" ">
				
				<div className="header-channels lg-header ">
					<div>Channels</div>
					<ButtonLink to="manage-channel" params={{ slug: 'new' }} className="plus-button" > <span className="glyphicon glyphicon-plus" /> Add </ButtonLink>
				</div>
				
				<div className="clearfix" />
				<div className="col1 col-sm-2 menuitem ">
					{list}
				</div>
				<div className="col2 col-sm-10 col-md-10 no-padding" >
					{this.state.newalert.show ? <Alert style={this.state.newalert.style} html={this.state.newalert.html} data={this.state.newalert.data} component={this.state.newalert.component} dismiss={_this.dismissAlert} /> : ''}
					<div className="col-sm-8 col-md-8" >
						
						<form className="form-horizontal"  onSubmit={this.submitForm}>	
							<fieldset>			
		 					
								<div className="form-group showinfo" data-name="name" >
									<label className="col-md-2 control-label" htmlFor="name">name</label>  
									<div className="col-md-10">
										<input id="name" name="name" type="text" placeholder="cable" className="form-control input-md"  onChange={this.handleChange} value={this.state.form.name} />
									</div>
								</div>
								
								<div className="clearfix" />
								<div ref="show" className={this.state.form.name !== '' ? 'fatalbert' : 'casper'}>
								
									<div className="clearfix" />
									<div className="form-group showinfo" data-name="expose">
										<label className="col-md-2 control-label" htmlFor="expose">api</label>
										<div className="col-md-10">
											<div className="checkbox">
												
												<label htmlFor="expose-0">
													<input type="checkbox" name="expose" id="expose-0" onChange={this.handleCheck} value={this.state.form.expose ? 'false' : 'true'} checked={this.state.form.expose ? 'checked' : ''} />
													{this.state.form.expose ? <div><a target="_blank" href={'/' + this.props.sockets.proxy + '/channel/' + this.state.form.slug || this.state.form.name + '/start'} >/{this.props.sockets.proxy}/channel/{this.state.form.slug || this.state.form.name}/start</a> <br /> <a target="_blank" href={'/' + this.props.sockets.proxy + '/' + this.state.form.slug || this.state.form.name} >/{this.props.sockets.proxy}/{this.state.form.slug || this.state.form.name}</a></div>: 'This channel is not exposed via API.'}
												</label>
											</div>
										</div>
									</div>
									
									<div className="clearfix" />
									<div className="form-group showinfo" data-name="input">
										<label className="col-md-2 control-label" htmlFor="source">input(s)</label>
										<div className="col-md-10">
											{source}
										</div>
									</div>
									
									<div className="clearfix" />
									<div ref="show" className={inputVars ? 'fatalbert' : 'casper'}>
										
										<div className="clearfix" />
										<div className="form-group showinfo" data-name="inputvars">
											<label className="col-md-2 control-label" htmlFor="inputlets">input vars</label>
											<div className="col-md-10">
												{inputVars}
											</div>
										</div>
										
									</div>
									
									<div className="clearfix" />
									<div className="form-group showinfo" data-name="loop">
											<label className="col-md-2 control-label" htmlFor="loop">loop</label>
											<div className="col-md-10">
												<div className="checkbox">
													
													<label htmlFor="loop-0">
														<input type="checkbox" name="loop" id="loop-0" onChange={this.handleCheck} value={this.state.form.loop ? false : true} checked={this.state.form.loop === true ? 'checked' : ''} />
														{this.state.form.loop === true ? 'Input is set to loop continuously' : 'Input(s) are set to play once'}
													</label>
												</div>
											</div>
									</div>
									
									<div className="clearfix" />
									<div className="form-group showinfo" data-name="output">
										<label className="col-md-2 control-label" htmlFor="stream">output</label>
										<div className="col-md-10">
											{stream}
										</div>
									</div>
									
									<div className="clearfix" />
									<div className="form-group showinfo" data-name="programs">
										<label className="col-md-2 control-label" htmlFor="programs">managers</label>
										<div className="col-md-10">
											{programs}
										</div>
									</div>
									
									<div className="clearfix" />
									<div ref="show" className={outputVars ? 'fatalbert' : 'casper'}>
										
										<div className="clearfix" />
										<div className="form-group showinfo" data-name="outputvars">
											<label className="col-md-2 control-label" htmlFor="outputvars">output vars</label>
											<div className="col-md-10">
												{outputVars}
											</div>
										</div>
										
									</div>
									
									
									<div className="form-group" style={{margin:'15px 0 30px'}}>
										<div className=" col-sm-2">
										</div>
										<div className=" col-sm-8">
											<Button className={"btninfo left square"} disabled={this.state.forming} ref="manage"  durationSuccess={this.state.durationSuccess} durationError={this.state.durationError} >{this.state.form._id ? 'Update Channel' : 'Add Channel'}</Button> 
											<input type="hidden" name="_id" value={this.state.form._id} /> 
											<input type="hidden" name="slug" value={this.state.form.slug} /> 
										</div>
										
										<div className=" col-sm-2">
											{!this.state.forming && this.state.form._id ? <Button className="btndanger right square small" onClick={this.deleteChannel} ref="remove"  durationSuccess={this.state.durationSuccess} durationError={this.state.durationError} ><span className="glyphicon glyphicon-trash" /></Button> : ''}
										</div>
									
									</div>
									
									<div className="clearfix" />
									<div className="form-group showinfo" data-name="outputlets">
										<div className="col-md-12">
											{this.state.form.source && this.state.form.stream ? <Flow {...this.state} {...this.props} /> : <span />}
										</div>
									</div>
									
									<div className="clearfix" />
										
								</div>
							</fieldset>
						</form>
					</div>
					<div className=" col-sm-4 col-md-4" >
						<div style={{ textAlign:'right', marginBottom:15 }} >
							{!this.state.forming && this.state.form._id && !this.state.form.streaming ? <Button className="btnsuccess" onClick={this.startChannel} ref="start"  durationSuccess={this.state.durationSuccess} durationError={this.state.durationError} ><span className="glyphicon glyphicon-play" aria-hidden="true" /> &nbsp;Start</Button> : ''}
						
						
							{!this.state.forming && this.state.form._id && this.state.form.streaming ? <Button className="btndanger" onClick={this.stopChannel} ref="stop" durationSuccess={this.state.durationSuccess} durationError={this.state.durationError} ><span className="glyphicon glyphicon-stop" aria-hidden="true" /> &nbsp;Stop</Button> : ''}
						</div>
						<div className="clearfix no-gutter" >
							{!this.state.forming && this.state.form._id && this.state.form.streaming ? this.renderPrograms() : ''}
						</div>
						<div className="clearfix" ><br /></div>
						 {this.state.currentField ? <Alert style="info" html={this.state.formInfo[this.state.currentField]} dismiss={_this.dismissInfoAlert} /> : ''}
					</div>
				</div>
					
			</div>
			
		);
			
	}
	
});

export default Channel;
