import React from 'react';
import { Link } from 'react-router';
import moment from 'moment';
import _ from 'lodash';
import Select from 'react-select';
import Alert from '../common/alert.js';
import * as utils from '../common/utils.js';
import { Collapse, ListGroupItem, ListGroup } from 'react-bootstrap';
import Loader from '../common/loader';
import { ListGroupItemLink,  ButtonLink } from 'react-router-bootstrap';
import Flow from './flow/flow';

import debugging from 'debug';
let	debug = debugging('snowstreams:client:pages:source');

let Button = utils.Button;

let Source = React.createClass({
	contextTypes: {
		router: React.PropTypes.func
	},
	getInitialState() {
		let obj = { 
			sources: false,
			streams: false,
			asset: 'Source',
			active: 'program',
			programs: false,
			newalert: {},
			form: this.defaultForm(),
			currentField: false,
			forming: false,
			showFiles: false,
			durationError:2000,
			durationSuccess:1500,
			menu: {},
			formInfo: {
				name: ' <p>You will reference this source by name in the UI.</p> <p> The name should be unique among your sources and should be easily identifiable.</p> ',
				type: '<p><b>file</b>: Use file to select a single file source or multiple files from a directory tree.</p><p><b>http</b>: Selecting http will allow you to enter a url.</p><p><b>program</b>: Use program to run a shell program.  You should add your programs first from the Programs section.</p> <p><b>udp</b>: Use a upd stream as a source.',
				file: 'Enter a valid file path, or a directory to traverse.',
				directory: 'You must check this to traverse a directory and select extensions to filter.',
				ext: '<p>Enter each extension without the dot(.)</p> <p>Separate extensions with a space. Only files matching the selected extensions will be applied.</p>',
				host: 'The host for your udp stream.  Local streams can use <b>@</b>',
				port: 'Port number for the udp stream',
				url: 'Valid stream url.  You can pipe the source stream through another source (program) or send it straight to a udp stream (channel).',
				respawn: 'Check this box to restart your program when it fails.',
				restarts: 'How many times should we restart the program before giving up?',
				sleep: 'How long to pause between restarts.  1000 = 1 second.',
				kill: 'Wait time before kill event on a stopped process.',
				argument: '<div><p>Select the appropriate argument line</p><p>Be sure your argument outputs to stdout if you will stream or save this source.</div>', 
				program: 'Select one of your programs.  The argument list will be supplied after you chose.',
				input: '<p>If your program requires an input select it here. </p><p> You will need to define any input sources first. </p><p>   Make sure your argument line contains %INPUT% for files or accepts stdin otherwise.</p>',
				output: '<p>A source program can output to stdout (pipe) to send to a stream selected later. </p><p> You can also select an output now.  When run the stream will end when the program ends. </p><p> You can create an open stream that survives input changes with Channels. </p><p> Make sure your argument line outputs to stdout so we can pipe the results into this output.</p>',
				expose: ' <p>Create API routes to manage this stream.  </p><p>Available arguments are: <b>play</b> (<i>alias:</i> <b>start</b>) <br /><b>restart</b><br /> <b>status</b><br /> <b>stop</b> <br /> <b>view</b><br />No argument will <b>play</b>.</p><p>2 routes are created:<br/> <b>/proxy/stream/:name/:argument</b><br/> sends Content-Type=mpegts for play.</p><p><b>/proxy/stream/:name/:<i>mime-type</i></b> sends the Content-Type you set as <i>mime-type</i>.</p><p>Status will give you program information.  Currently that is limited to the filename if available, codec information if available, start time and expected stop time. </p>',
				scan: '<p>Do you want to (re)scan the file or directory?</p><p>   If ffprobe is available, metadata information wil be obtained. </p><p> The program will process 30 files at a time.  Large file sets will take a moment to process.</p>'
			},
		}
		
		return obj;
	},
	
	defaultForm() {
		return {
			name: '',
			type: '',
			argument: '',			
		}
	},
	
	componentWillUpdate() {
	},
	
	componentDidMount() {
		let _this = this;
		this.getSources();
		this.checkProps(this.props);
		$(document).on('focus','#sourcediv .showinfo', function(e) {
			_this.showInfo(e);
		});
	},
	checkProps(props) {
		let _this = this;
		if(props.params.slug === 'new') {
			this.setState({ 
				form: _this.defaultForm(),
				newalert: props.query.alert || { show: false },
				currentField: false
			});
			return;
		} else if(props.params.slug) {
			this.getSource(props.params.slug);
			return;
		}
	},
	componentWillReceiveProps(nextProps) {
		let sameQuery = this.props.params.slug === nextProps.params.slug;
		if(!sameQuery && this.props.params.slug !== '') {
			this.checkProps(nextProps);
		}
		return;
	},
	componentDidUpdate: function() {
		
	},
	logChange: function (val) {
		debug("Selected: " + val);
	},
	getSources() {
		let _this = this;
		let myFn = function(data) {
			let send = _.isArray(data.data.sources) ? data.data.sources : [];
			//debug('received sources data', send);
			_this.setState({sources: send});
		}
		let myFn2 = function(data) {
			let send2 = _.isArray(data.data.programs) ? data.data.programs : [];
			//debug('received programs data', send2);
			_this.setState({programs: send2});
		}
		let myFn3 = function(data) {
			let send3 = _.isArray(data.data.streams) ? data.data.streams : [];
			//debug('received streams data', send3);
			_this.setState({streams: send3});
		}
		this.props.sockets.list('Program', {sort:{name:1}, populate:'arguments'}, myFn2);
		this.props.sockets.list('Source', {sort:{type:1,name:1}}, myFn); 
		this.props.sockets.list('Stream', {sort:{name:1}}, myFn3);
	},
	getSource(slug, callback) {
		let _this = this;
		
		// this.setState({ form: this.defaultForm() });
		
		if(!_.isFunction(callback)) {
			callback = ()=>{};
		}
		
		debug('get source', this.state);
		
		let myFn = (data) => {
			// We got an error
			if(data.error) {
				
				this.setState({ 
					newalert: {
						style: 'danger',
						data: data,
						show: true
					},
					currentField: 'error'
				}, callback);
				
			// we got a result that may contain a list
			} else if(_.isObject(data.data)) {
				
				let send = _.isArray(data.data.sources) && data.data.sources.length > 0 ? data.data.sources[0] : _.isObject(data.data.sources) ? data.data.sources : {};
				debug('source result', send, data)
				
				let name = _.isObject(send.updatedBy) && _.isObject(send.updatedBy.name) ?  send.updatedBy.name.first + '  ' + send.updatedBy.name.last: ' Unknown';
				
				let pos = false;
				
				_.each(this.state.programs, function(o,k) {
					if(o._id === send.program) pos = k;
				});
				
				let menu = {}
				menu[send.type] = true;
				
				this.setState({ 
					form: send,
					menu,
					forming: false,
					programPos: pos || 0,
					currentField: false,
					newalert: {
						style: 'info',
						component: (<div>Updated last on {moment(send.updatedAt).format('MMMM Do YYYY, h:mm:ss a')} by {name}</div>),
						show: true
					}
				}, callback);
			// something is wrong
			} else {
				
				_this.refs.manage.error();
				
				this.setState({ 
					currentField: 'error',
					newalert: {
						style: 'danger',
						html: 'Error. Please check console',
						show: true
					},
				}, callback);
			}
		} // end myFn

		let opts = {  
			exclude: '__v',
			slug: slug,
			sort: 'order',
			populate: 'arguments updatedBy',
		};
		this.props.sockets.getDocument('Source', opts, myFn);
	},
	dismissAlert() {
		this.setState({ 
			newalert: {
				show: false
			}
		});
	},
	dismissInfoAlert() {
		this.setState({ 
			currentField: false
		});
	},			
	submitForm(e) {
		e.preventDefault();
		
		
		this.refs.manage.loading();
		
		let _this = this;
		let form = { ...this.state.form };
		let isFile = form.type === 'file';
		let isDir = form.directory;
		let doScan = this.state.form.scan;
		 
		delete form.createdBy;
		delete form.updatedBy;
		delete form.createdAt;
		delete form.updatedAt;
		delete form.files
		 
		this.setState({forming: true, form: form }); 
		
		 
		if(this.state.form.scan) {
			 this.props.openConsole();
		}
		
		let reload = (_this.state.form._id) ? true : false;
		let go = (_this.state.form._id) ? 'update' : 'create';
		debug('update source');
		this.props.sockets.submitForm(this, e, go, 'Source', function(err, data) {
			debug('source updated... refresh');
			if(!err) {
				_this.getSources();	
				if(go === 'create') {
					_this.refs.manage.success();
					_this.context.router.transitionTo('manage-source', { slug: data.data.sources.slug } );
				} else {
					_this.getSource(data.data.sources.slug, () => {
						_this.refs.manage.success();
						
					});

				}
						
			} else {
				_this.refs.manage.error();
				_this.setState({ 
					newalert: {
						style: 'warning',
						html: err,
						show: true
					},
					loading: false,
					forming: false
				});
			}
			
			if(doScan && isFile && isDir) {
				_this.setState({ 
					newalert: {
						style: 'warning',
						component: <div>It may take a few minutes for all of your files to process.  You can follow progress in the console pane. <p> <b>Your source is loading and will be avialable soon.</b></p></div>,
						show: true
					}
				});
			} 
			
		});
		
	},
	
	handleChange(val, who) {
		let send = {form: this.state.form}
		let form = send.form;
		let value;
		let name;
		if(who){
			name = who;
			value = _.isArray(val) ? val : _.isObject(val) ? val.value : '';
			if(who === 'program') {
				send.programPos = parseFloat(val.pos);
				form.argument = ''
			}
		} else {
			name = val.target.name;
			value = val.target.value
		}
		form[name] = value;
		//debug('send', send, val, who);	
		this.setState(send);
    },
    showInfo(e) {
		//debug(e);
		let name = e.currentTarget.dataset.name;
		if(name.search(':') !== -1) {
			name = 'arguments'
		}
		this.setState({currentField: name});
    },
    deleteSource(e) {
		e.preventDefault();
		this.refs.remove.loading();
		utils.deleteDocument(this, e, 'Source', ()=>{
			this.refs.remove.error();
		});
	},
	reallyDelete() {
		let _this = this;
		let form = { ...this.state.form };
		this.props.sockets.reallyDelete(this, null, 'Source', function(err, data) {
			if(!err) {
				_this.getSources();
				if(_this.state.form._id) {
					_this.getSource(data.data.sources.slug);
				} else { 
					_this.context.router.transitionTo('sources', {}, { 
						alert: {
							style: 'danger',
							html: 'Source ' + form.slug + ' Deleted',
							show: true
						}
					});
				}
			} else {
				_this.refs.remove.error();
			}
		});
	},
	showFiles(e) {
		e.preventDefault();
		this.setState({showFiles: !this.state.showFiles});
	},
	
	
	render: function() {
		//debug(this.context.router.getCurrentPathname(), this.state);	
		
		let _this = this;
		let state = this.state;
		
		// show loading if waiting for initial sockets
		if(!state.streams || !state.sources || !state.programs) {
			return (<div id="channeldiv" className=" ">
				
				<div className="header-loading">Loading Sources</div>
				
				<div className="clearfix" />
				<div className="col1 col-md-2 menuitem ">
					<ListGroup>
						<ListGroupItem  bsSize="xlarge"  key={'loading-key'}><div className="clearfix"><div className="" style={{textAlign:'center'}}><Loader /></div></div></ListGroupItem>	
					</ListGroup>
				</div>
				<div className="col2 col-sm-10 col-md-10" >
					<div><Alert style={!state.programs ? 'warning' : 'success'} html={state.programs ? 'Programs ready' : 'Loading programs...'}  /></div>
					<div><Alert style={!state.sources ? 'warning' : 'success'} html={state.sources ? 'Sources ready' : 'Loading sources...'}  /></div>
					<div><Alert style={!state.streams ? 'warning' : 'success'} html={state.streams ? 'Streams ready' : 'Loading streams...'}  /></div>
				</div>
			</div>)
		}
		
		
		let typeOptions = [
			{ value: 'file', label: 'file' },
			{ value: 'http', label: 'http' },
			{ value: 'program', label: 'program' },
			{ value: 'udp', label: 'udp' }
		];
		 
		let type = <Select
			name="type"
			options={typeOptions}
			onChange={function(val, obj) {
				_this.handleChange(obj[0], 'type')
			}}
			value={this.state.form.type}
		/>
					
		let programOptions = this.state.programs.map(function(v, k) {
			return ({ value: v._id, label: v.name, pos: k });
		});
		let programs = <Select
			name="program"
			allowCreate={false}
			options={programOptions}
			onChange={function(val, obj) {
				_this.handleChange(obj[0], 'program')
			}}
			value={this.state.form.program}
		/>	
		let args = <span />
		let source = <span />
		let stream = <span />
		if(this.state.form.argument !== '' || this.state.programPos > -1) {
			
			let programArguments = this.state.programs[this.state.programPos].arguments.map(function(v, k) {
				return ({ value: v._id, label: v.anchor + ' : ' + v.argument });
			});
			args = <Select
				name="argument"
				addLabelText="Add a new argument to the set"
				allowCreate={false}
				options={programArguments}
				onChange={function(val, obj) {
					_this.handleChange(obj[0], 'argument')
				}}
				value={this.state.form.argument}
			/>
			let sourceOptions = this.state.sources.map(function(v, k) {
				return ({ value: v._id, label: v.name });
			});
			source = <Select
				name="source"
				allowCreate={false}
				options={sourceOptions}
				multi={true}
				onChange={function(val, obj) {
					_this.handleChange(val.split(','), 'source')
				}}
				value={this.state.form.source}
			/>
			let streamOptions = this.state.streams.map(function(v, k) {
				return ({ value: v._id, label: v.name });
			});
			stream = <Select
				name="stream"
				allowCreate={false}
				options={streamOptions}
				onChange={function(val, obj) {
					_this.handleChange(obj[0], 'stream')
				}}
				value={this.state.form.stream}
			/>	
		}
		
		// file info
		let fileInfo = <span />;
		let viewFiles = <span />;
		if(this.state.form.type === 'file') {
			fileInfo = (<div >
				<div className="col-xs-12" ><b >{_.isArray(this.state.form.files) ? this.state.form.files.length : 1} files</b></div>
				<hr />
			</div>);
		
		}
		if(_.isArray(this.state.form.files1)) {	
			let filelist = this.state.form.files.map(function(v) {
				let time = moment.duration(v.runtime, 'seconds');
				if(!_.isObject(v.codec)) v.codec = {}
				return (<div key={v._id}>
					<div className="col-sm-12">{v.file}</div>
					<div className="col-sm-3">{time.hours() + ':' + time.minutes() + '.' + time.seconds() }</div>
					<div className="col-sm-3">{Math.floor(v.bitrate/1024)} kb/s</div>
					<div className="col-sm-3">{v.codec.video}</div>
					<div className="col-sm-3">{v.codec.audio}</div>
					<div className="clearfix" ></div>
					<hr />
				</div>);
			});
			viewFiles = (<div >
				<div className="col-xs-12" style={{paddingBottom:10}} ><b><a href="#" onClick={_this.showFiles}>view files</a></b></div>
				<div className="col-xs-12" className={_this.state.showFiles ? 'fatalbert' : 'casper'} style={{paddingBottom:10}}>{filelist}</div>
				<hr />
			</div>);
			
		}
		let t;
		let push = {};
		
		_.each(this.state.sources, (source) => {
			let v = source;
			if(t !== v.type) {
				t = v.type;
				push[v.type] = [];
			} 
			push[t].push(<ListGroupItemLink bsSize="small" to='manage-source' params={{ slug: v.slug }} key={v.slug+'-key'}>{v.name}</ListGroupItemLink>);
		});
		let listItems = [];
		_.each(push, (sources, type) => {
			let item = (<ListGroupItem  style={{margin:0, padding: 0, border: 0}} >
				<Collapse in={this.state.menu[type]} className="">
					<ListGroup  >
						{sources}
					</ListGroup>
				</Collapse>
			</ListGroupItem>);
			
			listItems.push(<ListGroupItem    onClick={(e) => {
				let menu = {};
				menu[type] = !this.state.menu[type];
				this.setState({ menu });
			}} >{type.toUpperCase()}</ListGroupItem>);
			
			listItems.push(item);
			
		});
		if(listItems.length === 0) {
			listItems.push(<ListGroupItem  bsSize="xlarge"  key={'loading-key'}><div className="clearfix"><div className="" style={{float:'right'}}><Loader /></div></div></ListGroupItem>);
		}
		let list = (
			<ListGroup>
				{listItems}
			</ListGroup>
		);
		debug('render source', this.state);
		return (
			
			<div id="sourcediv" className=" ">
				
				<div className="header-sources lg-header ">
					<div>Sources</div>
						<ButtonLink to="manage-source" params={{ slug: 'new' }} className="plus-button" ><span className="glyphicon glyphicon-plus" /> Add </ButtonLink>
				</div>
				
				<div className="clearfix" />
				<div className="menuitem col1 col-md-2">
					{list}
				</div>
				<div className="col2 col-sm-10 col-md-10 no-padding" >
					<a name="top" />
					{this.state.newalert.show ? <Alert style={this.state.newalert.style} html={this.state.newalert.html} data={this.state.newalert.data} component={this.state.newalert.component} dismiss={_this.dismissAlert} /> : ''}
					<div className="col-sm-8 col-md-8" >
						
						<form className="form-horizontal"  onSubmit={this.submitForm}>	
							<fieldset>			
		 						
								<div className="form-group showinfo" data-name="name" >
									<label className="col-md-2 control-label" htmlFor="name">name</label>  
									<div className="col-md-10">
										<input id="name" name="name" type="text" placeholder="cable" className="form-control input-md"  onChange={this.handleChange} value={this.state.form.name} />
									</div>
								</div>
								
								<div className="clearfix" />
								<div ref="show" className={this.state.form.name !== '' ? 'fatalbert' : 'casper'}>
									
									<div className="clearfix" />
									<div className="form-group showinfo" data-name="type" >
										<label className="col-md-2 control-label" htmlFor="type">type</label>
										<div className="col-md-10">
											{type}
										</div>
									</div>
									
									<div ref="show" className={this.state.form.type !== 'file' ? 'fatalbert' : 'casper'}>
									
										<div className="clearfix" />
										<div className="form-group showinfo" data-name="expose">
											<label className="col-md-2 control-label" htmlFor="expose">api</label>
											<div className="col-md-10">
												<div className="checkbox">
													
													<label htmlFor="expose-0">
														<input type="checkbox" name="expose" id="expose-0" onChange={this.handleChange} value={this.state.form.expose === 'true' || this.state.form.expose === true ? 'false' : 'true'} checked={this.state.form.expose === 'true' || this.state.form.expose === true ? 'checked' : ''} />
														{this.state.form.expose === 'true' || this.state.form.expose === true ? <div><a href={"/proxy/source/"+_this.state.form.slug+"/status"}>/proxy/source/{_this.state.form.slug}/status</a><br /><a href={"/proxy/source/"+_this.state.form.slug+"/raw"}>/proxy/source/{_this.state.form.slug}/raw</a></div> : <span>This source is not managed via API.</span>}
													</label>
												</div>
											</div>
										</div>
									
									</div>
									
									<div className="clearfix" />
									<div ref="udp" className={this.state.form.type === 'udp' ? 'fatalbert' : 'casper'}>
										
										<div className="form-group showinfo" data-name="host" >
											<label className="col-md-2 control-label" htmlFor="host">host</label>  
											<div className="col-md-10">
												<input id="host" name="host" type="text" placeholder="@" className="form-control input-md"  onChange={this.handleChange} value={this.state.form.host}  />
												<span className="help-block">valid interface address</span>  
											</div>
										</div>
										
										<div className="clearfix" />
										<div className="form-group  showinfo" data-name="port">
											<label className="col-md-2 control-label" htmlFor="port">port</label>
											<div className="col-md-5">
												<div className="input-group">
													<input id="port" name="port" className="form-control" placeholder="11000" type="text"  onChange={this.handleChange} value={this.state.form.port}  />
													<span className="input-group-addon">number</span>
												</div>
											</div>
										</div>
									
									</div>	
									
									<div ref="http" className={this.state.form.type === 'http' ? 'fatalbert' : 'casper'}>
										
										<div className="form-group showinfo" data-name="url">
											<label className="col-md-2 control-label" htmlFor="url">url</label>  
											<div className="col-md-10">
												<input id="url" name="url" type="text" placeholder="http://host/file.ext" className="form-control input-md"  onChange={this.handleChange} value={this.state.form.url}  />
												<span className="help-block">valid http stream address</span>  
											</div>
										</div>
										<div className="clearfix" />
									
									</div>	
									
									<div ref="file" className={this.state.form.type === 'file' ? 'fatalbert' : 'casper'}>
										
										<div className="form-group showinfo" data-name="file">
											<label className="col-md-2 control-label" htmlFor="file">file</label>  
											<div className="col-md-10">
												<input id="file" name="file" type="text" placeholder="/path/to/file.ext" className="form-control input-md"  onChange={this.handleChange} value={this.state.form.file}  />
												<span className="help-block"><b>{fileInfo}</b></span>  
											</div>
										</div>
										
										<div className="clearfix" />
										<div className="form-group showinfo" data-name="scan">
											<label className="col-md-2 control-label" htmlFor="scan">scan</label>
											<div className="col-md-10">
												<div className="checkbox">
													
													<label htmlFor="scan-0">
														<input type="checkbox" name="scan" id="scan-0" onChange={this.handleChange} value={this.state.form.scan === 'true' || this.state.form.scan === true ? 'false' : 'true'} checked={this.state.form.scan === 'true'  || this.state.form.scan === true ? 'checked' : ''} />
														{this.state.form.scan === 'true' || this.state.form.scan === true ? 'The file or directory will be scanned and run through ffprobe if available.' : 'This file or directory will not be scanned when saving.'}
													</label>
												</div>
											</div>
										</div>
										
										<div className="clearfix" />
										<div className="form-group showinfo" data-name="directory">
											<label className="col-md-2 control-label" htmlFor="directory">directory</label>
											<div className="col-md-10">
												<div className="checkbox">
													
													<label htmlFor="directory-0">
														<input type="checkbox" name="directory" id="directory-0" onChange={this.handleChange} value={this.state.form.directory === 'true' || this.state.form.directory === true ? 'false' : 'true'} checked={this.state.form.directory === 'true'  || this.state.form.directory === true ? 'checked' : ''} />
														{this.state.form.directory === 'true' || this.state.form.directory === true ? 'This is a directory and the following file types will be used in the filter.' : 'No, when unchecked this is a file.'}
													</label>
												</div>
											</div>
										</div>
										
										<div className="clearfix" />
										<div ref="ext" className={this.state.form.directory === 'true'  || this.state.form.directory === true ? 'fatalbert' : 'casper'}>
											<div className="form-group showinfo" data-name="ext">
												<label className="col-md-2 control-label" htmlFor="ext">extensions</label>  
												<div className="col-md-10">
													<input id="ext" name="ext" type="text" placeholder="mp4 mkv avi" className="form-control input-md"  onChange={this.handleChange} value={this.state.form.ext} />
													 
												</div>
											</div>
										</div>
										
										<div className="clearfix" />
										<div className="form-group showinfo" data-name="fileinfo" >
											<div className="col-md-12">
												{viewFiles}
											</div>
										</div>
										
									</div>	
									
									<div ref="program" className={this.state.form.type === 'program' ? 'fatalbert' : 'casper'}>
										
										<div className="form-group showinfo" data-name="program">
											<label className="col-md-2 control-label" htmlFor="program">program</label>  
											<div className="col-md-10">
												{programs} 
											</div>
										</div>
										
										<div className="clearfix" />
										<div ref="programthings" className={this.state.form.argument !== '' || this.state.programPos > -1 ? 'fatalbert' : 'casper'}>
											
											<div className="form-group showinfo" data-name="argument">
												<label className="col-md-2 control-label" htmlFor="args">arguments</label>  
												<div className="col-md-10" ref="arguments">
													{args} 
												</div>
											</div>
											
											<div className="clearfix" />
											<div className="form-group showinfo" data-name="input">
												<label className="col-md-2 control-label" htmlFor="source">input(s)</label>
												<div className="col-md-10">
													{source}
												</div>
											</div>
											
											<div className="clearfix" />
											<div className="form-group showinfo" data-name="output">
												<label className="col-md-2 control-label" htmlFor="stream">output</label>
												<div className="col-md-10">
													{stream}
												</div>
											</div>
										</div>
										
										<div className="clearfix" />
										<div className="form-group showinfo" data-name="respawn">
											<label className="col-md-2 control-label" htmlFor="respawn">respawn</label>
											<div className="col-md-10">
												<div className="checkbox">
													
													<label htmlFor="respawn-0">
														<input type="checkbox" name="respawn" id="respawn-0" onChange={this.handleChange} value={this.state.form.respawn === 'true' || this.state.form.respawn === true ? 'false' : 'true'} checked={this.state.form.respawn === 'true' || this.state.form.respawn === true ? 'checked' : ''} />
														{this.state.form.respawn === 'true' || this.state.form.respawn === true ? 'This program will be restarted under the following requirements...' : 'No, when unchecked this program will not respawn.'}
													</label>
												</div>
											</div>
										</div>
										
										<div className="clearfix" ref="repawning" className={this.state.form.respawn === 'true' || this.state.form.respawn === true ? 'fatalbert' : 'casper'}>
											
											<div className="form-group showinfo" data-name="restarts">
												<label className="col-md-2 control-label" htmlFor="restarts">restarts</label>
												<div className="col-md-5">
													<div className="input-group">
														<input id="restarts" name="restarts" className="form-control" placeholder="10" type="text" onChange={this.handleChange} value={this.state.form.restarts} />
														<span className="input-group-addon">number</span>
													</div>

												</div>
											</div>
											<div className="clearfix" />
											<div className="form-group showinfo" data-name="sleep">
												<label className="col-md-2 control-label" htmlFor="sleep">sleep</label>
												<div className="col-md-5">
													<div className="input-group">
														<input id="sleep" name="sleep" className="form-control" placeholder="1000" type="text" onChange={this.handleChange} value={this.state.form.sleep} />
														<span className="input-group-addon">number</span>
													</div>

												</div>
											</div>
											<div className="clearfix" />
											<div className="form-group showinfo" data-name="kill">
												<label className="col-md-2 control-label" htmlFor="kill">kill</label>
												<div className="col-md-5">
													<div className="input-group">
														<input id="kill" name="kill" className="form-control" placeholder="30000" type="text" onChange={this.handleChange} value={this.state.form.kill} />
														<span className="input-group-addon">number</span>
													</div>
												</div>
											</div>
										</div>	
									</div>			
									<div className="clearfix" />
									<div className="form-group no-gutter" style={{margin:'15px 0 30px'}}>
																	
										<div className=" col-sm-2">
										</div>
										<div className=" col-sm-8">
											<Button className={"btninfo left square"} disabled={this.state.forming} ref="manage" durationSuccess={this.state.durationSuccess} durationError={this.state.durationError} >{this.state.form._id ? 'Update Source' : 'Add Source'}</Button> 
											<input type="hidden" name="_id" value={this.state.form._id} /> 
											<input type="hidden" name="slug" value={this.state.form.slug} /> 
										</div>
										
										<div className=" col-sm-2">
											{!this.state.forming && this.state.form._id ? <Button className="btndanger right square small" onClick={this.deleteSource} ref="remove" durationSuccess={this.state.durationSuccess} durationError={this.state.durationError} ><span className="glyphicon glyphicon-trash" /></Button> : ''}
										</div>
										
									</div>	
								
								</div>	
								
							</fieldset>
						</form>
						
						
						
					</div>
					<div className=" col-sm-4 col-md-4" >
						<div style={{ textAlign:'right', marginBottom:15 }} >
							{!this.state.forming && this.state.form._id && !this.state.form.streaming ? <Button className="btnsuccess"   onClick={this.startSource} ref="start" durationSuccess={this.state.durationSuccess} durationError={this.state.durationError} ><span className="glyphicon glyphicon-play" aria-hidden="true" /> &nbsp;Start</Button> : ''}
						
						
							{!this.state.forming && this.state.form._id && this.state.form.streaming ? <Button className="btndanger" onClick={this.stopSource} ref="stop" durationSuccess={this.state.durationSuccess} durationError={this.state.durationError} ><span className="glyphicon glyphicon-stop" aria-hidden="true" /> &nbsp;Stop</Button> : ''}
						</div>
						<div className="clearfix" ><br /><br /></div>
						 {this.state.currentField ? <Alert style="info" html={this.state.formInfo[this.state.currentField]} dismiss={_this.dismissInfoAlert} /> : ''}
					</div>
				
				
				
				</div>
					
			</div>
			
		);
			
	},
	startSource() {
		debug('start Source');
	
		let _this = this;
		
		this.refs.start.loading();
		
		_this.props.sockets.play('start', this, this.state.asset, function(err, data) {
			debug('start callback', data, err);
			if(err) {
				
				_this.setState({ 
					newalert: {
						style: 'danger',
						html: err,
						show: true
					}
				});
				_this.refs.start.error();
	
			} else {
				_this.refs.stop.success();
				_this.getSource(_this.state.form.slug, () => {
					_this.setState({ 
						newalert: {
							style: 'success',
							html: data.message,
							show: true
						}
					});
				});
			}
		});
	},
	
	stopSource() {
		debug('stop Source');
		
		let _this = this;
		
		this.refs.stop.loading();
		
		_this.props.sockets.play('stop', this, this.state.asset, function(err, data) {
			debug('stop callback', data, err);
			if(err) {
				
				_this.setState({ 
					newalert: {
						style: 'danger',
						html: err,
						show: true
					}
				});
				_this.refs.stop.error();
	
			} else {
				_this.refs.stop.success();
				_this.getSource(_this.state.form.slug, () => {
					_this.setState({ 
						newalert: {
							style: 'warning',
							html: data.message,
							show: true
						}
					});
				});
			}
			
		});
	},
});

export default Source;
