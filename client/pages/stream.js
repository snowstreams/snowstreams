import React from 'react';
import { Link } from 'react-router';
import moment from 'moment';
import _ from 'lodash';
import Select from 'react-select';
import Alert from '../common/alert.js';
import { Collapse, ListGroupItem, ListGroup } from 'react-bootstrap';
import Loader from '../common/loader';
import { ListGroupItemLink, ButtonLink } from 'react-router-bootstrap';
import Flow from './flow/flow';
import * as utils from '../common/utils.js';

import debugging from 'debug';
let	debug = debugging('snowstreams:client:pages:stream');

debug(utils);

let Button = utils.Button;

let Stream = React.createClass({
	contextTypes: {
		router: React.PropTypes.func
	},
	getInitialState() {
		let ret = { 
			sources: [],
			howmany: 0,
			programMap: {},
			programArgumentMap: {},
			streamMap: {},
			streams: false,
			asset: 'Stream',
			active: 'program',
			programs: false,
			newalert: {},
			form: this.defaultForm(),
			currentField: false,
			menu: {},
			durationError:2000,
			durationSuccess:1500,
			formInfo: {
				name: ' <p>You will reference this stream by name in the UI.</p> <p> The name should be unique among your streams and should be easily identifiable.</p> ',
				type: '<p><b>custom</b>: Create your own.</p><p><b>/dev/null</b>: Discard the stream. You can define special /dev/nulls in Programs.</p><p><b>file</b>: Save the stream to a file.</p><p><b>pipe</b>: Placeholder entry for the ui where a source or stream will output to stdout.</p><p><b>program</b>: Run a shell program that produces the stream.  You should add your programs first from the Programs section.</p> <p><b>udp</b>: Stream over udp (iptv).',
				file: ' <p>A valid path to save a file. </p> <p> You must include the filename.</p> <p>You can include variables that can be replaced in channels or programmatically...</p> <p>//@:%PORT%/videos/%FILE%</p>',
				directory: 'You must check this to traverse a directory and select extensions to filter.',
				host: ' <p>The host for your udp stream.  Local streams can use <b>@</b></p>',
				port: ' <p>Port number for the udp stream</p>',
				destination: '<p>The filename or address you will use to consume the final stream.  Leave empty for pipe.</p>',
				event: '<p>The emitted event we will listen for to run the program</p>',
				argument: '<div><p>Select the appropriate argument line</p><p> If there is an <b>%INPUT%</b> variable, we will subtitute the available source (usually a file or stream).  Without the <b>%INPUT%</b> variable we will pipe in the source, so you should expect <b>stdin</b>.</p><p> If there is an <b>%OUTPUT%</b> variable, the <b>destination</b> field or from a programmatic instance will be insterted.</p><p>Without an output, the expectation is  you are piping to a channel, have included the output in the argument, or this is a program that does not output.  Please use the destination field to denote the reachable address for the stream if needed.</p> </div>', 
				stderror: '<p>Choose where to send error messages.</p>',
				output: '<p>If this program is middleware select the final stream output.</p><p> Two streams will be produced.  The named stream will contain the program\'s stdout. </p><p> The final output stream will be named <b>slug:output</b>.</p><p>For example, if your stream name is <b>local files</b>, the output will be named <b>local-files:output</b>. </p><p><b>Note</b> that the slug is a unique value the database generates from name.</p>',
				program: ' <p>Select one of your programs. </p> <p> The argument list will be supplied after you chose.</p> ',
				expose: ' <p>Create API routes to manage this stream.  </p><p>Available arguments are: <b>play</b> (<i>alias:</i> <b>start</b>) <br /><b>restart</b><br /> <b>status</b><br /> <b>stop</b> <br /> <b>view</b><br />No argument will <b>play</b>.</p><p>2 routes are created:<br/> <b>/proxy/stream/:name/:argument</b><br/> sends Content-Type=mpegts for play.</p><p><b>/proxy/stream/:name/:<i>mime-type</i></b> sends the Content-Type you set as <i>mime-type</i>.</p><p>Status will give you program information.  Currently that is limited to the filename if available, codec information if available, start time and expected stop time. </p>',

			}
		};
		return ret;
	},
	defaultForm() {
		return {
			name: '',
			argument: ''
		}
	},
	componentDidMount() {
		let _this = this;
		this.getStreams();
		this.checkProps(this.props);
		$(document).on('focus','#Streamdiv .showinfo', function(e) {
			_this.showInfo(e);
		});
	},
	checkProps(props) {
		let _this = this;
		if(props.params.slug === 'new') {
			this.setState({ 
				form: _this.defaultForm(),
				newalert: props.query.alert || { show: false },
				currentField: false
			});
			return;
		} else if(props.params.slug) {
			this.getStream(props.params.slug);
			return;
		}
	},
	componentWillReceiveProps(nextProps) {
		let sameQuery = this.props.params.slug === nextProps.params.slug;
		if(!sameQuery && this.props.params.slug !== '') {
			this.checkProps(nextProps);
		}
		return;
	},
	componentDidUpdate: function() {
		
	},
	logChange: function (val) {
		debug("Selected: " + val);
	},
	getStreams() {
		let _this = this;
		let myFn2 = function(data) {
			let send = _.isArray(data.data.programs) ? data.data.programs : [];
			let programMap = {};
			let programArgumentMap = {};
			_.each(send, function(v) {
				programMap[v._id] = v;
				_.each(v.arguments, function(a) {
					programArgumentMap[a._id] = a;
				});
			});
			
			_this.setState({programs: send, programMap: programMap, programArgumentMap: programArgumentMap});
		}
		let myFn3 = function(data) {
			let send3 = _.isArray(data.data.streams) ? data.data.streams : [];
			let streamMap = {};
			_.each(send3, function(v) {
				streamMap[v._id] = v;
			});
			_this.setState({streams: send3, streamMap: streamMap});
		}

		this.props.sockets.list('Program', {sort:{name:1}, populate:'arguments'}, myFn2);
		this.props.sockets.list('Stream', {sort:{type: 1, name:1}, populate:'argument, program, stderror'}, myFn3);
	},
	getStream(slug, callback) {
		
		// this.setState({ form: this.defaultForm() });
		
		if(!_.isFunction(callback)) {
			callback = ()=>{};
		}
		
		let myFn = (data) => {
			
			// We got an error
			if(data.error) {
								
				this.setState({ 
					newalert: {
						style: 'danger',
						data: data,
						show: true
					},
					currentField: 'error'
				}, callback);
				
			// we got a result that may contain a list
			} else if(_.isObject(data.data)) {
								
				let send = _.isArray(data.data.streams) ? data.data.streams[0] : _.isObject(data.data.streams) ? data.data.streams : {};
				
				let name = _.isObject(send.updatedBy.name) ?  send.updatedBy.name.first + '  ' + send.updatedBy.name.last: ' Unknown';
				
				let pos = false;
				
				_.each(this.state.programs, function(o,k) {
					if(o._id === send.program) pos = k;
				});
				
				let menu = {}
				menu[send.type] = true;
				
				this.setState({ 
					form: send,
					menu,
					forming: false,
					programPos: pos || 0,
					currentField: false,
					newalert: {
						style: 'info',
						component: (<div>Updated last on {moment(send.updatedAt).format('MMMM Do YYYY, h:mm:ss a')} by {name}</div>),
						show: true
					}
				}, callback);
				
			// something is wrong
			} else {
				
				this.refs.manage.error();
				
				this.setState({ 
					currentField: 'error',
					newalert: {
						style: 'danger',
						html: 'Error. Please check console',
						show: true
					},
				}, callback);
				
			}
		} // end myFn

		let opts = {  
			exclude: '__v',
			slug: slug,
			sort: 'order',
			populate: 'arguments updatedBy',
		};
		this.props.sockets.getDocument('Stream', opts, myFn);
	},
	dismissAlert() {
		this.setState({ 
			newalert: {
				show: false
			}
		});
	},
	dismissInfoAlert() {
		this.setState({ 
			currentField: false
		});
	},			
	submitForm(e) {
		e.preventDefault();
		let _this = this;
		this.refs.manage.loading();
		this.setState({ forming: true });
		let reload = (this.state.form._id) ? true : false;
		let go = (_this.state.form._id) ? 'update' : 'create';
		let domany = false;
		let name;
		let cloned;
		let cloning;
		delete _this.state.form.createdBy;
		delete _this.state.form.updatedBy;
		delete _this.state.form.createdAt;
		delete _this.state.form.updatedAt;
		if(_this.state.howmany && !_this.state.form._id && _this.state.form.type === 'udp') {
			// clone form and create a fake setState
			cloning = { ...this.state.form };
			cloned = {
				state: {
					form: cloning
				}
			}
			cloned.state.form.port = parseFloat(cloned.state.form.port);
			cloned.setState = function(fake){};
			cloned.dismissAlert = function(fake){};
			cloned.defaultForm = function(fake){};
			domany = true;
			name = _this.state.form.name;
		}
		this.props.sockets.submitForm(this, e, go, 'Stream', function(err, data) {
			
			if(!err) {
				
				_this.getStreams();
				if(go === 'create') {
					_this.refs.manage.success();
					_this.context.router.transitionTo('manage-stream', { slug: data.data.streams.slug } );
				} else {
					_this.getStream(data.data.streams.slug, _this.refs.manage.success);

				}

			} else {
				_this.refs.manage.error();
				
				_this.setState({ 
					newalert: {
						style: 'warning',
						html: err,
						show: true
					},
					loading: false,
					forming: false
				});
			}
			debug('add more', domany, _this.state.howmany)
			if(domany) {
				for(var i=0;i<_this.state.howmany;i++) {
					cloned.state.form.port++;
					cloned.state.form.name = name + ':' + cloned.state.form.port;
					debug('add many', cloned, i)
					_this.props.sockets.submitForm(cloned, e, 'create', 'Stream', _this.getStreams);
				}
			}
		});
	},
	handleChange(val, who) {
		let send = {form: this.state.form}
		let form = send.form;
		let value;
		let name;
		if(who === 'howmany') {
			this.setState({ howmany: val.value});
			return;			
		} else if(who){
			name = who;
			value = _.isArray(val) ? val : _.isObject(val) ? val.value : '';
			if(who === 'program') {
				send.programPos = parseFloat(val.pos);
				form.argument = ''
			}
		} else {
			name = val.target.name;
			value = val.target.value
		}
		form[name] = value;
		debug('send', send, val, who);	
		this.setState(send);
    },
    showInfo(e) {
		debug(e);
		let name = e.currentTarget.dataset.name;
		if(name.search(':') !== -1) {
			name = 'arguments'
		}
		this.setState({currentField: name});
    },
    deleteStream(e) {
		e.preventDefault();
		this.refs.remove.loading();
		let _this = this;
		
		utils.deleteDocument(this, e, 'Stream', ()=>{
			_this.refs.remove.error();
		}); 
	},
	reallyDelete() {
		let _this = this;
		let form = { ...this.state.form };
		this.props.sockets.reallyDelete(this, null, 'Stream', function(err, data) {
			if(!err) {
				_this.getStreams();
				if(_this.state.form._id) {
					_this.getStream(data.data.streams.slug);
				} else {
					_this.context.router.transitionTo('streams', {}, { 
						alert: {
							style: 'danger',
							html: 'Stream ' + form.slug + ' Deleted',
							show: true
						}
					});
				}
			} else {
				_this.refs.remove.error();
			}
		});
	},
	render() {
		
		debug(this.state,  this.context.router.getCurrentPathname());	
		
		let _this = this;
		let state = this.state;
		
		// show loading if waiting for initial sockets
		if(!state.streams || !state.programs) {
			return (<div id="channeldiv" className=" ">
				
				<div className="header-loading">Loading Streams</div>
				
				<div className="clearfix" />
				<div className="col1 col-md-2 menuitem ">
					<ListGroup>
						<ListGroupItem  bsSize="xlarge"  key={'loading-key'}><div className="clearfix"><div className="" style={{textAlign:'center'}}><Loader /></div></div></ListGroupItem>	
					</ListGroup>
				</div>
				<div className="col2 col-sm-10 col-md-10" >
					<div><Alert style={!state.programs ? 'warning' : 'success'} html={state.programs ? 'Programs ready' : 'Loading programs...'}  /></div>
					
					<div><Alert style={!state.streams ? 'warning' : 'success'} html={state.streams ? 'Streams ready' : 'Loading streams...'}  /></div>
				</div>
			</div>)
		}
		
		
		let typeOptions = [
			{ value: 'custom', label: 'custom' },
			{ value: 'file', label: 'file' },
			{ value: 'pipe', label: 'pipe' },
			{ value: 'program', label: 'program' },
			{ value: 'udp', label: 'udp' } 
		];
		let type = <Select
			name="type"
			options={typeOptions}
			onChange={function(val, obj) {
				_this.handleChange(obj[0], 'type')
			}}
			value={this.state.form.type}
		/>
					
		let programOptions = this.state.programs.map(function(v, k) {
			return ({ value: v._id, label: v.name, pos: k });
		});
		let programs = <Select
			name="program"
			allowCreate={false}
			options={programOptions}
			onChange={function(val, obj) {
				_this.handleChange(obj[0], 'program')
			}}
			value={this.state.form.program}
			
		/>
		let udpOptions = [{value: 0, label: 'Single stream only'}];
		for(let i=0;i<101;i++) {
			udpOptions.push({value: i, label: i});
		}
		let udpoptions = <Select
			name="howmany"
			allowCreate={true}
			options={udpOptions}
			onChange={function(val, obj) {
				_this.handleChange(obj[0], 'howmany')
			}}
			value={this.state.howmany}
		/>	
		let args = <span />;
		let stream;
		if(this.state.form.argument !== '' || this.state.programPos > -1) {
			
			let programArguments = this.state.programs[this.state.programPos].arguments.map(function(v, k) {
				return ({ value: v._id, label: v.anchor + ' : ' + v.command });
			});
			
			args = <Select
				name="argument"
				options={programArguments}
				onChange={function(val, obj) {
					_this.handleChange(val.split(','), 'argument')
				}}
				multi={true}
				value={this.state.form.argument}
			/>;
			
			let streamOptions = this.state.streams.map(function(v, k) {
				return ({ value: v._id, label: v.name });
			});
			stream = <Select
				name="stream"
				allowCreate={false}
				options={streamOptions}
				onChange={function(val, obj) {
					_this.handleChange(obj[0], 'stream')
				}}
				value={this.state.form.stream}
			/>;
			
		}
		
		let stderrorOptions = [];
		_.each(this.state.programs, function(o) {
				if(o.type === 'internal') {
					_.each(o.arguments, function(a) {
						stderrorOptions.push({label: _this.state.programArgumentMap[a._id].command, value: _this.state.programArgumentMap[a._id]._id});
					})
				}
		});
		let stderror = <Select
			name="stderror"
			options={stderrorOptions}
			multi={true}
			onChange={function(val, obj) {
				_this.handleChange(val.split(','), 'stderror')
			}}
			value={this.state.form.stderror}
		/>
		
		let t;
		let push = {};
		
		_.each(this.state.streams, (source) => {
			let v = source;
			if(!v.type) v.type = 'unknown';
			if(t !== v.type) {
				t = v.type;
				push[v.type] = [];
			} 
			push[t].push(<ListGroupItemLink bsSize="small" to='manage-stream' params={{ slug: v.slug }} key={v.slug+'-key'}>{v.name}</ListGroupItemLink>);
		});
		debug('push', push);
		let listItems = [];
		_.each(push, (sources, type) => {
			let item = (<ListGroupItem  style={{margin:0, padding: 0, border: 0}} >
				<Collapse in={this.state.menu[type]} style={{margin:0, padding: 0, border: 0}} className="">
					<ListGroup  >
						{sources}
					</ListGroup>
				</Collapse>
			</ListGroupItem>);
			
			listItems.push(<ListGroupItem className="no-border" style={{ border:'none!important', backgroundColor:'#eee', fontWeight:'bold'}}  onClick={(e) => {
				let menu = {};
				menu[type] = !this.state.menu[type];
				this.setState({ menu });
			}} >{type.toUpperCase()}</ListGroupItem>);
			
			listItems.push(item);
			
		});
		if(listItems.length === 0) {
			listItems.push(<ListGroupItem  bsSize="xlarge"  key={'loading-key'}><div className="clearfix"><div className="" style={{float:'right'}}><Loader /></div></div></ListGroupItem>);
		}
		let list = (
			<ListGroup>
				{listItems}
			</ListGroup>
		);
		return (
			
			<div id="Streamdiv" className=" ">
				
				<div className="header-streams lg-header ">
					<div>Streams</div>
					<ButtonLink to="manage-stream" params={{ slug: 'new' }} className="plus-button" ><span className="glyphicon glyphicon-plus" /> Add </ButtonLink>
				</div>
				
				<div className="clearfix" />
				<div className="menuitem col1 col-md-2">
					{list}
				</div>
				<div className="col2 col-sm-10 col-md-10 no-padding" >
					{this.state.newalert.show ? <Alert style={this.state.newalert.style} html={this.state.newalert.html} data={this.state.newalert.data} component={this.state.newalert.component} dismiss={_this.dismissAlert} /> : ''}
					<div className="col-sm-8 col-md-8" >
						
						<form className="form-horizontal"  onSubmit={this.submitForm}>	
							<fieldset>			
		 						
								<div className="form-group showinfo" data-name="name" >
									<label className="col-md-2 control-label" htmlFor="name">name</label>  
									<div className="col-md-10">
										<input id="name" name="name" type="text" placeholder="cable" className="form-control input-md"  onChange={this.handleChange} value={this.state.form.name} />
										
									</div>
								</div>
								
								<div className="clearfix" />
								<div ref="show" className={this.state.form.name !== '' ? 'fatalbert' : 'casper'}>
								
									<div className="clearfix" />
									<div className="form-group showinfo" data-name="expose">
										<label className="col-md-2 control-label" htmlFor="expose">api</label>
										<div className="col-md-10">
											<div className="checkbox">
												
												<label htmlFor="expose-0">
													<input type="checkbox" name="expose" id="expose-0" onChange={this.handleChange} value={this.state.form.expose === 'true' || this.state.form.expose === true ? 'false' : 'true'} checked={this.state.form.expose === 'true' || this.state.form.expose === true ? 'checked' : ''} />
													{this.state.form.expose === 'true' || this.state.form.expose === true ? <div><a href={"/" + this.props.sockets.proxy + "/stream/"+_this.state.form.slug+"/status"}>/{this.props.sockets.proxy}/stream/{_this.state.form.slug}/status</a><br /><a href={"/" + this.props.sockets.proxy + "/stream/"+_this.state.form.slug+"/raw"}>/{this.props.sockets.proxy}/stream/{_this.state.form.slug}/raw</a></div> : <span>This stream is not managed via API.</span>}
												</label>
											</div>
										</div>
									</div>
									
									<div className="clearfix" />
									<div className="form-group showinfo" data-name="type" >
										<label className="col-md-2 control-label" htmlFor="type">type</label>
										<div className="col-md-10">
											{type}
										</div>
									</div>
									
									<div className="clearfix" />
									<div ref="udp" className={this.state.form.type === 'udp' || this.state.form.type === 'custom' ? 'fatalbert' : 'casper'}>
										
										<div className="form-group showinfo" data-name="host" >
											<label className="col-md-2 control-label" htmlFor="host">host</label>  
											<div className="col-md-10">
												<input id="host" name="host" type="text" placeholder="@" className="form-control input-md"  onChange={this.handleChange} value={this.state.form.host}  />
												  
											</div>
										</div>
										
										<div className="clearfix" />
										<div className="form-group  showinfo" data-name="port">
											<label className="col-md-2 control-label" htmlFor="port">port</label>
											<div className="col-md-5">
												<div className="input-group">
													<input id="port" name="port" className="form-control" placeholder="11000" type="text"  onChange={this.handleChange} value={this.state.form.port}  />
													<span className="input-group-addon">number</span>
												</div>
											</div>
										</div>
										
										<div className="clearfix" />
										<div ref="howmany" className={!this.state.form._id  ? 'fatalbert' : 'casper'}>
											<div className="form-group showinfo" data-name="howmany" >
												<label className="col-md-2 control-label" htmlFor="howmany">create more?</label>
												<div className="col-md-10">
													{udpoptions}
													<span className="help-block">A udp stream is <b>single</b> use.  You can create multiple udp streams by selecting a number.  Each new stream will simply increase the port number by 1 and create a name using name:port.</span> 
												</div>
											</div>
										</div>
										
										<div className="clearfix" />
										<div className="form-group showinfo" data-name="listen">
											<label className="col-md-2 control-label" htmlFor="listen">listen</label>
											<div className="col-md-10">
												<div className="checkbox">
													
													<label htmlFor="listen-0">
														<input type="checkbox" name="listen" id="listen-0" onChange={this.handleChange} value={this.state.form.listen === 'true' || this.state.form.listen === true ? 'false' : 'true'} checked={this.state.form.listen === 'true' || this.state.form.listen === true ? 'checked' : ''} />
														{this.state.form.listen === 'true' || this.state.form.listen === true ? <span>Listening for the following emitted event</span> : <span>This stream is not listening for commands.</span>}
													</label>
												</div>
											</div>
										</div>									
											
									</div>								
									
									<div className="clearfix" />
									<div ref="command" className={this.state.form.listen === true || this.state.form.listen === 'true' ? 'fatalbert' : 'casper'}>
									
										<div className="form-group showinfo" data-name="event" >
											<label className="col-md-2 control-label" htmlFor="event">event</label>  
											<div className="col-md-10">
												<input id="name" name="event" type="text" placeholder="change channel" className="form-control input-md"  onChange={this.handleChange} value={this.state.form.event} />
												
											</div>
										</div>
									
									</div>
									
									
									<div className="clearfix" />
									<div ref="others" className={this.state.form.type === 'program' || this.state.form.type === 'custom' || this.state.form.listen === 'true' || this.state.form.listen === true ? 'fatalbert' : 'casper'}>
										
										
										
										<div className="clearfix" />
										<div className="form-group showinfo" data-name="program">
											<label className="col-md-2 control-label" htmlFor="program">program</label>  
											<div className="col-md-10">
												{programs} 
											</div>
										</div>
										
										<div className="clearfix" />
										<div ref="programthings" className={this.state.form.argument !== '' || this.state.programPos > -1 ? 'fatalbert' : 'casper'}>
											
											<div className="form-group showinfo" data-name="argument">
												<label className="col-md-2 control-label" htmlFor="args">arguments</label>  
												<div className="col-md-10" ref="arguments">
													{args} 
													
												</div>
											</div>
										</div>
									
									</div>
									
									<div className="clearfix" />
									<div ref="others" className={this.state.form.type === 'program' || this.state.form.type === 'custom' ? 'fatalbert' : 'casper'}>
										
										
										<div className="form-group showinfo" data-name="output" >
											<label className="col-md-2 control-label" htmlFor="stream">output</label>
											<div className="col-md-10">
												{stream}
											</div>
										</div>
									</div>
									
									<div className="clearfix" />
									<div ref="others" className={this.state.form.type === 'program' || this.state.form.type === 'custom' || this.state.form.type === 'file' ? 'fatalbert' : 'casper'}>
										
										
											
										<div className="clearfix" />	
										<div className="form-group showinfo" data-name="destination">
											<label className="col-md-2 control-label" htmlFor="url">destination</label>  
											<div className="col-md-10">
												<input id="url" name="destination" type="text" placeholder="http://host:port/file.ext" className="form-control input-md"  onChange={this.handleChange} value={this.state.form.destination}  />  
											</div>
										</div>
										
									</div>
									
									<div className="clearfix" />
									<div ref="others" className={this.state.form.listen === 'true' || this.state.form.listen === true || this.state.form.type === 'custom' ? 'fatalbert' : 'casper'}>
											
											<div className="form-group showinfo" data-name="stderror" >
												<label className="col-md-2 control-label" htmlFor="stderror">listen error</label>
												<div className="col-md-10">
													{stderror}
												</div>
											</div>
																						
										

									</div>		
									
									<div className="clearfix" />
									<div className="form-group no-gutter" style={{margin:'15px 0 30px'}}>
										<div className=" col-sm-2">
										</div>
										<div className=" col-sm-8">
											<Button className={"btninfo left square"} ref="manage" disabled={this.state.forming} durationSuccess={this.state.durationSuccess} durationError={this.state.durationError} >{this.state.form._id ? 'Update Stream' : 'Add Stream'}</Button>
											<input type="hidden" name="_id" value={this.state.form._id} /> 
											<input type="hidden" name="slug" value={this.state.form.slug} /> 
										</div>
										<div className="col-sm-2">
											{this.state.form._id && !this.state.forming ? <Button className="btndanger square right small" ref="remove" onClick={this.deleteStream} durationSuccess={this.state.durationSuccess} durationError={this.state.durationError} ><span className="glyphicon glyphicon-trash" /></Button> : ''}
										</div>
									</div>
								</div>	
							</fieldset>
						</form>
					</div>
					<div className=" col-sm-4 col-md-4" >
									
						<div style={{ textAlign:'right', marginBottom:15 }} >
							{!this.state.forming && this.state.form._id && !this.state.form.streaming ? <Button className="btnsuccess" onClick={this.startStream} ref="start" durationSuccess={this.state.durationSuccess} durationError={this.state.durationError} ><span className="glyphicon glyphicon-play" aria-hidden="true" /> &nbsp;Start</Button> : ''}
						
						
							{!this.state.forming && this.state.form._id && this.state.form.streaming ? <Button  className="btndanger" onClick={this.stopStream} ref="stop" durationSuccess={this.state.durationSuccess} durationError={this.state.durationError} ><span className="glyphicon glyphicon-stop" aria-hidden="true" /> &nbsp;Stop</Button> : ''}
						</div>
						
						<hr />
						
						<div className="clearfix" ><br /><br /></div>
						 {this.state.currentField ? <Alert style="info" html={this.state.formInfo[this.state.currentField]} dismiss={_this.dismissInfoAlert} /> : ''}
					</div>
				</div>
					
			</div>
			
		);
			
	},
	
	startStream() {
		debug('start Stream');
	
		let _this = this;
		
		this.refs.start.loading();
		
		_this.props.sockets.play('start', this, this.state.asset, function(err, data) {
			debug('start callback', data, err);
			if(err) {
				
				_this.setState({ 
					newalert: {
						style: 'danger',
						html: err,
						show: true
					}
				});
				_this.refs.start.error();
	
			} else {
				_this.refs.stop.success();
				_this.getSource(_this.state.form.slug, () => {
					_this.setState({ 
						newalert: {
							style: 'success',
							html: data.message,
							show: true
						}
					});
				});
			}
		});
	},
	
	stopStream() {
		debug('stop Stream');
		
		let _this = this;
		
		this.refs.stop.loading();
		
		_this.props.sockets.play('stop', this, this.state.asset, function(err, data) {
			debug('stop callback', data, err);
			if(err) {
				
				_this.setState({ 
					newalert: {
						style: 'danger',
						html: err,
						show: true
					}
				});
				_this.refs.stop.error();
	
			} else {
				_this.refs.stop.success();
				_this.getStream(_this.state.form.slug, () => {
					_this.setState({ 
						newalert: {
							style: 'warning',
							html: data.message,
							show: true
						}
					});
				});
			}
			
		});
	},
});

export default Stream;
