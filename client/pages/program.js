import React from 'react';
import { Link } from 'react-router';
import moment from 'moment';
import _ from 'lodash';
import Select from 'react-select';
import Alert from '../common/alert.js';
import { ListGroupItem, ListGroup } from 'react-bootstrap';
import Loader from '../common/loader';
import { ListGroupItemLink } from 'react-router-bootstrap';
import Flow from './flow/flow';
import * as utils from '../common/utils.js';

import debugging from 'debug';
let	debug = debugging('snowstreams:client:pages:program');

let buildProgramQuery = utils.buildProgramQuery;
let Button = utils.Button;

let Program = React.createClass({
	contextTypes: {
		router: React.PropTypes.func
	},
	childContextTypes: {
		router: React.PropTypes.func
	},
	getInitialState() {
		let obj = { 
			programs: [],
			programMap: {},
			programArgumentMap: {},
			alert: '',
			newalert: {},
			forming: false,
			form: this.defaultForm(),
			currentField: false,
			durationError:2000,
			durationSuccess:1500,
			formInfo: {
				name: ' <p>You will reference this program by name in the UI.</p> <p> The name should be unique among your programs and should be easily identifiable.</p> ',
				expose: ' <p>Create an API route to execute this program.  </p><p>This can be useful for channel changers.</p><p>You can also attach a program to upd stream listener in the Stream section. </p>',
				program: '<p>The command you would execute in a shell.</p><p>  Do not include any arguments. </p>',
				arguments: '<div><p<b>Name is required</b></p><p>You can attach multiple argument lines to a program.  When using the program you can select which argument line to apply.</p> <p>Each argument line can include placeholders in the form of <b>%VARIABLE%.</b> </p><p>Programs like ffmpeg may need: <br/ >input: <b>%INPUT%</b><p /><p> an output...<br /> stream: <b>pipe:1</b><br /> named url: <b>%OUTPUT%</b></p></div>', 
				stderror: '<p>Choose where to send error messages.</p>',
				type: '<p>You probably want a shell program.</p><p>Change internal programs with caution</a>',
			},
		}
		return obj;
	},
	defaultForm() {
		return {
			name: '',
			program: '',
			arguments: [{
				anchor: '',
				argument: ''
			}],
			type: 'shell',
			stderror: []
		}
	},
	componentDidMount() {
		let _this = this;
		this.getPrograms();
		this.checkProps(this.props);
		$(document).on('focus', '#programdiv .showinfo', function(e) {
			_this.showInfo(e);
			if(e.target.name.search(':') !== -1) {
				let form = _this.state.form;
				let split = e.target.name.split(':');
				let order = form.arguments.length - 1;
				if(parseFloat(split[1]) === order) {
					
					form.arguments.push( {
						anchor: '',
						argument: ''
					});
					_this.setState({form: form});
				}
			}
		});
	},
	componentWillMount() {
		return;
	},
	
	checkProps(props) {
		let _this = this;
		if(props.params.slug === 'new') {
			this.setState({ 
				form: _this.defaultForm(),
				newalert: { show: false },
				currentField: false
			});
			return;
		} else if(props.params.slug) {
			this.getProgram(props.params.slug);
			return;
		}
	},
	
	componentWillReceiveProps(nextProps) {
		let sameQuery = this.props.params.slug === nextProps.params.slug;
		if(!sameQuery && this.props.params.slug !== '') {
			this.checkProps(nextProps);
		}
		return;
	},
	
	getPrograms() {
		let _this = this;
		let myFn = function(data) {
			let send = _.isArray(data.data.programs) ? data.data.programs : [];
			let programMap = {};
			let programArgumentMap = {};
			_.each(send, function(v) {
				programMap[v._id] = v;
				_.each(v.arguments, function(a) {
					programArgumentMap[a._id] = a;
				});
			});
			
			_this.setState({programs: send, programMap: programMap, programArgumentMap: programArgumentMap});
		}

		this.props.sockets.list('Program', {sort:{name:1}, populate: 'arguments createdBy updatedBy'}, myFn);
	},
	
	getProgram(slug, callback) {
		let _this = this;
		// _this.setState({ form: _this.defaultForm() });
		let myFn = function(data) {
			
			// We got an error
			if(data.error) {
				_this.setState({ 
					newalert: {
						style: 'danger',
						data: data,
						show: true
					},
					forming: false,
					currentField: 'error'
				}, callback);
			// we got a result that may contain a list
			} else if(_.isObject(data.data)) {
				
				let send = _.isArray(data.data.programs) ? data.data.programs[0] : _.isObject(data.data.programs) ? data.data.programs : {arguments:{}};
				send.arguments[send.arguments.length] = {
					anchor: '',
					argument: ''
				}
				
				let name = _.isObject(send.updatedBy.name) ?  send.updatedBy.name.first + '  ' + send.updatedBy.name.last: ' Unknown';
				
				_this.setState({ 
					form: send,
					currentField: false,
					forming: false,
					newalert: {
						style: 'info',
						component: (<div>Updated last on {moment(send.updatedAt).format('MMMM Do YYYY, h:mm:ss a')} by {name}</div>),
						show: true
					}
				}, callback);
			// something is wrong
			} else {
				
				_this.refs.manage.error();
				
				_this.setState({ 
					currentField: 'error',
					forming: false,
					newalert: {
						style: 'danger',
						html: 'Error. Please check console',
						show: true
					},
				}, callback);
			}
		} // end myFn

		let opts = {  
			exclude: '__v',
			slug: slug,
			sort: 'order',
			populate: 'arguments updatedBy',
		};
		
		this.props.sockets.getDocument('Program', opts, myFn);
		
	},
	
	dismissAlert() {
		this.setState({ 
			newalert: {
				show: false
			}
		});
	},
	
	dismissInfoAlert() {
		this.setState({ 
			currentField: false
		});
	},	
			
	submitForm(e) {
		e.preventDefault();
		
		this.refs.manage.loading();
		this.setState({ forming: true });
		
		let _this = this;
		let reload = (_this.state.form._id) ? true : false;
		
		this.props.sockets.submitForm(this, e, 'managePrograms', 'Program', function(err, data) {
			
			let d =  _.isArray(data.data.programs) ? data.data.programs[0] : data.data.programs;
			if(!err) {
				_this.getPrograms();
				if(!reload) {
					_this.refs.manage.success();
					_this.context.router.transitionTo('manage-program', { slug: data.data.programs.slug } );
				} else {
					_this.getProgram(d.slug, _this.refs.manage.success);

				}

			} else {
				_this.refs.manage.error();
				
				_this.setState({ 
					newalert: {
						style: 'warning',
						html: err,
						show: true
					},
					loading: false,
					forming: false
				});
			}
		});
	},
	
	handleChange(e, who) {
		let form = this.state.form;
		let _this = this;
		if(who){
			let value = _.isArray(e) ? e : _.isObject(e) ? e.value : '';
			form[who] = value;

		} else {
			let name = e.target.name;
			if(name.search(':') !== -1) {
				let split = name.split(':');
				form.arguments[split[1]][split[0]] = e.target.value;
			 
			} else {
				form[name] = e.target.value;			
			}
		}
		this.setState({form: form});
    },
    
    showInfo(e) {
		//debug(e);
		let name = e.currentTarget.dataset.name;
		if(name.search(':') !== -1) {
			name = 'arguments'
		}
		this.setState({currentField: name});
    },
    
    deleteProgram(e) {
		e.preventDefault();
		this.refs.remove.loading();
		utils.deleteDocument(this, e, 'Program', ()=>{
			this.refs.remove.error();
		});
	},
	
	reallyDelete() {
		let _this = this;
		
		let form = { ...this.state.form };
		
		this.props.sockets.reallyDelete(this, null, 'Program', function(err, data) {
			if(!err) {
				_this.getPrograms();
				if(_this.state.form._id) _this.getProgram(data.data.programs.slug);
			}
			if(!err) {
				_this.getPrograms();
				if(_this.state.form._id) {
					_this.getProgram(data.data.programs.slug);
				} else { 
					_this.context.router.transitionTo('programs', {}, { 
						alert: {
							style: 'danger',
							html: 'Program ' + form.slug + ' Deleted',
							show: true
						}
					});
				}
			} else {
				_this.refs.remove.error();
			}
			
		});
	},
	
	render() {
		debug('stream state', this.state);
		let _this = this;
		let args = [];
		_.each(_this.state.form.arguments, function(arg, k) {
			if(_.isObject(arg)) {
				args.push(addRow(arg, k));
			}
		}); 
		
		function addRow(arg,k) {
			return (
				<div  className="clearfix" key={'order:' + k}>
					
					<div className="col-sm-3">
						<input style={{fontSize:12}} name={'anchor:'+k} ref={'anchor:'+k} onChange={_this.handleChange}  className="form-control" placeholder="name" type="text"  value={_this.state.form.arguments[k].anchor} />
					</div>
					<div className="col-sm-9" >
						<input style={{fontSize:12}} name={'argument:'+k} ref={'argument:'+k} onChange={_this.handleChange}  className="form-control" placeholder=" -re -i %INPUT% -c copy -o %OUTPUT%" type="text" value={_this.state.form.arguments[k].argument} />
					</div>
				</div>
			);
		}
		
		let typeOptions = [
			{ value: 'shell', label: 'shell program' },
			{ value: 'fluent-ffmpeg', label: 'fluent-ffmpeg' },
			{ value: 'internal', label: 'internal program' },
		];
		let type = <Select
			name="type"
			options={typeOptions}
			onChange={function(val, obj) {
				_this.handleChange(obj[0], 'type')
			}}
			value={this.state.form.type}
		/>
		let stderrorOptions = [];
		_.each(this.state.programs, function(o) {
				if(o.type === 'internal') {
					_.each(o.arguments, function(a) {
						stderrorOptions.push({label: _this.state.programArgumentMap[a._id].command, value: _this.state.programArgumentMap[a._id]._id});
					})
				}
		});
		let stderror = <Select
			name="stderror"
			options={stderrorOptions}
			multi={true}
			onChange={function(val, obj) {
				_this.handleChange(val.split(','), 'stderror')
			}}
			value={this.state.form.stderror}
		/>
		
		
		let listItems = this.state.programs.map(function(v) {
			return (<ListGroupItemLink bsSize="small" to='manage-program' params={{ slug: v.slug }} key={v.slug+'-key'}>{v.name}</ListGroupItemLink>)
		});
		if(listItems.length === 0) {
			listItems.push(<ListGroupItem  bsSize="xlarge"  key={'loading-key'}><div className="clearfix"><div className="" style={{float:'right'}}><Loader /></div></div></ListGroupItem>);
		}
		let list = (
			<ListGroup>
				<ListGroupItemLink to='manage-program' params={{ slug: 'new' }}>New Program</ListGroupItemLink>
				{listItems}
			</ListGroup>
		);
		
		return (
			
			<div id="programdiv">
				
				<div className="header-programs lg-header "><div>Programs</div></div>
				
				<div className="clearfix" />
				<div className="menuitem col1 col-md-2">
					{list}
				</div>
				<div className="col2 col-sm-10 col-md-10 no-padding" >
					{this.state.alert}
					{this.state.newalert.show ? <Alert style={this.state.newalert.style} html={this.state.newalert.html} data={this.state.newalert.data} component={this.state.newalert.component} dismiss={_this.dismissAlert} /> : ''}
					<div className="col-sm-8 col-md-8" >
						<form className="form-horizontal" onSubmit={this.submitForm}>	
							<fieldset>			
		 						
								<div className="form-group showinfo" data-name="name">
									<label className="col-md-2 control-label" htmlFor="name">name</label>  
									<div className="col-md-10">
										<input ref="name" name="name"  onChange={this.handleChange} type="text" placeholder="ffmpeg" className="form-control input-md" required="" value={this.state.form.name} />
									</div>
								</div>
								
								<div className="clearfix" />
								<div ref="show" className={this.state.form.name !== '' ? 'fatalbert' : 'casper'}>
									
									<div className="clearfix" />
									<div className="form-group showinfo" data-name="type">
										<label className="col-md-2 control-label" htmlFor="type">type</label>
										<div className="col-md-10">
											{type}
										</div>
									</div>
									
									<div className="clearfix" />
									<div ref="show" className={_.contains(['shell','internal'],this.state.form.type) ? 'fatalbert' : 'casper'}>
										<div className="form-group showinfo" data-name="program">
											<label className="col-md-2 control-label" htmlFor="program">program</label>  
											<div className="col-md-10">
												<input ref="program" name="program" onChange={this.handleChange}  type="text" placeholder="ffmpeg" className="form-control input-md" required="" value={this.state.form.program} />
											</div>
										</div>
									</div>
									
									
									
									<div className="clearfix" />
									<div className="form-group showinfo" data-name="arguments">
										<label className="col-md-2 control-label" htmlFor="args">arguments</label>  
										<div className="col-md-10">
											{args}											
										</div>
									</div>
									
									<div className="clearfix" />
									<div ref="stederr" className={this.state.form.type !== 'internal' ? 'fatalbert' : 'casper'}>
										
										<div className="clearfix" />
										<div className="form-group showinfo" data-name="expose">
											<label className="col-md-2 control-label" htmlFor="expose">api</label>
											<div className="col-md-10">
												<div className="checkbox">
													
													<label htmlFor="expose-0">
														<input type="checkbox" name="expose" id="expose-0" onChange={this.handleChange} value={this.state.form.expose === 'true' || this.state.form.expose === true ? 'false' : 'true'} checked={this.state.form.expose === 'true' || this.state.form.expose === true ? 'checked' : ''} />
														{this.state.form.expose === 'true' || this.state.form.expose === true ? <a target="_blank" href={'/'+ this.props.sockets.proxy + '/program/' + _this.state.form.slug + '/?' + buildProgramQuery(this.state.form.arguments[0], this.state.form).line} >/{this.props.sockets.proxy}/program/{_this.state.form.slug}/?{buildProgramQuery(this.state.form.arguments[0], this.state.form).line}</a> : 'This program is not exposed via API.'}
													</label>
												</div>
											</div>
										</div>
									
										<div className="clearfix" />
										<div className="form-group showinfo" data-name="stderror">
											<label className="col-md-2 control-label" htmlFor="stderror">error output</label>
											<div className="col-md-10">
												{stderror}
											</div>
										</div> 
									</div>
										
									<div className="clearfix" />
									<div ref="show" className={this.state.form.type !== 'internal' ? 'fatalbert' : (_.isObject(this.state.programMap[this.state.form._id]) && this.state.programMap[this.state.form._id].type !== 'internal') ? 'fatalbert' : 'casper'}>
									
										<div className="clearfix" />
										<div className="form-group no-gutter"  style={{margin:'15px 0 30px'}}>
											<div className=" col-sm-2">
											</div>
											<div className=" col-sm-8">
												<Button className={"btninfo left square"} disabled={this.state.forming} ref="manage"  durationSuccess={this.state.durationSuccess} durationError={this.state.durationError} >{this.state.form._id ? 'Update Program' : 'Add Program'}</Button> 
												<input type="hidden" name="_id" value={this.state.form._id} /> 
												<input type="hidden" name="slug" value={this.state.form.slug} /> 
											</div>
											
											<div className=" col-sm-2">
												{!this.state.forming && this.state.form._id ? <Button className="btndanger right square small" onClick={this.deleteProgram} ref="remove"  durationSuccess={this.state.durationSuccess} durationError={this.state.durationError} ><span className="glyphicon glyphicon-trash" /></Button> : ''}
											</div>
											
											
										</div>
											
									</div>
									
								</div>
								
							</fieldset>
						</form>
					</div>
					<div className=" col-sm-4 col-md-4" >
						 {this.state.currentField ? <Alert style="info" html={this.state.formInfo[this.state.currentField]} dismiss={_this.dismissInfoAlert} /> : ''}
					</div>
				</div>
					
			</div>
			
		);
			
			
	},
	
	
	
});

export default Program;
