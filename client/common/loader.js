import React from 'react';

import classNames from 'classnames';

export default React.createClass({
	displayName: 'Spinner',
	propTypes: {
		className: React.PropTypes.string,
		size: React.PropTypes.oneOf(['sm', 'md', 'lg']),
		type: React.PropTypes.oneOf(['default', 'primary', 'inverted'])
	},
	getDefaultProps: function getDefaultProps() {
		return {
			type: 'primary',
			size: 'md'
		};
	},
	render: function render() {
		var componentClass = classNames('Spinner', 'Spinner--' + this.props.type, 'Spinner--' + this.props.size, this.props.className);

		return React.createElement(
			'div',
			{ className: componentClass },
			React.createElement('span', { className: 'Spinner_dot Spinner_dot--first' }),
			React.createElement('span', { className: 'Spinner_dot Spinner_dot--second' }),
			React.createElement('span', { className: 'Spinner_dot Spinner_dot--third' })
		);
	}
});

var Loader = React.createClass({
	render: function() {		
		return (<div className="sk-cube-grid">
			<div className="sk-cube sk-cube1"></div>
			<div className="sk-cube sk-cube2"></div>
			<div className="sk-cube sk-cube3"></div>
			<div className="sk-cube sk-cube4"></div>
			<div className="sk-cube sk-cube5"></div>
			<div className="sk-cube sk-cube6"></div>
			<div className="sk-cube sk-cube7"></div>
			<div className="sk-cube sk-cube8"></div>
			<div className="sk-cube sk-cube9"></div>
		</div>);
	}	
});

export { Loader };
