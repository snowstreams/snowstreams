import Debug from 'debug'; 
import ProgressButton from 'react-progress-button';
import _ from 'lodash';
import React from 'react';

let debug = Debug('snowstreams:client:common:util');

export function buildProgramQuery(program, values) {
	
	var ret = {
		argument: program.anchor,
	}
	
	var line = program.argument.split(' ')
				.filter((v) => {
					return v[0] === '%';
				})
				.map((v) => {
					var str =  v.substring(1, v.length-1).toLowerCase();
					debug(values, program._id + '_' + v, values[program._id + '_' + v]);
					ret[str] = values[program._id + '_' + v]
					return str + '=' + values[program._id + '_' + v];
				});
	
	return {
		line: 'argument=' + program.anchor + '&' + line.join('&'),
		query: ret
	}
};

export const Button = ProgressButton;

export function deleteDocument(context, e, page, onclick) {
	e.preventDefault();
	var _this = context;
	if(!_.isFunction(onclick)) onclick = function(){};
	
	var html = (<div><p>Are you sure you want to delete the {page + ' ' + _this.state.form.name}?</p><p>  This action is permanent.</p><p><a className="btn btn-danger" onClick={_this.reallyDelete} >Yes</a> &nbsp;&nbsp;&nbsp;&nbsp; <a className="btn btn-default" onClick={()=>{onclick();_this.dismissAlert();}}>Cancel</a></p></div>); 
	_this.setState({
		newalert: {
			style: 'danger',
			component: html,
			show: true
		}
	});
}
