import 'jquery';

import React from 'react';
import Base from './base.js';
import Home from './pages/home';
import Streams from './pages/stream';
import Programs from './pages/program';
import Sources from './pages/source';
import Channels from './pages/channel';
import Signin from './pages/signin';
import Logs from './pages/logs';
import { NotFoundRoute, run, Route, DefaultRoute, RouteHandler, Redirect, HistoryLocation }  from 'react-router';
import _ from 'lodash';

import gd from 'debug';
window.myDebug = gd;

import debugging from 'debug';
let	debug = debugging('snowstreams:client:app');


var routes = (
  <Route handler={Base}>
	<DefaultRoute handler={Home}/>
	<Route name="home" path="/client" handler={Home}/>
	
	<Redirect from="/client/sources" to="/client/sources/new" />
	<Route name="sources" path="/client/sources/new" handler={Sources}>
		<Route name="manage-source" path="/client/sources/:slug" handler={Sources} />
		<NotFoundRoute handler={Sources}/>
    </Route>
    <Redirect from="/client/programs" to="/client/programs/new" />
	<Route name="programs" path="/client/programs/new" handler={Programs}>
		<Route name="manage-program" path="/client/programs/:slug" handler={Programs} />
		<NotFoundRoute handler={Programs}/>
    </Route>
    <Redirect from="/client/streams" to="/client/streams/new" />
	<Route name="streams" path="/client/streams/new" handler={Streams}>
		<Route name="manage-stream" path="/client/streams/:slug" handler={Streams} />
		<NotFoundRoute handler={Streams}/>
    </Route>
    <Redirect from="/client/channels" to="/client/channels/new" />
	<Route name="channels" path="/client/channels/new" handler={Channels}>
		<Route name="manage-channel" path="/client/channels/:slug" handler={Channels} />
		<NotFoundRoute handler={Channels}/>
    </Route> 
	<Route name="logs" path="/client/logs" handler={Logs}/>
	<Route name="signin" path="/client/signin" handler={Signin}/>
	<NotFoundRoute handler={Home}/>
  </Route>
); 


run(routes, HistoryLocation, function (Handler, state) {
  var params = state.params;
  debug(state)
  React.render(<Handler { ...state } />, document.getElementById('snowstreams'));
});

$.fn.serializeFormJSON = function () {

	var o = {};
	var a = this.serializeArray();
	$.each(a, function () {
		if (o[this.name]) {
			if (!o[this.name].push) {
				o[this.name] = [o[this.name]];
			}
			o[this.name].push(this.value || '');
		} else {
			o[this.name] = this.value || '';
		}
	});
	return o;
};
