require('dotenv').load();

var fs = require('fs');
var	path = require('path');
var _ = require('lodash');
var async = require('async');
var EventEmitter = require("events").EventEmitter;
var util = require("util");
var debug = require('debug')('snowstreams:main');

var moduleRoot = (function(_rootPath) {
	var parts = _rootPath.split(path.sep);
	parts.pop(); //get rid of /node_modules from the end of the path
	return parts.join(path.sep);
})(module.paths[0]);


/**
 * snowstreams Class
 *
 * @api public
 */

var snowstreams = function() {
	this._options = {
		'name': 'snowstreams',
		'module root': moduleRoot,
		port: 10000,
		udpPort: 10000,
		mongo: 'mongodb://localhost/snowstreams',
		cookieSecret: 'ni0934piupouH08&OPB087GOYvbO^07860^f9T&CF9TO'
	};
	
	this.defaults = {
		port: 10000,
		udpPort: 10000,
		mongo: 'mongodb://localhost/snowstreams',
		cookieSecret: 'ni0934piupouH08&OPB087GOYvbO^07860^f9T&CF9TO'
	}
	
	this.emitters = [];
	this._stop = {}
	this.streams = {};
	this.sources = {};
	this._assets = {};
	this._channels = {};
	this.servers = {
		keystone: {
			sockets: {},
			proxies: [],
			name: 'keystone',
			host: '@',
			port: this._options.port,
		}
	};
	this.routes = [];
	
	// set a running server ID
	this.nextServerId = 12098;
	
	EventEmitter.call(this);
	
	this.emitterReg('source error');
	
	this.authSocket = {emit:function(){}}
	this.openSocket = {emit:function(){}}
};

util.inherits(snowstreams, EventEmitter);

// get an asset
snowstreams.prototype.asset = function(assetId) {
	debug('get asset');
	if(this._assets[assetId]) {
		return this._assets[assetId];
	}
	var ret = false;
	if(this.streams[assetId]) {
		ret = this.streams[assetId];
	}
	if(this.sources[assetId]) {
		if(ret) {
			ret = [ret, this.sources[assetId]];
		} else {
			ret = this.sources[assetId];
		}
	}
	if(!ret) {
		debug('Asset not found');
		return false;
	} else {
		return ret;
	}
}

// get a channel
snowstreams.prototype.channel = function(channelId) {
	debug('get channel', _.isObject(this._channels[channelId]));
	if(!this._channels[channelId]) {
		debug('Channel not found');
		return false;
	} else {
		return this._channels[channelId];
	}
}


// register a channel
snowstreams.prototype.registerChannel = function(channel, callback) {
	debug('register channel', channel.id);
	
	var cb = _.isFunction(callback) ? callback : ()=>{};
	
	
	if(this._channels[channel.id]) {
		console.log('Channel exists');
		cb('Channel Exists', channel);
		return this._channels[channel.id];
	} else {
		cb('Channel Exists', channel);
		return this._channels[channel.id] = channel;
	}
}

// delete a channel
snowstreams.prototype.removeChannel = function(channel, callback) {
	debug('remove channel', channel.id);
	
	var cb = _.isFunction(callback) ? callback : ()=>{};
	
	
	if(this._channels[channel.id]) {
		console.log('Channel exists');
		var deleteMe = () => {
			delete this._channels[channel.id];
		}
		return this._channels[channel.id].remove((err) => {
			debug(err, 'Channel deleted');
			deleteMe();
			this.notify('status', ss.Listen.status());
			return true;
		});
		
		cb(null, 'Channel deleted');
		
	} else {
		debug('no channel found');
		cb('No Channel found');
		return false;
	}
}

// register an asset
snowstreams.prototype.registerAsset = function(asset, callback) {
	debug('register asset', asset.id);
	
	var cb = _.isFunction(callback) ? callback : ()=>{};
	
	if(this._assets[asset.id]) {
		debug('Asset exists');
		cb('Asset Exists', asset);
		return this._assets[asset.id];
	} else {
		this[asset.asset + 's'][asset.name] = asset;
		debug('Asset set');
		cb(null, asset);
		return this._assets[asset.id] = asset;
	}
}

// delete an asset
snowstreams.prototype.removeAsset = function(asset, callback) {
	debug('remove asset', asset.id);
	
	var cb = _.isFunction(callback) ? callback : ()=>{};
	
	var ass = this._assets[asset.id];
	
	if(ass) {
		debug('Asset exists');
		// if this is a part of a channel and killChannel is set then remove the whole channel
		if(ass.options.killChannel && ass.Channel) {
			ass.options.killChannel = false;
			return ss.removeChannel(ass.Channel, callback);
		}
		var deleteMe = () => {
			delete this._assets[asset.id];
			delete this[asset.asset + 's'][asset.name];
		}
		ass.remove(null, (err) => {
			debug(err, 'Asset deleted');
			deleteMe();
			this.notify('status', ss.Listen.status());
			return true;
		});
		
		cb(null, 'Asset deleted');
		
	} else {
		debug('no asset found');
		cb('no asset found')
		return false;
	}
}

snowstreams.prototype.emitterReg = function(emitter) {
	if (!_.findWhere(this.emitters, emitter)) {
		this.emitters.push(emitter);
		this.on(emitter, function(data) {
			//ss.log(emitter, data);
		});
	}
}

snowstreams.prototype.notify = function(emitter, data) {
	process.nextTick(function() {
		//debug('notify');
		this.emit(emitter, data);
		this.openSocket.emit(emitter, data);
	}.bind(this));
}

snowstreams.prototype.talk = function(emitter, data, all) {
	process.nextTick(function() {
		//debug('talk');
		this.emit(emitter, data);
		this.authSocket.emit(emitter, data);
	}.bind(this));
}

_.extend(snowstreams.prototype, require('./lib/core/options')());
// standalone server
_.extend(snowstreams.prototype, require('./lib/core/createServer')());
// sockets
_.extend(snowstreams.prototype, require('./lib/core/socket')());
// keystone server
_.extend(snowstreams.prototype, require('./lib/core/server')());
/**
 * returns all .js modules (recursively) in the path specified, relative
 * to the module root (where the snowstreams project is being consumed from).
 *
 * ####Example:
 *
 *     var models = ss.import('models');
 *
 * @param {String} dirname
 * @api public
 */

snowstreams.prototype.import = function(dirname) {
	
	var _this = this;
	var initialPath = path.join(this.get('module root'), dirname);
	var doImport = function(fromPath) {
		
		var imported = {};
		
		fs.readdirSync(fromPath).forEach(function(name) {
			
			var fsPath = path.join(fromPath, name),
			info = fs.statSync(fsPath);
			
			// recur
			if (info.isDirectory()) {
				imported[name] = doImport(fsPath);
			} else {
				// only import files that we can `require`
				var ext = path.extname(name);
				var base = path.basename(name, ext);
				if (require.extensions[ext]) {
					debug(fsPath)
					imported[base] = require(fsPath)(_this);
				}
			}
			
		});
		
		return imported;
	};
	
	return doImport(initialPath);
};

/**
 * The exports object is an instance of snowstreams.
 *
 * @api public
 */

var ss = module.exports = exports = new snowstreams();

/**
 * Expose yourself
 * */
ss.snowstreams = snowstreams;

/**
 * Asset management
 * 
 * -- var myAsset = new ss.Asset('myAsset', options);
 * 
 * */
ss.Asset = require('./lib/asset.js')(ss);

/**
 * Channel management
 * 
 * -- var myChannel = new ss.Channel('myChannel', options);
 * 
 * */
ss.Channel = require('./lib/channel.js')(ss);

//source class
var source = function() {};
_.extend(source.prototype, ss.import('lib/source'));
ss.source = new source();

//stream class
var streamers = function() {};
_.extend(streamers.prototype, ss.import('lib/stream'));
ss.stream = new streamers();

// listener functions for sockets and routes
var socketListeners = function() {};
_.extend(socketListeners.prototype, snowstreams.prototype.listeners.call(ss));
ss.Listen = new socketListeners();

ss.version = require('./package.json').version;

