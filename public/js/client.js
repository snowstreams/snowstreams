(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
var React = require('react');
var Base = require('./base.js');
var Home = require('./pages/home');
var Streams = require('./pages/stream');
var Programs = require('./pages/program');
var Sources = require('./pages/source');
var Channels = require('./pages/channel');
var Logs = require('./pages/logs');
var Router = require('react-router');
var Route = Router.Route;
var DefaultRoute = Router.DefaultRoute;
var RouteHandler = Router.RouteHandler;
var Redirect = Router.Redirect;
var Sockets = require('./lib/sockets'); 
var openSocket;

var	debug = require('debug')('snowstreams:client:app');
window.myDebug = require("debug");

var _ = require('lodash');

var routes = (
  React.createElement(Route, {handler: Base}, 
	React.createElement(DefaultRoute, {handler: Home}), 
	React.createElement(Route, {name: "home", path: "/client", handler: Home}), 
	
	React.createElement(Redirect, {from: "/client/sources", to: "/client/sources/new"}), 
	React.createElement(Route, {name: "sources", path: "/client/sources/new", handler: Sources}, 
		React.createElement(Route, {name: "manage-source", path: "/client/sources/:slug", handler: Sources}), 
		React.createElement(Router.NotFoundRoute, {handler: Sources})
    ), 
    React.createElement(Redirect, {from: "/client/programs", to: "/client/programs/new"}), 
	React.createElement(Route, {name: "programs", path: "/client/programs/new", handler: Programs}, 
		React.createElement(Route, {name: "manage-program", path: "/client/programs/:slug", handler: Programs}), 
		React.createElement(Router.NotFoundRoute, {handler: Programs})
    ), 
    React.createElement(Redirect, {from: "/client/streams", to: "/client/streams/new"}), 
	React.createElement(Route, {name: "streams", path: "/client/streams/new", handler: Streams}, 
		React.createElement(Route, {name: "manage-stream", path: "/client/streams/:slug", handler: Streams}), 
		React.createElement(Router.NotFoundRoute, {handler: Streams})
    ), 
    React.createElement(Redirect, {from: "/client/channels", to: "/client/channels/new"}), 
	React.createElement(Route, {name: "channels", path: "/client/channels/new", handler: Channels}, 
		React.createElement(Route, {name: "manage-channel", path: "/client/channels/:slug", handler: Channels}), 
		React.createElement(Router.NotFoundRoute, {handler: Channels})
    ), 
	React.createElement(Route, {name: "logs", path: "/client/logs", handler: Logs}), 
	React.createElement(Router.NotFoundRoute, {handler: Home})
  )
); 

var socket = Sockets;

Router.run(routes, Router.HistoryLocation, function (Handler, state) {
  var params = state.params;
  debug(state.params)
  React.render(React.createElement(Handler, {sockets: socket, params: params}), document.getElementById('snowstreams'));
});

$.fn.serializeFormJSON = function () {

	var o = {};
	var a = this.serializeArray();
	$.each(a, function () {
		if (o[this.name]) {
			if (!o[this.name].push) {
				o[this.name] = [o[this.name]];
			}
			o[this.name].push(this.value || '');
		} else {
			o[this.name] = this.value || '';
		}
	});
	return o;
};

},{"./base.js":2,"./lib/sockets":6,"./pages/channel":7,"./pages/home":9,"./pages/logs":10,"./pages/program":11,"./pages/source":12,"./pages/stream":13,"debug":17,"lodash":undefined,"react":undefined,"react-router":undefined}],2:[function(require,module,exports){
var React = require('react');
var Router = require('react-router');
var Route = Router.Route;
var DefaultRoute = Router.DefaultRoute;
var RouteHandler = Router.RouteHandler;
var Link = Router.Link;
var Console = require('react-console');
var _ = require('lodash');

var async = require('async');
var debugging = require('debug');
var	debug = debugging('snowstreams:client:base');

var ReactBootstrap = require('react-bootstrap');
var ReactRouterBootstrap = require('react-router-bootstrap');

var Navbar = ReactBootstrap.Navbar;
var CollapsibleNav = ReactBootstrap.CollapsibleNav;
var Nav = ReactBootstrap.Nav;
//var NavItemLink = ReactBootstrap.NavItem; 
var MenuItem = ReactBootstrap.MenuItem;
var DropdownButton = ReactBootstrap.DropdownButton;
 
var MenuItemLink = ReactRouterBootstrap.MenuItemLink;
var NavItemLink = ReactRouterBootstrap.NavItemLink;
var ButtonLink = ReactRouterBootstrap.ButtonLink;
var ListGroupItemLink = ReactRouterBootstrap.ListGroupItemLink;  

var App = React.createClass({displayName: "App",
	contextTypes: {
		router: React.PropTypes.func
	},
	getInitialState: function() {
		
		//debugging.log = this.logger;
		var _this = this;
		
		this.props.sockets.authSocket.on('Source-log', this.logger);
		this.props.sockets.authSocket.on('File-log', this.logger);
		this.props.sockets.authSocket.on('Channel-log', this.logger);
		this.props.sockets.authSocket.on('Program-log', this.logger);
		this.props.sockets.authSocket.on('ProgramArgument-log', this.logger);
		this.props.sockets.authSocket.on('Stream-log', this.logger);
		this.props.sockets.authSocket.on('commander', this.logger);
		
		// cache log entries if they come in too fast
		this._cargo = async.cargo(function (logs,callback) {
			var send = {
				error: [],
				doc: [],
				message: []
			}
			for(var i=0; i<logs.length; i++){
				send.alive = logs[i].alive;
				if(logs[i].error !== '') send.error.push(logs[i].error);
				if(logs[i].doc !== '') send.doc.push(logs[i].doc);
				if(logs[i].message !== '') send.message.push(logs[i].message);
			}
			_this.setState({log : send});
			debug('sent ', send);
			callback();
		}, 25);
		return {
			status: 'status', 
			sockets: {
				auth: false,
				open: false,
			},
			log: {
				alive: false
			},
		}
	},
	componentWillReceiveProps: function() {
		return false;
	},
	componentDidMount: function() {
		_this = this;
		var Sockets = this.props.sockets;
		debug('emit status');
		Sockets.init(function() {
			openSocket = Sockets.openSocket;
			openSocket.emit('status');
			openSocket.on('status',function(data) {
				debug('status',data);
				_this.setState({
					status: data.status,
					sockets: {
						auth: Sockets.authSocket,
						open: Sockets.openSocket
					}
				});
			});
		});
			
	},
	render: function() {    
		var _this = this;
		
		var navbarInstance = (
		   React.createElement(Navbar, {brand: React.createElement("a", {href: "/client/home", onClick: this.go, "data-go": "home"}, React.createElement("span", {className: "glyphicon glyphicon-home", "aria-hidden": "true"})), toggleNavKey: 0}, 
		   
			   React.createElement(Nav, {navbar: true}, 
				 
				React.createElement(NavItemLink, {eventKey: 3, to: "streams"}, "Streams"), 
				React.createElement(NavItemLink, {eventKey: 2, to: "sources"}, "Sources"), 
				
				React.createElement(NavItemLink, {eventKey: 4, to: "channels"}, "Channels")
			  ), 
			  React.createElement(Nav, {navbar: true, right: true}, 
				React.createElement(NavItemLink, {eventKey: 5, to: "programs"}, "Programs"), 
				React.createElement(NavItemLink, {eventKey: 5, to: "logs"}, "Logs"), 
				React.createElement(DropdownButton, {eventKey: 6, title: React.createElement("span", {className: "glyphicon glyphicon-globe"})}, 				  
				  React.createElement(MenuItem, {eventKey: 7, href: "/testbed"}, "Testbed"), 
				  React.createElement(MenuItem, {eventKey: 8, href: "/keystone"}, "Keystone UI"), 
				  React.createElement(MenuItem, {divider: true}), 
				  React.createElement(MenuItem, {eventKey: 9, href: "/keystone/signout"}, "Logout")
				)
			  )
		   
		  )
		);
		
		if(this.state.sockets.auth) {
			var show = React.createElement(RouteHandler, React.__spread({logger: this.logger, openConsole: this.openConsole, toggleConsole: this.toggleConsole},  this.props))
		} else {
			var show = React.createElement("span", null, "Waiting for sockets")
		}
		
		return ( 
			React.createElement("div", {className: "body"}, 
				React.createElement(Console, {command: _this.command, toggle: this.toggleConsole, alive: this.state.log.alive, log: this.state.log}), 
				navbarInstance, 	
				React.createElement("div", {className: "row1  shakeme "}, 
					
					React.createElement("div", {className: " col-sm-12 col-md-10 page-box no-padding"}, 
						show
					), 
					React.createElement("div", {className: "col-md-2 no-padding "}, 
						React.createElement("div", {className: "header-home"}, "Status"), 
						React.createElement("div", {className: "col3 col-sm-12"}, 
							React.createElement("div", {dangerouslySetInnerHTML: {__html:this.state.status}}), 
							React.createElement("div", {className: "clearfix"})
						)
					)
				)
				
			)
		);
	},
	go(e) {
		e.preventDefault();
		debug(e)
		if(e.currentTarget) {
			var target = e.currentTarget;
		} else {
			var target = e.target;
		}
		this.context.router.transitionTo(target.dataset.go);
	}, 
	logger(data) {
		var _this = this; 
		var log = { alive: this.state.log.alive };
		log.message = '';
		log.doc = '';
		log.error = '';
		if(_.isObject(data)) {
			if(data.message) {
				log.message = data.message
			}
			if(data.doc) {
				log.doc = data.doc
			}
			if(data.data) {
				log.doc = data.data
			}
			if(data.error) {
				log.error = data.error
			}
			
		} else if(data) {
			log.message = data
			
		}
		_this._cargo.push(log);
	},
	toggleConsole(e) {
		e.preventDefault();
		this.setState({ log: {alive: !this.state.log.alive} });
	},
	openConsole() {
		this.setState({ log: {alive: true} });
	},
	command(value) {
		
		var doc, find;
		var aliases = {
			F : 'File',
			C: 'Channel',
			P: 'Program',
			A: 'ProgramArgument',
			I: 'Source',
			O: 'Stream',
			c: 'Channel',
			f: 'File',
			p: 'Program',
			i: 'Source',
			o: 'Stream',
			h: 'help',
		}
		
		var args;
		
		// search for doc and split there first
		if(value.search('doc') > -1) {
			var first = value.split('::doc::');
			args = first[0].split('::');
			doc = first[1].split('::')
		} else if(value.search('search') > -1) {
			var first = value.split('::search::');
			args = first[0].split('::');
			find = first[1].split('::')
		} else {
			args = value.split('::');
		}
		debug(args)
		if(!_.isArray(args)) {
			var main = value.length === 1 ? aliases[value] : value || 'help';
			args = [main,main];
		} else {
			var main = args[0].length === 1 ? aliases[args[0]] : args[0];
			if(!args[1]) args[1] = main;
		}		
		debug(args)
		var opts = {
			iden: 'commander' ,
			emit: 'help'
		}
		
		// for List args[0] = list and emitter = args[1]
		if(main && main[0] === main[0].toUpperCase()) {
			opts.list = main;
			opts.emit = args[1];
		} else {
			opts.manage = args[1].length === 1 ? aliases[args[1]] : args[1];
			if(main) opts.emit = main;
		}
		
		var arg = args.slice(2);
		_.each(arg, function(v) {
			var vv = v.split(':');
			if(_.isArray(vv)) {
				opts[vv[0]] = vv[1];
			}
		});
		if(_.isArray(doc)) {
			var sends = {}
			_.each(doc, function(v) {
				var vv = v.split(':');
				if(_.isArray(vv)) {
					sends[vv[0]] = vv[1];
				}
			});
			opts.doc = sends;
		}
		if(_.isArray(find)) {
			var sends = {}
			_.each(find, function(v) {
				var vv = v.split(':');
				if(_.isArray(vv)) {
					sends[vv[0]] = vv[1];
				}
			});
			opts.find = sends;
		}
		
		debug('emit', opts.emit, 'data', opts);
		this.logger({ message: 'request sent', data: opts});
		if(opts.list === 'wan') {
			this.props.sockets.auth.io.emit(opts.emit, opts);
		} else {
			this.props.sockets.auth.emit(opts.emit, opts);
		}
	}
});

module.exports = App;

},{"async":14,"debug":17,"lodash":undefined,"react":undefined,"react-bootstrap":undefined,"react-console":136,"react-router":undefined,"react-router-bootstrap":150}],3:[function(require,module,exports){
var React = require('react');
var Link = require('react-router').Link;
var _ = require('lodash');
var ReactBootstrap = require('react-bootstrap');
var Alerter = ReactBootstrap.Alert;
var Button = ReactBootstrap.Button;

var	debug = require('debug')('snowstreams:client:common:alert');

var Alert = React.createClass({displayName: "Alert",
	contextTypes: {
		router: React.PropTypes.func
	},
	childContextTypes: {
		router: React.PropTypes.func
	},
	getInitialState: function() {
		return {
			alertVisible: true
		};
	},
	getDefaultProps: function() {
		return { 
			style: 'danger',
			html: 'error with the alert.  This is placeholder text.',
			dismiss: false,
			data: false
		}
	},
	componentDidMount: function() {
		//this.getStreams();
	},
	componentWillMount: function() {
		
	},
	componentWillReceiveProps: function(props) {
		//debug('receive props',props)
		/*
			*/	
		return false;
	},
	renderError(data) {
		try {
			var myerror = JSON.stringify(data.error, null, 4);
			var myrequest = JSON.stringify(data.received, null, 4);
		} catch(e) {
			var myerror = 'I encountered an error. Please check the console for the error object';
			var myrequest = ''; 
			debug(data);
		}
		var senderror = (React.createElement("div", null, 
			React.createElement("div", null, "ERROR"), 
			React.createElement("pre", null, myerror), 
			React.createElement("div", null, "REQUEST"), 
			React.createElement("pre", null, myrequest)
		));
		return senderror;
	},
	renderSuccess(data) {
		return data;
	},
	renderHTML() {
		if(this.props.data) {
			if(this.props.data.error) {
				return this.renderError(this.props.data);
			}
			return this.renderSuccess(this.props.data);
		} else if(this.props.component) {
			return this.props.component;
		} else {
			return React.createElement("div", {dangerouslySetInnerHTML: {__html:this.props.html}})
		}
	},
	render() {
		if (this.state.alertVisible) {
			return (
				React.createElement(Alerter, {bsStyle: this.props.style, onDismiss: this.handleAlertDismiss}, 
					this.renderHTML(), 
					React.createElement("div", {className: "clearfix"})
				)
			);
		}

		return (
			React.createElement("span", null)
		);
	},

	handleAlertDismiss() {
		this.setState({alertVisible: false});
		if(_.isFunction(this.props.dismiss)) {
			this.props.dismiss();
		}
	},

	handleAlertShow() {
		this.setState({alertVisible: true});
	}
	
	
	
});

module.exports = Alert;

},{"debug":17,"lodash":undefined,"react":undefined,"react-bootstrap":undefined,"react-router":undefined}],4:[function(require,module,exports){
var React = require('react');

var Loader = React.createClass({displayName: "Loader",
	render: function() {		
		return (React.createElement("div", {className: "sk-cube-grid"}, 
			React.createElement("div", {className: "sk-cube sk-cube1"}), 
			React.createElement("div", {className: "sk-cube sk-cube2"}), 
			React.createElement("div", {className: "sk-cube sk-cube3"}), 
			React.createElement("div", {className: "sk-cube sk-cube4"}), 
			React.createElement("div", {className: "sk-cube sk-cube5"}), 
			React.createElement("div", {className: "sk-cube sk-cube6"}), 
			React.createElement("div", {className: "sk-cube sk-cube7"}), 
			React.createElement("div", {className: "sk-cube sk-cube8"}), 
			React.createElement("div", {className: "sk-cube sk-cube9"})
		));
	}	
});

module.exports = Loader;

},{"react":undefined}],5:[function(require,module,exports){
var path = require('path');
var	_ = require('lodash');
var	debug = require('debug')('snowstreams:client:lib:socketFunctions');
var randomNumber = require('hat');
var React = require('react');


function options() {
	 
	var exports = {};
	
	exports.trapResponse = function(socket, callback) {
		
		var unique = randomNumber();

		var cb = function(data) {
			socket.removeListener(unique, cb);
			callback(data);
		}
		//debug('trap response', unique);
		socket.on(unique, cb);

		return unique;
	}
	exports.list = function(list, options, callback) {
		
		if(_.isFunction(options)) {
			callback = options;
			options = {};
		}
		
		if(!_.isObject(options)) options = {};
		
		if(!_.isFunction(callback)) return;

		this.authSocket.emit('list',{ 
			list: list, 
			exclude: options.exclude,
			include: options.include,
			limit: options.limit,
			skip: options.skip,
			populate: options.populate,
			query: options.query,
			sort: options.sort,
			iden: this.trapResponse(this.authSocket, callback)
		});
	}
	exports.getDocument = function(list, options, callback) {
		
		if(!_.isFunction(callback)) return;

		this.authSocket.emit('get',{ 
			list: list, 
			exclude: options.exclude || '__v',
			find: { slug: options.slug },
			id: options.id || false,
			sort: options.sort,
			include: options.include,
			limit: options.limit,
			skip: options.skip,
			query: options.query,
			populate: options.populate,
			iden: this.trapResponse(this.auth, callback) 
		});
	}
	exports.submitForm = function(context, e, emit, page, callback) {
		var _this = context;
		_this.dismissAlert();
		var form = _this.state.form;
		
		if(!_.isFunction(callback)) callback = function(){};
		
		var myFn = function(data) {
			if(data.error) {
				_this.setState({ 
					newalert: {
						style: 'danger',
						data: data,
						show: true
					}
				});
				callback(data);
			} else {
				debug('form received response', data);
				var msg = form._id ? page + ' updated successfully... reloading' : page + ' added successfully... reloading'
				_this.setState({ 
					newalert: {
						style: 'success',
						html: msg,
						show: true
					},
					form:  _this.defaultForm()
				});
				callback(null, data);
			}
			
		}
		this.authSocket.emit(emit,{
			list: page, 
			action: form._id ? 'update' : 'create',
			doc: form,
			id: form._id || false,
			iden: this.trapResponse(this.authSocket, myFn) 
		});
		
	}
	exports.deleteDocument = function(context, e, page, callback) {
		e.preventDefault();
		var _this = context;
		if(!_.isFunction(callback)) callback = function(){};
		
		var html = (React.createElement("div", null, React.createElement("p", null, "Are you sure you want to delete the ", page + ' ' + _this.state.form.name, "?"), React.createElement("p", null, "  This action is permanent."), React.createElement("p", null, React.createElement("a", {className: "btn btn-danger", onClick: _this.reallyDelete}, "Yes"), "      ", React.createElement("a", {className: "btn btn-default", onClick: _this.dismissAlert}, "Cancel")))); 
		_this.setState({
			newalert: {
				style: 'danger',
				component: html,
				show: true
			}
		});
	}
	exports.reallyDelete = function(context, e, page, callback) {
		debug('delete', context.state.form.name, context.state.form._id)
		var _this = context;
		var sock = this;
		if(!_.isFunction(callback)) callback = function(){};
		
		var myFn = function(data) {
			if(data.error) {
				_this.setState({ 
					newalert: {
						style: 'danger',
						data: data,
						show: true
					}
				});
				callback(data);
			} else {
				debug('form received response', data);
				var msg =  page + ' deleted'
				_this.setState({ 
					newalert: {
						style: 'warning',
						html: msg,
						show: true
					},
					form:  _this.defaultForm()
				});
				callback(null, data);
			} 
		}
		sock.authSocket.emit('remove',{ 
			list: page, 
			id: _this.state.form._id,
			iden: sock.trapResponse(sock.auth, myFn) 
		});
	}
	
	exports.manager = function manager(context, options, callback) {
		debug('manager', options)
		
		var _this = context;
		var sock = this;
		if(!_.isFunction(callback)) callback = function(){};
		
		var myFn = function(data) {
			if(data.error) {
				_this.setState({ 
					newalert: {
						style: 'danger',
						data: data,
						show: true
					}
				});
				callback(data);
			} else {
				debug('manager received response', data);
				_this.props.logger(data);
				callback(null, data);
			} 
		}
		options.iden = 'commander';
		
		sock.authSocket.emit('manager',options);
		
	}
	
	return exports;
	
}

module.exports = options;

},{"debug":17,"hat":20,"lodash":undefined,"path":15,"react":undefined}],6:[function(require,module,exports){
var _ = require('lodash');
var	debug = require('debug')('snowstreams:client:lib:sockets');
var port = window.httpPort;

var Sockets = function() {
	
	// connected
	this.connected = {
		auth: false,
		open: false
	}
	this.authSocket = io('//@:' + port + '/lists');
	this.auth = this.authSocket;
	this.openSocket = io('//@:' + port + '/snowstreams');
	this.open = this.openSocket;
	debug('new sockets', '//@:' + port + '/lists');
	
}
Sockets.prototype.init = function(callback) {
	this.authSocket.on('connect',function(data) {
		updateConsole('connected', 'lists');
		this.connected.auth = true;
		
	});
	this.authSocket.on('connect-error',function(err) {
		updateConsole('connect-error',err)
	});
	this.authSocket.on('error',function(err) {
		updateConsole('error',err)
	});
	this.openSocket.on('connect',function(data) {
		updateConsole('connected','snowstreams');
		this.connected.open = true;
		callback(); 
	});
	this.openSocket.on('connect-error',function(err) {
		updateConsole('connect-error',err)
	});
	this.openSocket.on('error',function(err) {
		updateConsole('error',err)
	});
	function updateConsole(a,b) {
		debug(a,b); 
	}
	
	
}

_.extend(Sockets.prototype, require('./socketFunctions')());


var sockets = module.exports = new Sockets();

},{"./socketFunctions":5,"debug":17,"lodash":undefined}],7:[function(require,module,exports){
var React = require('react');
var _ = require('lodash');
var Select = require('react-select');
var Link = require('react-router').Link;
var Alert = require('../common/alert.js');
var ReactBootstrap = require('react-bootstrap');
var ListGroupItem = ReactBootstrap.ListGroupItem;
var Loader = require('../common/loader');
var ReactRouterBootstrap = require('react-router-bootstrap');
var ListGroupItemLink = ReactRouterBootstrap.ListGroupItemLink;
var ListGroup = ReactBootstrap.ListGroup;
var moment = require('moment');
var Flow = require('./flow/flow');

var	debug = require('debug')('snowstreams:client:pages:channel');

var Channel = React.createClass({displayName: "Channel",
	contextTypes: {
		router: React.PropTypes.func
	},
	getInitialState() {
		obj = { 
			ready: false,
			channels: [],
			channelMap: {},
			streams: [],
			streamMap: {},
			active: 'program',
			sources: [],
			sourceMap: {},
			programs: [],
			programMap: {},
			programArgumentMap: {},
			newalert: {},
			form: this.defaultForm(),
			currentField: false,
			formInfo: {
				name: ' <p>You will reference this channel by name everywhere.</p> <p> The name should be unique among your channels and should be easily identifiable.</p> ',
				input: '<p>Select the sources to use for inputs.</p>  <p>  You can override variables after your selection</p> ',
				output: ' <p>Select a stream to use as our output.</p>  <p>  You can override variables after your selection</p> ',
				loop: ' <p>Should we run this channel non-stop?</p> ',
				expose: ' <p>Create an API route to manage this channel.  </p><p>Available commands are stop, start, restart and status.</p><p>Status will give you program information.  Currently that is limited to the filename, codec information if available, start time and expected stop time </p>',
			},	
		}
		return obj;
	},
	defaultForm() {
		return {
			name: '',
			program: '',
			type: '',
			sources: [],
			stream: false,
			
		}
	},
	componentDidMount() {
		_this = this;
		this.getChannels();
		this.checkProps(this.props);
		$(document).on('focus','#channeldiv .showinfo', function(e) {
			_this.showInfo(e);
		});
	},
	checkProps(props) {
		var _this = this;
		if(props.params.slug === 'new') {
			this.setState({ 
				form: _this.defaultForm(),
				newalert: { show: false },
				currentField: false
			});
			return;
		} else if(props.params.slug) {
			this.getChannel(props.params.slug);
			return;
		}
	},
	componentWillReceiveProps(nextProps) {
		var sameQuery = this.props.params.slug === nextProps.params.slug;
		if(!sameQuery && this.props.params.slug !== '') {
			this.checkProps(nextProps);
		}
		return;
	},
	componentDidUpdate: function() {
		
	},
	logChange: function (val) {
		debug("Selected: " + val);
	},
	getChannels() {
		var _this = this;
		var myFn = function(data) {
			var send = _.isArray(data.data.channels) ? data.data.channels : [];
			var channelMap = {};
			_.each(send, function(v) {
				channelMap[v._id] = v;
			});
			_this.setState({channels: send, channelMap: channelMap});
		}
		var myFn2 = function(data) {
			var send2 = _.isArray(data.data.sources) ? data.data.sources : [];
			var sourceMap = {};
			_.each(send2, function(v) {
				sourceMap[v._id] = v;
			});
			_this.setState({sources: send2, sourceMap: sourceMap});
		}
		var myFn3 = function(data) {
			var send3 = _.isArray(data.data.streams) ? data.data.streams : [];
			var streamMap = {};
			_.each(send3, function(v) {
				streamMap[v._id] = v;
			});
			_this.setState({streams: send3, streamMap: streamMap});
		}
		var myFn1 = function(data) {
			var send = _.isArray(data.data.programs) ? data.data.programs : [];
			var programMap = {};
			var programArgumentMap = {};
			_.each(send, function(v) {
				programMap[v._id] = v;
				_.each(v.arguments, function(a) {
					programArgumentMap[a._id] = a;
				});
			});
			
			_this.setState({programs: send, programMap: programMap, programArgumentMap: programArgumentMap});
		}

		this.props.sockets.list('Program', {sort:{name:1}, populate: 'arguments createdBy updatedBy'}, myFn1);
		this.props.sockets.list('Source', {populate: 'files program argument source stream'}, myFn2);
		this.props.sockets.list('Channel', {sort:{name:1}}, myFn);
		this.props.sockets.list('Stream', {populate: 'program argument stderr stream', sort:{name:1}}, myFn3);
	},
	getChannel(slug) {
		var _this = this;
		_this.setState({ form: _this.defaultForm() });
		var myFn = function(data) {
			// We got an error
			if(data.error) {
				_this.setState({ 
					newalert: {
						style: 'danger',
						data: data,
						show: true
					},
					currentField: 'error'
				});
			// we got a result that may contain a list
			} else if(_.isObject(data.data)) {
				
				var send = _.isArray(data.data.channels) ? data.data.channels[0] : _.isObject(data.data.channels) ? data.data.channels : {};
				var name = _.isObject(send.updatedBy.name) ?  send.updatedBy.name.first + '  ' + send.updatedBy.name.last: ' Unknown';
				_this.setState({ 
					form: send,
					currentField: false,
					newalert: {
						style: 'info',
						component: (React.createElement("div", null, "Updated last on ", moment(send.updatedAt).format('MMMM Do YYYY, h:mm:ss a'), " by ", name)),
						show: true
					}
				});
			// something is wrong
			} else {
				_this.setState({ 
					currentField: 'error',
					newalert: {
						style: 'danger',
						html: 'Error. Please check console',
						show: true
					},
				});
			}
		} // end myFn

		var opts = {  
			exclude: '__v',
			slug: slug,
			sort: 'order',
			populate: 'arguments updatedBy',
		};
		this.props.sockets.getDocument('Channel', opts, myFn);
	},
	dismissAlert() {
		this.setState({ 
			newalert: {
				show: false
			}
		});
	},
	dismissInfoAlert() {
		this.setState({ 
			currentField: false
		});
	},			
	submitForm(e) {
		e.preventDefault();
		 var _this = this;
		 var reload = (_this.state.form._id) ? true : false;
		 var go = (_this.state.form._id) ? 'update' : 'create';
		 delete _this.state.form.createdBy;
		 delete _this.state.form.updatedBy;
		 delete _this.state.form.createdAt;
		 delete _this.state.form.updatedAt;
		 this.props.sockets.submitForm(this, e, go, 'Channel', function(err, data) {
			_this.getChannels();
			if(!err) _this.getChannel(data.data.channels.slug);
		 });
	},
	handleChange(val, who) {
		var send = {form: this.state.form}
		var form = send.form;
		if(who){
			var name = who;
			var value = _.isArray(val) ? val : _.isObject(val) ? val.value : '';
			if(who === 'program') {
				send.programPos = parseFloat(val.pos);
				form.argument = ''
			}
		} else {
			var name = val.target.name;
			var value = val.target.value
		}
		form[name] = value;
		this.setState(send);
    },
    showInfo(e) {
		var name = e.currentTarget.dataset.name;
		if(name.search(':') !== -1) {
			name = 'arguments'
		}
		this.setState({currentField: name});
    },
    deleteChannel(e) {
		e.preventDefault();
		this.props.sockets.deleteDocument(this, e, 'Channel'); 
	},
	reallyDelete() {
		var _this = this;
		this.props.sockets.reallyDelete(this, null, 'Channel', function(err, data) {
			if(!err) {
				_this.getChannels();
				if(_this.state.form._id) _this.getChannel(data.data.channels.slug);
			}
		});
	},
	renderOption (option) {
		return React.createElement("span", {style: { color: option.hex}}, option.label);

	},
	renderValue (option) {
		return React.createElement("span", {style: { color: option.hex, fontSize:'small'}}, React.createElement("strong", null, option.label), " ", option.len ? '('+option.len+')' : '')
	},
	render: function() {
		debug(this.state,  this.context.router.getCurrentPathname());	
		var _this = this;
		var typeOptions = [
			{ value: 'file', label: 'file' },
			{ value: 'http', label: 'http' },
			{ value: 'program', label: 'program' },
			{ value: 'udp', label: 'udp' }
		];
		 
		var type = React.createElement(Select, {
			name: "type", 
			options: typeOptions, 
			onChange: function(val, obj) {
				_this.handleChange(obj[0], 'type')
			}, 
			value: this.state.form.type}
		)
		var source = React.createElement("span", null)
		var stream = React.createElement("span", null)
		var colors = {
			program: '#E6771C',
			file: '#6B38EA',
			http: '#46714B',
			udp: '#7E8F8F',
		}
		var sourceOptions = this.state.sources.map(function(v, k) {
			var len = 0
			len = v.type === 'file' && _.isArray(v.files) && v.files.length > 0 ? v.files.length : v.type === 'file' ? 1 : 0;
			return ({ value: v._id, label: v.name , hex: colors[v.type], len: len});
		});
		var source = React.createElement(Select, {
			name: "source", 
			allowCreate: false, 
			options: sourceOptions, 
			multi: true, 
			onChange: function(val, obj) {
				//debug(val)
				_this.handleChange(val.split(','), 'source')
			}, 
			optionRenderer: this.renderOption, 
			valueRenderer: this.renderValue, 
			value: this.state.form.source}
		)
		var streamOptions = this.state.streams.map(function(v, k) {
			return ({ value: v._id, label: v.name });
		});
		var stream = React.createElement(Select, {
			name: "stream", 
			allowCreate: false, 
			options: streamOptions, 
			onChange: function(val, obj) {
				_this.handleChange(obj[0], 'stream')
			}, 
			value: this.state.form.stream}
		)	
		var inputVars = false;
		var outputVars = false;
		
		var listItems = this.state.channels.map(function(v) {
			return (React.createElement(ListGroupItemLink, {bsSize: "small", to: "manage-channel", params: { slug: v.slug}, key: v.slug+'-key'}, v.name));
		});
		if(listItems.length === 0) {
			listItems.push(React.createElement(ListGroupItem, {bsSize: "xlarge", key: 'loading-key'}, React.createElement("div", {className: "clearfix"}, React.createElement("div", {className: "", style: {float:'right'}}, React.createElement(Loader, null)))));
		}
		var list = (
			React.createElement(ListGroup, null, 
				React.createElement(ListGroupItemLink, {to: "manage-channel", params: { slug: 'new'}}, "New Channel"), 
				listItems
			)
		);
		return (
			
			React.createElement("div", {id: "channeldiv", className: " "}, 
				
				React.createElement("div", {className: "header-channels"}, "Channels"), 
				
				React.createElement("div", {className: "clearfix"}), 
				React.createElement("div", {className: "col1 col-md-2 menuitem "}, 
					list
				), 
				React.createElement("div", {className: "col2 col-sm-10 col-md-10 no-padding"}, 
					this.state.newalert.show ? React.createElement(Alert, {style: this.state.newalert.style, html: this.state.newalert.html, data: this.state.newalert.data, component: this.state.newalert.component, dismiss: _this.dismissAlert}) : '', 
					React.createElement("div", {className: "col-sm-8 col-md-8"}, 
						
						React.createElement("form", {className: "form-horizontal", onSubmit: this.submitForm}, 	
							React.createElement("fieldset", null, 			
		 					
								React.createElement("div", {className: "form-group showinfo", "data-name": "name"}, 
									React.createElement("label", {className: "col-md-2 control-label", htmlFor: "name"}, "name"), 
									React.createElement("div", {className: "col-md-10"}, 
										React.createElement("input", {id: "name", name: "name", type: "text", placeholder: "cable", className: "form-control input-md", onChange: this.handleChange, value: this.state.form.name})
									)
								), 
								
								React.createElement("div", {className: "clearfix"}), 
								React.createElement("div", {ref: "show", className: this.state.form.name !== '' ? 'fatalbert' : 'casper'}, 
								
									React.createElement("div", {className: "clearfix"}), 
									React.createElement("div", {className: "form-group showinfo", "data-name": "expose"}, 
										React.createElement("label", {className: "col-md-2 control-label", htmlFor: "expose"}, "api"), 
										React.createElement("div", {className: "col-md-10"}, 
											React.createElement("div", {className: "checkbox"}, 
												
												React.createElement("label", {htmlFor: "expose-0"}, 
													React.createElement("input", {type: "checkbox", name: "expose", id: "expose-0", onChange: this.handleChange, value: this.state.form.expose === 'true' || this.state.form.expose === true ? 'false' : 'true', checked: this.state.form.expose === 'true' || this.state.form.expose === true ? 'checked' : ''}), 
													this.state.form.expose === 'true' || this.state.form.expose === true ? React.createElement("div", null, React.createElement("a", {href: '/proxy/channel/' + _this.state.form.slug + '/start'}, "/proxy/channel/", _this.state.form.slug, "/start"), " ", React.createElement("br", null), " ", React.createElement("a", {href: '/proxy/channel/' + _this.state.form.slug + '/start'}, "/proxy/", _this.state.form.slug)): 'This channel is not exposed via API.'
												)
											)
										)
									), 
									
									React.createElement("div", {className: "clearfix"}), 
									React.createElement("div", {className: "form-group showinfo", "data-name": "input"}, 
										React.createElement("label", {className: "col-md-2 control-label", htmlFor: "source"}, "input(s)"), 
										React.createElement("div", {className: "col-md-10"}, 
											source
										)
									), 
									
									React.createElement("div", {className: "clearfix"}), 
									React.createElement("div", {ref: "show", className: inputVars ? 'fatalbert' : 'casper'}, 
										
										React.createElement("div", {className: "clearfix"}), 
										React.createElement("div", {className: "form-group showinfo", "data-name": "inputvars"}, 
											React.createElement("label", {className: "col-md-2 control-label", htmlFor: "inputvars"}, "input vars"), 
											React.createElement("div", {className: "col-md-10"}, 
												inputVars
											)
										)
										
									), 
									
									React.createElement("div", {className: "clearfix"}), 
									React.createElement("div", {className: "form-group showinfo", "data-name": "loop"}, 
											React.createElement("label", {className: "col-md-2 control-label", htmlFor: "loop"}, "loop"), 
											React.createElement("div", {className: "col-md-10"}, 
												React.createElement("div", {className: "checkbox"}, 
													
													React.createElement("label", {htmlFor: "loop-0"}, 
														React.createElement("input", {type: "checkbox", name: "loop", id: "loop-0", onChange: this.handleChange, value: this.state.form.loop === 'true' || this.state.form.loop === true ? 'false' : 'true', checked: this.state.form.loop === 'true' || this.state.form.loop === true ? 'checked' : ''}), 
														this.state.form.loop === 'true' || this.state.form.loop === true ? 'Input is set to loop continuously' : 'Input(s) are set to play once'
													)
												)
											)
									), 
									
									React.createElement("div", {className: "clearfix"}), 
									React.createElement("div", {className: "form-group showinfo", "data-name": "output"}, 
										React.createElement("label", {className: "col-md-2 control-label", htmlFor: "stream"}, "output"), 
										React.createElement("div", {className: "col-md-10"}, 
											stream
										)
									), 
									
									React.createElement("div", {className: "clearfix"}), 
									React.createElement("div", {ref: "show", className: outputVars ? 'fatalbert' : 'casper'}, 
										
										React.createElement("div", {className: "clearfix"}), 
										React.createElement("div", {className: "form-group showinfo", "data-name": "outputvars"}, 
											React.createElement("label", {className: "col-md-2 control-label", htmlFor: "outputvars"}, "output vars"), 
											React.createElement("div", {className: "col-md-10"}, 
												outputVars
											)
										)
										
									), 
									
									React.createElement("div", {className: "clearfix"}), 
									React.createElement("div", {className: "form-group showinfo", "data-name": "outputvars"}, 
										React.createElement("div", {className: "col-md-12"}, 
											this.state.form.source && this.state.form.stream ? React.createElement(Flow, React.__spread({},  this.state)) : React.createElement("span", null)
										)
									), 
									
									React.createElement("div", {className: "clearfix"}), 
									React.createElement("div", {className: "form-group"}, 
										React.createElement("div", {className: "col-md-offset-2 col-md-5"}, 
											React.createElement("button", {className: "btn btn-info"}, this.state.form._id ? 'Update Channel' : 'Add Channel'), 
											React.createElement("input", {type: "hidden", name: "_id", value: this.state.form._id}), 
											React.createElement("input", {type: "hidden", name: "slug", value: this.state.form.slug})
										), 
										React.createElement("div", {className: " col-md-5"}, 
											this.state.form._id ? React.createElement("button", {className: "btn btn-danger pull-right", onClick: this.deleteChannel}, "Delete") : ''
											
										)
									)	
								)
							)
						)
					), 
					React.createElement("div", {className: " col-sm-4 col-md-4"}, 
						 React.createElement("div", {className: " col-md-4 no-padding"}, 
							!this.state.forming && this.state.form._id ? React.createElement("button", {className: "btn btn-success", onClick: this.deleteSource}, React.createElement("span", {className: "glyphicon glyphicon-play", "aria-hidden": "true"}), " Start") : ''
						), 
						React.createElement("div", {className: " col-md-4 no-padding"}, 
							!this.state.forming && this.state.form._id ? React.createElement("button", {className: "btn btn-danger", onClick: this.deleteSource}, React.createElement("span", {className: "glyphicon glyphicon-stop", "aria-hidden": "true"}), " Stop") : ''
						), 
						React.createElement("div", {className: " col-md-4 no-padding"}, 
							!this.state.forming && this.state.form._id ? React.createElement("button", {className: "btn btn-info", onClick: this.deleteSource}, React.createElement("span", {className: "glyphicon glyphicon-info-sign", "aria-hidden": "true"}), " Status") : ''
						), 
						React.createElement("div", {className: "clearfix"}, React.createElement("br", null)), 
						 this.state.currentField ? React.createElement(Alert, {style: "info", html: this.state.formInfo[this.state.currentField], dismiss: _this.dismissInfoAlert}) : ''
					)
				)
					
			)
			
		);
			
	}
	
});

module.exports = Channel;

},{"../common/alert.js":3,"../common/loader":4,"./flow/flow":8,"debug":17,"lodash":undefined,"moment":undefined,"react":undefined,"react-bootstrap":undefined,"react-router":undefined,"react-router-bootstrap":150,"react-select":undefined}],8:[function(require,module,exports){
var React = require('react');
var Select = require('react-select');
var _ = require('lodash');


var Flow = React.createClass({displayName: "Flow",
	contextTypes: {
		router: React.PropTypes.func
	},
	getInitialState: function() {
		var _this = this;
		var all = {};
		if(this.props.form.source) {
			_.each(this.props.form.source, function(id) {
				var s = _this.props.sourceMap[id];
				all[id] = s;
			});
		}
		return { 
			inputList: [],
			inputs: all, 
			output: _this.props.streamMap[_this.props.form.stream] || {}
		};
	},
	componentDidMount: function() {
	},
	componentWillMount: function() {
	},
	componentWillReceiveProps: function(props) {
		console.log('receive props',props)
		var _this = this;
		var line = [];
		var all = {};
		if(props.form.source) {
			_.each(props.form.source, function(id) {
				var s = props.sourceMap[id];
				all[id] = s;
			});
		}
		
		_this.setState({inputs: all, output: props.streamMap[props.form.stream] || {} });
		return false;
	},
	render: function() {
		console.log('flow state', this.state);
		var _this = this;
		var inputs = [];
		var outputs = [];
		var args = {};
		var hasArg = false;
		if(_this.state.output.program) {
			hasArg = _this.state.output.argument;
			_.each(_this.state.output.program.arguments, function(arg, k) {
				if(_.isObject(_this.props.programArgumentMap[arg])) args[_this.props.programArgumentMap[arg].anchor] = _this.props.programArgumentMap[arg];
			});			
		}
		if(this.state.inputs) {
			_.each(this.state.inputs, function(s, id) {
				if(!_.isObject(s)) {
					
				} else if( s.type === 'file') {
					inputs.push(React.createElement("div", {key: id}, 
						React.createElement("div", {className: "col-xs-12"}, React.createElement("b", null, s.name)), 
						React.createElement("div", {className: "col-xs-12", style: {paddingBottom:10}}, s.type + ' : ' + s.file), 
						React.createElement("div", {className: "col-xs-4 bg-info"}, "isDir"), 
						React.createElement("div", {className: "col-xs-4 bg-info"}, "# of files"), 
						React.createElement("div", {className: "col-xs-4 bg-info"}, "extensions"), 
						React.createElement("div", {className: "clearfix"}), 
						React.createElement("div", {className: "col-xs-4"}, s.directory ? 'YES' : 'NO'), 
						
						React.createElement("div", {className: "col-xs-4"}, _.isArray(s.files) ? s.files.length : 1), 
						React.createElement("div", {className: "col-xs-4"}, s.ext), 
						React.createElement("hr", null)
					));
					if(!hasArg && s.ext) {
						var pattern = s.ext.split(' ');
						_.each(pattern, function(ext) {
							inputs.push(React.createElement("div", {key: ext + id}, React.createElement("div", {className: "clearfix"}, 
								React.createElement("div", {className: "col-sm-2"}, ext), 
								React.createElement("div", {className: "col-sm-10"}, args[ext].command)
								
							), React.createElement("hr", null)));
						});
					} else if(!hasArg) {
						if(_.isObject(s.files[0]) && args[s.files[0].ext.replace('.','')]) {
							var aa = args[s.files[0].ext.replace('.','')];
							inputs.push(React.createElement("div", {key: s.files[0]._id+'45'}, React.createElement("div", {className: "clearfix"}, 
								React.createElement("div", {className: "col-sm-2"}, aa.anchor), 
								React.createElement("div", {className: "col-sm-10"}, aa.command)
								
							), React.createElement("hr", null)));
						} else { 
							_.each(args, function(arg, k) {
								inputs.push(React.createElement("div", {key: arg._id+'45'}, React.createElement("div", {className: "clearfix"}, 
									React.createElement("div", {className: "col-sm-2"}, arg.anchor), 
									React.createElement("div", {className: "col-sm-10"}, arg.command)
									
								), React.createElement("hr", null)));
							});
						}
					} else {
						inputs.push(React.createElement("div", {key: 'ext' + id}, React.createElement("div", {className: "clearfix"}, 
							React.createElement("div", {className: "col-sm-2"}, "cmd"), 
							React.createElement("div", {className: "col-sm-10"}, _this.state.output.command)
							
						), React.createElement("hr", null)));
					}
				} else if(s.type === 'program') {
						
					inputs.push(React.createElement("div", {key: id+s.name}, 
						React.createElement("div", {className: "col-xs-12"}, React.createElement("b", null, s.name)), 
						React.createElement("div", {className: "col-xs-12", style: {paddingBottom:10}}, s.type), 
						React.createElement("hr", null)
					));
					inputs.push(React.createElement("div", {key: 'e23xt' + s._id}, React.createElement("div", {className: "clearfix"}, 
						React.createElement("div", {className: "col-sm-2"}, "cmd"), 
						React.createElement("div", {className: "col-sm-10"}, s.command)		
					), React.createElement("hr", null)));
						
				} else {
					inputs.push(React.createElement("div", {key: id+s.name}, 
						React.createElement("div", {className: "col-xs-12"}, React.createElement("b", null, s.name)), 
						React.createElement("div", {className: "col-xs-12", style: {paddingBottom:10}}, s.type), 
						React.createElement("hr", null)
					));
				}
					
			});
			if(_.isObject(this.state.output)) {
				var o = this.state.output;
				if(o.type === 'program') {
					// we have a program... it may have an %OUTPUT% to replace or be middleware
					var piped = _.isObject(o.stream) ? React.createElement("span", null, " ", '>', " ", o.stream.type.toUpperCase(), " ", React.createElement("b", null, o.stream.name)) : o.host ? React.createElement("span", null, " ", '>', " ", React.createElement("b", null, o.host)): React.createElement("span", null);
					var out = React.createElement("h5", null, '>', " Program ", React.createElement("b", null, o.program.program, " "), " ", piped);
					
					outputs.push(React.createElement("div", {key: o._id+'name'}, 
						React.createElement("div", {className: "col-xs-12"}, out), 
						React.createElement("div", {className: "clearfix"}), 
						React.createElement("hr", null)
					));
					
					if(_.isObject(o.stream)) {
						outputs.push(displayStream.call(_this, o.stream)); 
					}
				}
				if(o.type === 'udp') {
					// we have a udp stream
					var piped =  React.createElement("b", null, o.name);
					var out = React.createElement("h5", null, '>', " UDP ", piped);
					
					outputs.push(React.createElement("div", {key: o._id+'name'}, 
						React.createElement("div", {className: "col-xs-12"}, out), 
						React.createElement("div", {className: "clearfix"}), 
						React.createElement("hr", null)
					));
					outputs.push(displayStream.call(_this, o)); 
					
				}
				
			}
		}
		if(this.state.inputs) {
			
			return (
				
				React.createElement("div", {className: " "}, 
					
					React.createElement("div", {className: "header-home"}, "Flow"), 
					React.createElement("div", {className: "clearfix"}), 
					React.createElement("div", {className: "col3 col-sm-12 col-md-12 no-padding"}, 
					
						inputs	
					), 
					React.createElement("div", {className: "clearfix"}), 
					React.createElement("div", {className: "col3 col-sm-12 col-md-12 no-padding"}, 
						outputs
					)	
				)
				
			);
		} else {
			return React.createElement("span", null)
		}	
			
	},
	
	
	
});

module.exports = Flow;

function displayStream(s) {
	if(s.type === 'udp') {
		var ret = (React.createElement("div", {className: "clearfix", key: s._id+s.type}, 
			React.createElement("div", {className: "col-sm-2"}, "UDP"), 
			React.createElement("div", {className: "col-sm-10"}, React.createElement("a", {href: 'udp://' + s.host + ':' + s.port}, 'udp://' + s.host + ':' + s.port)), 
			React.createElement("div", {className: "clearfix"}), 
			React.createElement("hr", null), 
			React.createElement("div", {className: "col-sm-2"}, "Proxy"), 
			React.createElement("div", {className: "col-sm-10"}, React.createElement("a", {href: '/proxy/' + this.props.form.slug}, '/proxy/' + this.props.form.slug)), 
			React.createElement("div", {className: "clearfix"}), 
			React.createElement("hr", null)
		));
		return ret;
	}
}

},{"lodash":undefined,"react":undefined,"react-select":undefined}],9:[function(require,module,exports){
var React = require('react');
var Link = require('react-router').Link;
var moment = require('moment');
 
var Home = React.createClass({displayName: "Home",
	contextTypes: {
		router: React.PropTypes.func
	},
	getInitialState: function() {
		return { status: React.createElement("span", null)};
	},
	componentDidMount: function() {
		
	},
	componentWillMount: function() {
		
	},
	componentWillReceiveProps: function(props) {
		return false;
	},
	render: function() {
			console.log('home state', this.state);
			
			return (
				React.createElement("div", {className: " "}, 
					React.createElement("div", {className: "header-home"}, "Home"), 
					React.createElement("div", {className: "clearfix"}), 
					
					React.createElement("div", {className: "col0 col-md-4"}, 
						React.createElement("div", {className: "jumbotron"}, 
							React.createElement("h1", null, "Streams"), 
							React.createElement("p", null, "list or something"), 
							React.createElement("p", null, React.createElement("a", {className: "btn btn-success btn-lg", id: "streams", onClick: this.go}, "Manage"))
						)
					), 
					React.createElement("div", {className: "col0 col-md-4"}, 
						React.createElement("div", {className: "jumbotron"}, 
							React.createElement("h1", null, "Sources"), 
							React.createElement("p", null, "list or something"), 
							React.createElement("p", null, React.createElement("a", {className: "btn btn-info btn-lg", id: "sources", onClick: this.go}, "Manage"))
						)
					), 
					React.createElement("div", {className: "col0 col-md-4"}, 
						React.createElement("div", {className: "jumbotron"}, 
							React.createElement("h1", null, "Channels"), 
							React.createElement("p", null, "list or something"), 
							React.createElement("p", null, React.createElement("a", {className: "btn btn-default btn-lg bg-info", id: "channels", onClick: this.go}, "Manage"))
						)
					)
				)
			);
			
	},
	go: function(e) {
		this.context.router.transitionTo(e.target.id);
	}
	
	
	
});

module.exports = Home;

},{"moment":undefined,"react":undefined,"react-router":undefined}],10:[function(require,module,exports){
var React = require('react');
var Link = require('react-router').Link;
var moment = require('moment');
var Select = require('react-select');
var _ = require('lodash');

var Logs = React.createClass({displayName: "Logs",
	contextTypes: {
		router: React.PropTypes.func
	},
	getInitialState: function() {
		return { 
			status: React.createElement("span", null)
		};
	},
	componentDidMount: function() {
		//this.getStreams();
	},
	componentWillMount: function() {
		
	},
	componentWillReceiveProps: function(props) {
		//console.log('receive props',props)
		//this.getStreams();
		return false;
	},
	getLogs: function() {
		console.log('get logs',this.state)
		var _this = this;
		var myFn = function(data) {
			console.log(data);
			
			_this.setState({streams: data.data.streams});
		}

		this.props.sockets.auth.emit('list',{ 
			list: 'Program', 
			exclude: '__v',
			populate: 0,
			iden: this.props.sockets.trap(this.props.sockets.auth, myFn) 
		});
	},
	render: function() {
		console.log('log state', this.state);
		
		return (
			
			React.createElement("div", {className: " "}, 
				
				React.createElement("div", {className: "header-logs"}, "Logs"), 
				
				React.createElement("div", {className: "clearfix"}), 
				React.createElement("div", {className: "col1 col-md-2"}, 
					"Available Logs", 
					
					React.createElement("div", {className: "col1"}
						
					)
				), 
				React.createElement("div", {className: "col2 col-sm-10 col-md-10 no-padding", style: {minHeight:250}}
					
				)
					
			)
			
		);
			
			
	},
	
	
	
});

module.exports = Logs;

},{"lodash":undefined,"moment":undefined,"react":undefined,"react-router":undefined,"react-select":undefined}],11:[function(require,module,exports){
var React = require('react');
var Link = require('react-router').Link;
var moment = require('moment');
var Select = require('react-select');
var _ = require('lodash');
var Alert = require('../common/alert.js');
var ReactBootstrap = require('react-bootstrap');
var ReactRouterBootstrap = require('react-router-bootstrap');
var ListGroupItem = ReactBootstrap.ListGroupItem;
var ListGroupItemLink = ReactRouterBootstrap.ListGroupItemLink;
var ListGroup = ReactBootstrap.ListGroup;
var Loader = require('../common/loader');

var	debug = require('debug')('snowstreams:client:pages:program');


var Program = React.createClass({displayName: "Program",
	contextTypes: {
		router: React.PropTypes.func
	},
	childContextTypes: {
		router: React.PropTypes.func
	},
	getInitialState() {
		obj = { 
			programs: [],
			programMap: {},
			programArgumentMap: {},
			alert: '',
			newalert: {},
			form: this.defaultForm(),
			currentField: false,
			formInfo: {
				name: ' <p>You will reference this program by name in the UI.</p> <p> The name should be unique among your programs and should be easily identifiable.</p> ',
				expose: ' <p>Create an API route to execute this program.  </p><p>This can be useful for channel changers.</p><p>You can also attach a program to upd stream listener in the Stream section. </p>',
				program: '<p>The command you would execute in a shell.</p><p>  Do not include any arguments. </p>',
				arguments: '<div><p<b>Name is required</b></p><p>You can attach multiple argument lines to a program.  When using the program you can select which argument line to apply.</p> <p>Each argument line can include placeholders in the form of <b>%VARIABLE%.</b> </p><p>Programs like ffmpeg may need: <br/ >input: <b>%INPUT%</b><p /><p> an output...<br /> stream: <b>pipe:1</b><br /> named url: <b>%OUTPUT%</b></p></div>', 
				stderror: '<p>Choose where to send error messages.</p>',
				type: '<p>You probably want a shell program.</p><p>Change internal programs with caution</a>',
			},
		}
		return obj;
	},
	defaultForm() {
		return {
			name: '',
			program: '',
			arguments: [{
				anchor: '',
				argument: ''
			}],
			type: 'shell',
			stderror: []
		}
	},
	componentDidMount() {
		_this = this;
		this.getPrograms();
		this.checkProps(this.props);
		$(document).on('focus', '#programdiv .showinfo', function(e) {
			_this.showInfo(e);
			if(e.target.name.search(':') !== -1) {
				var form = _this.state.form;
				var split = e.target.name.split(':');
				var order = form.arguments.length - 1;
				if(parseFloat(split[1]) === order) {
					
					form.arguments.push( {
						anchor: '',
						argument: ''
					});
					_this.setState({form: form});
				}
			}
		});
	},
	componentWillMount() {
		return;
	},
	checkProps(props) {
		var _this = this;
		if(props.params.slug === 'new') {
			this.setState({ 
				form: _this.defaultForm(),
				newalert: { show: false },
				currentField: false
			});
			return;
		} else if(props.params.slug) {
			this.getProgram(props.params.slug);
			return;
		}
	},
	componentWillReceiveProps(nextProps) {
		var sameQuery = this.props.params.slug === nextProps.params.slug;
		if(!sameQuery && this.props.params.slug !== '') {
			this.checkProps(nextProps);
		}
		return;
	},
	getPrograms() {
		var _this = this;
		var myFn = function(data) {
			var send = _.isArray(data.data.programs) ? data.data.programs : [];
			var programMap = {};
			var programArgumentMap = {};
			_.each(send, function(v) {
				programMap[v._id] = v;
				_.each(v.arguments, function(a) {
					programArgumentMap[a._id] = a;
				});
			});
			
			_this.setState({programs: send, programMap: programMap, programArgumentMap: programArgumentMap});
		}

		this.props.sockets.list('Program', {sort:{name:1}, populate: 'arguments createdBy updatedBy'}, myFn);
	},
	getProgram(slug) {
		var _this = this;
		_this.setState({ form: _this.defaultForm() });
		var myFn = function(data) {
			// We got an error
			if(data.error) {
				_this.setState({ 
					newalert: {
						style: 'danger',
						data: data,
						show: true
					},
					currentField: 'error'
				});
			// we got a result that may contain a list
			} else if(_.isObject(data.data)) {
				
				var send = _.isArray(data.data.programs) ? data.data.programs[0] : _.isObject(data.data.programs) ? data.data.programs : {arguments:{}};
				send.arguments[send.arguments.length] = {
					anchor: '',
					argument: ''
				}
				
				var name = _.isObject(send.updatedBy.name) ?  send.updatedBy.name.first + '  ' + send.updatedBy.name.last: ' Unknown';
				_this.setState({ 
					form: send,
					currentField: false,
					newalert: {
						style: 'info',
						component: (React.createElement("div", null, "Updated last on ", moment(send.updatedAt).format('MMMM Do YYYY, h:mm:ss a'), " by ", name)),
						show: true
					}
				});
			// something is wrong
			} else {
				_this.setState({ 
					currentField: 'error',
					newalert: {
						style: 'danger',
						html: 'Error. Please check console',
						show: true
					},
				});
			}
		} // end myFn

		var opts = {  
			exclude: '__v',
			slug: slug,
			sort: 'order',
			populate: 'arguments updatedBy',
		};
		this.props.sockets.getDocument('Program', opts, myFn);
	},
	dismissAlert() {
		this.setState({ 
			newalert: {
				show: false
			}
		});
	},
	dismissInfoAlert() {
		this.setState({ 
			currentField: false
		});
	},			
	submitForm(e) {
		e.preventDefault();
		 var _this = this;
		 var reload = (_this.state.form._id) ? true : false;
		 this.props.sockets.submitForm(this, e, 'managePrograms', 'Program', function(err, data) {
			_this.getPrograms();
			var d =  _.isArray(data.data.programs) ? data.data.programs[0] : data.data.programs;
			if(!err) _this.getProgram(d.slug);
		 });
	},
	handleChange(e, who) {
		var form = this.state.form;
		var _this = this;
		if(who){
			var value = _.isArray(e) ? e : _.isObject(e) ? e.value : '';
			form[who] = value;

		} else {
			var name = e.target.name;
			if(name.search(':') !== -1) {
				var split = name.split(':');
				form.arguments[split[1]][split[0]] = e.target.value;
			 
			} else {
				form[name] = e.target.value;			
			}
		}
		this.setState({form: form});
    },
     showInfo(e) {
		//debug(e);
		var name = e.currentTarget.dataset.name;
		if(name.search(':') !== -1) {
			name = 'arguments'
		}
		this.setState({currentField: name});
    },
    deleteProgram(e) {
		e.preventDefault();
		this.props.sockets.deleteDocument(this, e, 'Program');
	},
	reallyDelete() {
		var _this = this;
		this.props.sockets.reallyDelete(this, null, 'Program', function(err, data) {
			if(!err) {
				_this.getPrograms();
				if(_this.state.form._id) _this.getProgram(data.data.programs.slug);
			}
		});
	},
	render() {
		debug('stream state', this.state);
		var _this = this;
		var args = [];
		_.each(_this.state.form.arguments, function(arg, k) {
			if(_.isObject(arg)) {
				args.push(addRow(arg, k));
			}
		}); 
		
		function addRow(arg,k) {
			return (
				React.createElement("div", {className: "clearfix", key: 'order:' + k}, 
					
					React.createElement("div", {className: "col-sm-3"}, 
						React.createElement("input", {style: {fontSize:12}, name: 'anchor:'+k, ref: 'anchor:'+k, onChange: _this.handleChange, className: "form-control", placeholder: "name", type: "text", value: _this.state.form.arguments[k].anchor})
					), 
					React.createElement("div", {className: "col-sm-9"}, 
						React.createElement("input", {style: {fontSize:12}, name: 'argument:'+k, ref: 'argument:'+k, onChange: _this.handleChange, className: "form-control", placeholder: " -re -i %INPUT% -c copy -o %OUTPUT%", type: "text", value: _this.state.form.arguments[k].argument})
					)
				)
			);
		}
		
		var typeOptions = [
			{ value: 'shell', label: 'shell program' },
			{ value: 'fluent-ffmpeg', label: 'fluent-ffmpeg' },
			{ value: 'internal', label: 'internal program' },
		];
		var type = React.createElement(Select, {
			name: "type", 
			options: typeOptions, 
			onChange: function(val, obj) {
				_this.handleChange(obj[0], 'type')
			}, 
			value: this.state.form.type}
		)
		var stderrorOptions = [];
		_.each(this.state.programs, function(o) {
				if(o.type === 'internal') {
					_.each(o.arguments, function(a) {
						stderrorOptions.push({label: _this.state.programArgumentMap[a._id].command, value: _this.state.programArgumentMap[a._id]._id});
					})
				}
		});
		var stderror = React.createElement(Select, {
			name: "stderror", 
			options: stderrorOptions, 
			multi: true, 
			onChange: function(val, obj) {
				_this.handleChange(val.split(','), 'stderror')
			}, 
			value: this.state.form.stderror}
		)
		
		
		var listItems = this.state.programs.map(function(v) {
			return (React.createElement(ListGroupItemLink, {bsSize: "small", to: "manage-program", params: { slug: v.slug}, key: v.slug+'-key'}, v.name))
		});
		if(listItems.length === 0) {
			listItems.push(React.createElement(ListGroupItem, {bsSize: "xlarge", key: 'loading-key'}, React.createElement("div", {className: "clearfix"}, React.createElement("div", {className: "", style: {float:'right'}}, React.createElement(Loader, null)))));
		}
		var list = (
			React.createElement(ListGroup, null, 
				React.createElement(ListGroupItemLink, {to: "manage-program", params: { slug: 'new'}}, "New Program"), 
				listItems
			)
		);
		return (
			
			React.createElement("div", {id: "programdiv"}, 
				
				React.createElement("div", {className: "header-programs"}, "Programs"), 
				
				React.createElement("div", {className: "clearfix"}), 
				React.createElement("div", {className: "menuitem col1 col-md-2"}, 
					list
				), 
				React.createElement("div", {className: "col2 col-sm-10 col-md-10 no-padding"}, 
					this.state.alert, 
					this.state.newalert.show ? React.createElement(Alert, {style: this.state.newalert.style, html: this.state.newalert.html, data: this.state.newalert.data, component: this.state.newalert.component, dismiss: _this.dismissAlert}) : '', 
					React.createElement("div", {className: "col-sm-8 col-md-8"}, 
						React.createElement("form", {className: "form-horizontal", onSubmit: this.submitForm}, 	
							React.createElement("fieldset", null, 			
		 						
								React.createElement("div", {className: "form-group showinfo", "data-name": "name"}, 
									React.createElement("label", {className: "col-md-2 control-label", htmlFor: "name"}, "name"), 
									React.createElement("div", {className: "col-md-10"}, 
										React.createElement("input", {ref: "name", name: "name", onChange: this.handleChange, type: "text", placeholder: "ffmpeg", className: "form-control input-md", required: "", value: this.state.form.name})
									)
								), 
								
								React.createElement("div", {className: "clearfix"}), 
								React.createElement("div", {ref: "show", className: this.state.form.name !== '' ? 'fatalbert' : 'casper'}, 
									
									React.createElement("div", {className: "clearfix"}), 
									React.createElement("div", {className: "form-group showinfo", "data-name": "type"}, 
										React.createElement("label", {className: "col-md-2 control-label", htmlFor: "type"}, "type"), 
										React.createElement("div", {className: "col-md-10"}, 
											type
										)
									), 
									
									React.createElement("div", {className: "clearfix"}), 
									React.createElement("div", {ref: "show", className: _.contains(['shell','internal'],this.state.form.type) ? 'fatalbert' : 'casper'}, 
										React.createElement("div", {className: "form-group showinfo", "data-name": "program"}, 
											React.createElement("label", {className: "col-md-2 control-label", htmlFor: "program"}, "program"), 
											React.createElement("div", {className: "col-md-10"}, 
												React.createElement("input", {ref: "program", name: "program", onChange: this.handleChange, type: "text", placeholder: "ffmpeg", className: "form-control input-md", required: "", value: this.state.form.program})
											)
										)
									), 
									
									
									
									React.createElement("div", {className: "clearfix"}), 
									React.createElement("div", {className: "form-group showinfo", "data-name": "arguments"}, 
										React.createElement("label", {className: "col-md-2 control-label", htmlFor: "args"}, "arguments"), 
										React.createElement("div", {className: "col-md-10"}, 
											args											
										)
									), 
									
									React.createElement("div", {className: "clearfix"}), 
									React.createElement("div", {ref: "stederr", className: this.state.form.type !== 'internal' ? 'fatalbert' : 'casper'}, 
										
										React.createElement("div", {className: "clearfix"}), 
										React.createElement("div", {className: "form-group showinfo", "data-name": "expose"}, 
											React.createElement("label", {className: "col-md-2 control-label", htmlFor: "expose"}, "api"), 
											React.createElement("div", {className: "col-md-10"}, 
												React.createElement("div", {className: "checkbox"}, 
													
													React.createElement("label", {htmlFor: "expose-0"}, 
														React.createElement("input", {type: "checkbox", name: "expose", id: "expose-0", onChange: this.handleChange, value: this.state.form.expose === 'true' || this.state.form.expose === true ? 'false' : 'true', checked: this.state.form.expose === 'true' || this.state.form.expose === true ? 'checked' : ''}), 
														this.state.form.expose === 'true' || this.state.form.expose === true ? React.createElement("a", {href: '/proxy/program/' + _this.state.form.slug + '/?argument=0&channel=724'}, "/proxy/program/", _this.state.form.slug, "/?argument=0&channel=724") : 'This program is not exposed via API.'
													)
												)
											)
										), 
									
										React.createElement("div", {className: "clearfix"}), 
										React.createElement("div", {className: "form-group showinfo", "data-name": "stderror"}, 
											React.createElement("label", {className: "col-md-2 control-label", htmlFor: "stderror"}, "error output"), 
											React.createElement("div", {className: "col-md-10"}, 
												stderror
											)
										)
									), 
										
									React.createElement("div", {className: "clearfix"}), 
									React.createElement("div", {ref: "show", className: this.state.form.type !== 'internal' ? 'fatalbert' : (_.isObject(this.state.programMap[this.state.form._id]) && this.state.programMap[this.state.form._id].type !== 'internal') ? 'fatalbert' : 'casper'}, 
									
										React.createElement("div", {className: "clearfix"}), 
										React.createElement("div", {className: "form-group"}, 
											React.createElement("div", {className: "col-md-offset-2 col-md-5"}, 
												React.createElement("button", {className: "btn btn-info"}, this.state.form._id ? 'Update Program' : 'Add Program'), 
												React.createElement("input", {type: "hidden", name: "_id", value: this.state.form._id}), 
												React.createElement("input", {type: "hidden", name: "slug", value: this.state.form.slug})
											), 
											React.createElement("div", {className: " col-md-5"}, 
												this.state.form._id ? React.createElement("button", {className: "btn btn-danger pull-right", onClick: this.deleteProgram}, "Delete") : ''
												
											)
										)
											
									)
									
								)
								
							)
						)
					), 
					React.createElement("div", {className: " col-sm-4 col-md-4"}, 
						 this.state.currentField ? React.createElement(Alert, {style: "info", html: this.state.formInfo[this.state.currentField], dismiss: _this.dismissInfoAlert}) : ''
					)
				)
					
			)
			
		);
			
			
	},
	
	
	
});

module.exports = Program;

},{"../common/alert.js":3,"../common/loader":4,"debug":17,"lodash":undefined,"moment":undefined,"react":undefined,"react-bootstrap":undefined,"react-router":undefined,"react-router-bootstrap":150,"react-select":undefined}],12:[function(require,module,exports){
var React = require('react');
var _ = require('lodash');
var Select = require('react-select');
var Link = require('react-router').Link;
var Alert = require('../common/alert');
var Loader = require('../common/loader');
var ReactBootstrap = require('react-bootstrap');
var ReactRouterBootstrap = require('react-router-bootstrap');
var ListGroupItemLink = ReactRouterBootstrap.ListGroupItemLink;
var ListGroupItem = ReactBootstrap.ListGroupItem;
var ListGroup = ReactBootstrap.ListGroup;
var moment = require('moment');

var	debug = require('debug')('snowstreams:client:pages:source');

var Source = React.createClass({displayName: "Source",
	contextTypes: {
		router: React.PropTypes.func
	},
	getInitialState() {
		obj = { 
			sources: [],
			streams: [],
			active: 'program',
			programs: [],
			newalert: {},
			form: this.defaultForm(),
			currentField: false,
			forming: false,
			showFiles: false,
			
			formInfo: {
				name: ' <p>You will reference this source by name in the UI.</p> <p> The name should be unique among your sources and should be easily identifiable.</p> ',
				type: '<p><b>file</b>: Use file to select a single file source or multiple files from a directory tree.</p><p><b>http</b>: Selecting http will allow you to enter a url.</p><p><b>program</b>: Use program to run a shell program.  You should add your programs first from the Programs section.</p> <p><b>udp</b>: Use a upd stream as a source.',
				file: 'Enter a valid file path, or a directory to traverse.',
				directory: 'You must check this to traverse a directory and select extensions to filter.',
				ext: '<p>Enter each extension without the dot(.)</p> <p>Separate extensions with a space. Only files matching the selected extensions will be applied.</p>',
				host: 'The host for your udp stream.  Local streams can use <b>@</b>',
				port: 'Port number for the udp stream',
				url: 'Valid stream url.  You can pipe the source stream through another source (program) or send it straight to a udp stream (channel).',
				respawn: 'Check this box to restart your program when it fails.',
				restarts: 'How many times should we restart the program before giving up?',
				sleep: 'How long to pause between restarts.  1000 = 1 second.',
				kill: 'Wait time before kill event on a stopped process.',
				argument: '<div><p>Select the appropriate argument line</p><p>Be sure your argument outputs to stdout if you will stream or save this source.</div>', 
				program: 'Select one of your programs.  The argument list will be supplied after you chose.',
				input: '<p>If your program requires an input select it here. </p><p> You will need to define any input sources first. </p><p>   Make sure your argument line contains %INPUT% for files or accepts stdin otherwise.</p>',
				output: '<p>A source program can output to stdout (pipe) to send to a stream selected later. </p><p> You can also select an output now.  When run the stream will end when the program ends. </p><p> You can create an open stream that survives input changes with Channels. </p><p> Make sure your argument line outputs to stdout so we can pipe the results into this output.</p>',
				expose: ' <p>Create API routes to manage this stream.  </p><p>Available arguments are: <b>play</b> (<i>alias:</i> <b>start</b>) <br /><b>restart</b><br /> <b>status</b><br /> <b>stop</b> <br /> <b>view</b><br />No argument will <b>play</b>.</p><p>2 routes are created:<br/> <b>/proxy/stream/:name/:argument</b><br/> sends Content-Type=mpegts for play.</p><p><b>/proxy/stream/:name/:<i>mime-type</i></b> sends the Content-Type you set as <i>mime-type</i>.</p><p>Status will give you program information.  Currently that is limited to the filename if available, codec information if available, start time and expected stop time. </p>',
				scan: '<p>Do you want to (re)scan the file or directory?</p><p>   If ffprobe is available, metadata information wil be obtained. </p><p> The program will process 30 files at a time.  Large file sets will take a moment to process.</p>'
			},
		}
		
		return obj;
	},
	defaultForm() {
		return {
			name: '',
			type: '',
			argument: '',			
		}
	},
	componentWillUpdate() {
		_this = this;		
	},
	componentDidMount() {
		_this = this;
		this.getSources();
		this.checkProps(this.props);
		$(document).on('focus','#sourcediv .showinfo', function(e) {
			_this.showInfo(e);
		});
	},
	checkProps(props) {
		var _this = this;
		if(props.params.slug === 'new') {
			this.setState({ 
				form: _this.defaultForm(),
				newalert: { show: false },
				currentField: false
			});
			return;
		} else if(props.params.slug) {
			this.getSource(props.params.slug);
			return;
		}
	},
	componentWillReceiveProps(nextProps) {
		var sameQuery = this.props.params.slug === nextProps.params.slug;
		if(!sameQuery && this.props.params.slug !== '') {
			this.checkProps(nextProps);
		}
		return;
	},
	componentDidUpdate: function() {
		
	},
	logChange: function (val) {
		debug("Selected: " + val);
	},
	getSources() {
		var _this = this;
		var myFn = function(data) {
			var send = _.isArray(data.data.sources) ? data.data.sources : [];
			//debug('received sources data', send);
			_this.setState({sources: send});
		}
		var myFn2 = function(data) {
			var send2 = _.isArray(data.data.programs) ? data.data.programs : [];
			//debug('received programs data', send2);
			_this.setState({programs: send2});
		}
		var myFn3 = function(data) {
			var send3 = _.isArray(data.data.streams) ? data.data.streams : [];
			//debug('received streams data', send3);
			_this.setState({streams: send3});
		}
		this.props.sockets.list('Program', {sort:{name:1}, populate:'arguments'}, myFn2);
		this.props.sockets.list('Source', {sort:{type:1,name:1}}, myFn); 
		this.props.sockets.list('Stream', {sort:{name:1}}, myFn3);
	},
	getSource(slug) {
		var _this = this;
		
		_this.setState({ form: _this.defaultForm() });
		
		var myFn = function(data) {
			// We got an error
			if(data.error) {
				_this.setState({ 
					newalert: {
						style: 'danger',
						data: data,
						show: true
					},
					currentField: 'error'
				});
			// we got a result that may contain a list
			} else if(_.isObject(data.data)) {
				
				var send = _.isArray(data.data.sources) && data.data.sources.length > 0 ? data.data.sources[0] : _.isObject(data.data.sources) ? data.data.sources : {};
				debug('result', send, data)
				var name = _.isObject(send.updatedBy) && _.isObject(send.updatedBy.name) ?  send.updatedBy.name.first + '  ' + send.updatedBy.name.last: ' Unknown';
				var pos = false;
				_.each(_this.state.programs, function(o,k) {
					if(o._id === send.program) pos = k;
				});
				_this.setState({ 
					form: send,
					programPos: pos || 0,
					currentField: false,
					newalert: {
						style: 'info',
						component: (React.createElement("div", null, "Updated last on ", moment(send.updatedAt).format('MMMM Do YYYY, h:mm:ss a'), " by ", name)),
						show: true
					}
				});
			// something is wrong
			} else {
				_this.setState({ 
					currentField: 'error',
					newalert: {
						style: 'danger',
						html: 'Error. Please check console',
						show: true
					},
				});
			}
		} // end myFn

		var opts = {  
			exclude: '__v',
			slug: slug,
			sort: 'order',
			populate: 'arguments updatedBy',
		};
		this.props.sockets.getDocument('Source', opts, myFn);
	},
	dismissAlert() {
		this.setState({ 
			newalert: {
				show: false
			}
		});
	},
	dismissInfoAlert() {
		this.setState({ 
			currentField: false
		});
	},			
	submitForm(e) {
		e.preventDefault();
		 
		 var _this = this;
		 var form = this.state.form;
		 var isFile = form.type === 'file';
		 var isDir = form.directory;
		 var doScan = this.state.form.scan;
		 
		 delete form.createdBy;
		 delete form.updatedBy;
		 delete form.createdAt;
		 delete form.updatedAt;
		 delete form.files
		 
		 this.setState({forming: true, form: form });
		
		 if(this.state.form.scan)this.props.openConsole();
		
		 var reload = (_this.state.form._id) ? true : false;
		 var go = (_this.state.form._id) ? 'update' : 'create';
		
		 this.props.sockets.submitForm(this, e, go, 'Source', function(err, data) {
			_this.getSources();
			if(!err) _this.getSource(data.data.sources.slug);
			if(doScan && isFile && isDir) {
				_this.setState({ 
					newalert: {
						style: 'warning',
						component: React.createElement("div", null, "It may take a few minutes for all of your files to process.  You can follow progress in the console pane. ", React.createElement("p", null, " ", React.createElement("b", null, "Your source is loading and will be avialable soon."))),
						show: true
					},
					forming: false
				});
			} else {
				_this.setState({ forming: false });
			}
		 });
		
	},
	
	handleChange(val, who) {
		var send = {form: this.state.form}
		var form = send.form;
		if(who){
			var name = who;
			var value = _.isArray(val) ? val : _.isObject(val) ? val.value : '';
			if(who === 'program') {
				send.programPos = parseFloat(val.pos);
				form.argument = ''
			}
		} else {
			var name = val.target.name;
			var value = val.target.value
		}
		form[name] = value;
		//debug('send', send, val, who);	
		this.setState(send);
    },
    showInfo(e) {
		//debug(e);
		var name = e.currentTarget.dataset.name;
		if(name.search(':') !== -1) {
			name = 'arguments'
		}
		this.setState({currentField: name});
    },
    deleteSource(e) {
		e.preventDefault();
		this.props.sockets.deleteDocument(this, e, 'Source'); 
	},
	reallyDelete() {
		var _this = this;
		this.props.sockets.reallyDelete(this, null, 'Source', function(err, data) {
			if(!err) {
				_this.getSources();
				if(_this.state.form._id) _this.getSource(data.data.sources.slug);
			}
		});
	},
	showFiles(e) {
		e.preventDefault();
		this.setState({showFiles: !this.state.showFiles});
	},
	
	render: function() {
		//debug(this.context.router.getCurrentPathname(), this.state);	
		var _this = this;
		var typeOptions = [
			{ value: 'file', label: 'file' },
			{ value: 'http', label: 'http' },
			{ value: 'program', label: 'program' },
			{ value: 'udp', label: 'udp' }
		];
		 
		var type = React.createElement(Select, {
			name: "type", 
			options: typeOptions, 
			onChange: function(val, obj) {
				_this.handleChange(obj[0], 'type')
			}, 
			value: this.state.form.type}
		)
					
		var programOptions = this.state.programs.map(function(v, k) {
			return ({ value: v._id, label: v.name, pos: k });
		});
		var programs = React.createElement(Select, {
			name: "program", 
			allowCreate: false, 
			options: programOptions, 
			onChange: function(val, obj) {
				_this.handleChange(obj[0], 'program')
			}, 
			value: this.state.form.program}
		)	
		var arguments = React.createElement("span", null)
		var source = React.createElement("span", null)
		var stream = React.createElement("span", null)
		if(this.state.form.argument !== '' || this.state.programPos > -1) {
			
			var programArguments = this.state.programs[this.state.programPos].arguments.map(function(v, k) {
				return ({ value: v._id, label: v.anchor + ' : ' + v.argument });
			});
			arguments = React.createElement(Select, {
				name: "argument", 
				addLabelText: "Add a new argument to the set", 
				allowCreate: false, 
				options: programArguments, 
				onChange: function(val, obj) {
					_this.handleChange(obj[0], 'argument')
				}, 
				value: this.state.form.argument}
			)
			var sourceOptions = this.state.sources.map(function(v, k) {
				return ({ value: v._id, label: v.name });
			});
			var source = React.createElement(Select, {
				name: "source", 
				allowCreate: false, 
				options: sourceOptions, 
				multi: true, 
				onChange: function(val, obj) {
					_this.handleChange(val.split(','), 'source')
				}, 
				value: this.state.form.source}
			)
			var streamOptions = this.state.streams.map(function(v, k) {
				return ({ value: v._id, label: v.name });
			});
			var stream = React.createElement(Select, {
				name: "stream", 
				allowCreate: false, 
				options: streamOptions, 
				onChange: function(val, obj) {
					_this.handleChange(obj[0], 'stream')
				}, 
				value: this.state.form.stream}
			)	
		}
		
		// file info
		var fileInfo = React.createElement("span", null);
		var viewFiles = React.createElement("span", null);
		if(this.state.form.type === 'file') {
			fileInfo = (React.createElement("div", null, 
				React.createElement("div", {className: "col-xs-12"}, React.createElement("b", null, _.isArray(this.state.form.files) ? this.state.form.files.length : 1, " files")), 
				React.createElement("hr", null)
			));
		
		}
		if(_.isArray(this.state.form.files1)) {	
			var filelist = this.state.form.files.map(function(v) {
				var time = moment.duration(v.runtime, 'seconds');
				if(!_.isObject(v.codec)) v.codec = {}
				return (React.createElement("div", {key: v._id}, 
					React.createElement("div", {className: "col-sm-12"}, v.file), 
					React.createElement("div", {className: "col-sm-3"}, time.hours() + ':' + time.minutes() + '.' + time.seconds()), 
					React.createElement("div", {className: "col-sm-3"}, Math.floor(v.bitrate/1024), " kb/s"), 
					React.createElement("div", {className: "col-sm-3"}, v.codec.video), 
					React.createElement("div", {className: "col-sm-3"}, v.codec.audio), 
					React.createElement("div", {className: "clearfix"}), 
					React.createElement("hr", null)
				));
			});
			viewFiles = (React.createElement("div", null, 
				React.createElement("div", {className: "col-xs-12", style: {paddingBottom:10}}, React.createElement("b", null, React.createElement("a", {href: "#", onClick: _this.showFiles}, "view files"))), 
				React.createElement("div", {className: "col-xs-12", className: _this.state.showFiles ? 'fatalbert' : 'casper', style: {paddingBottom:10}}, filelist), 
				React.createElement("hr", null)
			));
			
		}
		var t;
		var listItems = this.state.sources.map(function(v) {
			if(t !== v.type) {
				t = v.type;
				return (React.createElement(ListGroupItemLink, {header: v.type, bsSize: "small", to: "manage-source", params: { slug: v.slug}, key: v.slug+'-key'}, v.name))
			} else {
				return (React.createElement(ListGroupItemLink, {bsSize: "small", to: "manage-source", params: { slug: v.slug}, key: v.slug+'-key'}, v.name))
			}
		});
		if(listItems.length === 0) {
			listItems.push(React.createElement(ListGroupItem, {bsSize: "xlarge", key: 'loading-key'}, React.createElement("div", {className: "clearfix"}, React.createElement("div", {className: "", style: {float:'right'}}, React.createElement(Loader, null)))));
		}
		var list = (
			React.createElement(ListGroup, null, 
				React.createElement(ListGroupItemLink, {bsSize: "small", to: "manage-source", params: { slug: 'new'}}, "New Source"), 
				listItems
			)
		);
		
		return (
			
			React.createElement("div", {id: "sourcediv", className: " "}, 
				
				React.createElement("div", {className: "header-sources"}, "Sources"), 
				
				React.createElement("div", {className: "clearfix"}), 
				React.createElement("div", {className: "menuitem col1 col-md-2"}, 
					list
				), 
				React.createElement("div", {className: "col2 col-sm-10 col-md-10 no-padding"}, 
					React.createElement("a", {name: "top"}), 
					this.state.newalert.show ? React.createElement(Alert, {style: this.state.newalert.style, html: this.state.newalert.html, data: this.state.newalert.data, component: this.state.newalert.component, dismiss: _this.dismissAlert}) : '', 
					React.createElement("div", {className: "col-sm-8 col-md-8"}, 
						
						React.createElement("form", {className: "form-horizontal", onSubmit: this.submitForm}, 	
							React.createElement("fieldset", null, 			
		 						
								React.createElement("div", {className: "form-group showinfo", "data-name": "name"}, 
									React.createElement("label", {className: "col-md-2 control-label", htmlFor: "name"}, "name"), 
									React.createElement("div", {className: "col-md-10"}, 
										React.createElement("input", {id: "name", name: "name", type: "text", placeholder: "cable", className: "form-control input-md", onChange: this.handleChange, value: this.state.form.name})
									)
								), 
								
								React.createElement("div", {className: "clearfix"}), 
								React.createElement("div", {ref: "show", className: this.state.form.name !== '' ? 'fatalbert' : 'casper'}, 
									
									React.createElement("div", {className: "clearfix"}), 
									React.createElement("div", {className: "form-group showinfo", "data-name": "type"}, 
										React.createElement("label", {className: "col-md-2 control-label", htmlFor: "type"}, "type"), 
										React.createElement("div", {className: "col-md-10"}, 
											type
										)
									), 
									
									React.createElement("div", {ref: "show", className: this.state.form.type !== 'file' ? 'fatalbert' : 'casper'}, 
									
										React.createElement("div", {className: "clearfix"}), 
										React.createElement("div", {className: "form-group showinfo", "data-name": "expose"}, 
											React.createElement("label", {className: "col-md-2 control-label", htmlFor: "expose"}, "api"), 
											React.createElement("div", {className: "col-md-10"}, 
												React.createElement("div", {className: "checkbox"}, 
													
													React.createElement("label", {htmlFor: "expose-0"}, 
														React.createElement("input", {type: "checkbox", name: "expose", id: "expose-0", onChange: this.handleChange, value: this.state.form.expose === 'true' || this.state.form.expose === true ? 'false' : 'true', checked: this.state.form.expose === 'true' || this.state.form.expose === true ? 'checked' : ''}), 
														this.state.form.expose === 'true' || this.state.form.expose === true ? React.createElement("div", null, React.createElement("a", {href: "/proxy/source/"+_this.state.form.slug+"/status"}, "/proxy/source/", _this.state.form.slug, "/status"), React.createElement("br", null), React.createElement("a", {href: "/proxy/source/"+_this.state.form.slug+"/raw"}, "/proxy/source/", _this.state.form.slug, "/raw")) : React.createElement("span", null, "This source is not managed via API.")
													)
												)
											)
										)
									
									), 
									
									React.createElement("div", {className: "clearfix"}), 
									React.createElement("div", {ref: "udp", className: this.state.form.type === 'udp' ? 'fatalbert' : 'casper'}, 
										
										React.createElement("div", {className: "form-group showinfo", "data-name": "host"}, 
											React.createElement("label", {className: "col-md-2 control-label", htmlFor: "host"}, "host"), 
											React.createElement("div", {className: "col-md-10"}, 
												React.createElement("input", {id: "host", name: "host", type: "text", placeholder: "@", className: "form-control input-md", onChange: this.handleChange, value: this.state.form.host}), 
												React.createElement("span", {className: "help-block"}, "valid interface address")
											)
										), 
										
										React.createElement("div", {className: "clearfix"}), 
										React.createElement("div", {className: "form-group  showinfo", "data-name": "port"}, 
											React.createElement("label", {className: "col-md-2 control-label", htmlFor: "port"}, "port"), 
											React.createElement("div", {className: "col-md-5"}, 
												React.createElement("div", {className: "input-group"}, 
													React.createElement("input", {id: "port", name: "port", className: "form-control", placeholder: "11000", type: "text", onChange: this.handleChange, value: this.state.form.port}), 
													React.createElement("span", {className: "input-group-addon"}, "number")
												)
											)
										)
									
									), 	
									
									React.createElement("div", {ref: "http", className: this.state.form.type === 'http' ? 'fatalbert' : 'casper'}, 
										
										React.createElement("div", {className: "form-group showinfo", "data-name": "url"}, 
											React.createElement("label", {className: "col-md-2 control-label", htmlFor: "url"}, "url"), 
											React.createElement("div", {className: "col-md-10"}, 
												React.createElement("input", {id: "url", name: "url", type: "text", placeholder: "http://host/file.ext", className: "form-control input-md", onChange: this.handleChange, value: this.state.form.url}), 
												React.createElement("span", {className: "help-block"}, "valid http stream address")
											)
										), 
										React.createElement("div", {className: "clearfix"})
									
									), 	
									
									React.createElement("div", {ref: "file", className: this.state.form.type === 'file' ? 'fatalbert' : 'casper'}, 
										
										React.createElement("div", {className: "form-group showinfo", "data-name": "file"}, 
											React.createElement("label", {className: "col-md-2 control-label", htmlFor: "file"}, "file"), 
											React.createElement("div", {className: "col-md-10"}, 
												React.createElement("input", {id: "file", name: "file", type: "text", placeholder: "/path/to/file.ext", className: "form-control input-md", onChange: this.handleChange, value: this.state.form.file}), 
												React.createElement("span", {className: "help-block"}, React.createElement("b", null, fileInfo))
											)
										), 
										
										React.createElement("div", {className: "clearfix"}), 
										React.createElement("div", {className: "form-group showinfo", "data-name": "scan"}, 
											React.createElement("label", {className: "col-md-2 control-label", htmlFor: "scan"}, "scan"), 
											React.createElement("div", {className: "col-md-10"}, 
												React.createElement("div", {className: "checkbox"}, 
													
													React.createElement("label", {htmlFor: "scan-0"}, 
														React.createElement("input", {type: "checkbox", name: "scan", id: "scan-0", onChange: this.handleChange, value: this.state.form.scan === 'true' || this.state.form.scan === true ? 'false' : 'true', checked: this.state.form.scan === 'true'  || this.state.form.scan === true ? 'checked' : ''}), 
														this.state.form.scan === 'true' || this.state.form.scan === true ? 'The file or directory will be scanned and run through ffprobe if available.' : 'This file or directory will not be scanned when saving.'
													)
												)
											)
										), 
										
										React.createElement("div", {className: "clearfix"}), 
										React.createElement("div", {className: "form-group showinfo", "data-name": "directory"}, 
											React.createElement("label", {className: "col-md-2 control-label", htmlFor: "directory"}, "directory"), 
											React.createElement("div", {className: "col-md-10"}, 
												React.createElement("div", {className: "checkbox"}, 
													
													React.createElement("label", {htmlFor: "directory-0"}, 
														React.createElement("input", {type: "checkbox", name: "directory", id: "directory-0", onChange: this.handleChange, value: this.state.form.directory === 'true' || this.state.form.directory === true ? 'false' : 'true', checked: this.state.form.directory === 'true'  || this.state.form.directory === true ? 'checked' : ''}), 
														this.state.form.directory === 'true' || this.state.form.directory === true ? 'This is a directory and the following file types will be used in the filter.' : 'No, when unchecked this is a file.'
													)
												)
											)
										), 
										
										React.createElement("div", {className: "clearfix"}), 
										React.createElement("div", {ref: "ext", className: this.state.form.directory === 'true'  || this.state.form.directory === true ? 'fatalbert' : 'casper'}, 
											React.createElement("div", {className: "form-group showinfo", "data-name": "ext"}, 
												React.createElement("label", {className: "col-md-2 control-label", htmlFor: "ext"}, "extensions"), 
												React.createElement("div", {className: "col-md-10"}, 
													React.createElement("input", {id: "ext", name: "ext", type: "text", placeholder: "mp4 mkv avi", className: "form-control input-md", onChange: this.handleChange, value: this.state.form.ext})
													 
												)
											)
										), 
										
										React.createElement("div", {className: "clearfix"}), 
										React.createElement("div", {className: "form-group showinfo", "data-name": "fileinfo"}, 
											React.createElement("div", {className: "col-md-12"}, 
												viewFiles
											)
										)
										
									), 	
									
									React.createElement("div", {ref: "program", className: this.state.form.type === 'program' ? 'fatalbert' : 'casper'}, 
										
										React.createElement("div", {className: "form-group showinfo", "data-name": "program"}, 
											React.createElement("label", {className: "col-md-2 control-label", htmlFor: "program"}, "program"), 
											React.createElement("div", {className: "col-md-10"}, 
												programs
											)
										), 
										
										React.createElement("div", {className: "clearfix"}), 
										React.createElement("div", {ref: "programthings", className: this.state.form.argument !== '' || this.state.programPos > -1 ? 'fatalbert' : 'casper'}, 
											
											React.createElement("div", {className: "form-group showinfo", "data-name": "argument"}, 
												React.createElement("label", {className: "col-md-2 control-label", htmlFor: "args"}, "arguments"), 
												React.createElement("div", {className: "col-md-10", ref: "arguments"}, 
													arguments
												)
											), 
											
											React.createElement("div", {className: "clearfix"}), 
											React.createElement("div", {className: "form-group showinfo", "data-name": "input"}, 
												React.createElement("label", {className: "col-md-2 control-label", htmlFor: "source"}, "input(s)"), 
												React.createElement("div", {className: "col-md-10"}, 
													source
												)
											), 
											
											React.createElement("div", {className: "clearfix"}), 
											React.createElement("div", {className: "form-group showinfo", "data-name": "output"}, 
												React.createElement("label", {className: "col-md-2 control-label", htmlFor: "stream"}, "output"), 
												React.createElement("div", {className: "col-md-10"}, 
													stream
												)
											)
										), 
										
										React.createElement("div", {className: "clearfix"}), 
										React.createElement("div", {className: "form-group showinfo", "data-name": "respawn"}, 
											React.createElement("label", {className: "col-md-2 control-label", htmlFor: "respawn"}, "respawn"), 
											React.createElement("div", {className: "col-md-10"}, 
												React.createElement("div", {className: "checkbox"}, 
													
													React.createElement("label", {htmlFor: "respawn-0"}, 
														React.createElement("input", {type: "checkbox", name: "respawn", id: "respawn-0", onChange: this.handleChange, value: this.state.form.respawn === 'true' || this.state.form.respawn === true ? 'false' : 'true', checked: this.state.form.respawn === 'true' || this.state.form.respawn === true ? 'checked' : ''}), 
														this.state.form.respawn === 'true' || this.state.form.respawn === true ? 'This program will be restarted under the following requirements...' : 'No, when unchecked this program will not respawn.'
													)
												)
											)
										), 
										
										React.createElement("div", {className: "clearfix", ref: "repawning", className: this.state.form.respawn === 'true' || this.state.form.respawn === true ? 'fatalbert' : 'casper'}, 
											
											React.createElement("div", {className: "form-group showinfo", "data-name": "restarts"}, 
												React.createElement("label", {className: "col-md-2 control-label", htmlFor: "restarts"}, "restarts"), 
												React.createElement("div", {className: "col-md-5"}, 
													React.createElement("div", {className: "input-group"}, 
														React.createElement("input", {id: "restarts", name: "restarts", className: "form-control", placeholder: "10", type: "text", onChange: this.handleChange, value: this.state.form.restarts}), 
														React.createElement("span", {className: "input-group-addon"}, "number")
													)

												)
											), 
											React.createElement("div", {className: "clearfix"}), 
											React.createElement("div", {className: "form-group showinfo", "data-name": "sleep"}, 
												React.createElement("label", {className: "col-md-2 control-label", htmlFor: "sleep"}, "sleep"), 
												React.createElement("div", {className: "col-md-5"}, 
													React.createElement("div", {className: "input-group"}, 
														React.createElement("input", {id: "sleep", name: "sleep", className: "form-control", placeholder: "1000", type: "text", onChange: this.handleChange, value: this.state.form.sleep}), 
														React.createElement("span", {className: "input-group-addon"}, "number")
													)

												)
											), 
											React.createElement("div", {className: "clearfix"}), 
											React.createElement("div", {className: "form-group showinfo", "data-name": "kill"}, 
												React.createElement("label", {className: "col-md-2 control-label", htmlFor: "kill"}, "kill"), 
												React.createElement("div", {className: "col-md-5"}, 
													React.createElement("div", {className: "input-group"}, 
														React.createElement("input", {id: "kill", name: "kill", className: "form-control", placeholder: "30000", type: "text", onChange: this.handleChange, value: this.state.form.kill}), 
														React.createElement("span", {className: "input-group-addon"}, "number")
													)
												)
											)
										)	
									), 			
									React.createElement("div", {className: "clearfix"}), 
									React.createElement("div", {className: "form-group"}, 
										React.createElement("div", {className: "col-md-offset-2 col-md-5"}, 
											React.createElement("button", {className: "btn btn-info", disabled: this.state.forming}, this.state.forming ? ' working' : this.state.form._id ? 'Update Source' : 'Add Source', this.state.forming ? React.createElement(Loader, null) : ''), 
											React.createElement("input", {type: "hidden", name: "_id", value: this.state.form._id}), 
											React.createElement("input", {type: "hidden", name: "slug", value: this.state.form.slug})
										), 
										React.createElement("div", {className: " col-md-5"}, 
											!this.state.forming && this.state.form._id ? React.createElement("button", {className: "btn btn-danger pull-right", onClick: this.deleteSource}, "Delete") : ''
											
										)
									)	
								
								)	
								
							)
						)
						
						
						
					), 
					React.createElement("div", {className: " col-sm-4 col-md-4"}, 
						 React.createElement("div", {className: " col-md-4 no-padding"}, 
							!this.state.forming && this.state.form._id ? React.createElement("button", {className: "btn btn-success", onClick: this.startSource}, React.createElement("span", {className: "glyphicon glyphicon-play", "aria-hidden": "true"}), " Start") : ''
						), 
						React.createElement("div", {className: " col-md-4 no-padding"}, 
							!this.state.forming && this.state.form._id ? React.createElement("button", {className: "btn btn-danger", onClick: this.stopSource}, React.createElement("span", {className: "glyphicon glyphicon-stop", "aria-hidden": "true"}), " Stop") : ''
						), 
						React.createElement("div", {className: " col-md-4 no-padding"}, 
							!this.state.forming && this.state.form._id ? React.createElement("button", {className: "btn btn-info", onClick: this.deleteSource}, React.createElement("span", {className: "glyphicon glyphicon-info-sign", "aria-hidden": "true"}), " Status") : ''
						), 
						React.createElement("div", {className: "clearfix"}, React.createElement("br", null), React.createElement("br", null)), 
						 this.state.currentField ? React.createElement(Alert, {style: "info", html: this.state.formInfo[this.state.currentField], dismiss: _this.dismissInfoAlert}) : ''
					)
				
				
				
				)
					
			)
			
		);
			
	},
	startSource(e) {
		e.preventDefault();
		this.props.sockets.manager(this, { manage: 'Source', action: 'start', find: {slug: this.state.form.slug}});
	},
	stopSource(e) {
		e.preventDefault();
		this.props.sockets.manager(this, {manage: 'Source', action: 'stop'});
	},
});

module.exports = Source;

},{"../common/alert":3,"../common/loader":4,"debug":17,"lodash":undefined,"moment":undefined,"react":undefined,"react-bootstrap":undefined,"react-router":undefined,"react-router-bootstrap":150,"react-select":undefined}],13:[function(require,module,exports){
var React = require('react');
var _ = require('lodash');
var Select = require('react-select');
var Link = require('react-router').Link;
var Alert = require('../common/alert.js');
var ReactBootstrap = require('react-bootstrap');
var ReactRouterBootstrap = require('react-router-bootstrap');
var ListGroupItemLink = ReactRouterBootstrap.ListGroupItemLink;
var ListGroup = ReactBootstrap.ListGroup;
var ListGroupItem = ReactBootstrap.ListGroupItem;
var moment = require('moment');
var Loader = require('../common/loader');

var	debug = require('debug')('snowstreams:client:pages:stream');


var Stream = React.createClass({displayName: "Stream",
	contextTypes: {
		router: React.PropTypes.func
	},
	getInitialState() {
		obj = { 
			sources: [],
			howmany: 0,
			programMap: {},
			programArgumentMap: {},
			streamMap: {},
			streams: [],
			active: 'program',
			programs: [],
			newalert: {},
			form: this.defaultForm(),
			currentField: false,
			formInfo: {
				name: ' <p>You will reference this stream by name in the UI.</p> <p> The name should be unique among your streams and should be easily identifiable.</p> ',
				type: '<p><b>custom</b>: Create your own.</p><p><b>/dev/null</b>: Discard the stream. You can define special /dev/nulls in Programs.</p><p><b>file</b>: Save the stream to a file.</p><p><b>pipe</b>: Placeholder entry for the ui where a source or stream will output to stdout.</p><p><b>program</b>: Run a shell program that produces the stream.  You should add your programs first from the Programs section.</p> <p><b>udp</b>: Stream over udp (iptv).',
				file: ' <p>A valid path to save a file. </p> <p> You must include the filename.</p> <p>You can include variables that can be replaced in channels or programmatically...</p> <p>//@:%PORT%/videos/%FILE%</p>',
				directory: 'You must check this to traverse a directory and select extensions to filter.',
				host: ' <p>The host for your udp stream.  Local streams can use <b>@</b></p>',
				port: ' <p>Port number for the udp stream</p>',
				destination: '<p>The filename or address you will use to consume the final stream.  Leave empty for pipe.</p>',
				event: '<p>The emitted event we will listen for to run the program</p>',
				argument: '<div><p>Select the appropriate argument line</p><p>You <b>must</b> accept an input and produce some form of output.</p><p> If there is an <b>%INPUT%</b> variable, we will subtitute the available source (usually a file or stream).  Without the <b>%INPUT%</b> variable we will pipe in the source, so you should expect <b>stdin</b>.</p><p> If there is an <b>%OUTPUT%</b> variable, the url above or from a programmatic instance will be insterted.</p><p>Without an output, the expectation is this is a program that does not output, you are piping to a channel, or have included the output in the argument.  Please use the destination field to denote the reachable address for the stream.</p> </div>', 
				stderror: '<p>Choose where to send error messages.</p>',
				output: '<p>If this program is middleware select the final stream output.</p><p> Two streams will be produced.  The named stream will contain the program\'s stdout. </p><p> The final output stream will be named <b>slug:output</b>.</p><p>For example, if your stream name is <b>local files</b>, the output will be named <b>local-files:output</b>. </p><p><b>Note</b> that the slug is a unique value the database generates from name.</p>',
				program: ' <p>Select one of your programs. </p> <p> The argument list will be supplied after you chose.</p> ',
				expose: ' <p>Create API routes to manage this stream.  </p><p>Available arguments are: <b>play</b> (<i>alias:</i> <b>start</b>) <br /><b>restart</b><br /> <b>status</b><br /> <b>stop</b> <br /> <b>view</b><br />No argument will <b>play</b>.</p><p>2 routes are created:<br/> <b>/proxy/stream/:name/:argument</b><br/> sends Content-Type=mpegts for play.</p><p><b>/proxy/stream/:name/:<i>mime-type</i></b> sends the Content-Type you set as <i>mime-type</i>.</p><p>Status will give you program information.  Currently that is limited to the filename if available, codec information if available, start time and expected stop time. </p>',

			}, 
		}
		return obj;
	},
	defaultForm() {
		return {
			name: '',
			argument: ''
		}
	},
	componentDidMount() {
		_this = this;
		this.getStreams();
		this.checkProps(this.props);
		$(document).on('focus','#Streamdiv .showinfo', function(e) {
			_this.showInfo(e);
		});
	},
	checkProps(props) {
		var _this = this;
		if(props.params.slug === 'new') {
			this.setState({ 
				form: _this.defaultForm(),
				newalert: { show: false },
				currentField: false
			});
			return;
		} else if(props.params.slug) {
			this.getStream(props.params.slug);
			return;
		}
	},
	componentWillReceiveProps(nextProps) {
		var sameQuery = this.props.params.slug === nextProps.params.slug;
		if(!sameQuery && this.props.params.slug !== '') {
			this.checkProps(nextProps);
		}
		return;
	},
	componentDidUpdate: function() {
		
	},
	logChange: function (val) {
		debug("Selected: " + val);
	},
	getStreams() {
		var _this = this;
		var myFn2 = function(data) {
			var send = _.isArray(data.data.programs) ? data.data.programs : [];
			var programMap = {};
			var programArgumentMap = {};
			_.each(send, function(v) {
				programMap[v._id] = v;
				_.each(v.arguments, function(a) {
					programArgumentMap[a._id] = a;
				});
			});
			
			_this.setState({programs: send, programMap: programMap, programArgumentMap: programArgumentMap});
		}
		var myFn3 = function(data) {
			var send3 = _.isArray(data.data.streams) ? data.data.streams : [];
			var streamMap = {};
			_.each(send3, function(v) {
				streamMap[v._id] = v;
			});
			_this.setState({streams: send3, streamMap: streamMap});
		}

		this.props.sockets.list('Program', {sort:{name:1}, populate:'arguments'}, myFn2);
		this.props.sockets.list('Stream', {sort:{type: 1, name:1}, populate:'argument, program, stderror'}, myFn3);
	},
	getStream(slug) {
		var _this = this;
		_this.setState({ form: _this.defaultForm() });
		var myFn = function(data) {
			// We got an error
			if(data.error) {
				_this.setState({ 
					newalert: {
						style: 'danger',
						data: data,
						show: true
					},
					currentField: 'error'
				});
			// we got a result that may contain a list
			} else if(_.isObject(data.data)) {
				
				var send = _.isArray(data.data.streams) ? data.data.streams[0] : _.isObject(data.data.streams) ? data.data.streams : {};
				var name = _.isObject(send.updatedBy.name) ?  send.updatedBy.name.first + '  ' + send.updatedBy.name.last: ' Unknown';
				var pos = false;
				_.each(_this.state.programs, function(o,k) {
					if(o._id === send.program) pos = k;
				});
				_this.setState({ 
					form: send,
					programPos: pos || 0,
					currentField: false,
					newalert: {
						style: 'info',
						component: (React.createElement("div", null, "Updated last on ", moment(send.updatedAt).format('MMMM Do YYYY, h:mm:ss a'), " by ", name)),
						show: true
					}
				});
			// something is wrong
			} else {
				_this.setState({ 
					currentField: 'error',
					newalert: {
						style: 'danger',
						html: 'Error. Please check console',
						show: true
					},
				});
			}
		} // end myFn

		var opts = {  
			exclude: '__v',
			slug: slug,
			sort: 'order',
			populate: 'arguments updatedBy',
		};
		this.props.sockets.getDocument('Stream', opts, myFn);
	},
	dismissAlert() {
		this.setState({ 
			newalert: {
				show: false
			}
		});
	},
	dismissInfoAlert() {
		this.setState({ 
			currentField: false
		});
	},			
	submitForm(e) {
		e.preventDefault();
		 var _this = this;
		 var reload = (_this.state.form._id) ? true : false;
		 var go = (_this.state.form._id) ? 'update' : 'create';
		 var domany = false;
		 var name;
		 delete _this.state.form.createdBy;
		 delete _this.state.form.updatedBy;
		 delete _this.state.form.createdAt;
		 delete _this.state.form.updatedAt;
		 if(_this.state.howmany && !_this.state.form._id && _this.state.form.type === 'udp') {
			// clone form and create a fake setState
			var cloning = _.clone(_this.state.form);
			var cloned = {
				state: {
					form: cloning
				}
			}
			cloned.state.form.port = parseFloat(cloned.state.form.port);
			cloned.setState = function(fake){};
			cloned.dismissAlert = function(fake){};
			cloned.defaultForm = function(fake){};
			domany = true;
			name = _this.state.form.name;
		}
		 this.props.sockets.submitForm(this, e, go, 'Stream', function(err, data) {
			_this.getStreams();
			if(!err) _this.getStream(data.data.streams.slug);
			debug('add more', domany, _this.state.howmany)
			if(domany) {
				for(i=0;i<_this.state.howmany;i++) {
					cloned.state.form.port++;
					cloned.state.form.name = name + ':' + cloned.state.form.port;
					debug('add many', cloned, i)
					_this.props.sockets.submitForm(cloned, e, 'create', 'Stream',_this.getStreams);
				}
			}
		 });
	},
	handleChange(val, who) {
		var send = {form: this.state.form}
		var form = send.form;
		if(who === 'howmany') {
			this.setState({ howmany: val.value});
			return;			
		} else if(who){
			var name = who;
			var value = _.isArray(val) ? val : _.isObject(val) ? val.value : '';
			if(who === 'program') {
				send.programPos = parseFloat(val.pos);
				form.argument = ''
			}
		} else {
			var name = val.target.name;
			var value = val.target.value
		}
		form[name] = value;
		debug('send', send, val, who);	
		this.setState(send);
    },
    showInfo(e) {
		debug(e);
		var name = e.currentTarget.dataset.name;
		if(name.search(':') !== -1) {
			name = 'arguments'
		}
		this.setState({currentField: name});
    },
    deleteStream(e) {
		e.preventDefault();
		this.props.sockets.deleteDocument(this, e, 'Stream'); 
	},
	reallyDelete() {
		var _this = this;
		this.props.sockets.reallyDelete(this, null, 'Stream', function(err, data) {
			if(!err) {
				_this.getStreams();
				if(_this.state.form._id) _this.getStream(data.data.streams.slug);
			}
		});
	},
	render: function() {
		debug(this.state,  this.context.router.getCurrentPathname());	
		var _this = this;
		var typeOptions = [
			{ value: 'custom', label: 'custom' },
			{ value: 'file', label: 'file' },
			{ value: 'pipe', label: 'pipe' },
			{ value: 'program', label: 'program' },
			{ value: 'udp', label: 'udp' } 
		];
		var type = React.createElement(Select, {
			name: "type", 
			options: typeOptions, 
			onChange: function(val, obj) {
				_this.handleChange(obj[0], 'type')
			}, 
			value: this.state.form.type}
		)
					
		var programOptions = this.state.programs.map(function(v, k) {
			return ({ value: v._id, label: v.name, pos: k });
		});
		var programs = React.createElement(Select, {
			name: "program", 
			allowCreate: false, 
			options: programOptions, 
			onChange: function(val, obj) {
				_this.handleChange(obj[0], 'program')
			}, 
			value: this.state.form.program}
			
		)
		var udpOptions = [{value: 0, label: 'Single stream only'}];
		for(var i=0;i<101;i++) {
			udpOptions.push({value: i, label: i});
		}
		var udpoptions = React.createElement(Select, {
			name: "howmany", 
			allowCreate: true, 
			options: udpOptions, 
			onChange: function(val, obj) {
				_this.handleChange(obj[0], 'howmany')
			}, 
			value: this.state.howmany}
		)	
		var arguments = React.createElement("span", null)
		if(this.state.form.argument !== '' || this.state.programPos > -1) {
			
			var programArguments = this.state.programs[this.state.programPos].arguments.map(function(v, k) {
				return ({ value: v._id, label: v.anchor + ' : ' + v.command });
			});
			
			arguments = React.createElement(Select, {
				name: "argument", 
				options: programArguments, 
				onChange: function(val, obj) {
					_this.handleChange(obj[0], 'argument')
				}, 
				value: this.state.form.argument}
			)
			
			var streamOptions = this.state.streams.map(function(v, k) {
				return ({ value: v._id, label: v.name });
			});
			var stream = React.createElement(Select, {
				name: "stream", 
				allowCreate: false, 
				options: streamOptions, 
				onChange: function(val, obj) {
					_this.handleChange(obj[0], 'stream')
				}, 
				value: this.state.form.stream}
			)	
			
		}
		
		var stderrorOptions = [];
		_.each(this.state.programs, function(o) {
				if(o.type === 'internal') {
					_.each(o.arguments, function(a) {
						stderrorOptions.push({label: _this.state.programArgumentMap[a._id].command, value: _this.state.programArgumentMap[a._id]._id});
					})
				}
		});
		var stderror = React.createElement(Select, {
			name: "stderror", 
			options: stderrorOptions, 
			multi: true, 
			onChange: function(val, obj) {
				_this.handleChange(val.split(','), 'stderror')
			}, 
			value: this.state.form.stderror}
		)
		
		var t;
		var listItems = this.state.streams.map(function(v) {
			if(t !== v.type) {
				t = v.type;
				return (React.createElement(ListGroupItemLink, {header: v.type, bsSize: "small", to: "manage-stream", params: { slug: v.slug}, key: v.slug+'-key'}, v.name))
			} else {
				return (React.createElement(ListGroupItemLink, {bsSize: "small", to: "manage-stream", params: { slug: v.slug}, key: v.slug+'-key'}, v.name))
			}
		});
		if(listItems.length === 0) {
			listItems.push(React.createElement(ListGroupItem, {bsSize: "xlarge", key: 'loading-key'}, React.createElement("div", {className: "clearfix"}, React.createElement("div", {className: "", style: {float:'right'}}, React.createElement(Loader, null)))));
		}
		var list = (
			React.createElement(ListGroup, null, 
				React.createElement(ListGroupItemLink, {to: "manage-stream", params: { slug: 'new'}}, "New Stream"), 
				listItems
			)
		);
		return (
			
			React.createElement("div", {id: "Streamdiv", className: " "}, 
				
				React.createElement("div", {className: "header-streams"}, "Streams"), 
				
				React.createElement("div", {className: "clearfix"}), 
				React.createElement("div", {className: "menuitem col1 col-md-2"}, 
					list
				), 
				React.createElement("div", {className: "col2 col-sm-10 col-md-10 no-padding"}, 
					this.state.newalert.show ? React.createElement(Alert, {style: this.state.newalert.style, html: this.state.newalert.html, data: this.state.newalert.data, component: this.state.newalert.component, dismiss: _this.dismissAlert}) : '', 
					React.createElement("div", {className: "col-sm-8 col-md-8"}, 
						
						React.createElement("form", {className: "form-horizontal", onSubmit: this.submitForm}, 	
							React.createElement("fieldset", null, 			
		 						
								React.createElement("div", {className: "form-group showinfo", "data-name": "name"}, 
									React.createElement("label", {className: "col-md-2 control-label", htmlFor: "name"}, "name"), 
									React.createElement("div", {className: "col-md-10"}, 
										React.createElement("input", {id: "name", name: "name", type: "text", placeholder: "cable", className: "form-control input-md", onChange: this.handleChange, value: this.state.form.name})
										
									)
								), 
								
								React.createElement("div", {className: "clearfix"}), 
								React.createElement("div", {ref: "show", className: this.state.form.name !== '' ? 'fatalbert' : 'casper'}, 
								
									React.createElement("div", {className: "clearfix"}), 
									React.createElement("div", {className: "form-group showinfo", "data-name": "expose"}, 
										React.createElement("label", {className: "col-md-2 control-label", htmlFor: "expose"}, "api"), 
										React.createElement("div", {className: "col-md-10"}, 
											React.createElement("div", {className: "checkbox"}, 
												
												React.createElement("label", {htmlFor: "expose-0"}, 
													React.createElement("input", {type: "checkbox", name: "expose", id: "expose-0", onChange: this.handleChange, value: this.state.form.expose === 'true' || this.state.form.expose === true ? 'false' : 'true', checked: this.state.form.expose === 'true' || this.state.form.expose === true ? 'checked' : ''}), 
													this.state.form.expose === 'true' || this.state.form.expose === true ? React.createElement("div", null, React.createElement("a", {href: "/proxy/stream/"+_this.state.form.slug+"/status"}, "/proxy/stream/", _this.state.form.slug, "/status"), React.createElement("br", null), React.createElement("a", {href: "/proxy/stream/"+_this.state.form.slug+"/raw"}, "/proxy/stream/", _this.state.form.slug, "/raw")) : React.createElement("span", null, "This stream is not managed via API.")
												)
											)
										)
									), 
									
									React.createElement("div", {className: "clearfix"}), 
									React.createElement("div", {className: "form-group showinfo", "data-name": "type"}, 
										React.createElement("label", {className: "col-md-2 control-label", htmlFor: "type"}, "type"), 
										React.createElement("div", {className: "col-md-10"}, 
											type
										)
									), 
									
									React.createElement("div", {className: "clearfix"}), 
									React.createElement("div", {ref: "udp", className: this.state.form.type === 'udp' || this.state.form.type === 'custom' ? 'fatalbert' : 'casper'}, 
										
										React.createElement("div", {className: "form-group showinfo", "data-name": "host"}, 
											React.createElement("label", {className: "col-md-2 control-label", htmlFor: "host"}, "host"), 
											React.createElement("div", {className: "col-md-10"}, 
												React.createElement("input", {id: "host", name: "host", type: "text", placeholder: "@", className: "form-control input-md", onChange: this.handleChange, value: this.state.form.host})
												  
											)
										), 
										
										React.createElement("div", {className: "clearfix"}), 
										React.createElement("div", {className: "form-group  showinfo", "data-name": "port"}, 
											React.createElement("label", {className: "col-md-2 control-label", htmlFor: "port"}, "port"), 
											React.createElement("div", {className: "col-md-5"}, 
												React.createElement("div", {className: "input-group"}, 
													React.createElement("input", {id: "port", name: "port", className: "form-control", placeholder: "11000", type: "text", onChange: this.handleChange, value: this.state.form.port}), 
													React.createElement("span", {className: "input-group-addon"}, "number")
												)
											)
										), 
										
										React.createElement("div", {className: "clearfix"}), 
										React.createElement("div", {ref: "howmany", className: !this.state.form._id  ? 'fatalbert' : 'casper'}, 
											React.createElement("div", {className: "form-group showinfo", "data-name": "howmany"}, 
												React.createElement("label", {className: "col-md-2 control-label", htmlFor: "howmany"}, "create more?"), 
												React.createElement("div", {className: "col-md-10"}, 
													udpoptions, 
													React.createElement("span", {className: "help-block"}, "A udp stream is ", React.createElement("b", null, "single"), " use.  You can create multiple udp streams by selecting a number.  Each new stream will simply increase the port number by 1 and create a name using name:port.")
												)
											)
										), 
										
										React.createElement("div", {className: "clearfix"}), 
										React.createElement("div", {className: "form-group showinfo", "data-name": "listen"}, 
											React.createElement("label", {className: "col-md-2 control-label", htmlFor: "listen"}, "listen"), 
											React.createElement("div", {className: "col-md-10"}, 
												React.createElement("div", {className: "checkbox"}, 
													
													React.createElement("label", {htmlFor: "listen-0"}, 
														React.createElement("input", {type: "checkbox", name: "listen", id: "listen-0", onChange: this.handleChange, value: this.state.form.listen === 'true' || this.state.form.listen === true ? 'false' : 'true', checked: this.state.form.listen === 'true' || this.state.form.listen === true ? 'checked' : ''}), 
														this.state.form.listen === 'true' || this.state.form.listen === true ? React.createElement("span", null, "Listening for the following emitted event") : React.createElement("span", null, "This stream is not listening for commands.")
													)
												)
											)
										)									
											
									), 								
									
									React.createElement("div", {className: "clearfix"}), 
									React.createElement("div", {ref: "command", className: this.state.form.listen === true || this.state.form.listen === 'true' ? 'fatalbert' : 'casper'}, 
									
										React.createElement("div", {className: "form-group showinfo", "data-name": "event"}, 
											React.createElement("label", {className: "col-md-2 control-label", htmlFor: "event"}, "event"), 
											React.createElement("div", {className: "col-md-10"}, 
												React.createElement("input", {id: "name", name: "event", type: "text", placeholder: "change channel", className: "form-control input-md", onChange: this.handleChange, value: this.state.form.event})
												
											)
										)
									
									), 
									
									React.createElement("div", {className: "clearfix"}), 
									React.createElement("div", {ref: "others", className: this.state.form.type === 'program' || this.state.form.type === 'custom' ? 'fatalbert' : 'casper'}, 
										
										
											
										React.createElement("div", {className: "clearfix"}), 	
										React.createElement("div", {className: "form-group showinfo", "data-name": "destination"}, 
											React.createElement("label", {className: "col-md-2 control-label", htmlFor: "url"}, "destination"), 
											React.createElement("div", {className: "col-md-10"}, 
												React.createElement("input", {id: "url", name: "destination", type: "text", placeholder: "http://host:port/file.ext", className: "form-control input-md", onChange: this.handleChange, value: this.state.form.destination})
											)
										)
										
									), 
									
									React.createElement("div", {className: "clearfix"}), 
									React.createElement("div", {ref: "others", className: this.state.form.type === 'program' || this.state.form.type === 'custom' || this.state.form.listen === 'true' || this.state.form.listen === true ? 'fatalbert' : 'casper'}, 
										
										
										
										React.createElement("div", {className: "clearfix"}), 
										React.createElement("div", {className: "form-group showinfo", "data-name": "program"}, 
											React.createElement("label", {className: "col-md-2 control-label", htmlFor: "program"}, "program"), 
											React.createElement("div", {className: "col-md-10"}, 
												programs
											)
										), 
										
										React.createElement("div", {className: "clearfix"}), 
										React.createElement("div", {ref: "programthings", className: this.state.form.argument !== '' || this.state.programPos > -1 ? 'fatalbert' : 'casper'}, 
											
											React.createElement("div", {className: "form-group showinfo", "data-name": "argument"}, 
												React.createElement("label", {className: "col-md-2 control-label", htmlFor: "args"}, "arguments"), 
												React.createElement("div", {className: "col-md-10", ref: "arguments"}, 
													arguments
													
												)
											)
										)
									
									), 
									
									React.createElement("div", {className: "clearfix"}), 
									React.createElement("div", {ref: "others", className: this.state.form.type === 'program' || this.state.form.type === 'custom' ? 'fatalbert' : 'casper'}, 
										
										
										React.createElement("div", {className: "form-group showinfo", "data-name": "output"}, 
											React.createElement("label", {className: "col-md-2 control-label", htmlFor: "stream"}, "output"), 
											React.createElement("div", {className: "col-md-10"}, 
												stream
											)
										)
									), 
									
									React.createElement("div", {className: "clearfix"}), 
									React.createElement("div", {ref: "others", className: this.state.form.listen === 'true' || this.state.form.listen === true || this.state.form.type === 'custom' ? 'fatalbert' : 'casper'}, 
											
											React.createElement("div", {className: "form-group showinfo", "data-name": "stderror"}, 
												React.createElement("label", {className: "col-md-2 control-label", htmlFor: "stderror"}, "listen error"), 
												React.createElement("div", {className: "col-md-10"}, 
													stderror
												)
											)
																						
										

									), 		
									
									React.createElement("div", {className: "clearfix"}), 
									React.createElement("div", {className: "form-group"}, 
										
										React.createElement("div", {className: "col-md-offset-2 col-md-5"}, 
											React.createElement("button", {className: "btn btn-info"}, this.state.form._id ? 'Update Stream' : 'Add Stream'), 
											React.createElement("input", {type: "hidden", name: "_id", value: this.state.form._id}), 
											React.createElement("input", {type: "hidden", name: "slug", value: this.state.form.slug})
										), 
										React.createElement("div", {className: " col-md-5"}, 
											this.state.form._id ? React.createElement("button", {className: "btn btn-danger pull-right", onClick: this.deleteStream}, "Delete") : ''
											
										)
										
									)
								)	
							)
						)
					), 
					React.createElement("div", {className: " col-sm-4 col-md-4"}, 
						 React.createElement("div", {className: " col-md-4 no-padding"}, 
							!this.state.forming && this.state.form._id ? React.createElement("button", {className: "btn btn-success", onClick: this.deleteSource}, React.createElement("span", {className: "glyphicon glyphicon-play", "aria-hidden": "true"}), " Start") : ''
						), 
						React.createElement("div", {className: " col-md-4 no-padding"}, 
							!this.state.forming && this.state.form._id ? React.createElement("button", {className: "btn btn-danger", onClick: this.deleteSource}, React.createElement("span", {className: "glyphicon glyphicon-stop", "aria-hidden": "true"}), " Stop") : ''
						), 
						React.createElement("div", {className: " col-md-4 no-padding"}, 
							!this.state.forming && this.state.form._id ? React.createElement("button", {className: "btn btn-info", onClick: this.deleteSource}, React.createElement("span", {className: "glyphicon glyphicon-info-sign", "aria-hidden": "true"}), " Status") : ''
						), 
						React.createElement("div", {className: "clearfix"}, React.createElement("br", null), React.createElement("br", null)), 
						 this.state.currentField ? React.createElement(Alert, {style: "info", html: this.state.formInfo[this.state.currentField], dismiss: _this.dismissInfoAlert}) : ''
					)
				)
					
			)
			
		);
			
	}
});

module.exports = Stream;

},{"../common/alert.js":3,"../common/loader":4,"debug":17,"lodash":undefined,"moment":undefined,"react":undefined,"react-bootstrap":undefined,"react-router":undefined,"react-router-bootstrap":150,"react-select":undefined}],14:[function(require,module,exports){
(function (process,global){
/*!
 * async
 * https://github.com/caolan/async
 *
 * Copyright 2010-2014 Caolan McMahon
 * Released under the MIT license
 */
(function () {

    var async = {};
    function noop() {}
    function identity(v) {
        return v;
    }
    function toBool(v) {
        return !!v;
    }
    function notId(v) {
        return !v;
    }

    // global on the server, window in the browser
    var previous_async;

    // Establish the root object, `window` (`self`) in the browser, `global`
    // on the server, or `this` in some virtual machines. We use `self`
    // instead of `window` for `WebWorker` support.
    var root = typeof self === 'object' && self.self === self && self ||
            typeof global === 'object' && global.global === global && global ||
            this;

    if (root != null) {
        previous_async = root.async;
    }

    async.noConflict = function () {
        root.async = previous_async;
        return async;
    };

    function only_once(fn) {
        return function() {
            if (fn === null) throw new Error("Callback was already called.");
            fn.apply(this, arguments);
            fn = null;
        };
    }

    function _once(fn) {
        return function() {
            if (fn === null) return;
            fn.apply(this, arguments);
            fn = null;
        };
    }

    //// cross-browser compatiblity functions ////

    var _toString = Object.prototype.toString;

    var _isArray = Array.isArray || function (obj) {
        return _toString.call(obj) === '[object Array]';
    };

    // Ported from underscore.js isObject
    var _isObject = function(obj) {
        var type = typeof obj;
        return type === 'function' || type === 'object' && !!obj;
    };

    function _isArrayLike(arr) {
        return _isArray(arr) || (
            // has a positive integer length property
            typeof arr.length === "number" &&
            arr.length >= 0 &&
            arr.length % 1 === 0
        );
    }

    function _each(coll, iterator) {
        return _isArrayLike(coll) ?
            _arrayEach(coll, iterator) :
            _forEachOf(coll, iterator);
    }

    function _arrayEach(arr, iterator) {
        var index = -1,
            length = arr.length;

        while (++index < length) {
            iterator(arr[index], index, arr);
        }
    }

    function _map(arr, iterator) {
        var index = -1,
            length = arr.length,
            result = Array(length);

        while (++index < length) {
            result[index] = iterator(arr[index], index, arr);
        }
        return result;
    }

    function _range(count) {
        return _map(Array(count), function (v, i) { return i; });
    }

    function _reduce(arr, iterator, memo) {
        _arrayEach(arr, function (x, i, a) {
            memo = iterator(memo, x, i, a);
        });
        return memo;
    }

    function _forEachOf(object, iterator) {
        _arrayEach(_keys(object), function (key) {
            iterator(object[key], key);
        });
    }

    function _indexOf(arr, item) {
        for (var i = 0; i < arr.length; i++) {
            if (arr[i] === item) return i;
        }
        return -1;
    }

    var _keys = Object.keys || function (obj) {
        var keys = [];
        for (var k in obj) {
            if (obj.hasOwnProperty(k)) {
                keys.push(k);
            }
        }
        return keys;
    };

    function _keyIterator(coll) {
        var i = -1;
        var len;
        var keys;
        if (_isArrayLike(coll)) {
            len = coll.length;
            return function next() {
                i++;
                return i < len ? i : null;
            };
        } else {
            keys = _keys(coll);
            len = keys.length;
            return function next() {
                i++;
                return i < len ? keys[i] : null;
            };
        }
    }

    // Similar to ES6's rest param (http://ariya.ofilabs.com/2013/03/es6-and-rest-parameter.html)
    // This accumulates the arguments passed into an array, after a given index.
    // From underscore.js (https://github.com/jashkenas/underscore/pull/2140).
    function _restParam(func, startIndex) {
        startIndex = startIndex == null ? func.length - 1 : +startIndex;
        return function() {
            var length = Math.max(arguments.length - startIndex, 0);
            var rest = Array(length);
            for (var index = 0; index < length; index++) {
                rest[index] = arguments[index + startIndex];
            }
            switch (startIndex) {
                case 0: return func.call(this, rest);
                case 1: return func.call(this, arguments[0], rest);
            }
            // Currently unused but handle cases outside of the switch statement:
            // var args = Array(startIndex + 1);
            // for (index = 0; index < startIndex; index++) {
            //     args[index] = arguments[index];
            // }
            // args[startIndex] = rest;
            // return func.apply(this, args);
        };
    }

    function _withoutIndex(iterator) {
        return function (value, index, callback) {
            return iterator(value, callback);
        };
    }

    //// exported async module functions ////

    //// nextTick implementation with browser-compatible fallback ////

    // capture the global reference to guard against fakeTimer mocks
    var _setImmediate = typeof setImmediate === 'function' && setImmediate;

    var _delay = _setImmediate ? function(fn) {
        // not a direct alias for IE10 compatibility
        _setImmediate(fn);
    } : function(fn) {
        setTimeout(fn, 0);
    };

    if (typeof process === 'object' && typeof process.nextTick === 'function') {
        async.nextTick = process.nextTick;
    } else {
        async.nextTick = _delay;
    }
    async.setImmediate = _setImmediate ? _delay : async.nextTick;


    async.forEach =
    async.each = function (arr, iterator, callback) {
        return async.eachOf(arr, _withoutIndex(iterator), callback);
    };

    async.forEachSeries =
    async.eachSeries = function (arr, iterator, callback) {
        return async.eachOfSeries(arr, _withoutIndex(iterator), callback);
    };


    async.forEachLimit =
    async.eachLimit = function (arr, limit, iterator, callback) {
        return _eachOfLimit(limit)(arr, _withoutIndex(iterator), callback);
    };

    async.forEachOf =
    async.eachOf = function (object, iterator, callback) {
        callback = _once(callback || noop);
        object = object || [];
        var size = _isArrayLike(object) ? object.length : _keys(object).length;
        var completed = 0;
        if (!size) {
            return callback(null);
        }
        _each(object, function (value, key) {
            iterator(object[key], key, only_once(done));
        });
        function done(err) {
            if (err) {
                callback(err);
            }
            else {
                completed += 1;
                if (completed >= size) {
                    callback(null);
                }
            }
        }
    };

    async.forEachOfSeries =
    async.eachOfSeries = function (obj, iterator, callback) {
        callback = _once(callback || noop);
        obj = obj || [];
        var nextKey = _keyIterator(obj);
        var key = nextKey();
        function iterate() {
            var sync = true;
            if (key === null) {
                return callback(null);
            }
            iterator(obj[key], key, only_once(function (err) {
                if (err) {
                    callback(err);
                }
                else {
                    key = nextKey();
                    if (key === null) {
                        return callback(null);
                    } else {
                        if (sync) {
                            async.nextTick(iterate);
                        } else {
                            iterate();
                        }
                    }
                }
            }));
            sync = false;
        }
        iterate();
    };



    async.forEachOfLimit =
    async.eachOfLimit = function (obj, limit, iterator, callback) {
        _eachOfLimit(limit)(obj, iterator, callback);
    };

    function _eachOfLimit(limit) {

        return function (obj, iterator, callback) {
            callback = _once(callback || noop);
            obj = obj || [];
            var nextKey = _keyIterator(obj);
            if (limit <= 0) {
                return callback(null);
            }
            var done = false;
            var running = 0;
            var errored = false;

            (function replenish () {
                if (done && running <= 0) {
                    return callback(null);
                }

                while (running < limit && !errored) {
                    var key = nextKey();
                    if (key === null) {
                        done = true;
                        if (running <= 0) {
                            callback(null);
                        }
                        return;
                    }
                    running += 1;
                    iterator(obj[key], key, only_once(function (err) {
                        running -= 1;
                        if (err) {
                            callback(err);
                            errored = true;
                        }
                        else {
                            replenish();
                        }
                    }));
                }
            })();
        };
    }


    function doParallel(fn) {
        return function (obj, iterator, callback) {
            return fn(async.eachOf, obj, iterator, callback);
        };
    }
    function doParallelLimit(fn) {
        return function (obj, limit, iterator, callback) {
            return fn(_eachOfLimit(limit), obj, iterator, callback);
        };
    }
    function doSeries(fn) {
        return function (obj, iterator, callback) {
            return fn(async.eachOfSeries, obj, iterator, callback);
        };
    }

    function _asyncMap(eachfn, arr, iterator, callback) {
        callback = _once(callback || noop);
        var results = [];
        eachfn(arr, function (value, index, callback) {
            iterator(value, function (err, v) {
                results[index] = v;
                callback(err);
            });
        }, function (err) {
            callback(err, results);
        });
    }

    async.map = doParallel(_asyncMap);
    async.mapSeries = doSeries(_asyncMap);
    async.mapLimit = doParallelLimit(_asyncMap);

    // reduce only has a series version, as doing reduce in parallel won't
    // work in many situations.
    async.inject =
    async.foldl =
    async.reduce = function (arr, memo, iterator, callback) {
        async.eachOfSeries(arr, function (x, i, callback) {
            iterator(memo, x, function (err, v) {
                memo = v;
                callback(err);
            });
        }, function (err) {
            callback(err || null, memo);
        });
    };

    async.foldr =
    async.reduceRight = function (arr, memo, iterator, callback) {
        var reversed = _map(arr, identity).reverse();
        async.reduce(reversed, memo, iterator, callback);
    };

    function _filter(eachfn, arr, iterator, callback) {
        var results = [];
        eachfn(arr, function (x, index, callback) {
            iterator(x, function (v) {
                if (v) {
                    results.push({index: index, value: x});
                }
                callback();
            });
        }, function () {
            callback(_map(results.sort(function (a, b) {
                return a.index - b.index;
            }), function (x) {
                return x.value;
            }));
        });
    }

    async.select =
    async.filter = doParallel(_filter);

    async.selectLimit =
    async.filterLimit = doParallelLimit(_filter);

    async.selectSeries =
    async.filterSeries = doSeries(_filter);

    function _reject(eachfn, arr, iterator, callback) {
        _filter(eachfn, arr, function(value, cb) {
            iterator(value, function(v) {
                cb(!v);
            });
        }, callback);
    }
    async.reject = doParallel(_reject);
    async.rejectLimit = doParallelLimit(_reject);
    async.rejectSeries = doSeries(_reject);

    function _createTester(eachfn, check, getResult) {
        return function(arr, limit, iterator, cb) {
            function done() {
                if (cb) cb(getResult(false, void 0));
            }
            function iteratee(x, _, callback) {
                if (!cb) return callback();
                iterator(x, function (v) {
                    if (cb && check(v)) {
                        cb(getResult(true, x));
                        cb = iterator = false;
                    }
                    callback();
                });
            }
            if (arguments.length > 3) {
                eachfn(arr, limit, iteratee, done);
            } else {
                cb = iterator;
                iterator = limit;
                eachfn(arr, iteratee, done);
            }
        };
    }

    async.any =
    async.some = _createTester(async.eachOf, toBool, identity);

    async.someLimit = _createTester(async.eachOfLimit, toBool, identity);

    async.all =
    async.every = _createTester(async.eachOf, notId, notId);

    async.everyLimit = _createTester(async.eachOfLimit, notId, notId);

    function _findGetResult(v, x) {
        return x;
    }
    async.detect = _createTester(async.eachOf, identity, _findGetResult);
    async.detectSeries = _createTester(async.eachOfSeries, identity, _findGetResult);
    async.detectLimit = _createTester(async.eachOfLimit, identity, _findGetResult);

    async.sortBy = function (arr, iterator, callback) {
        async.map(arr, function (x, callback) {
            iterator(x, function (err, criteria) {
                if (err) {
                    callback(err);
                }
                else {
                    callback(null, {value: x, criteria: criteria});
                }
            });
        }, function (err, results) {
            if (err) {
                return callback(err);
            }
            else {
                callback(null, _map(results.sort(comparator), function (x) {
                    return x.value;
                }));
            }

        });

        function comparator(left, right) {
            var a = left.criteria, b = right.criteria;
            return a < b ? -1 : a > b ? 1 : 0;
        }
    };

    async.auto = function (tasks, callback) {
        callback = _once(callback || noop);
        var keys = _keys(tasks);
        var remainingTasks = keys.length;
        if (!remainingTasks) {
            return callback(null);
        }

        var results = {};

        var listeners = [];
        function addListener(fn) {
            listeners.unshift(fn);
        }
        function removeListener(fn) {
            var idx = _indexOf(listeners, fn);
            if (idx >= 0) listeners.splice(idx, 1);
        }
        function taskComplete() {
            remainingTasks--;
            _arrayEach(listeners.slice(0), function (fn) {
                fn();
            });
        }

        addListener(function () {
            if (!remainingTasks) {
                callback(null, results);
            }
        });

        _arrayEach(keys, function (k) {
            var task = _isArray(tasks[k]) ? tasks[k]: [tasks[k]];
            var taskCallback = _restParam(function(err, args) {
                if (args.length <= 1) {
                    args = args[0];
                }
                if (err) {
                    var safeResults = {};
                    _forEachOf(results, function(val, rkey) {
                        safeResults[rkey] = val;
                    });
                    safeResults[k] = args;
                    callback(err, safeResults);
                }
                else {
                    results[k] = args;
                    async.setImmediate(taskComplete);
                }
            });
            var requires = task.slice(0, task.length - 1);
            // prevent dead-locks
            var len = requires.length;
            var dep;
            while (len--) {
                if (!(dep = tasks[requires[len]])) {
                    throw new Error('Has inexistant dependency');
                }
                if (_isArray(dep) && _indexOf(dep, k) >= 0) {
                    throw new Error('Has cyclic dependencies');
                }
            }
            function ready() {
                return _reduce(requires, function (a, x) {
                    return (a && results.hasOwnProperty(x));
                }, true) && !results.hasOwnProperty(k);
            }
            if (ready()) {
                task[task.length - 1](taskCallback, results);
            }
            else {
                addListener(listener);
            }
            function listener() {
                if (ready()) {
                    removeListener(listener);
                    task[task.length - 1](taskCallback, results);
                }
            }
        });
    };



    async.retry = function(times, task, callback) {
        var DEFAULT_TIMES = 5;
        var DEFAULT_INTERVAL = 0;

        var attempts = [];

        var opts = {
            times: DEFAULT_TIMES,
            interval: DEFAULT_INTERVAL
        };

        function parseTimes(acc, t){
            if(typeof t === 'number'){
                acc.times = parseInt(t, 10) || DEFAULT_TIMES;
            } else if(typeof t === 'object'){
                acc.times = parseInt(t.times, 10) || DEFAULT_TIMES;
                acc.interval = parseInt(t.interval, 10) || DEFAULT_INTERVAL;
            } else {
                throw new Error('Unsupported argument type for \'times\': ' + typeof t);
            }
        }

        var length = arguments.length;
        if (length < 1 || length > 3) {
            throw new Error('Invalid arguments - must be either (task), (task, callback), (times, task) or (times, task, callback)');
        } else if (length <= 2 && typeof times === 'function') {
            callback = task;
            task = times;
        }
        if (typeof times !== 'function') {
            parseTimes(opts, times);
        }
        opts.callback = callback;
        opts.task = task;

        function wrappedTask(wrappedCallback, wrappedResults) {
            function retryAttempt(task, finalAttempt) {
                return function(seriesCallback) {
                    task(function(err, result){
                        seriesCallback(!err || finalAttempt, {err: err, result: result});
                    }, wrappedResults);
                };
            }

            function retryInterval(interval){
                return function(seriesCallback){
                    setTimeout(function(){
                        seriesCallback(null);
                    }, interval);
                };
            }

            while (opts.times) {

                var finalAttempt = !(opts.times-=1);
                attempts.push(retryAttempt(opts.task, finalAttempt));
                if(!finalAttempt && opts.interval > 0){
                    attempts.push(retryInterval(opts.interval));
                }
            }

            async.series(attempts, function(done, data){
                data = data[data.length - 1];
                (wrappedCallback || opts.callback)(data.err, data.result);
            });
        }

        // If a callback is passed, run this as a controll flow
        return opts.callback ? wrappedTask() : wrappedTask;
    };

    async.waterfall = function (tasks, callback) {
        callback = _once(callback || noop);
        if (!_isArray(tasks)) {
            var err = new Error('First argument to waterfall must be an array of functions');
            return callback(err);
        }
        if (!tasks.length) {
            return callback();
        }
        function wrapIterator(iterator) {
            return _restParam(function (err, args) {
                if (err) {
                    callback.apply(null, [err].concat(args));
                }
                else {
                    var next = iterator.next();
                    if (next) {
                        args.push(wrapIterator(next));
                    }
                    else {
                        args.push(callback);
                    }
                    ensureAsync(iterator).apply(null, args);
                }
            });
        }
        wrapIterator(async.iterator(tasks))();
    };

    function _parallel(eachfn, tasks, callback) {
        callback = callback || noop;
        var results = _isArrayLike(tasks) ? [] : {};

        eachfn(tasks, function (task, key, callback) {
            task(_restParam(function (err, args) {
                if (args.length <= 1) {
                    args = args[0];
                }
                results[key] = args;
                callback(err);
            }));
        }, function (err) {
            callback(err, results);
        });
    }

    async.parallel = function (tasks, callback) {
        _parallel(async.eachOf, tasks, callback);
    };

    async.parallelLimit = function(tasks, limit, callback) {
        _parallel(_eachOfLimit(limit), tasks, callback);
    };

    async.series = function(tasks, callback) {
        _parallel(async.eachOfSeries, tasks, callback);
    };

    async.iterator = function (tasks) {
        function makeCallback(index) {
            function fn() {
                if (tasks.length) {
                    tasks[index].apply(null, arguments);
                }
                return fn.next();
            }
            fn.next = function () {
                return (index < tasks.length - 1) ? makeCallback(index + 1): null;
            };
            return fn;
        }
        return makeCallback(0);
    };

    async.apply = _restParam(function (fn, args) {
        return _restParam(function (callArgs) {
            return fn.apply(
                null, args.concat(callArgs)
            );
        });
    });

    function _concat(eachfn, arr, fn, callback) {
        var result = [];
        eachfn(arr, function (x, index, cb) {
            fn(x, function (err, y) {
                result = result.concat(y || []);
                cb(err);
            });
        }, function (err) {
            callback(err, result);
        });
    }
    async.concat = doParallel(_concat);
    async.concatSeries = doSeries(_concat);

    async.whilst = function (test, iterator, callback) {
        callback = callback || noop;
        if (test()) {
            var next = _restParam(function(err, args) {
                if (err) {
                    callback(err);
                } else if (test.apply(this, args)) {
                    iterator(next);
                } else {
                    callback(null);
                }
            });
            iterator(next);
        } else {
            callback(null);
        }
    };

    async.doWhilst = function (iterator, test, callback) {
        var calls = 0;
        return async.whilst(function() {
            return ++calls <= 1 || test.apply(this, arguments);
        }, iterator, callback);
    };

    async.until = function (test, iterator, callback) {
        return async.whilst(function() {
            return !test.apply(this, arguments);
        }, iterator, callback);
    };

    async.doUntil = function (iterator, test, callback) {
        return async.doWhilst(iterator, function() {
            return !test.apply(this, arguments);
        }, callback);
    };

    async.during = function (test, iterator, callback) {
        callback = callback || noop;

        var next = _restParam(function(err, args) {
            if (err) {
                callback(err);
            } else {
                args.push(check);
                test.apply(this, args);
            }
        });

        var check = function(err, truth) {
            if (err) {
                callback(err);
            } else if (truth) {
                iterator(next);
            } else {
                callback(null);
            }
        };

        test(check);
    };

    async.doDuring = function (iterator, test, callback) {
        var calls = 0;
        async.during(function(next) {
            if (calls++ < 1) {
                next(null, true);
            } else {
                test.apply(this, arguments);
            }
        }, iterator, callback);
    };

    function _queue(worker, concurrency, payload) {
        if (concurrency == null) {
            concurrency = 1;
        }
        else if(concurrency === 0) {
            throw new Error('Concurrency must not be zero');
        }
        function _insert(q, data, pos, callback) {
            if (callback != null && typeof callback !== "function") {
                throw new Error("task callback must be a function");
            }
            q.started = true;
            if (!_isArray(data)) {
                data = [data];
            }
            if(data.length === 0 && q.idle()) {
                // call drain immediately if there are no tasks
                return async.setImmediate(function() {
                    q.drain();
                });
            }
            _arrayEach(data, function(task) {
                var item = {
                    data: task,
                    callback: callback || noop
                };

                if (pos) {
                    q.tasks.unshift(item);
                } else {
                    q.tasks.push(item);
                }

                if (q.tasks.length === q.concurrency) {
                    q.saturated();
                }
            });
            async.setImmediate(q.process);
        }
        function _next(q, tasks) {
            return function(){
                workers -= 1;
                var args = arguments;
                _arrayEach(tasks, function (task) {
                    task.callback.apply(task, args);
                });
                if (q.tasks.length + workers === 0) {
                    q.drain();
                }
                q.process();
            };
        }

        var workers = 0;
        var q = {
            tasks: [],
            concurrency: concurrency,
            payload: payload,
            saturated: noop,
            empty: noop,
            drain: noop,
            started: false,
            paused: false,
            push: function (data, callback) {
                _insert(q, data, false, callback);
            },
            kill: function () {
                q.drain = noop;
                q.tasks = [];
            },
            unshift: function (data, callback) {
                _insert(q, data, true, callback);
            },
            process: function () {
                if (!q.paused && workers < q.concurrency && q.tasks.length) {
                    while(workers < q.concurrency && q.tasks.length){
                        var tasks = q.payload ?
                            q.tasks.splice(0, q.payload) :
                            q.tasks.splice(0, q.tasks.length);

                        var data = _map(tasks, function (task) {
                            return task.data;
                        });

                        if (q.tasks.length === 0) {
                            q.empty();
                        }
                        workers += 1;
                        var cb = only_once(_next(q, tasks));
                        worker(data, cb);
                    }
                }
            },
            length: function () {
                return q.tasks.length;
            },
            running: function () {
                return workers;
            },
            idle: function() {
                return q.tasks.length + workers === 0;
            },
            pause: function () {
                q.paused = true;
            },
            resume: function () {
                if (q.paused === false) { return; }
                q.paused = false;
                var resumeCount = Math.min(q.concurrency, q.tasks.length);
                // Need to call q.process once per concurrent
                // worker to preserve full concurrency after pause
                for (var w = 1; w <= resumeCount; w++) {
                    async.setImmediate(q.process);
                }
            }
        };
        return q;
    }

    async.queue = function (worker, concurrency) {
        var q = _queue(function (items, cb) {
            worker(items[0], cb);
        }, concurrency, 1);

        return q;
    };

    async.priorityQueue = function (worker, concurrency) {

        function _compareTasks(a, b){
            return a.priority - b.priority;
        }

        function _binarySearch(sequence, item, compare) {
            var beg = -1,
                end = sequence.length - 1;
            while (beg < end) {
                var mid = beg + ((end - beg + 1) >>> 1);
                if (compare(item, sequence[mid]) >= 0) {
                    beg = mid;
                } else {
                    end = mid - 1;
                }
            }
            return beg;
        }

        function _insert(q, data, priority, callback) {
            if (callback != null && typeof callback !== "function") {
                throw new Error("task callback must be a function");
            }
            q.started = true;
            if (!_isArray(data)) {
                data = [data];
            }
            if(data.length === 0) {
                // call drain immediately if there are no tasks
                return async.setImmediate(function() {
                    q.drain();
                });
            }
            _arrayEach(data, function(task) {
                var item = {
                    data: task,
                    priority: priority,
                    callback: typeof callback === 'function' ? callback : noop
                };

                q.tasks.splice(_binarySearch(q.tasks, item, _compareTasks) + 1, 0, item);

                if (q.tasks.length === q.concurrency) {
                    q.saturated();
                }
                async.setImmediate(q.process);
            });
        }

        // Start with a normal queue
        var q = async.queue(worker, concurrency);

        // Override push to accept second parameter representing priority
        q.push = function (data, priority, callback) {
            _insert(q, data, priority, callback);
        };

        // Remove unshift function
        delete q.unshift;

        return q;
    };

    async.cargo = function (worker, payload) {
        return _queue(worker, 1, payload);
    };

    function _console_fn(name) {
        return _restParam(function (fn, args) {
            fn.apply(null, args.concat([_restParam(function (err, args) {
                if (typeof console === 'object') {
                    if (err) {
                        if (console.error) {
                            console.error(err);
                        }
                    }
                    else if (console[name]) {
                        _arrayEach(args, function (x) {
                            console[name](x);
                        });
                    }
                }
            })]));
        });
    }
    async.log = _console_fn('log');
    async.dir = _console_fn('dir');
    /*async.info = _console_fn('info');
    async.warn = _console_fn('warn');
    async.error = _console_fn('error');*/

    async.memoize = function (fn, hasher) {
        var memo = {};
        var queues = {};
        hasher = hasher || identity;
        var memoized = _restParam(function memoized(args) {
            var callback = args.pop();
            var key = hasher.apply(null, args);
            if (key in memo) {
                async.nextTick(function () {
                    callback.apply(null, memo[key]);
                });
            }
            else if (key in queues) {
                queues[key].push(callback);
            }
            else {
                queues[key] = [callback];
                fn.apply(null, args.concat([_restParam(function (args) {
                    memo[key] = args;
                    var q = queues[key];
                    delete queues[key];
                    for (var i = 0, l = q.length; i < l; i++) {
                        q[i].apply(null, args);
                    }
                })]));
            }
        });
        memoized.memo = memo;
        memoized.unmemoized = fn;
        return memoized;
    };

    async.unmemoize = function (fn) {
        return function () {
            return (fn.unmemoized || fn).apply(null, arguments);
        };
    };

    function _times(mapper) {
        return function (count, iterator, callback) {
            mapper(_range(count), iterator, callback);
        };
    }

    async.times = _times(async.map);
    async.timesSeries = _times(async.mapSeries);
    async.timesLimit = function (count, limit, iterator, callback) {
        return async.mapLimit(_range(count), limit, iterator, callback);
    };

    async.seq = function (/* functions... */) {
        var fns = arguments;
        return _restParam(function (args) {
            var that = this;

            var callback = args[args.length - 1];
            if (typeof callback == 'function') {
                args.pop();
            } else {
                callback = noop;
            }

            async.reduce(fns, args, function (newargs, fn, cb) {
                fn.apply(that, newargs.concat([_restParam(function (err, nextargs) {
                    cb(err, nextargs);
                })]));
            },
            function (err, results) {
                callback.apply(that, [err].concat(results));
            });
        });
    };

    async.compose = function (/* functions... */) {
        return async.seq.apply(null, Array.prototype.reverse.call(arguments));
    };


    function _applyEach(eachfn) {
        return _restParam(function(fns, args) {
            var go = _restParam(function(args) {
                var that = this;
                var callback = args.pop();
                return eachfn(fns, function (fn, _, cb) {
                    fn.apply(that, args.concat([cb]));
                },
                callback);
            });
            if (args.length) {
                return go.apply(this, args);
            }
            else {
                return go;
            }
        });
    }

    async.applyEach = _applyEach(async.eachOf);
    async.applyEachSeries = _applyEach(async.eachOfSeries);


    async.forever = function (fn, callback) {
        var done = only_once(callback || noop);
        var task = ensureAsync(fn);
        function next(err) {
            if (err) {
                return done(err);
            }
            task(next);
        }
        next();
    };

    function ensureAsync(fn) {
        return _restParam(function (args) {
            var callback = args.pop();
            args.push(function () {
                var innerArgs = arguments;
                if (sync) {
                    async.setImmediate(function () {
                        callback.apply(null, innerArgs);
                    });
                } else {
                    callback.apply(null, innerArgs);
                }
            });
            var sync = true;
            fn.apply(this, args);
            sync = false;
        });
    }

    async.ensureAsync = ensureAsync;

    async.constant = _restParam(function(values) {
        var args = [null].concat(values);
        return function (callback) {
            return callback.apply(this, args);
        };
    });

    async.wrapSync =
    async.asyncify = function asyncify(func) {
        return _restParam(function (args) {
            var callback = args.pop();
            var result;
            try {
                result = func.apply(this, args);
            } catch (e) {
                return callback(e);
            }
            // if result is Promise object
            if (_isObject(result) && typeof result.then === "function") {
                result.then(function(value) {
                    callback(null, value);
                })["catch"](function(err) {
                    callback(err.message ? err : new Error(err));
                });
            } else {
                callback(null, result);
            }
        });
    };

    // Node.js
    if (typeof module === 'object' && module.exports) {
        module.exports = async;
    }
    // AMD / RequireJS
    else if (typeof define === 'function' && define.amd) {
        define([], function () {
            return async;
        });
    }
    // included directly via <script> tag
    else {
        root.async = async;
    }

}());

}).call(this,require('_process'),typeof global !== "undefined" ? global : typeof self !== "undefined" ? self : typeof window !== "undefined" ? window : {})
},{"_process":16}],15:[function(require,module,exports){
(function (process){
// Copyright Joyent, Inc. and other Node contributors.
//
// Permission is hereby granted, free of charge, to any person obtaining a
// copy of this software and associated documentation files (the
// "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit
// persons to whom the Software is furnished to do so, subject to the
// following conditions:
//
// The above copyright notice and this permission notice shall be included
// in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
// OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN
// NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
// OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE
// USE OR OTHER DEALINGS IN THE SOFTWARE.

// resolves . and .. elements in a path array with directory names there
// must be no slashes, empty elements, or device names (c:\) in the array
// (so also no leading and trailing slashes - it does not distinguish
// relative and absolute paths)
function normalizeArray(parts, allowAboveRoot) {
  // if the path tries to go above the root, `up` ends up > 0
  var up = 0;
  for (var i = parts.length - 1; i >= 0; i--) {
    var last = parts[i];
    if (last === '.') {
      parts.splice(i, 1);
    } else if (last === '..') {
      parts.splice(i, 1);
      up++;
    } else if (up) {
      parts.splice(i, 1);
      up--;
    }
  }

  // if the path is allowed to go above the root, restore leading ..s
  if (allowAboveRoot) {
    for (; up--; up) {
      parts.unshift('..');
    }
  }

  return parts;
}

// Split a filename into [root, dir, basename, ext], unix version
// 'root' is just a slash, or nothing.
var splitPathRe =
    /^(\/?|)([\s\S]*?)((?:\.{1,2}|[^\/]+?|)(\.[^.\/]*|))(?:[\/]*)$/;
var splitPath = function(filename) {
  return splitPathRe.exec(filename).slice(1);
};

// path.resolve([from ...], to)
// posix version
exports.resolve = function() {
  var resolvedPath = '',
      resolvedAbsolute = false;

  for (var i = arguments.length - 1; i >= -1 && !resolvedAbsolute; i--) {
    var path = (i >= 0) ? arguments[i] : process.cwd();

    // Skip empty and invalid entries
    if (typeof path !== 'string') {
      throw new TypeError('Arguments to path.resolve must be strings');
    } else if (!path) {
      continue;
    }

    resolvedPath = path + '/' + resolvedPath;
    resolvedAbsolute = path.charAt(0) === '/';
  }

  // At this point the path should be resolved to a full absolute path, but
  // handle relative paths to be safe (might happen when process.cwd() fails)

  // Normalize the path
  resolvedPath = normalizeArray(filter(resolvedPath.split('/'), function(p) {
    return !!p;
  }), !resolvedAbsolute).join('/');

  return ((resolvedAbsolute ? '/' : '') + resolvedPath) || '.';
};

// path.normalize(path)
// posix version
exports.normalize = function(path) {
  var isAbsolute = exports.isAbsolute(path),
      trailingSlash = substr(path, -1) === '/';

  // Normalize the path
  path = normalizeArray(filter(path.split('/'), function(p) {
    return !!p;
  }), !isAbsolute).join('/');

  if (!path && !isAbsolute) {
    path = '.';
  }
  if (path && trailingSlash) {
    path += '/';
  }

  return (isAbsolute ? '/' : '') + path;
};

// posix version
exports.isAbsolute = function(path) {
  return path.charAt(0) === '/';
};

// posix version
exports.join = function() {
  var paths = Array.prototype.slice.call(arguments, 0);
  return exports.normalize(filter(paths, function(p, index) {
    if (typeof p !== 'string') {
      throw new TypeError('Arguments to path.join must be strings');
    }
    return p;
  }).join('/'));
};


// path.relative(from, to)
// posix version
exports.relative = function(from, to) {
  from = exports.resolve(from).substr(1);
  to = exports.resolve(to).substr(1);

  function trim(arr) {
    var start = 0;
    for (; start < arr.length; start++) {
      if (arr[start] !== '') break;
    }

    var end = arr.length - 1;
    for (; end >= 0; end--) {
      if (arr[end] !== '') break;
    }

    if (start > end) return [];
    return arr.slice(start, end - start + 1);
  }

  var fromParts = trim(from.split('/'));
  var toParts = trim(to.split('/'));

  var length = Math.min(fromParts.length, toParts.length);
  var samePartsLength = length;
  for (var i = 0; i < length; i++) {
    if (fromParts[i] !== toParts[i]) {
      samePartsLength = i;
      break;
    }
  }

  var outputParts = [];
  for (var i = samePartsLength; i < fromParts.length; i++) {
    outputParts.push('..');
  }

  outputParts = outputParts.concat(toParts.slice(samePartsLength));

  return outputParts.join('/');
};

exports.sep = '/';
exports.delimiter = ':';

exports.dirname = function(path) {
  var result = splitPath(path),
      root = result[0],
      dir = result[1];

  if (!root && !dir) {
    // No dirname whatsoever
    return '.';
  }

  if (dir) {
    // It has a dirname, strip trailing slash
    dir = dir.substr(0, dir.length - 1);
  }

  return root + dir;
};


exports.basename = function(path, ext) {
  var f = splitPath(path)[2];
  // TODO: make this comparison case-insensitive on windows?
  if (ext && f.substr(-1 * ext.length) === ext) {
    f = f.substr(0, f.length - ext.length);
  }
  return f;
};


exports.extname = function(path) {
  return splitPath(path)[3];
};

function filter (xs, f) {
    if (xs.filter) return xs.filter(f);
    var res = [];
    for (var i = 0; i < xs.length; i++) {
        if (f(xs[i], i, xs)) res.push(xs[i]);
    }
    return res;
}

// String.prototype.substr - negative index don't work in IE8
var substr = 'ab'.substr(-1) === 'b'
    ? function (str, start, len) { return str.substr(start, len) }
    : function (str, start, len) {
        if (start < 0) start = str.length + start;
        return str.substr(start, len);
    }
;

}).call(this,require('_process'))
},{"_process":16}],16:[function(require,module,exports){
// shim for using process in browser

var process = module.exports = {};
var queue = [];
var draining = false;
var currentQueue;
var queueIndex = -1;

function cleanUpNextTick() {
    draining = false;
    if (currentQueue.length) {
        queue = currentQueue.concat(queue);
    } else {
        queueIndex = -1;
    }
    if (queue.length) {
        drainQueue();
    }
}

function drainQueue() {
    if (draining) {
        return;
    }
    var timeout = setTimeout(cleanUpNextTick);
    draining = true;

    var len = queue.length;
    while(len) {
        currentQueue = queue;
        queue = [];
        while (++queueIndex < len) {
            if (currentQueue) {
                currentQueue[queueIndex].run();
            }
        }
        queueIndex = -1;
        len = queue.length;
    }
    currentQueue = null;
    draining = false;
    clearTimeout(timeout);
}

process.nextTick = function (fun) {
    var args = new Array(arguments.length - 1);
    if (arguments.length > 1) {
        for (var i = 1; i < arguments.length; i++) {
            args[i - 1] = arguments[i];
        }
    }
    queue.push(new Item(fun, args));
    if (queue.length === 1 && !draining) {
        setTimeout(drainQueue, 0);
    }
};

// v8 likes predictible objects
function Item(fun, array) {
    this.fun = fun;
    this.array = array;
}
Item.prototype.run = function () {
    this.fun.apply(null, this.array);
};
process.title = 'browser';
process.browser = true;
process.env = {};
process.argv = [];
process.version = ''; // empty string to avoid regexp issues
process.versions = {};

function noop() {}

process.on = noop;
process.addListener = noop;
process.once = noop;
process.off = noop;
process.removeListener = noop;
process.removeAllListeners = noop;
process.emit = noop;

process.binding = function (name) {
    throw new Error('process.binding is not supported');
};

process.cwd = function () { return '/' };
process.chdir = function (dir) {
    throw new Error('process.chdir is not supported');
};
process.umask = function() { return 0; };

},{}],17:[function(require,module,exports){

/**
 * This is the web browser implementation of `debug()`.
 *
 * Expose `debug()` as the module.
 */

exports = module.exports = require('./debug');
exports.log = log;
exports.formatArgs = formatArgs;
exports.save = save;
exports.load = load;
exports.useColors = useColors;
exports.storage = 'undefined' != typeof chrome
               && 'undefined' != typeof chrome.storage
                  ? chrome.storage.local
                  : localstorage();

/**
 * Colors.
 */

exports.colors = [
  'lightseagreen',
  'forestgreen',
  'goldenrod',
  'dodgerblue',
  'darkorchid',
  'crimson'
];

/**
 * Currently only WebKit-based Web Inspectors, Firefox >= v31,
 * and the Firebug extension (any Firefox version) are known
 * to support "%c" CSS customizations.
 *
 * TODO: add a `localStorage` variable to explicitly enable/disable colors
 */

function useColors() {
  // is webkit? http://stackoverflow.com/a/16459606/376773
  return ('WebkitAppearance' in document.documentElement.style) ||
    // is firebug? http://stackoverflow.com/a/398120/376773
    (window.console && (console.firebug || (console.exception && console.table))) ||
    // is firefox >= v31?
    // https://developer.mozilla.org/en-US/docs/Tools/Web_Console#Styling_messages
    (navigator.userAgent.toLowerCase().match(/firefox\/(\d+)/) && parseInt(RegExp.$1, 10) >= 31);
}

/**
 * Map %j to `JSON.stringify()`, since no Web Inspectors do that by default.
 */

exports.formatters.j = function(v) {
  return JSON.stringify(v);
};


/**
 * Colorize log arguments if enabled.
 *
 * @api public
 */

function formatArgs() {
  var args = arguments;
  var useColors = this.useColors;

  args[0] = (useColors ? '%c' : '')
    + this.namespace
    + (useColors ? ' %c' : ' ')
    + args[0]
    + (useColors ? '%c ' : ' ')
    + '+' + exports.humanize(this.diff);

  if (!useColors) return args;

  var c = 'color: ' + this.color;
  args = [args[0], c, 'color: inherit'].concat(Array.prototype.slice.call(args, 1));

  // the final "%c" is somewhat tricky, because there could be other
  // arguments passed either before or after the %c, so we need to
  // figure out the correct index to insert the CSS into
  var index = 0;
  var lastC = 0;
  args[0].replace(/%[a-z%]/g, function(match) {
    if ('%%' === match) return;
    index++;
    if ('%c' === match) {
      // we only are interested in the *last* %c
      // (the user may have provided their own)
      lastC = index;
    }
  });

  args.splice(lastC, 0, c);
  return args;
}

/**
 * Invokes `console.log()` when available.
 * No-op when `console.log` is not a "function".
 *
 * @api public
 */

function log() {
  // this hackery is required for IE8/9, where
  // the `console.log` function doesn't have 'apply'
  return 'object' === typeof console
    && console.log
    && Function.prototype.apply.call(console.log, console, arguments);
}

/**
 * Save `namespaces`.
 *
 * @param {String} namespaces
 * @api private
 */

function save(namespaces) {
  try {
    if (null == namespaces) {
      exports.storage.removeItem('debug');
    } else {
      exports.storage.debug = namespaces;
    }
  } catch(e) {}
}

/**
 * Load `namespaces`.
 *
 * @return {String} returns the previously persisted debug modes
 * @api private
 */

function load() {
  var r;
  try {
    r = exports.storage.debug;
  } catch(e) {}
  return r;
}

/**
 * Enable namespaces listed in `localStorage.debug` initially.
 */

exports.enable(load());

/**
 * Localstorage attempts to return the localstorage.
 *
 * This is necessary because safari throws
 * when a user disables cookies/localstorage
 * and you attempt to access it.
 *
 * @return {LocalStorage}
 * @api private
 */

function localstorage(){
  try {
    return window.localStorage;
  } catch (e) {}
}

},{"./debug":18}],18:[function(require,module,exports){

/**
 * This is the common logic for both the Node.js and web browser
 * implementations of `debug()`.
 *
 * Expose `debug()` as the module.
 */

exports = module.exports = debug;
exports.coerce = coerce;
exports.disable = disable;
exports.enable = enable;
exports.enabled = enabled;
exports.humanize = require('ms');

/**
 * The currently active debug mode names, and names to skip.
 */

exports.names = [];
exports.skips = [];

/**
 * Map of special "%n" handling functions, for the debug "format" argument.
 *
 * Valid key names are a single, lowercased letter, i.e. "n".
 */

exports.formatters = {};

/**
 * Previously assigned color.
 */

var prevColor = 0;

/**
 * Previous log timestamp.
 */

var prevTime;

/**
 * Select a color.
 *
 * @return {Number}
 * @api private
 */

function selectColor() {
  return exports.colors[prevColor++ % exports.colors.length];
}

/**
 * Create a debugger with the given `namespace`.
 *
 * @param {String} namespace
 * @return {Function}
 * @api public
 */

function debug(namespace) {

  // define the `disabled` version
  function disabled() {
  }
  disabled.enabled = false;

  // define the `enabled` version
  function enabled() {

    var self = enabled;

    // set `diff` timestamp
    var curr = +new Date();
    var ms = curr - (prevTime || curr);
    self.diff = ms;
    self.prev = prevTime;
    self.curr = curr;
    prevTime = curr;

    // add the `color` if not set
    if (null == self.useColors) self.useColors = exports.useColors();
    if (null == self.color && self.useColors) self.color = selectColor();

    var args = Array.prototype.slice.call(arguments);

    args[0] = exports.coerce(args[0]);

    if ('string' !== typeof args[0]) {
      // anything else let's inspect with %o
      args = ['%o'].concat(args);
    }

    // apply any `formatters` transformations
    var index = 0;
    args[0] = args[0].replace(/%([a-z%])/g, function(match, format) {
      // if we encounter an escaped % then don't increase the array index
      if (match === '%%') return match;
      index++;
      var formatter = exports.formatters[format];
      if ('function' === typeof formatter) {
        var val = args[index];
        match = formatter.call(self, val);

        // now we need to remove `args[index]` since it's inlined in the `format`
        args.splice(index, 1);
        index--;
      }
      return match;
    });

    if ('function' === typeof exports.formatArgs) {
      args = exports.formatArgs.apply(self, args);
    }
    var logFn = enabled.log || exports.log || console.log.bind(console);
    logFn.apply(self, args);
  }
  enabled.enabled = true;

  var fn = exports.enabled(namespace) ? enabled : disabled;

  fn.namespace = namespace;

  return fn;
}

/**
 * Enables a debug mode by namespaces. This can include modes
 * separated by a colon and wildcards.
 *
 * @param {String} namespaces
 * @api public
 */

function enable(namespaces) {
  exports.save(namespaces);

  var split = (namespaces || '').split(/[\s,]+/);
  var len = split.length;

  for (var i = 0; i < len; i++) {
    if (!split[i]) continue; // ignore empty strings
    namespaces = split[i].replace(/\*/g, '.*?');
    if (namespaces[0] === '-') {
      exports.skips.push(new RegExp('^' + namespaces.substr(1) + '$'));
    } else {
      exports.names.push(new RegExp('^' + namespaces + '$'));
    }
  }
}

/**
 * Disable debug output.
 *
 * @api public
 */

function disable() {
  exports.enable('');
}

/**
 * Returns true if the given mode name is enabled, false otherwise.
 *
 * @param {String} name
 * @return {Boolean}
 * @api public
 */

function enabled(name) {
  var i, len;
  for (i = 0, len = exports.skips.length; i < len; i++) {
    if (exports.skips[i].test(name)) {
      return false;
    }
  }
  for (i = 0, len = exports.names.length; i < len; i++) {
    if (exports.names[i].test(name)) {
      return true;
    }
  }
  return false;
}

/**
 * Coerce `val`.
 *
 * @param {Mixed} val
 * @return {Mixed}
 * @api private
 */

function coerce(val) {
  if (val instanceof Error) return val.stack || val.message;
  return val;
}

},{"ms":19}],19:[function(require,module,exports){
/**
 * Helpers.
 */

var s = 1000;
var m = s * 60;
var h = m * 60;
var d = h * 24;
var y = d * 365.25;

/**
 * Parse or format the given `val`.
 *
 * Options:
 *
 *  - `long` verbose formatting [false]
 *
 * @param {String|Number} val
 * @param {Object} options
 * @return {String|Number}
 * @api public
 */

module.exports = function(val, options){
  options = options || {};
  if ('string' == typeof val) return parse(val);
  return options.long
    ? long(val)
    : short(val);
};

/**
 * Parse the given `str` and return milliseconds.
 *
 * @param {String} str
 * @return {Number}
 * @api private
 */

function parse(str) {
  str = '' + str;
  if (str.length > 10000) return;
  var match = /^((?:\d+)?\.?\d+) *(milliseconds?|msecs?|ms|seconds?|secs?|s|minutes?|mins?|m|hours?|hrs?|h|days?|d|years?|yrs?|y)?$/i.exec(str);
  if (!match) return;
  var n = parseFloat(match[1]);
  var type = (match[2] || 'ms').toLowerCase();
  switch (type) {
    case 'years':
    case 'year':
    case 'yrs':
    case 'yr':
    case 'y':
      return n * y;
    case 'days':
    case 'day':
    case 'd':
      return n * d;
    case 'hours':
    case 'hour':
    case 'hrs':
    case 'hr':
    case 'h':
      return n * h;
    case 'minutes':
    case 'minute':
    case 'mins':
    case 'min':
    case 'm':
      return n * m;
    case 'seconds':
    case 'second':
    case 'secs':
    case 'sec':
    case 's':
      return n * s;
    case 'milliseconds':
    case 'millisecond':
    case 'msecs':
    case 'msec':
    case 'ms':
      return n;
  }
}

/**
 * Short format for `ms`.
 *
 * @param {Number} ms
 * @return {String}
 * @api private
 */

function short(ms) {
  if (ms >= d) return Math.round(ms / d) + 'd';
  if (ms >= h) return Math.round(ms / h) + 'h';
  if (ms >= m) return Math.round(ms / m) + 'm';
  if (ms >= s) return Math.round(ms / s) + 's';
  return ms + 'ms';
}

/**
 * Long format for `ms`.
 *
 * @param {Number} ms
 * @return {String}
 * @api private
 */

function long(ms) {
  return plural(ms, d, 'day')
    || plural(ms, h, 'hour')
    || plural(ms, m, 'minute')
    || plural(ms, s, 'second')
    || ms + ' ms';
}

/**
 * Pluralization helper.
 */

function plural(ms, n, name) {
  if (ms < n) return;
  if (ms < n * 1.5) return Math.floor(ms / n) + ' ' + name;
  return Math.ceil(ms / n) + ' ' + name + 's';
}

},{}],20:[function(require,module,exports){
var hat = module.exports = function (bits, base) {
    if (!base) base = 16;
    if (bits === undefined) bits = 128;
    if (bits <= 0) return '0';
    
    var digits = Math.log(Math.pow(2, bits)) / Math.log(base);
    for (var i = 2; digits === Infinity; i *= 2) {
        digits = Math.log(Math.pow(2, bits / i)) / Math.log(base) * i;
    }
    
    var rem = digits - Math.floor(digits);
    
    var res = '';
    
    for (var i = 0; i < Math.floor(digits); i++) {
        var x = Math.floor(Math.random() * base).toString(base);
        res = x + res;
    }
    
    if (rem) {
        var b = Math.pow(base, rem);
        var x = Math.floor(Math.random() * b).toString(base);
        res = x + res;
    }
    
    var parsed = parseInt(res, base);
    if (parsed !== Infinity && parsed >= Math.pow(2, bits)) {
        return hat(bits, base)
    }
    else return res;
};

hat.rack = function (bits, base, expandBy) {
    var fn = function (data) {
        var iters = 0;
        do {
            if (iters ++ > 10) {
                if (expandBy) bits += expandBy;
                else throw new Error('too many ID collisions, use more bits')
            }
            
            var id = hat(bits, base);
        } while (Object.hasOwnProperty.call(hats, id));
        
        hats[id] = data;
        return id;
    };
    var hats = fn.hats = {};
    
    fn.get = function (id) {
        return fn.hats[id];
    };
    
    fn.set = function (id, value) {
        fn.hats[id] = value;
        return fn;
    };
    
    fn.bits = bits || 128;
    fn.base = base || 16;
    return fn;
};

},{}],21:[function(require,module,exports){
/** Used as the `TypeError` message for "Functions" methods. */
var FUNC_ERROR_TEXT = 'Expected a function';

/* Native method references for those with the same name as other `lodash` methods. */
var nativeMax = Math.max;

/**
 * Creates a function that invokes `func` with the `this` binding of the
 * created function and arguments from `start` and beyond provided as an array.
 *
 * **Note:** This method is based on the [rest parameter](https://developer.mozilla.org/Web/JavaScript/Reference/Functions/rest_parameters).
 *
 * @static
 * @memberOf _
 * @category Function
 * @param {Function} func The function to apply a rest parameter to.
 * @param {number} [start=func.length-1] The start position of the rest parameter.
 * @returns {Function} Returns the new function.
 * @example
 *
 * var say = _.restParam(function(what, names) {
 *   return what + ' ' + _.initial(names).join(', ') +
 *     (_.size(names) > 1 ? ', & ' : '') + _.last(names);
 * });
 *
 * say('hello', 'fred', 'barney', 'pebbles');
 * // => 'hello fred, barney, & pebbles'
 */
function restParam(func, start) {
  if (typeof func != 'function') {
    throw new TypeError(FUNC_ERROR_TEXT);
  }
  start = nativeMax(start === undefined ? (func.length - 1) : (+start || 0), 0);
  return function() {
    var args = arguments,
        index = -1,
        length = nativeMax(args.length - start, 0),
        rest = Array(length);

    while (++index < length) {
      rest[index] = args[start + index];
    }
    switch (start) {
      case 0: return func.call(this, rest);
      case 1: return func.call(this, args[0], rest);
      case 2: return func.call(this, args[0], args[1], rest);
    }
    var otherArgs = Array(start + 1);
    index = -1;
    while (++index < start) {
      otherArgs[index] = args[index];
    }
    otherArgs[start] = rest;
    return func.apply(this, otherArgs);
  };
}

module.exports = restParam;

},{}],22:[function(require,module,exports){
/**
 * Appends the elements of `values` to `array`.
 *
 * @private
 * @param {Array} array The array to modify.
 * @param {Array} values The values to append.
 * @returns {Array} Returns `array`.
 */
function arrayPush(array, values) {
  var index = -1,
      length = values.length,
      offset = array.length;

  while (++index < length) {
    array[offset + index] = values[index];
  }
  return array;
}

module.exports = arrayPush;

},{}],23:[function(require,module,exports){
var arrayPush = require('./arrayPush'),
    isArguments = require('../lang/isArguments'),
    isArray = require('../lang/isArray'),
    isArrayLike = require('./isArrayLike'),
    isObjectLike = require('./isObjectLike');

/**
 * The base implementation of `_.flatten` with added support for restricting
 * flattening and specifying the start index.
 *
 * @private
 * @param {Array} array The array to flatten.
 * @param {boolean} [isDeep] Specify a deep flatten.
 * @param {boolean} [isStrict] Restrict flattening to arrays-like objects.
 * @param {Array} [result=[]] The initial result value.
 * @returns {Array} Returns the new flattened array.
 */
function baseFlatten(array, isDeep, isStrict, result) {
  result || (result = []);

  var index = -1,
      length = array.length;

  while (++index < length) {
    var value = array[index];
    if (isObjectLike(value) && isArrayLike(value) &&
        (isStrict || isArray(value) || isArguments(value))) {
      if (isDeep) {
        // Recursively flatten arrays (susceptible to call stack limits).
        baseFlatten(value, isDeep, isStrict, result);
      } else {
        arrayPush(result, value);
      }
    } else if (!isStrict) {
      result[result.length] = value;
    }
  }
  return result;
}

module.exports = baseFlatten;

},{"../lang/isArguments":38,"../lang/isArray":39,"./arrayPush":22,"./isArrayLike":31,"./isObjectLike":34}],24:[function(require,module,exports){
var createBaseFor = require('./createBaseFor');

/**
 * The base implementation of `baseForIn` and `baseForOwn` which iterates
 * over `object` properties returned by `keysFunc` invoking `iteratee` for
 * each property. Iteratee functions may exit iteration early by explicitly
 * returning `false`.
 *
 * @private
 * @param {Object} object The object to iterate over.
 * @param {Function} iteratee The function invoked per iteration.
 * @param {Function} keysFunc The function to get the keys of `object`.
 * @returns {Object} Returns `object`.
 */
var baseFor = createBaseFor();

module.exports = baseFor;

},{"./createBaseFor":28}],25:[function(require,module,exports){
var baseFor = require('./baseFor'),
    keysIn = require('../object/keysIn');

/**
 * The base implementation of `_.forIn` without support for callback
 * shorthands and `this` binding.
 *
 * @private
 * @param {Object} object The object to iterate over.
 * @param {Function} iteratee The function invoked per iteration.
 * @returns {Object} Returns `object`.
 */
function baseForIn(object, iteratee) {
  return baseFor(object, iteratee, keysIn);
}

module.exports = baseForIn;

},{"../object/keysIn":43,"./baseFor":24}],26:[function(require,module,exports){
/**
 * The base implementation of `_.property` without support for deep paths.
 *
 * @private
 * @param {string} key The key of the property to get.
 * @returns {Function} Returns the new function.
 */
function baseProperty(key) {
  return function(object) {
    return object == null ? undefined : object[key];
  };
}

module.exports = baseProperty;

},{}],27:[function(require,module,exports){
var identity = require('../utility/identity');

/**
 * A specialized version of `baseCallback` which only supports `this` binding
 * and specifying the number of arguments to provide to `func`.
 *
 * @private
 * @param {Function} func The function to bind.
 * @param {*} thisArg The `this` binding of `func`.
 * @param {number} [argCount] The number of arguments to provide to `func`.
 * @returns {Function} Returns the callback.
 */
function bindCallback(func, thisArg, argCount) {
  if (typeof func != 'function') {
    return identity;
  }
  if (thisArg === undefined) {
    return func;
  }
  switch (argCount) {
    case 1: return function(value) {
      return func.call(thisArg, value);
    };
    case 3: return function(value, index, collection) {
      return func.call(thisArg, value, index, collection);
    };
    case 4: return function(accumulator, value, index, collection) {
      return func.call(thisArg, accumulator, value, index, collection);
    };
    case 5: return function(value, other, key, object, source) {
      return func.call(thisArg, value, other, key, object, source);
    };
  }
  return function() {
    return func.apply(thisArg, arguments);
  };
}

module.exports = bindCallback;

},{"../utility/identity":45}],28:[function(require,module,exports){
var toObject = require('./toObject');

/**
 * Creates a base function for `_.forIn` or `_.forInRight`.
 *
 * @private
 * @param {boolean} [fromRight] Specify iterating from right to left.
 * @returns {Function} Returns the new base function.
 */
function createBaseFor(fromRight) {
  return function(object, iteratee, keysFunc) {
    var iterable = toObject(object),
        props = keysFunc(object),
        length = props.length,
        index = fromRight ? length : -1;

    while ((fromRight ? index-- : ++index < length)) {
      var key = props[index];
      if (iteratee(iterable[key], key, iterable) === false) {
        break;
      }
    }
    return object;
  };
}

module.exports = createBaseFor;

},{"./toObject":37}],29:[function(require,module,exports){
var baseProperty = require('./baseProperty');

/**
 * Gets the "length" property value of `object`.
 *
 * **Note:** This function is used to avoid a [JIT bug](https://bugs.webkit.org/show_bug.cgi?id=142792)
 * that affects Safari on at least iOS 8.1-8.3 ARM64.
 *
 * @private
 * @param {Object} object The object to query.
 * @returns {*} Returns the "length" value.
 */
var getLength = baseProperty('length');

module.exports = getLength;

},{"./baseProperty":26}],30:[function(require,module,exports){
var isNative = require('../lang/isNative');

/**
 * Gets the native function at `key` of `object`.
 *
 * @private
 * @param {Object} object The object to query.
 * @param {string} key The key of the method to get.
 * @returns {*} Returns the function if it's native, else `undefined`.
 */
function getNative(object, key) {
  var value = object == null ? undefined : object[key];
  return isNative(value) ? value : undefined;
}

module.exports = getNative;

},{"../lang/isNative":41}],31:[function(require,module,exports){
var getLength = require('./getLength'),
    isLength = require('./isLength');

/**
 * Checks if `value` is array-like.
 *
 * @private
 * @param {*} value The value to check.
 * @returns {boolean} Returns `true` if `value` is array-like, else `false`.
 */
function isArrayLike(value) {
  return value != null && isLength(getLength(value));
}

module.exports = isArrayLike;

},{"./getLength":29,"./isLength":33}],32:[function(require,module,exports){
/** Used to detect unsigned integer values. */
var reIsUint = /^\d+$/;

/**
 * Used as the [maximum length](http://ecma-international.org/ecma-262/6.0/#sec-number.max_safe_integer)
 * of an array-like value.
 */
var MAX_SAFE_INTEGER = 9007199254740991;

/**
 * Checks if `value` is a valid array-like index.
 *
 * @private
 * @param {*} value The value to check.
 * @param {number} [length=MAX_SAFE_INTEGER] The upper bounds of a valid index.
 * @returns {boolean} Returns `true` if `value` is a valid index, else `false`.
 */
function isIndex(value, length) {
  value = (typeof value == 'number' || reIsUint.test(value)) ? +value : -1;
  length = length == null ? MAX_SAFE_INTEGER : length;
  return value > -1 && value % 1 == 0 && value < length;
}

module.exports = isIndex;

},{}],33:[function(require,module,exports){
/**
 * Used as the [maximum length](http://ecma-international.org/ecma-262/6.0/#sec-number.max_safe_integer)
 * of an array-like value.
 */
var MAX_SAFE_INTEGER = 9007199254740991;

/**
 * Checks if `value` is a valid array-like length.
 *
 * **Note:** This function is based on [`ToLength`](http://ecma-international.org/ecma-262/6.0/#sec-tolength).
 *
 * @private
 * @param {*} value The value to check.
 * @returns {boolean} Returns `true` if `value` is a valid length, else `false`.
 */
function isLength(value) {
  return typeof value == 'number' && value > -1 && value % 1 == 0 && value <= MAX_SAFE_INTEGER;
}

module.exports = isLength;

},{}],34:[function(require,module,exports){
/**
 * Checks if `value` is object-like.
 *
 * @private
 * @param {*} value The value to check.
 * @returns {boolean} Returns `true` if `value` is object-like, else `false`.
 */
function isObjectLike(value) {
  return !!value && typeof value == 'object';
}

module.exports = isObjectLike;

},{}],35:[function(require,module,exports){
var toObject = require('./toObject');

/**
 * A specialized version of `_.pick` which picks `object` properties specified
 * by `props`.
 *
 * @private
 * @param {Object} object The source object.
 * @param {string[]} props The property names to pick.
 * @returns {Object} Returns the new object.
 */
function pickByArray(object, props) {
  object = toObject(object);

  var index = -1,
      length = props.length,
      result = {};

  while (++index < length) {
    var key = props[index];
    if (key in object) {
      result[key] = object[key];
    }
  }
  return result;
}

module.exports = pickByArray;

},{"./toObject":37}],36:[function(require,module,exports){
var baseForIn = require('./baseForIn');

/**
 * A specialized version of `_.pick` which picks `object` properties `predicate`
 * returns truthy for.
 *
 * @private
 * @param {Object} object The source object.
 * @param {Function} predicate The function invoked per iteration.
 * @returns {Object} Returns the new object.
 */
function pickByCallback(object, predicate) {
  var result = {};
  baseForIn(object, function(value, key, object) {
    if (predicate(value, key, object)) {
      result[key] = value;
    }
  });
  return result;
}

module.exports = pickByCallback;

},{"./baseForIn":25}],37:[function(require,module,exports){
var isObject = require('../lang/isObject');

/**
 * Converts `value` to an object if it's not one.
 *
 * @private
 * @param {*} value The value to process.
 * @returns {Object} Returns the object.
 */
function toObject(value) {
  return isObject(value) ? value : Object(value);
}

module.exports = toObject;

},{"../lang/isObject":42}],38:[function(require,module,exports){
var isArrayLike = require('../internal/isArrayLike'),
    isObjectLike = require('../internal/isObjectLike');

/** Used for native method references. */
var objectProto = Object.prototype;

/** Used to check objects for own properties. */
var hasOwnProperty = objectProto.hasOwnProperty;

/** Native method references. */
var propertyIsEnumerable = objectProto.propertyIsEnumerable;

/**
 * Checks if `value` is classified as an `arguments` object.
 *
 * @static
 * @memberOf _
 * @category Lang
 * @param {*} value The value to check.
 * @returns {boolean} Returns `true` if `value` is correctly classified, else `false`.
 * @example
 *
 * _.isArguments(function() { return arguments; }());
 * // => true
 *
 * _.isArguments([1, 2, 3]);
 * // => false
 */
function isArguments(value) {
  return isObjectLike(value) && isArrayLike(value) &&
    hasOwnProperty.call(value, 'callee') && !propertyIsEnumerable.call(value, 'callee');
}

module.exports = isArguments;

},{"../internal/isArrayLike":31,"../internal/isObjectLike":34}],39:[function(require,module,exports){
var getNative = require('../internal/getNative'),
    isLength = require('../internal/isLength'),
    isObjectLike = require('../internal/isObjectLike');

/** `Object#toString` result references. */
var arrayTag = '[object Array]';

/** Used for native method references. */
var objectProto = Object.prototype;

/**
 * Used to resolve the [`toStringTag`](http://ecma-international.org/ecma-262/6.0/#sec-object.prototype.tostring)
 * of values.
 */
var objToString = objectProto.toString;

/* Native method references for those with the same name as other `lodash` methods. */
var nativeIsArray = getNative(Array, 'isArray');

/**
 * Checks if `value` is classified as an `Array` object.
 *
 * @static
 * @memberOf _
 * @category Lang
 * @param {*} value The value to check.
 * @returns {boolean} Returns `true` if `value` is correctly classified, else `false`.
 * @example
 *
 * _.isArray([1, 2, 3]);
 * // => true
 *
 * _.isArray(function() { return arguments; }());
 * // => false
 */
var isArray = nativeIsArray || function(value) {
  return isObjectLike(value) && isLength(value.length) && objToString.call(value) == arrayTag;
};

module.exports = isArray;

},{"../internal/getNative":30,"../internal/isLength":33,"../internal/isObjectLike":34}],40:[function(require,module,exports){
var isObject = require('./isObject');

/** `Object#toString` result references. */
var funcTag = '[object Function]';

/** Used for native method references. */
var objectProto = Object.prototype;

/**
 * Used to resolve the [`toStringTag`](http://ecma-international.org/ecma-262/6.0/#sec-object.prototype.tostring)
 * of values.
 */
var objToString = objectProto.toString;

/**
 * Checks if `value` is classified as a `Function` object.
 *
 * @static
 * @memberOf _
 * @category Lang
 * @param {*} value The value to check.
 * @returns {boolean} Returns `true` if `value` is correctly classified, else `false`.
 * @example
 *
 * _.isFunction(_);
 * // => true
 *
 * _.isFunction(/abc/);
 * // => false
 */
function isFunction(value) {
  // The use of `Object#toString` avoids issues with the `typeof` operator
  // in older versions of Chrome and Safari which return 'function' for regexes
  // and Safari 8 which returns 'object' for typed array constructors.
  return isObject(value) && objToString.call(value) == funcTag;
}

module.exports = isFunction;

},{"./isObject":42}],41:[function(require,module,exports){
var isFunction = require('./isFunction'),
    isObjectLike = require('../internal/isObjectLike');

/** Used to detect host constructors (Safari > 5). */
var reIsHostCtor = /^\[object .+?Constructor\]$/;

/** Used for native method references. */
var objectProto = Object.prototype;

/** Used to resolve the decompiled source of functions. */
var fnToString = Function.prototype.toString;

/** Used to check objects for own properties. */
var hasOwnProperty = objectProto.hasOwnProperty;

/** Used to detect if a method is native. */
var reIsNative = RegExp('^' +
  fnToString.call(hasOwnProperty).replace(/[\\^$.*+?()[\]{}|]/g, '\\$&')
  .replace(/hasOwnProperty|(function).*?(?=\\\()| for .+?(?=\\\])/g, '$1.*?') + '$'
);

/**
 * Checks if `value` is a native function.
 *
 * @static
 * @memberOf _
 * @category Lang
 * @param {*} value The value to check.
 * @returns {boolean} Returns `true` if `value` is a native function, else `false`.
 * @example
 *
 * _.isNative(Array.prototype.push);
 * // => true
 *
 * _.isNative(_);
 * // => false
 */
function isNative(value) {
  if (value == null) {
    return false;
  }
  if (isFunction(value)) {
    return reIsNative.test(fnToString.call(value));
  }
  return isObjectLike(value) && reIsHostCtor.test(value);
}

module.exports = isNative;

},{"../internal/isObjectLike":34,"./isFunction":40}],42:[function(require,module,exports){
/**
 * Checks if `value` is the [language type](https://es5.github.io/#x8) of `Object`.
 * (e.g. arrays, functions, objects, regexes, `new Number(0)`, and `new String('')`)
 *
 * @static
 * @memberOf _
 * @category Lang
 * @param {*} value The value to check.
 * @returns {boolean} Returns `true` if `value` is an object, else `false`.
 * @example
 *
 * _.isObject({});
 * // => true
 *
 * _.isObject([1, 2, 3]);
 * // => true
 *
 * _.isObject(1);
 * // => false
 */
function isObject(value) {
  // Avoid a V8 JIT bug in Chrome 19-20.
  // See https://code.google.com/p/v8/issues/detail?id=2291 for more details.
  var type = typeof value;
  return !!value && (type == 'object' || type == 'function');
}

module.exports = isObject;

},{}],43:[function(require,module,exports){
var isArguments = require('../lang/isArguments'),
    isArray = require('../lang/isArray'),
    isIndex = require('../internal/isIndex'),
    isLength = require('../internal/isLength'),
    isObject = require('../lang/isObject');

/** Used for native method references. */
var objectProto = Object.prototype;

/** Used to check objects for own properties. */
var hasOwnProperty = objectProto.hasOwnProperty;

/**
 * Creates an array of the own and inherited enumerable property names of `object`.
 *
 * **Note:** Non-object values are coerced to objects.
 *
 * @static
 * @memberOf _
 * @category Object
 * @param {Object} object The object to query.
 * @returns {Array} Returns the array of property names.
 * @example
 *
 * function Foo() {
 *   this.a = 1;
 *   this.b = 2;
 * }
 *
 * Foo.prototype.c = 3;
 *
 * _.keysIn(new Foo);
 * // => ['a', 'b', 'c'] (iteration order is not guaranteed)
 */
function keysIn(object) {
  if (object == null) {
    return [];
  }
  if (!isObject(object)) {
    object = Object(object);
  }
  var length = object.length;
  length = (length && isLength(length) &&
    (isArray(object) || isArguments(object)) && length) || 0;

  var Ctor = object.constructor,
      index = -1,
      isProto = typeof Ctor == 'function' && Ctor.prototype === object,
      result = Array(length),
      skipIndexes = length > 0;

  while (++index < length) {
    result[index] = (index + '');
  }
  for (var key in object) {
    if (!(skipIndexes && isIndex(key, length)) &&
        !(key == 'constructor' && (isProto || !hasOwnProperty.call(object, key)))) {
      result.push(key);
    }
  }
  return result;
}

module.exports = keysIn;

},{"../internal/isIndex":32,"../internal/isLength":33,"../lang/isArguments":38,"../lang/isArray":39,"../lang/isObject":42}],44:[function(require,module,exports){
var baseFlatten = require('../internal/baseFlatten'),
    bindCallback = require('../internal/bindCallback'),
    pickByArray = require('../internal/pickByArray'),
    pickByCallback = require('../internal/pickByCallback'),
    restParam = require('../function/restParam');

/**
 * Creates an object composed of the picked `object` properties. Property
 * names may be specified as individual arguments or as arrays of property
 * names. If `predicate` is provided it's invoked for each property of `object`
 * picking the properties `predicate` returns truthy for. The predicate is
 * bound to `thisArg` and invoked with three arguments: (value, key, object).
 *
 * @static
 * @memberOf _
 * @category Object
 * @param {Object} object The source object.
 * @param {Function|...(string|string[])} [predicate] The function invoked per
 *  iteration or property names to pick, specified as individual property
 *  names or arrays of property names.
 * @param {*} [thisArg] The `this` binding of `predicate`.
 * @returns {Object} Returns the new object.
 * @example
 *
 * var object = { 'user': 'fred', 'age': 40 };
 *
 * _.pick(object, 'user');
 * // => { 'user': 'fred' }
 *
 * _.pick(object, _.isString);
 * // => { 'user': 'fred' }
 */
var pick = restParam(function(object, props) {
  if (object == null) {
    return {};
  }
  return typeof props[0] == 'function'
    ? pickByCallback(object, bindCallback(props[0], props[1], 3))
    : pickByArray(object, baseFlatten(props));
});

module.exports = pick;

},{"../function/restParam":21,"../internal/baseFlatten":23,"../internal/bindCallback":27,"../internal/pickByArray":35,"../internal/pickByCallback":36}],45:[function(require,module,exports){
/**
 * This method returns the first argument provided to it.
 *
 * @static
 * @memberOf _
 * @category Utility
 * @param {*} value Any value.
 * @returns {*} Returns `value`.
 * @example
 *
 * var object = { 'user': 'fred' };
 *
 * _.identity(object) === object;
 * // => true
 */
function identity(value) {
  return value;
}

module.exports = identity;

},{}],46:[function(require,module,exports){
'use strict';

var _interopRequireDefault = require('babel-runtime/helpers/interop-require-default')['default'];

exports.__esModule = true;

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _styleMaps = require('./styleMaps');

var _styleMaps2 = _interopRequireDefault(_styleMaps);

var _utilsCustomPropTypes = require('./utils/CustomPropTypes');

var _utilsCustomPropTypes2 = _interopRequireDefault(_utilsCustomPropTypes);

var BootstrapMixin = {
  propTypes: {
    /**
     * bootstrap className
     * @private
     */
    bsClass: _utilsCustomPropTypes2['default'].keyOf(_styleMaps2['default'].CLASSES),
    /**
     * Style variants
     * @type {("default"|"primary"|"success"|"info"|"warning"|"danger"|"link")}
     */
    bsStyle: _react2['default'].PropTypes.oneOf(_styleMaps2['default'].STYLES),
    /**
     * Size variants
     * @type {("xsmall"|"small"|"medium"|"large"|"xs"|"sm"|"md"|"lg")}
     */
    bsSize: _utilsCustomPropTypes2['default'].keyOf(_styleMaps2['default'].SIZES)
  },

  getBsClassSet: function getBsClassSet() {
    var classes = {};

    var bsClass = this.props.bsClass && _styleMaps2['default'].CLASSES[this.props.bsClass];
    if (bsClass) {
      classes[bsClass] = true;

      var prefix = bsClass + '-';

      var bsSize = this.props.bsSize && _styleMaps2['default'].SIZES[this.props.bsSize];
      if (bsSize) {
        classes[prefix + bsSize] = true;
      }

      if (this.props.bsStyle) {
        if (_styleMaps2['default'].STYLES.indexOf(this.props.bsStyle) >= 0) {
          classes[prefix + this.props.bsStyle] = true;
        } else {
          classes[this.props.bsStyle] = true;
        }
      }
    }

    return classes;
  },

  prefixClass: function prefixClass(subClass) {
    return _styleMaps2['default'].CLASSES[this.props.bsClass] + '-' + subClass;
  }
};

exports['default'] = BootstrapMixin;
module.exports = exports['default'];
},{"./styleMaps":61,"./utils/CustomPropTypes":62,"babel-runtime/helpers/interop-require-default":76,"react":undefined}],47:[function(require,module,exports){
'use strict';

var _extends = require('babel-runtime/helpers/extends')['default'];

var _interopRequireDefault = require('babel-runtime/helpers/interop-require-default')['default'];

exports.__esModule = true;

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _classnames = require('classnames');

var _classnames2 = _interopRequireDefault(_classnames);

var _BootstrapMixin = require('./BootstrapMixin');

var _BootstrapMixin2 = _interopRequireDefault(_BootstrapMixin);

var _utilsCustomPropTypes = require('./utils/CustomPropTypes');

var _utilsCustomPropTypes2 = _interopRequireDefault(_utilsCustomPropTypes);

var _ButtonInput = require('./ButtonInput');

var _ButtonInput2 = _interopRequireDefault(_ButtonInput);

var Button = _react2['default'].createClass({
  displayName: 'Button',

  mixins: [_BootstrapMixin2['default']],

  propTypes: {
    active: _react2['default'].PropTypes.bool,
    disabled: _react2['default'].PropTypes.bool,
    block: _react2['default'].PropTypes.bool,
    navItem: _react2['default'].PropTypes.bool,
    navDropdown: _react2['default'].PropTypes.bool,
    /**
     * You can use a custom element for this component
     */
    componentClass: _utilsCustomPropTypes2['default'].elementType,
    href: _react2['default'].PropTypes.string,
    target: _react2['default'].PropTypes.string,
    /**
     * Defines HTML button type Attribute
     * @type {("button"|"reset"|"submit")}
     * @defaultValue 'button'
     */
    type: _react2['default'].PropTypes.oneOf(_ButtonInput2['default'].types)
  },

  getDefaultProps: function getDefaultProps() {
    return {
      active: false,
      block: false,
      bsClass: 'button',
      bsStyle: 'default',
      disabled: false,
      navItem: false,
      navDropdown: false
    };
  },

  render: function render() {
    var classes = this.props.navDropdown ? {} : this.getBsClassSet();
    var renderFuncName = undefined;

    classes = _extends({
      active: this.props.active,
      'btn-block': this.props.block
    }, classes);

    if (this.props.navItem) {
      return this.renderNavItem(classes);
    }

    renderFuncName = this.props.href || this.props.target || this.props.navDropdown ? 'renderAnchor' : 'renderButton';

    return this[renderFuncName](classes);
  },

  renderAnchor: function renderAnchor(classes) {

    var Component = this.props.componentClass || 'a';
    var href = this.props.href || '#';
    classes.disabled = this.props.disabled;

    return _react2['default'].createElement(
      Component,
      _extends({}, this.props, {
        href: href,
        className: _classnames2['default'](this.props.className, classes),
        role: 'button' }),
      this.props.children
    );
  },

  renderButton: function renderButton(classes) {
    var Component = this.props.componentClass || 'button';

    return _react2['default'].createElement(
      Component,
      _extends({}, this.props, {
        type: this.props.type || 'button',
        className: _classnames2['default'](this.props.className, classes) }),
      this.props.children
    );
  },

  renderNavItem: function renderNavItem(classes) {
    var liClasses = {
      active: this.props.active
    };

    return _react2['default'].createElement(
      'li',
      { className: _classnames2['default'](liClasses) },
      this.renderAnchor(classes)
    );
  }
});

exports['default'] = Button;
module.exports = exports['default'];
},{"./BootstrapMixin":46,"./ButtonInput":48,"./utils/CustomPropTypes":62,"babel-runtime/helpers/extends":74,"babel-runtime/helpers/interop-require-default":76,"classnames":102,"react":undefined}],48:[function(require,module,exports){
'use strict';

var _inherits = require('babel-runtime/helpers/inherits')['default'];

var _classCallCheck = require('babel-runtime/helpers/class-call-check')['default'];

var _objectWithoutProperties = require('babel-runtime/helpers/object-without-properties')['default'];

var _extends = require('babel-runtime/helpers/extends')['default'];

var _interopRequireDefault = require('babel-runtime/helpers/interop-require-default')['default'];

exports.__esModule = true;

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _Button = require('./Button');

var _Button2 = _interopRequireDefault(_Button);

var _FormGroup = require('./FormGroup');

var _FormGroup2 = _interopRequireDefault(_FormGroup);

var _InputBase2 = require('./InputBase');

var _InputBase3 = _interopRequireDefault(_InputBase2);

var _utilsChildrenValueInputValidation = require('./utils/childrenValueInputValidation');

var _utilsChildrenValueInputValidation2 = _interopRequireDefault(_utilsChildrenValueInputValidation);

var ButtonInput = (function (_InputBase) {
  _inherits(ButtonInput, _InputBase);

  function ButtonInput() {
    _classCallCheck(this, ButtonInput);

    _InputBase.apply(this, arguments);
  }

  ButtonInput.prototype.renderFormGroup = function renderFormGroup(children) {
    var _props = this.props;
    var bsStyle = _props.bsStyle;
    var value = _props.value;

    var other = _objectWithoutProperties(_props, ['bsStyle', 'value']);

    return _react2['default'].createElement(
      _FormGroup2['default'],
      other,
      children
    );
  };

  ButtonInput.prototype.renderInput = function renderInput() {
    var _props2 = this.props;
    var children = _props2.children;
    var value = _props2.value;

    var other = _objectWithoutProperties(_props2, ['children', 'value']);

    var val = children ? children : value;
    return _react2['default'].createElement(_Button2['default'], _extends({}, other, { componentClass: 'input', ref: 'input', key: 'input', value: val }));
  };

  return ButtonInput;
})(_InputBase3['default']);

ButtonInput.types = ['button', 'reset', 'submit'];

ButtonInput.defaultProps = {
  type: 'button'
};

ButtonInput.propTypes = {
  type: _react2['default'].PropTypes.oneOf(ButtonInput.types),
  bsStyle: function bsStyle(props) {
    //defer to Button propTypes of bsStyle
    return null;
  },
  children: _utilsChildrenValueInputValidation2['default'],
  value: _utilsChildrenValueInputValidation2['default']
};

exports['default'] = ButtonInput;
module.exports = exports['default'];
},{"./Button":47,"./FormGroup":50,"./InputBase":52,"./utils/childrenValueInputValidation":65,"babel-runtime/helpers/class-call-check":73,"babel-runtime/helpers/extends":74,"babel-runtime/helpers/inherits":75,"babel-runtime/helpers/interop-require-default":76,"babel-runtime/helpers/object-without-properties":77,"react":undefined}],49:[function(require,module,exports){
'use strict';

var _inherits = require('babel-runtime/helpers/inherits')['default'];

var _classCallCheck = require('babel-runtime/helpers/class-call-check')['default'];

var _extends = require('babel-runtime/helpers/extends')['default'];

var _interopRequireDefault = require('babel-runtime/helpers/interop-require-default')['default'];

exports.__esModule = true;

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _reactOverlaysLibTransition = require('react-overlays/lib/Transition');

var _reactOverlaysLibTransition2 = _interopRequireDefault(_reactOverlaysLibTransition);

var _utilsCustomPropTypes = require('./utils/CustomPropTypes');

var _utilsCustomPropTypes2 = _interopRequireDefault(_utilsCustomPropTypes);

var _utilsDeprecationWarning = require('./utils/deprecationWarning');

var _utilsDeprecationWarning2 = _interopRequireDefault(_utilsDeprecationWarning);

var Fade = (function (_React$Component) {
  _inherits(Fade, _React$Component);

  function Fade() {
    _classCallCheck(this, Fade);

    _React$Component.apply(this, arguments);
  }

  // Explicitly copied from Transition for doc generation.
  // TODO: Remove duplication once #977 is resolved.

  Fade.prototype.render = function render() {
    var timeout = this.props.timeout || this.props.duration;

    return _react2['default'].createElement(
      _reactOverlaysLibTransition2['default'],
      _extends({}, this.props, {
        timeout: timeout,
        className: 'fade',
        enteredClassName: 'in',
        enteringClassName: 'in'
      }),
      this.props.children
    );
  };

  return Fade;
})(_react2['default'].Component);

Fade.propTypes = {
  /**
   * Show the component; triggers the fade in or fade out animation
   */
  'in': _react2['default'].PropTypes.bool,

  /**
   * Unmount the component (remove it from the DOM) when it is faded out
   */
  unmountOnExit: _react2['default'].PropTypes.bool,

  /**
   * Run the fade in animation when the component mounts, if it is initially
   * shown
   */
  transitionAppear: _react2['default'].PropTypes.bool,

  /**
   * Duration of the fade animation in milliseconds, to ensure that finishing
   * callbacks are fired even if the original browser transition end events are
   * canceled
   */
  timeout: _react2['default'].PropTypes.number,

  /**
   * duration
   * @private
   */
  duration: _utilsCustomPropTypes2['default'].all([_react2['default'].PropTypes.number, function (props) {
    if (props.duration != null) {
      _utilsDeprecationWarning2['default']('Fade `duration`', 'the `timeout` prop');
    }
    return null;
  }]),

  /**
   * Callback fired before the component fades in
   */
  onEnter: _react2['default'].PropTypes.func,
  /**
   * Callback fired after the component starts to fade in
   */
  onEntering: _react2['default'].PropTypes.func,
  /**
   * Callback fired after the has component faded in
   */
  onEntered: _react2['default'].PropTypes.func,
  /**
   * Callback fired before the component fades out
   */
  onExit: _react2['default'].PropTypes.func,
  /**
   * Callback fired after the component starts to fade out
   */
  onExiting: _react2['default'].PropTypes.func,
  /**
   * Callback fired after the component has faded out
   */
  onExited: _react2['default'].PropTypes.func
};

Fade.defaultProps = {
  'in': false,
  timeout: 300,
  unmountOnExit: false,
  transitionAppear: false
};

exports['default'] = Fade;
module.exports = exports['default'];
},{"./utils/CustomPropTypes":62,"./utils/deprecationWarning":68,"babel-runtime/helpers/class-call-check":73,"babel-runtime/helpers/extends":74,"babel-runtime/helpers/inherits":75,"babel-runtime/helpers/interop-require-default":76,"react":undefined,"react-overlays/lib/Transition":127}],50:[function(require,module,exports){
'use strict';

var _inherits = require('babel-runtime/helpers/inherits')['default'];

var _classCallCheck = require('babel-runtime/helpers/class-call-check')['default'];

var _interopRequireDefault = require('babel-runtime/helpers/interop-require-default')['default'];

exports.__esModule = true;

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _classnames = require('classnames');

var _classnames2 = _interopRequireDefault(_classnames);

var FormGroup = (function (_React$Component) {
  _inherits(FormGroup, _React$Component);

  function FormGroup() {
    _classCallCheck(this, FormGroup);

    _React$Component.apply(this, arguments);
  }

  FormGroup.prototype.render = function render() {
    var classes = {
      'form-group': !this.props.standalone,
      'form-group-lg': !this.props.standalone && this.props.bsSize === 'large',
      'form-group-sm': !this.props.standalone && this.props.bsSize === 'small',
      'has-feedback': this.props.hasFeedback,
      'has-success': this.props.bsStyle === 'success',
      'has-warning': this.props.bsStyle === 'warning',
      'has-error': this.props.bsStyle === 'error'
    };

    return _react2['default'].createElement(
      'div',
      { className: _classnames2['default'](classes, this.props.groupClassName) },
      this.props.children
    );
  };

  return FormGroup;
})(_react2['default'].Component);

FormGroup.defaultProps = {
  hasFeedback: false,
  standalone: false
};

FormGroup.propTypes = {
  standalone: _react2['default'].PropTypes.bool,
  hasFeedback: _react2['default'].PropTypes.bool,
  bsSize: function bsSize(props) {
    if (props.standalone && props.bsSize !== undefined) {
      return new Error('bsSize will not be used when `standalone` is set.');
    }

    return _react2['default'].PropTypes.oneOf(['small', 'medium', 'large']).apply(null, arguments);
  },
  bsStyle: _react2['default'].PropTypes.oneOf(['success', 'warning', 'error']),
  groupClassName: _react2['default'].PropTypes.string
};

exports['default'] = FormGroup;
module.exports = exports['default'];
},{"babel-runtime/helpers/class-call-check":73,"babel-runtime/helpers/inherits":75,"babel-runtime/helpers/interop-require-default":76,"classnames":102,"react":undefined}],51:[function(require,module,exports){
'use strict';

var _extends = require('babel-runtime/helpers/extends')['default'];

var _interopRequireDefault = require('babel-runtime/helpers/interop-require-default')['default'];

exports.__esModule = true;

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _classnames = require('classnames');

var _classnames2 = _interopRequireDefault(_classnames);

var Glyphicon = _react2['default'].createClass({
  displayName: 'Glyphicon',

  propTypes: {
    /**
     * bootstrap className
     * @private
     */
    bsClass: _react2['default'].PropTypes.string,
    /**
     * An icon name. See e.g. http://getbootstrap.com/components/#glyphicons
     */
    glyph: _react2['default'].PropTypes.string.isRequired,
    /**
     * Adds 'form-control-feedback' class
     * @private
     */
    formControlFeedback: _react2['default'].PropTypes.bool
  },

  getDefaultProps: function getDefaultProps() {
    return {
      bsClass: 'glyphicon',
      formControlFeedback: false
    };
  },

  render: function render() {
    var _classNames;

    var className = _classnames2['default'](this.props.className, (_classNames = {}, _classNames[this.props.bsClass] = true, _classNames['glyphicon-' + this.props.glyph] = true, _classNames['form-control-feedback'] = this.props.formControlFeedback, _classNames));

    return _react2['default'].createElement(
      'span',
      _extends({}, this.props, { className: className }),
      this.props.children
    );
  }
});

exports['default'] = Glyphicon;
module.exports = exports['default'];
},{"babel-runtime/helpers/extends":74,"babel-runtime/helpers/interop-require-default":76,"classnames":102,"react":undefined}],52:[function(require,module,exports){
'use strict';

var _inherits = require('babel-runtime/helpers/inherits')['default'];

var _classCallCheck = require('babel-runtime/helpers/class-call-check')['default'];

var _extends = require('babel-runtime/helpers/extends')['default'];

var _interopRequireDefault = require('babel-runtime/helpers/interop-require-default')['default'];

exports.__esModule = true;

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _classnames = require('classnames');

var _classnames2 = _interopRequireDefault(_classnames);

var _FormGroup = require('./FormGroup');

var _FormGroup2 = _interopRequireDefault(_FormGroup);

var _Glyphicon = require('./Glyphicon');

var _Glyphicon2 = _interopRequireDefault(_Glyphicon);

var InputBase = (function (_React$Component) {
  _inherits(InputBase, _React$Component);

  function InputBase() {
    _classCallCheck(this, InputBase);

    _React$Component.apply(this, arguments);
  }

  InputBase.prototype.getInputDOMNode = function getInputDOMNode() {
    return _react2['default'].findDOMNode(this.refs.input);
  };

  InputBase.prototype.getValue = function getValue() {
    if (this.props.type === 'static') {
      return this.props.value;
    } else if (this.props.type) {
      if (this.props.type === 'select' && this.props.multiple) {
        return this.getSelectedOptions();
      } else {
        return this.getInputDOMNode().value;
      }
    } else {
      throw 'Cannot use getValue without specifying input type.';
    }
  };

  InputBase.prototype.getChecked = function getChecked() {
    return this.getInputDOMNode().checked;
  };

  InputBase.prototype.getSelectedOptions = function getSelectedOptions() {
    var values = [];

    Array.prototype.forEach.call(this.getInputDOMNode().getElementsByTagName('option'), function (option) {
      if (option.selected) {
        var value = option.getAttribute('value') || option.innerHtml;
        values.push(value);
      }
    });

    return values;
  };

  InputBase.prototype.isCheckboxOrRadio = function isCheckboxOrRadio() {
    return this.props.type === 'checkbox' || this.props.type === 'radio';
  };

  InputBase.prototype.isFile = function isFile() {
    return this.props.type === 'file';
  };

  InputBase.prototype.renderInputGroup = function renderInputGroup(children) {
    var addonBefore = this.props.addonBefore ? _react2['default'].createElement(
      'span',
      { className: 'input-group-addon', key: 'addonBefore' },
      this.props.addonBefore
    ) : null;

    var addonAfter = this.props.addonAfter ? _react2['default'].createElement(
      'span',
      { className: 'input-group-addon', key: 'addonAfter' },
      this.props.addonAfter
    ) : null;

    var buttonBefore = this.props.buttonBefore ? _react2['default'].createElement(
      'span',
      { className: 'input-group-btn' },
      this.props.buttonBefore
    ) : null;

    var buttonAfter = this.props.buttonAfter ? _react2['default'].createElement(
      'span',
      { className: 'input-group-btn' },
      this.props.buttonAfter
    ) : null;

    var inputGroupClassName = undefined;
    switch (this.props.bsSize) {
      case 'small':
        inputGroupClassName = 'input-group-sm';break;
      case 'large':
        inputGroupClassName = 'input-group-lg';break;
      default:
    }

    return addonBefore || addonAfter || buttonBefore || buttonAfter ? _react2['default'].createElement(
      'div',
      { className: _classnames2['default'](inputGroupClassName, 'input-group'), key: 'input-group' },
      addonBefore,
      buttonBefore,
      children,
      addonAfter,
      buttonAfter
    ) : children;
  };

  InputBase.prototype.renderIcon = function renderIcon() {
    if (this.props.hasFeedback) {
      if (this.props.feedbackIcon) {
        return _react2['default'].cloneElement(this.props.feedbackIcon, { formControlFeedback: true });
      }

      switch (this.props.bsStyle) {
        case 'success':
          return _react2['default'].createElement(_Glyphicon2['default'], { formControlFeedback: true, glyph: 'ok', key: 'icon' });
        case 'warning':
          return _react2['default'].createElement(_Glyphicon2['default'], { formControlFeedback: true, glyph: 'warning-sign', key: 'icon' });
        case 'error':
          return _react2['default'].createElement(_Glyphicon2['default'], { formControlFeedback: true, glyph: 'remove', key: 'icon' });
        default:
          return _react2['default'].createElement('span', { className: 'form-control-feedback', key: 'icon' });
      }
    } else {
      return null;
    }
  };

  InputBase.prototype.renderHelp = function renderHelp() {
    return this.props.help ? _react2['default'].createElement(
      'span',
      { className: 'help-block', key: 'help' },
      this.props.help
    ) : null;
  };

  InputBase.prototype.renderCheckboxAndRadioWrapper = function renderCheckboxAndRadioWrapper(children) {
    var classes = {
      'checkbox': this.props.type === 'checkbox',
      'radio': this.props.type === 'radio'
    };

    return _react2['default'].createElement(
      'div',
      { className: _classnames2['default'](classes), key: 'checkboxRadioWrapper' },
      children
    );
  };

  InputBase.prototype.renderWrapper = function renderWrapper(children) {
    return this.props.wrapperClassName ? _react2['default'].createElement(
      'div',
      { className: this.props.wrapperClassName, key: 'wrapper' },
      children
    ) : children;
  };

  InputBase.prototype.renderLabel = function renderLabel(children) {
    var classes = {
      'control-label': !this.isCheckboxOrRadio()
    };
    classes[this.props.labelClassName] = this.props.labelClassName;

    return this.props.label ? _react2['default'].createElement(
      'label',
      { htmlFor: this.props.id, className: _classnames2['default'](classes), key: 'label' },
      children,
      this.props.label
    ) : children;
  };

  InputBase.prototype.renderInput = function renderInput() {
    if (!this.props.type) {
      return this.props.children;
    }

    switch (this.props.type) {
      case 'select':
        return _react2['default'].createElement(
          'select',
          _extends({}, this.props, { className: _classnames2['default'](this.props.className, 'form-control'), ref: 'input', key: 'input' }),
          this.props.children
        );
      case 'textarea':
        return _react2['default'].createElement('textarea', _extends({}, this.props, { className: _classnames2['default'](this.props.className, 'form-control'), ref: 'input', key: 'input' }));
      case 'static':
        return _react2['default'].createElement(
          'p',
          _extends({}, this.props, { className: _classnames2['default'](this.props.className, 'form-control-static'), ref: 'input', key: 'input' }),
          this.props.value
        );
      default:
        var className = this.isCheckboxOrRadio() || this.isFile() ? '' : 'form-control';
        return _react2['default'].createElement('input', _extends({}, this.props, { className: _classnames2['default'](this.props.className, className), ref: 'input', key: 'input' }));
    }
  };

  InputBase.prototype.renderFormGroup = function renderFormGroup(children) {
    return _react2['default'].createElement(
      _FormGroup2['default'],
      this.props,
      children
    );
  };

  InputBase.prototype.renderChildren = function renderChildren() {
    return !this.isCheckboxOrRadio() ? [this.renderLabel(), this.renderWrapper([this.renderInputGroup(this.renderInput()), this.renderIcon(), this.renderHelp()])] : this.renderWrapper([this.renderCheckboxAndRadioWrapper(this.renderLabel(this.renderInput())), this.renderHelp()]);
  };

  InputBase.prototype.render = function render() {
    var children = this.renderChildren();
    return this.renderFormGroup(children);
  };

  return InputBase;
})(_react2['default'].Component);

InputBase.propTypes = {
  type: _react2['default'].PropTypes.string,
  label: _react2['default'].PropTypes.node,
  help: _react2['default'].PropTypes.node,
  addonBefore: _react2['default'].PropTypes.node,
  addonAfter: _react2['default'].PropTypes.node,
  buttonBefore: _react2['default'].PropTypes.node,
  buttonAfter: _react2['default'].PropTypes.node,
  bsSize: _react2['default'].PropTypes.oneOf(['small', 'medium', 'large']),
  bsStyle: _react2['default'].PropTypes.oneOf(['success', 'warning', 'error']),
  hasFeedback: _react2['default'].PropTypes.bool,
  feedbackIcon: _react2['default'].PropTypes.node,
  id: _react2['default'].PropTypes.oneOfType([_react2['default'].PropTypes.string, _react2['default'].PropTypes.number]),
  groupClassName: _react2['default'].PropTypes.string,
  wrapperClassName: _react2['default'].PropTypes.string,
  labelClassName: _react2['default'].PropTypes.string,
  multiple: _react2['default'].PropTypes.bool,
  disabled: _react2['default'].PropTypes.bool,
  value: _react2['default'].PropTypes.any
};

InputBase.defaultProps = {
  disabled: false,
  hasFeedback: false,
  multiple: false
};

exports['default'] = InputBase;
module.exports = exports['default'];
},{"./FormGroup":50,"./Glyphicon":51,"babel-runtime/helpers/class-call-check":73,"babel-runtime/helpers/extends":74,"babel-runtime/helpers/inherits":75,"babel-runtime/helpers/interop-require-default":76,"classnames":102,"react":undefined}],53:[function(require,module,exports){
'use strict';

var _extends = require('babel-runtime/helpers/extends')['default'];

var _interopRequireDefault = require('babel-runtime/helpers/interop-require-default')['default'];

exports.__esModule = true;

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _BootstrapMixin = require('./BootstrapMixin');

var _BootstrapMixin2 = _interopRequireDefault(_BootstrapMixin);

var _classnames = require('classnames');

var _classnames2 = _interopRequireDefault(_classnames);

var ListGroupItem = _react2['default'].createClass({
  displayName: 'ListGroupItem',

  mixins: [_BootstrapMixin2['default']],

  propTypes: {
    bsStyle: _react2['default'].PropTypes.oneOf(['danger', 'info', 'success', 'warning']),
    className: _react2['default'].PropTypes.string,
    active: _react2['default'].PropTypes.any,
    disabled: _react2['default'].PropTypes.any,
    header: _react2['default'].PropTypes.node,
    listItem: _react2['default'].PropTypes.bool,
    onClick: _react2['default'].PropTypes.func,
    eventKey: _react2['default'].PropTypes.any,
    href: _react2['default'].PropTypes.string,
    target: _react2['default'].PropTypes.string
  },

  getDefaultProps: function getDefaultProps() {
    return {
      bsClass: 'list-group-item',
      listItem: false
    };
  },

  render: function render() {
    var classes = this.getBsClassSet();

    classes.active = this.props.active;
    classes.disabled = this.props.disabled;

    if (this.props.href) {
      return this.renderAnchor(classes);
    } else if (this.props.onClick) {
      return this.renderButton(classes);
    } else if (this.props.listItem) {
      return this.renderLi(classes);
    } else {
      return this.renderSpan(classes);
    }
  },

  renderLi: function renderLi(classes) {
    return _react2['default'].createElement(
      'li',
      _extends({}, this.props, { className: _classnames2['default'](this.props.className, classes) }),
      this.props.header ? this.renderStructuredContent() : this.props.children
    );
  },

  renderAnchor: function renderAnchor(classes) {
    return _react2['default'].createElement(
      'a',
      _extends({}, this.props, {
        className: _classnames2['default'](this.props.className, classes)
      }),
      this.props.header ? this.renderStructuredContent() : this.props.children
    );
  },

  renderButton: function renderButton(classes) {
    return _react2['default'].createElement(
      'button',
      _extends({
        type: 'button'
      }, this.props, {
        className: _classnames2['default'](this.props.className, classes) }),
      this.props.children
    );
  },

  renderSpan: function renderSpan(classes) {
    return _react2['default'].createElement(
      'span',
      _extends({}, this.props, { className: _classnames2['default'](this.props.className, classes) }),
      this.props.header ? this.renderStructuredContent() : this.props.children
    );
  },

  renderStructuredContent: function renderStructuredContent() {
    var header = undefined;
    if (_react2['default'].isValidElement(this.props.header)) {
      header = _react.cloneElement(this.props.header, {
        key: 'header',
        className: _classnames2['default'](this.props.header.props.className, 'list-group-item-heading')
      });
    } else {
      header = _react2['default'].createElement(
        'h4',
        { key: 'header', className: 'list-group-item-heading' },
        this.props.header
      );
    }

    var content = _react2['default'].createElement(
      'p',
      { key: 'content', className: 'list-group-item-text' },
      this.props.children
    );

    return [header, content];
  }
});

exports['default'] = ListGroupItem;
module.exports = exports['default'];
},{"./BootstrapMixin":46,"babel-runtime/helpers/extends":74,"babel-runtime/helpers/interop-require-default":76,"classnames":102,"react":undefined}],54:[function(require,module,exports){
'use strict';

var _inherits = require('babel-runtime/helpers/inherits')['default'];

var _classCallCheck = require('babel-runtime/helpers/class-call-check')['default'];

var _interopRequireDefault = require('babel-runtime/helpers/interop-require-default')['default'];

exports.__esModule = true;

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _classnames = require('classnames');

var _classnames2 = _interopRequireDefault(_classnames);

var _utilsCustomPropTypes = require('./utils/CustomPropTypes');

var _utilsCustomPropTypes2 = _interopRequireDefault(_utilsCustomPropTypes);

var _SafeAnchor = require('./SafeAnchor');

var _SafeAnchor2 = _interopRequireDefault(_SafeAnchor);

var MenuItem = (function (_React$Component) {
  _inherits(MenuItem, _React$Component);

  function MenuItem(props) {
    _classCallCheck(this, MenuItem);

    _React$Component.call(this, props);

    this.handleClick = this.handleClick.bind(this);
  }

  MenuItem.prototype.handleClick = function handleClick(event) {
    if (!this.props.href || this.props.disabled) {
      event.preventDefault();
    }

    if (this.props.disabled) {
      return;
    }

    if (this.props.onSelect) {
      this.props.onSelect(event, this.props.eventKey);
    }
  };

  MenuItem.prototype.render = function render() {
    if (this.props.divider) {
      return _react2['default'].createElement('li', { role: 'separator', className: 'divider' });
    }

    if (this.props.header) {
      return _react2['default'].createElement(
        'li',
        { role: 'heading', className: 'dropdown-header' },
        this.props.children
      );
    }

    var classes = {
      disabled: this.props.disabled
    };

    return _react2['default'].createElement(
      'li',
      { role: 'presentation',
        className: _classnames2['default'](this.props.className, classes),
        style: this.props.style
      },
      _react2['default'].createElement(
        _SafeAnchor2['default'],
        {
          role: 'menuitem',
          tabIndex: '-1',
          id: this.props.id,
          target: this.props.target,
          title: this.props.title,
          href: this.props.href || '',
          onKeyDown: this.props.onKeyDown,
          onClick: this.handleClick },
        this.props.children
      )
    );
  };

  return MenuItem;
})(_react2['default'].Component);

exports['default'] = MenuItem;

MenuItem.propTypes = {
  disabled: _react2['default'].PropTypes.bool,
  divider: _utilsCustomPropTypes2['default'].all([_react2['default'].PropTypes.bool, function (props, propName, componentName) {
    if (props.divider && props.children) {
      return new Error('Children will not be rendered for dividers');
    }
  }]),
  eventKey: _react2['default'].PropTypes.oneOfType([_react2['default'].PropTypes.number, _react2['default'].PropTypes.string]),
  header: _react2['default'].PropTypes.bool,
  href: _react2['default'].PropTypes.string,
  target: _react2['default'].PropTypes.string,
  title: _react2['default'].PropTypes.string,
  onKeyDown: _react2['default'].PropTypes.func,
  onSelect: _react2['default'].PropTypes.func,
  id: _react2['default'].PropTypes.oneOfType([_react2['default'].PropTypes.string, _react2['default'].PropTypes.number])
};

MenuItem.defaultProps = {
  divider: false,
  disabled: false,
  header: false
};
module.exports = exports['default'];
},{"./SafeAnchor":59,"./utils/CustomPropTypes":62,"babel-runtime/helpers/class-call-check":73,"babel-runtime/helpers/inherits":75,"babel-runtime/helpers/interop-require-default":76,"classnames":102,"react":undefined}],55:[function(require,module,exports){
'use strict';

var _objectWithoutProperties = require('babel-runtime/helpers/object-without-properties')['default'];

var _extends = require('babel-runtime/helpers/extends')['default'];

var _interopRequireDefault = require('babel-runtime/helpers/interop-require-default')['default'];

exports.__esModule = true;

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _classnames = require('classnames');

var _classnames2 = _interopRequireDefault(_classnames);

var _BootstrapMixin = require('./BootstrapMixin');

var _BootstrapMixin2 = _interopRequireDefault(_BootstrapMixin);

var _SafeAnchor = require('./SafeAnchor');

var _SafeAnchor2 = _interopRequireDefault(_SafeAnchor);

var NavItem = _react2['default'].createClass({
  displayName: 'NavItem',

  mixins: [_BootstrapMixin2['default']],

  propTypes: {
    linkId: _react2['default'].PropTypes.string,
    onSelect: _react2['default'].PropTypes.func,
    active: _react2['default'].PropTypes.bool,
    disabled: _react2['default'].PropTypes.bool,
    href: _react2['default'].PropTypes.string,
    role: _react2['default'].PropTypes.string,
    title: _react2['default'].PropTypes.node,
    eventKey: _react2['default'].PropTypes.any,
    target: _react2['default'].PropTypes.string,
    'aria-controls': _react2['default'].PropTypes.string
  },

  getDefaultProps: function getDefaultProps() {
    return {
      active: false,
      disabled: false
    };
  },

  render: function render() {
    var _props = this.props;
    var role = _props.role;
    var linkId = _props.linkId;
    var disabled = _props.disabled;
    var active = _props.active;
    var href = _props.href;
    var title = _props.title;
    var target = _props.target;
    var children = _props.children;
    var ariaControls = _props['aria-controls'];

    var props = _objectWithoutProperties(_props, ['role', 'linkId', 'disabled', 'active', 'href', 'title', 'target', 'children', 'aria-controls']);

    var classes = {
      active: active,
      disabled: disabled
    };
    var linkProps = {
      role: role,
      href: href,
      title: title,
      target: target,
      id: linkId,
      onClick: this.handleClick
    };

    if (!role && href === '#') {
      linkProps.role = 'button';
    }

    return _react2['default'].createElement(
      'li',
      _extends({}, props, { role: 'presentation', className: _classnames2['default'](props.className, classes) }),
      _react2['default'].createElement(
        _SafeAnchor2['default'],
        _extends({}, linkProps, { 'aria-selected': active, 'aria-controls': ariaControls }),
        children
      )
    );
  },

  handleClick: function handleClick(e) {
    if (this.props.onSelect) {
      e.preventDefault();

      if (!this.props.disabled) {
        this.props.onSelect(this.props.eventKey, this.props.href, this.props.target);
      }
    }
  }
});

exports['default'] = NavItem;
module.exports = exports['default'];
},{"./BootstrapMixin":46,"./SafeAnchor":59,"babel-runtime/helpers/extends":74,"babel-runtime/helpers/interop-require-default":76,"babel-runtime/helpers/object-without-properties":77,"classnames":102,"react":undefined}],56:[function(require,module,exports){
/* eslint react/prop-types: [2, {ignore: ["container", "containerPadding", "target", "placement", "children"] }] */
/* These properties are validated in 'Portal' and 'Position' components */

'use strict';

var _inherits = require('babel-runtime/helpers/inherits')['default'];

var _classCallCheck = require('babel-runtime/helpers/class-call-check')['default'];

var _extends = require('babel-runtime/helpers/extends')['default'];

var _objectWithoutProperties = require('babel-runtime/helpers/object-without-properties')['default'];

var _interopRequireDefault = require('babel-runtime/helpers/interop-require-default')['default'];

exports.__esModule = true;

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _reactOverlaysLibOverlay = require('react-overlays/lib/Overlay');

var _reactOverlaysLibOverlay2 = _interopRequireDefault(_reactOverlaysLibOverlay);

var _utilsCustomPropTypes = require('./utils/CustomPropTypes');

var _utilsCustomPropTypes2 = _interopRequireDefault(_utilsCustomPropTypes);

var _Fade = require('./Fade');

var _Fade2 = _interopRequireDefault(_Fade);

var _classnames = require('classnames');

var _classnames2 = _interopRequireDefault(_classnames);

var Overlay = (function (_React$Component) {
  _inherits(Overlay, _React$Component);

  function Overlay() {
    _classCallCheck(this, Overlay);

    _React$Component.apply(this, arguments);
  }

  Overlay.prototype.render = function render() {
    var _props = this.props;
    var child = _props.children;
    var transition = _props.animation;

    var props = _objectWithoutProperties(_props, ['children', 'animation']);

    if (transition === true) {
      transition = _Fade2['default'];
    }

    if (!transition) {
      child = _react.cloneElement(child, {
        className: _classnames2['default']('in', child.props.className)
      });
    }

    return _react2['default'].createElement(
      _reactOverlaysLibOverlay2['default'],
      _extends({}, props, {
        transition: transition
      }),
      child
    );
  };

  return Overlay;
})(_react2['default'].Component);

Overlay.propTypes = _extends({}, _reactOverlaysLibOverlay2['default'].propTypes, {

  /**
   * Set the visibility of the Overlay
   */
  show: _react2['default'].PropTypes.bool,
  /**
   * Specify whether the overlay should trigger onHide when the user clicks outside the overlay
   */
  rootClose: _react2['default'].PropTypes.bool,
  /**
   * A Callback fired by the Overlay when it wishes to be hidden.
   */
  onHide: _react2['default'].PropTypes.func,

  /**
   * Use animation
   */
  animation: _react2['default'].PropTypes.oneOfType([_react2['default'].PropTypes.bool, _utilsCustomPropTypes2['default'].elementType]),

  /**
   * Callback fired before the Overlay transitions in
   */
  onEnter: _react2['default'].PropTypes.func,

  /**
   * Callback fired as the Overlay begins to transition in
   */
  onEntering: _react2['default'].PropTypes.func,

  /**
   * Callback fired after the Overlay finishes transitioning in
   */
  onEntered: _react2['default'].PropTypes.func,

  /**
   * Callback fired right before the Overlay transitions out
   */
  onExit: _react2['default'].PropTypes.func,

  /**
   * Callback fired as the Overlay begins to transition out
   */
  onExiting: _react2['default'].PropTypes.func,

  /**
   * Callback fired after the Overlay finishes transitioning out
   */
  onExited: _react2['default'].PropTypes.func
});

Overlay.defaultProps = {
  animation: _Fade2['default'],
  rootClose: false,
  show: false
};

exports['default'] = Overlay;
module.exports = exports['default'];
},{"./Fade":49,"./utils/CustomPropTypes":62,"babel-runtime/helpers/class-call-check":73,"babel-runtime/helpers/extends":74,"babel-runtime/helpers/inherits":75,"babel-runtime/helpers/interop-require-default":76,"babel-runtime/helpers/object-without-properties":77,"classnames":102,"react":undefined,"react-overlays/lib/Overlay":123}],57:[function(require,module,exports){
/*eslint-disable react/prop-types */
'use strict';

var _extends = require('babel-runtime/helpers/extends')['default'];

var _Object$keys = require('babel-runtime/core-js/object/keys')['default'];

var _interopRequireDefault = require('babel-runtime/helpers/interop-require-default')['default'];

exports.__esModule = true;

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _utilsCreateChainedFunction = require('./utils/createChainedFunction');

var _utilsCreateChainedFunction2 = _interopRequireDefault(_utilsCreateChainedFunction);

var _utilsCreateContextWrapper = require('./utils/createContextWrapper');

var _utilsCreateContextWrapper2 = _interopRequireDefault(_utilsCreateContextWrapper);

var _Overlay = require('./Overlay');

var _Overlay2 = _interopRequireDefault(_Overlay);

var _reactLibWarning = require('react/lib/warning');

var _reactLibWarning2 = _interopRequireDefault(_reactLibWarning);

var _lodashObjectPick = require('lodash/object/pick');

var _lodashObjectPick2 = _interopRequireDefault(_lodashObjectPick);

/**
 * Check if value one is inside or equal to the of value
 *
 * @param {string} one
 * @param {string|array} of
 * @returns {boolean}
 */
function isOneOf(one, of) {
  if (Array.isArray(of)) {
    return of.indexOf(one) >= 0;
  }
  return one === of;
}

var OverlayTrigger = _react2['default'].createClass({
  displayName: 'OverlayTrigger',

  propTypes: _extends({}, _Overlay2['default'].propTypes, {

    /**
    * Specify which action or actions trigger Overlay visibility
    */
    trigger: _react2['default'].PropTypes.oneOfType([_react2['default'].PropTypes.oneOf(['click', 'hover', 'focus']), _react2['default'].PropTypes.arrayOf(_react2['default'].PropTypes.oneOf(['click', 'hover', 'focus']))]),

    /**
     * A millisecond delay amount to show and hide the Overlay once triggered
     */
    delay: _react2['default'].PropTypes.number,
    /**
     * A millisecond delay amount before showing the Overlay once triggered.
     */
    delayShow: _react2['default'].PropTypes.number,
    /**
     * A millisecond delay amount before hiding the Overlay once triggered.
     */
    delayHide: _react2['default'].PropTypes.number,

    /**
     * The initial visibility state of the Overlay, for more nuanced visibility controll consider
     * using the Overlay component directly.
     */
    defaultOverlayShown: _react2['default'].PropTypes.bool,

    /**
     * An element or text to overlay next to the target.
     */
    overlay: _react2['default'].PropTypes.node.isRequired,

    /**
     * @private
     */
    onBlur: _react2['default'].PropTypes.func,
    /**
     * @private
     */
    onClick: _react2['default'].PropTypes.func,
    /**
     * @private
     */
    onFocus: _react2['default'].PropTypes.func,
    /**
     * @private
     */
    onMouseEnter: _react2['default'].PropTypes.func,
    /**
     * @private
     */
    onMouseLeave: _react2['default'].PropTypes.func,

    //override specific overlay props
    /**
     * @private
     */
    target: function target() {},
    /**
    * @private
    */
    onHide: function onHide() {},
    /**
     * @private
     */
    show: function show() {}
  }),

  getDefaultProps: function getDefaultProps() {
    return {
      defaultOverlayShown: false,
      trigger: ['hover', 'focus']
    };
  },

  getInitialState: function getInitialState() {
    return {
      isOverlayShown: this.props.defaultOverlayShown
    };
  },

  show: function show() {
    this.setState({
      isOverlayShown: true
    });
  },

  hide: function hide() {
    this.setState({
      isOverlayShown: false
    });
  },

  toggle: function toggle() {
    if (this.state.isOverlayShown) {
      this.hide();
    } else {
      this.show();
    }
  },

  componentDidMount: function componentDidMount() {
    this._mountNode = document.createElement('div');
    _react2['default'].render(this._overlay, this._mountNode);
  },

  componentWillUnmount: function componentWillUnmount() {
    _react2['default'].unmountComponentAtNode(this._mountNode);
    this._mountNode = null;
    clearTimeout(this._hoverDelay);
  },

  componentDidUpdate: function componentDidUpdate() {
    if (this._mountNode) {
      _react2['default'].render(this._overlay, this._mountNode);
    }
  },

  getOverlayTarget: function getOverlayTarget() {
    return _react2['default'].findDOMNode(this);
  },

  getOverlay: function getOverlay() {
    var overlayProps = _extends({}, _lodashObjectPick2['default'](this.props, _Object$keys(_Overlay2['default'].propTypes)), {
      show: this.state.isOverlayShown,
      onHide: this.hide,
      target: this.getOverlayTarget,
      onExit: this.props.onExit,
      onExiting: this.props.onExiting,
      onExited: this.props.onExited,
      onEnter: this.props.onEnter,
      onEntering: this.props.onEntering,
      onEntered: this.props.onEntered
    });

    var overlay = _react.cloneElement(this.props.overlay, {
      placement: overlayProps.placement,
      container: overlayProps.container
    });

    return _react2['default'].createElement(
      _Overlay2['default'],
      overlayProps,
      overlay
    );
  },

  render: function render() {
    var trigger = _react2['default'].Children.only(this.props.children);

    var props = {
      'aria-describedby': this.props.overlay.props.id
    };

    // create in render otherwise owner is lost...
    this._overlay = this.getOverlay();

    props.onClick = _utilsCreateChainedFunction2['default'](trigger.props.onClick, this.props.onClick);

    if (isOneOf('click', this.props.trigger)) {
      props.onClick = _utilsCreateChainedFunction2['default'](this.toggle, props.onClick);
    }

    if (isOneOf('hover', this.props.trigger)) {
      _reactLibWarning2['default'](!(this.props.trigger === 'hover'), '[react-bootstrap] Specifying only the `"hover"` trigger limits the visibilty of the overlay to just mouse users. ' + 'Consider also including the `"focus"` trigger so that touch and keyboard only users can see the overlay as well.');

      props.onMouseOver = _utilsCreateChainedFunction2['default'](this.handleDelayedShow, this.props.onMouseOver);
      props.onMouseOut = _utilsCreateChainedFunction2['default'](this.handleDelayedHide, this.props.onMouseOut);
    }

    if (isOneOf('focus', this.props.trigger)) {
      props.onFocus = _utilsCreateChainedFunction2['default'](this.handleDelayedShow, this.props.onFocus);
      props.onBlur = _utilsCreateChainedFunction2['default'](this.handleDelayedHide, this.props.onBlur);
    }

    return _react.cloneElement(trigger, props);
  },

  handleDelayedShow: function handleDelayedShow() {
    var _this = this;

    if (this._hoverDelay != null) {
      clearTimeout(this._hoverDelay);
      this._hoverDelay = null;
      return;
    }

    var delay = this.props.delayShow != null ? this.props.delayShow : this.props.delay;

    if (!delay) {
      this.show();
      return;
    }

    this._hoverDelay = setTimeout(function () {
      _this._hoverDelay = null;
      _this.show();
    }, delay);
  },

  handleDelayedHide: function handleDelayedHide() {
    var _this2 = this;

    if (this._hoverDelay != null) {
      clearTimeout(this._hoverDelay);
      this._hoverDelay = null;
      return;
    }

    var delay = this.props.delayHide != null ? this.props.delayHide : this.props.delay;

    if (!delay) {
      this.hide();
      return;
    }

    this._hoverDelay = setTimeout(function () {
      _this2._hoverDelay = null;
      _this2.hide();
    }, delay);
  }

});

/**
 * Creates a new OverlayTrigger class that forwards the relevant context
 *
 * This static method should only be called at the module level, instead of in
 * e.g. a render() method, because it's expensive to create new classes.
 *
 * For example, you would want to have:
 *
 * > export default OverlayTrigger.withContext({
 * >   myContextKey: React.PropTypes.object
 * > });
 *
 * and import this when needed.
 */
OverlayTrigger.withContext = _utilsCreateContextWrapper2['default'](OverlayTrigger, 'overlay');

exports['default'] = OverlayTrigger;
module.exports = exports['default'];
},{"./Overlay":56,"./utils/createChainedFunction":66,"./utils/createContextWrapper":67,"babel-runtime/core-js/object/keys":71,"babel-runtime/helpers/extends":74,"babel-runtime/helpers/interop-require-default":76,"lodash/object/pick":44,"react":undefined,"react/lib/warning":152}],58:[function(require,module,exports){
'use strict';

var _extends = require('babel-runtime/helpers/extends')['default'];

var _interopRequireDefault = require('babel-runtime/helpers/interop-require-default')['default'];

exports.__esModule = true;

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _classnames = require('classnames');

var _classnames2 = _interopRequireDefault(_classnames);

var _SafeAnchor = require('./SafeAnchor');

var _SafeAnchor2 = _interopRequireDefault(_SafeAnchor);

var PageItem = _react2['default'].createClass({
  displayName: 'PageItem',

  propTypes: {
    href: _react2['default'].PropTypes.string,
    target: _react2['default'].PropTypes.string,
    title: _react2['default'].PropTypes.string,
    disabled: _react2['default'].PropTypes.bool,
    previous: _react2['default'].PropTypes.bool,
    next: _react2['default'].PropTypes.bool,
    onSelect: _react2['default'].PropTypes.func,
    eventKey: _react2['default'].PropTypes.any
  },

  getDefaultProps: function getDefaultProps() {
    return {
      disabled: false,
      previous: false,
      next: false
    };
  },

  render: function render() {
    var classes = {
      'disabled': this.props.disabled,
      'previous': this.props.previous,
      'next': this.props.next
    };

    return _react2['default'].createElement(
      'li',
      _extends({}, this.props, {
        className: _classnames2['default'](this.props.className, classes) }),
      _react2['default'].createElement(
        _SafeAnchor2['default'],
        {
          href: this.props.href,
          title: this.props.title,
          target: this.props.target,
          onClick: this.handleSelect },
        this.props.children
      )
    );
  },

  handleSelect: function handleSelect(e) {
    if (this.props.onSelect || this.props.disabled) {
      e.preventDefault();

      if (!this.props.disabled) {
        this.props.onSelect(this.props.eventKey, this.props.href, this.props.target);
      }
    }
  }
});

exports['default'] = PageItem;
module.exports = exports['default'];
},{"./SafeAnchor":59,"babel-runtime/helpers/extends":74,"babel-runtime/helpers/interop-require-default":76,"classnames":102,"react":undefined}],59:[function(require,module,exports){
'use strict';

var _inherits = require('babel-runtime/helpers/inherits')['default'];

var _classCallCheck = require('babel-runtime/helpers/class-call-check')['default'];

var _extends = require('babel-runtime/helpers/extends')['default'];

var _interopRequireDefault = require('babel-runtime/helpers/interop-require-default')['default'];

exports.__esModule = true;

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _utilsCreateChainedFunction = require('./utils/createChainedFunction');

var _utilsCreateChainedFunction2 = _interopRequireDefault(_utilsCreateChainedFunction);

/**
 * Note: This is intended as a stop-gap for accessibility concerns that the
 * Bootstrap CSS does not address as they have styled anchors and not buttons
 * in many cases.
 */

var SafeAnchor = (function (_React$Component) {
  _inherits(SafeAnchor, _React$Component);

  function SafeAnchor(props) {
    _classCallCheck(this, SafeAnchor);

    _React$Component.call(this, props);

    this.handleClick = this.handleClick.bind(this);
  }

  SafeAnchor.prototype.handleClick = function handleClick(event) {
    if (this.props.href === undefined) {
      event.preventDefault();
    }
  };

  SafeAnchor.prototype.render = function render() {
    return _react2['default'].createElement('a', _extends({ role: this.props.href ? undefined : 'button'
    }, this.props, {
      onClick: _utilsCreateChainedFunction2['default'](this.props.onClick, this.handleClick),
      href: this.props.href || '' }));
  };

  return SafeAnchor;
})(_react2['default'].Component);

exports['default'] = SafeAnchor;

SafeAnchor.propTypes = {
  href: _react2['default'].PropTypes.string,
  onClick: _react2['default'].PropTypes.func
};
module.exports = exports['default'];
},{"./utils/createChainedFunction":66,"babel-runtime/helpers/class-call-check":73,"babel-runtime/helpers/extends":74,"babel-runtime/helpers/inherits":75,"babel-runtime/helpers/interop-require-default":76,"react":undefined}],60:[function(require,module,exports){
'use strict';

var _extends = require('babel-runtime/helpers/extends')['default'];

var _interopRequireDefault = require('babel-runtime/helpers/interop-require-default')['default'];

exports.__esModule = true;

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _classnames = require('classnames');

var _classnames2 = _interopRequireDefault(_classnames);

var _BootstrapMixin = require('./BootstrapMixin');

var _BootstrapMixin2 = _interopRequireDefault(_BootstrapMixin);

var _SafeAnchor = require('./SafeAnchor');

var _SafeAnchor2 = _interopRequireDefault(_SafeAnchor);

var Thumbnail = _react2['default'].createClass({
  displayName: 'Thumbnail',

  mixins: [_BootstrapMixin2['default']],

  propTypes: {
    alt: _react2['default'].PropTypes.string,
    href: _react2['default'].PropTypes.string,
    src: _react2['default'].PropTypes.string
  },

  getDefaultProps: function getDefaultProps() {
    return {
      bsClass: 'thumbnail'
    };
  },

  render: function render() {
    var classes = this.getBsClassSet();

    if (this.props.href) {
      return _react2['default'].createElement(
        _SafeAnchor2['default'],
        _extends({}, this.props, { href: this.props.href, className: _classnames2['default'](this.props.className, classes) }),
        _react2['default'].createElement('img', { src: this.props.src, alt: this.props.alt })
      );
    } else {
      if (this.props.children) {
        return _react2['default'].createElement(
          'div',
          _extends({}, this.props, { className: _classnames2['default'](this.props.className, classes) }),
          _react2['default'].createElement('img', { src: this.props.src, alt: this.props.alt }),
          _react2['default'].createElement(
            'div',
            { className: 'caption' },
            this.props.children
          )
        );
      } else {
        return _react2['default'].createElement(
          'div',
          _extends({}, this.props, { className: _classnames2['default'](this.props.className, classes) }),
          _react2['default'].createElement('img', { src: this.props.src, alt: this.props.alt })
        );
      }
    }
  }
});

exports['default'] = Thumbnail;
module.exports = exports['default'];
},{"./BootstrapMixin":46,"./SafeAnchor":59,"babel-runtime/helpers/extends":74,"babel-runtime/helpers/interop-require-default":76,"classnames":102,"react":undefined}],61:[function(require,module,exports){
'use strict';

exports.__esModule = true;
var styleMaps = {
  CLASSES: {
    'alert': 'alert',
    'button': 'btn',
    'button-group': 'btn-group',
    'button-toolbar': 'btn-toolbar',
    'column': 'col',
    'input-group': 'input-group',
    'form': 'form',
    'glyphicon': 'glyphicon',
    'label': 'label',
    'thumbnail': 'thumbnail',
    'list-group-item': 'list-group-item',
    'panel': 'panel',
    'panel-group': 'panel-group',
    'pagination': 'pagination',
    'progress-bar': 'progress-bar',
    'nav': 'nav',
    'navbar': 'navbar',
    'modal': 'modal',
    'row': 'row',
    'well': 'well'
  },
  STYLES: ['default', 'primary', 'success', 'info', 'warning', 'danger', 'link', 'inline', 'tabs', 'pills'],
  addStyle: function addStyle(name) {
    styleMaps.STYLES.push(name);
  },
  SIZES: {
    'large': 'lg',
    'medium': 'md',
    'small': 'sm',
    'xsmall': 'xs',
    'lg': 'lg',
    'md': 'md',
    'sm': 'sm',
    'xs': 'xs'
  },
  GRID_COLUMNS: 12
};

exports['default'] = styleMaps;
module.exports = exports['default'];
},{}],62:[function(require,module,exports){
'use strict';

var _Object$keys = require('babel-runtime/core-js/object/keys')['default'];

var _interopRequireDefault = require('babel-runtime/helpers/interop-require-default')['default'];

exports.__esModule = true;

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _reactLibWarning = require('react/lib/warning');

var _reactLibWarning2 = _interopRequireDefault(_reactLibWarning);

var _childrenToArray = require('./childrenToArray');

var _childrenToArray2 = _interopRequireDefault(_childrenToArray);

var ANONYMOUS = '<<anonymous>>';

/**
 * Create chain-able isRequired validator
 *
 * Largely copied directly from:
 *  https://github.com/facebook/react/blob/0.11-stable/src/core/ReactPropTypes.js#L94
 */
function createChainableTypeChecker(validate) {
  function checkType(isRequired, props, propName, componentName) {
    componentName = componentName || ANONYMOUS;
    if (props[propName] == null) {
      if (isRequired) {
        return new Error('Required prop \'' + propName + '\' was not specified in \'' + componentName + '\'.');
      }
    } else {
      return validate(props, propName, componentName);
    }
  }

  var chainedCheckType = checkType.bind(null, false);
  chainedCheckType.isRequired = checkType.bind(null, true);

  return chainedCheckType;
}

var CustomPropTypes = {

  deprecated: function deprecated(propType, explanation) {
    return function (props, propName, componentName) {
      if (props[propName] != null) {
        _reactLibWarning2['default'](false, '"' + propName + '" property of "' + componentName + '" has been deprecated.\n' + explanation);
      }

      return propType(props, propName, componentName);
    };
  },

  isRequiredForA11y: function isRequiredForA11y(propType) {
    return function (props, propName, componentName) {
      if (props[propName] == null) {
        return new Error('The prop `' + propName + '` is required to make ' + componentName + ' accessible ' + 'for users using assistive technologies such as screen readers `');
      }

      return propType(props, propName, componentName);
    };
  },

  requiredRoles: function requiredRoles() {
    for (var _len = arguments.length, roles = Array(_len), _key = 0; _key < _len; _key++) {
      roles[_key] = arguments[_key];
    }

    return createChainableTypeChecker(function requiredRolesValidator(props, propName, component) {
      var missing = undefined;
      var children = _childrenToArray2['default'](props.children);

      var inRole = function inRole(role, child) {
        return role === child.props.bsRole;
      };

      roles.every(function (role) {
        if (!children.some(function (child) {
          return inRole(role, child);
        })) {
          missing = role;
          return false;
        }
        return true;
      });

      if (missing) {
        return new Error('(children) ' + component + ' - Missing a required child with bsRole: ' + missing + '. ' + (component + ' must have at least one child of each of the following bsRoles: ' + roles.join(', ')));
      }
    });
  },

  exclusiveRoles: function exclusiveRoles() {
    for (var _len2 = arguments.length, roles = Array(_len2), _key2 = 0; _key2 < _len2; _key2++) {
      roles[_key2] = arguments[_key2];
    }

    return createChainableTypeChecker(function exclusiveRolesValidator(props, propName, component) {
      var children = _childrenToArray2['default'](props.children);
      var duplicate = undefined;

      roles.every(function (role) {
        var childrenWithRole = children.filter(function (child) {
          return child.props.bsRole === role;
        });

        if (childrenWithRole.length > 1) {
          duplicate = role;
          return false;
        }
        return true;
      });

      if (duplicate) {
        return new Error('(children) ' + component + ' - Duplicate children detected of bsRole: ' + duplicate + '. ' + ('Only one child each allowed with the following bsRoles: ' + roles.join(', ')));
      }
    });
  },

  /**
   * Checks whether a prop provides a DOM element
   *
   * The element can be provided in two forms:
   * - Directly passed
   * - Or passed an object that has a `render` method
   *
   * @param props
   * @param propName
   * @param componentName
   * @returns {Error|undefined}
   */
  mountable: createMountableChecker(),

  /**
   * Checks whether a prop provides a type of element.
   *
   * The type of element can be provided in two forms:
   * - tag name (string)
   * - a return value of React.createClass(...)
   *
   * @param props
   * @param propName
   * @param componentName
   * @returns {Error|undefined}
   */
  elementType: createElementTypeChecker(),

  /**
   * Checks whether a prop matches a key of an associated object
   *
   * @param props
   * @param propName
   * @param componentName
   * @returns {Error|undefined}
   */
  keyOf: createKeyOfChecker,
  /**
   * Checks if only one of the listed properties is in use. An error is given
   * if multiple have a value
   *
   * @param props
   * @param propName
   * @param componentName
   * @returns {Error|undefined}
   */
  singlePropFrom: createSinglePropFromChecker,

  all: all
};

function errMsg(props, propName, componentName, msgContinuation) {
  return 'Invalid prop \'' + propName + '\' of value \'' + props[propName] + '\'' + (' supplied to \'' + componentName + '\'' + msgContinuation);
}

function createMountableChecker() {
  function validate(props, propName, componentName) {
    if (typeof props[propName] !== 'object' || typeof props[propName].render !== 'function' && props[propName].nodeType !== 1) {
      return new Error(errMsg(props, propName, componentName, ', expected a DOM element or an object that has a `render` method'));
    }
  }

  return createChainableTypeChecker(validate);
}

function createKeyOfChecker(obj) {
  function validate(props, propName, componentName) {
    var propValue = props[propName];
    if (!obj.hasOwnProperty(propValue)) {
      var valuesString = JSON.stringify(_Object$keys(obj));
      return new Error(errMsg(props, propName, componentName, ', expected one of ' + valuesString + '.'));
    }
  }
  return createChainableTypeChecker(validate);
}

function createSinglePropFromChecker(arrOfProps) {
  function validate(props, propName, componentName) {
    var usedPropCount = arrOfProps.map(function (listedProp) {
      return props[listedProp];
    }).reduce(function (acc, curr) {
      return acc + (curr !== undefined ? 1 : 0);
    }, 0);

    if (usedPropCount > 1) {
      var first = arrOfProps[0];
      var others = arrOfProps.slice(1);

      var message = others.join(', ') + ' and ' + first;
      return new Error('Invalid prop \'' + propName + '\', only one of the following ' + ('may be provided: ' + message));
    }
  }
  return validate;
}

function all(propTypes) {
  if (propTypes === undefined) {
    throw new Error('No validations provided');
  }

  if (!(propTypes instanceof Array)) {
    throw new Error('Invalid argument must be an array');
  }

  if (propTypes.length === 0) {
    throw new Error('No validations provided');
  }

  return function (props, propName, componentName) {
    for (var i = 0; i < propTypes.length; i++) {
      var result = propTypes[i](props, propName, componentName);

      if (result !== undefined && result !== null) {
        return result;
      }
    }
  };
}

function createElementTypeChecker() {
  function validate(props, propName, componentName) {
    var errBeginning = errMsg(props, propName, componentName, '. Expected an Element `type`');

    if (typeof props[propName] !== 'function') {
      if (_react2['default'].isValidElement(props[propName])) {
        return new Error(errBeginning + ', not an actual Element');
      }

      if (typeof props[propName] !== 'string') {
        return new Error(errBeginning + ' such as a tag name or return value of React.createClass(...)');
      }
    }
  }

  return createChainableTypeChecker(validate);
}

exports['default'] = CustomPropTypes;
module.exports = exports['default'];
},{"./childrenToArray":64,"babel-runtime/core-js/object/keys":71,"babel-runtime/helpers/interop-require-default":76,"react":undefined,"react/lib/warning":152}],63:[function(require,module,exports){
'use strict';

var _interopRequireDefault = require('babel-runtime/helpers/interop-require-default')['default'];

exports.__esModule = true;

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

/**
 * Maps children that are typically specified as `props.children`,
 * but only iterates over children that are "valid components".
 *
 * The mapFunction provided index will be normalised to the components mapped,
 * so an invalid component would not increase the index.
 *
 * @param {?*} children Children tree container.
 * @param {function(*, int)} mapFunction.
 * @param {*} mapContext Context for mapFunction.
 * @return {object} Object containing the ordered map of results.
 */
function mapValidComponents(children, func, context) {
  var index = 0;

  return _react2['default'].Children.map(children, function (child) {
    if (_react2['default'].isValidElement(child)) {
      var lastIndex = index;
      index++;
      return func.call(context, child, lastIndex);
    }

    return child;
  });
}

/**
 * Iterates through children that are typically specified as `props.children`,
 * but only iterates over children that are "valid components".
 *
 * The provided forEachFunc(child, index) will be called for each
 * leaf child with the index reflecting the position relative to "valid components".
 *
 * @param {?*} children Children tree container.
 * @param {function(*, int)} forEachFunc.
 * @param {*} forEachContext Context for forEachContext.
 */
function forEachValidComponents(children, func, context) {
  var index = 0;

  return _react2['default'].Children.forEach(children, function (child) {
    if (_react2['default'].isValidElement(child)) {
      func.call(context, child, index);
      index++;
    }
  });
}

/**
 * Count the number of "valid components" in the Children container.
 *
 * @param {?*} children Children tree container.
 * @returns {number}
 */
function numberOfValidComponents(children) {
  var count = 0;

  _react2['default'].Children.forEach(children, function (child) {
    if (_react2['default'].isValidElement(child)) {
      count++;
    }
  });

  return count;
}

/**
 * Determine if the Child container has one or more "valid components".
 *
 * @param {?*} children Children tree container.
 * @returns {boolean}
 */
function hasValidComponent(children) {
  var hasValid = false;

  _react2['default'].Children.forEach(children, function (child) {
    if (!hasValid && _react2['default'].isValidElement(child)) {
      hasValid = true;
    }
  });

  return hasValid;
}

exports['default'] = {
  map: mapValidComponents,
  forEach: forEachValidComponents,
  numberOf: numberOfValidComponents,
  hasValidComponent: hasValidComponent
};
module.exports = exports['default'];
},{"babel-runtime/helpers/interop-require-default":76,"react":undefined}],64:[function(require,module,exports){
'use strict';

var _interopRequireDefault = require('babel-runtime/helpers/interop-require-default')['default'];

exports.__esModule = true;
exports['default'] = childrenAsArray;

var _ValidComponentChildren = require('./ValidComponentChildren');

var _ValidComponentChildren2 = _interopRequireDefault(_ValidComponentChildren);

function childrenAsArray(children) {
  var result = [];

  if (children === undefined) {
    return result;
  }

  _ValidComponentChildren2['default'].forEach(children, function (child) {
    result.push(child);
  });

  return result;
}

module.exports = exports['default'];
},{"./ValidComponentChildren":63,"babel-runtime/helpers/interop-require-default":76}],65:[function(require,module,exports){
'use strict';

var _interopRequireDefault = require('babel-runtime/helpers/interop-require-default')['default'];

exports.__esModule = true;
exports['default'] = valueValidation;

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _CustomPropTypes = require('./CustomPropTypes');

var propList = ['children', 'value'];
var typeList = [_react2['default'].PropTypes.number, _react2['default'].PropTypes.string];

function valueValidation(props, propName, componentName) {
  var error = _CustomPropTypes.singlePropFrom(propList)(props, propName, componentName);
  if (!error) {
    var oneOfType = _react2['default'].PropTypes.oneOfType(typeList);
    error = oneOfType(props, propName, componentName);
  }
  return error;
}

module.exports = exports['default'];
},{"./CustomPropTypes":62,"babel-runtime/helpers/interop-require-default":76,"react":undefined}],66:[function(require,module,exports){
/**
 * Safe chained function
 *
 * Will only create a new function if needed,
 * otherwise will pass back existing functions or null.
 *
 * @param {function} functions to chain
 * @returns {function|null}
 */
'use strict';

exports.__esModule = true;
function createChainedFunction() {
  for (var _len = arguments.length, funcs = Array(_len), _key = 0; _key < _len; _key++) {
    funcs[_key] = arguments[_key];
  }

  return funcs.filter(function (f) {
    return f != null;
  }).reduce(function (acc, f) {
    if (typeof f !== 'function') {
      throw new Error('Invalid Argument Type, must only provide functions, undefined, or null.');
    }

    if (acc === null) {
      return f;
    }

    return function chainedFunction() {
      for (var _len2 = arguments.length, args = Array(_len2), _key2 = 0; _key2 < _len2; _key2++) {
        args[_key2] = arguments[_key2];
      }

      acc.apply(this, args);
      f.apply(this, args);
    };
  }, null);
}

exports['default'] = createChainedFunction;
module.exports = exports['default'];
},{}],67:[function(require,module,exports){
'use strict';

var _inherits = require('babel-runtime/helpers/inherits')['default'];

var _classCallCheck = require('babel-runtime/helpers/class-call-check')['default'];

var _extends = require('babel-runtime/helpers/extends')['default'];

var _objectWithoutProperties = require('babel-runtime/helpers/object-without-properties')['default'];

var _interopRequireDefault = require('babel-runtime/helpers/interop-require-default')['default'];

exports.__esModule = true;
exports['default'] = createContextWrapper;

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

/**
 * Creates new trigger class that injects context into overlay.
 */

function createContextWrapper(Trigger, propName) {
  return function (contextTypes) {
    var ContextWrapper = (function (_React$Component) {
      _inherits(ContextWrapper, _React$Component);

      function ContextWrapper() {
        _classCallCheck(this, ContextWrapper);

        _React$Component.apply(this, arguments);
      }

      ContextWrapper.prototype.getChildContext = function getChildContext() {
        return this.props.context;
      };

      ContextWrapper.prototype.render = function render() {
        // Strip injected props from below.
        var _props = this.props;
        var wrapped = _props.wrapped;
        var context = _props.context;

        var props = _objectWithoutProperties(_props, ['wrapped', 'context']);

        return _react2['default'].cloneElement(wrapped, props);
      };

      return ContextWrapper;
    })(_react2['default'].Component);

    ContextWrapper.childContextTypes = contextTypes;

    var TriggerWithContext = (function () {
      function TriggerWithContext() {
        _classCallCheck(this, TriggerWithContext);
      }

      TriggerWithContext.prototype.render = function render() {
        var props = _extends({}, this.props);
        props[propName] = this.getWrappedOverlay();

        return _react2['default'].createElement(
          Trigger,
          props,
          this.props.children
        );
      };

      TriggerWithContext.prototype.getWrappedOverlay = function getWrappedOverlay() {
        return _react2['default'].createElement(ContextWrapper, {
          context: this.context,
          wrapped: this.props[propName]
        });
      };

      return TriggerWithContext;
    })();

    TriggerWithContext.contextTypes = contextTypes;

    return TriggerWithContext;
  };
}

module.exports = exports['default'];
},{"babel-runtime/helpers/class-call-check":73,"babel-runtime/helpers/extends":74,"babel-runtime/helpers/inherits":75,"babel-runtime/helpers/interop-require-default":76,"babel-runtime/helpers/object-without-properties":77,"react":undefined}],68:[function(require,module,exports){
'use strict';

var _inherits = require('babel-runtime/helpers/inherits')['default'];

var _classCallCheck = require('babel-runtime/helpers/class-call-check')['default'];

var _interopRequireDefault = require('babel-runtime/helpers/interop-require-default')['default'];

exports.__esModule = true;

var _reactLibWarning = require('react/lib/warning');

var _reactLibWarning2 = _interopRequireDefault(_reactLibWarning);

var warned = {};

function deprecationWarning(oldname, newname, link) {
  var message = undefined;

  if (typeof oldname === 'object') {
    message = oldname.message;
  } else {
    message = oldname + ' is deprecated. Use ' + newname + ' instead.';

    if (link) {
      message += '\nYou can read more about it at ' + link;
    }
  }

  if (warned[message]) {
    return;
  }

  _reactLibWarning2['default'](false, message);
  warned[message] = true;
}

deprecationWarning.wrapper = function (Component) {
  for (var _len = arguments.length, args = Array(_len > 1 ? _len - 1 : 0), _key = 1; _key < _len; _key++) {
    args[_key - 1] = arguments[_key];
  }

  return (function (_Component) {
    _inherits(DeprecatedComponent, _Component);

    function DeprecatedComponent() {
      _classCallCheck(this, DeprecatedComponent);

      _Component.apply(this, arguments);
    }

    DeprecatedComponent.prototype.componentWillMount = function componentWillMount() {
      deprecationWarning.apply(undefined, args);

      if (_Component.prototype.componentWillMount) {
        var _Component$prototype$componentWillMount;

        for (var _len2 = arguments.length, methodArgs = Array(_len2), _key2 = 0; _key2 < _len2; _key2++) {
          methodArgs[_key2] = arguments[_key2];
        }

        (_Component$prototype$componentWillMount = _Component.prototype.componentWillMount).call.apply(_Component$prototype$componentWillMount, [this].concat(methodArgs));
      }
    };

    return DeprecatedComponent;
  })(Component);
};

exports['default'] = deprecationWarning;
module.exports = exports['default'];
},{"babel-runtime/helpers/class-call-check":73,"babel-runtime/helpers/inherits":75,"babel-runtime/helpers/interop-require-default":76,"react/lib/warning":152}],69:[function(require,module,exports){
module.exports = { "default": require("core-js/library/fn/object/assign"), __esModule: true };
},{"core-js/library/fn/object/assign":78}],70:[function(require,module,exports){
module.exports = { "default": require("core-js/library/fn/object/create"), __esModule: true };
},{"core-js/library/fn/object/create":79}],71:[function(require,module,exports){
module.exports = { "default": require("core-js/library/fn/object/keys"), __esModule: true };
},{"core-js/library/fn/object/keys":80}],72:[function(require,module,exports){
module.exports = { "default": require("core-js/library/fn/object/set-prototype-of"), __esModule: true };
},{"core-js/library/fn/object/set-prototype-of":81}],73:[function(require,module,exports){
"use strict";

exports["default"] = function (instance, Constructor) {
  if (!(instance instanceof Constructor)) {
    throw new TypeError("Cannot call a class as a function");
  }
};

exports.__esModule = true;
},{}],74:[function(require,module,exports){
"use strict";

var _Object$assign = require("babel-runtime/core-js/object/assign")["default"];

exports["default"] = _Object$assign || function (target) {
  for (var i = 1; i < arguments.length; i++) {
    var source = arguments[i];

    for (var key in source) {
      if (Object.prototype.hasOwnProperty.call(source, key)) {
        target[key] = source[key];
      }
    }
  }

  return target;
};

exports.__esModule = true;
},{"babel-runtime/core-js/object/assign":69}],75:[function(require,module,exports){
"use strict";

var _Object$create = require("babel-runtime/core-js/object/create")["default"];

var _Object$setPrototypeOf = require("babel-runtime/core-js/object/set-prototype-of")["default"];

exports["default"] = function (subClass, superClass) {
  if (typeof superClass !== "function" && superClass !== null) {
    throw new TypeError("Super expression must either be null or a function, not " + typeof superClass);
  }

  subClass.prototype = _Object$create(superClass && superClass.prototype, {
    constructor: {
      value: subClass,
      enumerable: false,
      writable: true,
      configurable: true
    }
  });
  if (superClass) _Object$setPrototypeOf ? _Object$setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass;
};

exports.__esModule = true;
},{"babel-runtime/core-js/object/create":70,"babel-runtime/core-js/object/set-prototype-of":72}],76:[function(require,module,exports){
"use strict";

exports["default"] = function (obj) {
  return obj && obj.__esModule ? obj : {
    "default": obj
  };
};

exports.__esModule = true;
},{}],77:[function(require,module,exports){
"use strict";

exports["default"] = function (obj, keys) {
  var target = {};

  for (var i in obj) {
    if (keys.indexOf(i) >= 0) continue;
    if (!Object.prototype.hasOwnProperty.call(obj, i)) continue;
    target[i] = obj[i];
  }

  return target;
};

exports.__esModule = true;
},{}],78:[function(require,module,exports){
require('../../modules/es6.object.assign');
module.exports = require('../../modules/$.core').Object.assign;
},{"../../modules/$.core":86,"../../modules/es6.object.assign":99}],79:[function(require,module,exports){
var $ = require('../../modules/$');
module.exports = function create(P, D){
  return $.create(P, D);
};
},{"../../modules/$":95}],80:[function(require,module,exports){
require('../../modules/es6.object.keys');
module.exports = require('../../modules/$.core').Object.keys;
},{"../../modules/$.core":86,"../../modules/es6.object.keys":100}],81:[function(require,module,exports){
require('../../modules/es6.object.set-prototype-of');
module.exports = require('../../modules/$.core').Object.setPrototypeOf;
},{"../../modules/$.core":86,"../../modules/es6.object.set-prototype-of":101}],82:[function(require,module,exports){
module.exports = function(it){
  if(typeof it != 'function')throw TypeError(it + ' is not a function!');
  return it;
};
},{}],83:[function(require,module,exports){
var isObject = require('./$.is-object');
module.exports = function(it){
  if(!isObject(it))throw TypeError(it + ' is not an object!');
  return it;
};
},{"./$.is-object":94}],84:[function(require,module,exports){
// 19.1.2.1 Object.assign(target, source, ...)
var toObject = require('./$.to-object')
  , IObject  = require('./$.iobject')
  , enumKeys = require('./$.enum-keys');

module.exports = require('./$.fails')(function(){
  return Symbol() in Object.assign({}); // Object.assign available and Symbol is native
}) ? function assign(target, source){   // eslint-disable-line no-unused-vars
  var T = toObject(target)
    , l = arguments.length
    , i = 1;
  while(l > i){
    var S      = IObject(arguments[i++])
      , keys   = enumKeys(S)
      , length = keys.length
      , j      = 0
      , key;
    while(length > j)T[key = keys[j++]] = S[key];
  }
  return T;
} : Object.assign;
},{"./$.enum-keys":90,"./$.fails":91,"./$.iobject":93,"./$.to-object":98}],85:[function(require,module,exports){
var toString = {}.toString;

module.exports = function(it){
  return toString.call(it).slice(8, -1);
};
},{}],86:[function(require,module,exports){
var core = module.exports = {};
if(typeof __e == 'number')__e = core; // eslint-disable-line no-undef
},{}],87:[function(require,module,exports){
// optional / simple context binding
var aFunction = require('./$.a-function');
module.exports = function(fn, that, length){
  aFunction(fn);
  if(that === undefined)return fn;
  switch(length){
    case 1: return function(a){
      return fn.call(that, a);
    };
    case 2: return function(a, b){
      return fn.call(that, a, b);
    };
    case 3: return function(a, b, c){
      return fn.call(that, a, b, c);
    };
  } return function(/* ...args */){
      return fn.apply(that, arguments);
    };
};
},{"./$.a-function":82}],88:[function(require,module,exports){
var global    = require('./$.global')
  , core      = require('./$.core')
  , PROTOTYPE = 'prototype';
var ctx = function(fn, that){
  return function(){
    return fn.apply(that, arguments);
  };
};
var $def = function(type, name, source){
  var key, own, out, exp
    , isGlobal = type & $def.G
    , isProto  = type & $def.P
    , target   = isGlobal ? global : type & $def.S
        ? global[name] : (global[name] || {})[PROTOTYPE]
    , exports  = isGlobal ? core : core[name] || (core[name] = {});
  if(isGlobal)source = name;
  for(key in source){
    // contains in native
    own = !(type & $def.F) && target && key in target;
    if(own && key in exports)continue;
    // export native or passed
    out = own ? target[key] : source[key];
    // prevent global pollution for namespaces
    if(isGlobal && typeof target[key] != 'function')exp = source[key];
    // bind timers to global for call from export context
    else if(type & $def.B && own)exp = ctx(out, global);
    // wrap global constructors for prevent change them in library
    else if(type & $def.W && target[key] == out)!function(C){
      exp = function(param){
        return this instanceof C ? new C(param) : C(param);
      };
      exp[PROTOTYPE] = C[PROTOTYPE];
    }(out);
    else exp = isProto && typeof out == 'function' ? ctx(Function.call, out) : out;
    // export
    exports[key] = exp;
    if(isProto)(exports[PROTOTYPE] || (exports[PROTOTYPE] = {}))[key] = out;
  }
};
// type bitmap
$def.F = 1;  // forced
$def.G = 2;  // global
$def.S = 4;  // static
$def.P = 8;  // proto
$def.B = 16; // bind
$def.W = 32; // wrap
module.exports = $def;
},{"./$.core":86,"./$.global":92}],89:[function(require,module,exports){
// 7.2.1 RequireObjectCoercible(argument)
module.exports = function(it){
  if(it == undefined)throw TypeError("Can't call method on  " + it);
  return it;
};
},{}],90:[function(require,module,exports){
// all enumerable object keys, includes symbols
var $ = require('./$');
module.exports = function(it){
  var keys       = $.getKeys(it)
    , getSymbols = $.getSymbols;
  if(getSymbols){
    var symbols = getSymbols(it)
      , isEnum  = $.isEnum
      , i       = 0
      , key;
    while(symbols.length > i)if(isEnum.call(it, key = symbols[i++]))keys.push(key);
  }
  return keys;
};
},{"./$":95}],91:[function(require,module,exports){
module.exports = function(exec){
  try {
    return !!exec();
  } catch(e){
    return true;
  }
};
},{}],92:[function(require,module,exports){
// https://github.com/zloirock/core-js/issues/86#issuecomment-115759028
var UNDEFINED = 'undefined';
var global = module.exports = typeof window != UNDEFINED && window.Math == Math
  ? window : typeof self != UNDEFINED && self.Math == Math ? self : Function('return this')();
if(typeof __g == 'number')__g = global; // eslint-disable-line no-undef
},{}],93:[function(require,module,exports){
// indexed object, fallback for non-array-like ES3 strings
var cof = require('./$.cof');
module.exports = 0 in Object('z') ? Object : function(it){
  return cof(it) == 'String' ? it.split('') : Object(it);
};
},{"./$.cof":85}],94:[function(require,module,exports){
// http://jsperf.com/core-js-isobject
module.exports = function(it){
  return it !== null && (typeof it == 'object' || typeof it == 'function');
};
},{}],95:[function(require,module,exports){
var $Object = Object;
module.exports = {
  create:     $Object.create,
  getProto:   $Object.getPrototypeOf,
  isEnum:     {}.propertyIsEnumerable,
  getDesc:    $Object.getOwnPropertyDescriptor,
  setDesc:    $Object.defineProperty,
  setDescs:   $Object.defineProperties,
  getKeys:    $Object.keys,
  getNames:   $Object.getOwnPropertyNames,
  getSymbols: $Object.getOwnPropertySymbols,
  each:       [].forEach
};
},{}],96:[function(require,module,exports){
// most Object methods by ES6 should accept primitives
module.exports = function(KEY, exec){
  var $def = require('./$.def')
    , fn   = (require('./$.core').Object || {})[KEY] || Object[KEY]
    , exp  = {};
  exp[KEY] = exec(fn);
  $def($def.S + $def.F * require('./$.fails')(function(){ fn(1); }), 'Object', exp);
};
},{"./$.core":86,"./$.def":88,"./$.fails":91}],97:[function(require,module,exports){
// Works with __proto__ only. Old v8 can't work with null proto objects.
/* eslint-disable no-proto */
var getDesc  = require('./$').getDesc
  , isObject = require('./$.is-object')
  , anObject = require('./$.an-object');
var check = function(O, proto){
  anObject(O);
  if(!isObject(proto) && proto !== null)throw TypeError(proto + ": can't set as prototype!");
};
module.exports = {
  set: Object.setPrototypeOf || ('__proto__' in {} // eslint-disable-line
    ? function(buggy, set){
        try {
          set = require('./$.ctx')(Function.call, getDesc(Object.prototype, '__proto__').set, 2);
          set({}, []);
        } catch(e){ buggy = true; }
        return function setPrototypeOf(O, proto){
          check(O, proto);
          if(buggy)O.__proto__ = proto;
          else set(O, proto);
          return O;
        };
      }()
    : undefined),
  check: check
};
},{"./$":95,"./$.an-object":83,"./$.ctx":87,"./$.is-object":94}],98:[function(require,module,exports){
// 7.1.13 ToObject(argument)
var defined = require('./$.defined');
module.exports = function(it){
  return Object(defined(it));
};
},{"./$.defined":89}],99:[function(require,module,exports){
// 19.1.3.1 Object.assign(target, source)
var $def = require('./$.def');

$def($def.S + $def.F, 'Object', {assign: require('./$.assign')});
},{"./$.assign":84,"./$.def":88}],100:[function(require,module,exports){
// 19.1.2.14 Object.keys(O)
var toObject = require('./$.to-object');

require('./$.object-sap')('keys', function($keys){
  return function keys(it){
    return $keys(toObject(it));
  };
});
},{"./$.object-sap":96,"./$.to-object":98}],101:[function(require,module,exports){
// 19.1.3.19 Object.setPrototypeOf(O, proto)
var $def = require('./$.def');
$def($def.S, 'Object', {setPrototypeOf: require('./$.set-proto').set});
},{"./$.def":88,"./$.set-proto":97}],102:[function(require,module,exports){
/*!
  Copyright (c) 2015 Jed Watson.
  Licensed under the MIT License (MIT), see
  http://jedwatson.github.io/classnames
*/

(function () {
	'use strict';

	function classNames () {

		var classes = '';

		for (var i = 0; i < arguments.length; i++) {
			var arg = arguments[i];
			if (!arg) continue;

			var argType = typeof arg;

			if ('string' === argType || 'number' === argType) {
				classes += ' ' + arg;

			} else if (Array.isArray(arg)) {
				classes += ' ' + classNames.apply(null, arg);

			} else if ('object' === argType) {
				for (var key in arg) {
					if (arg.hasOwnProperty(key) && arg[key]) {
						classes += ' ' + key;
					}
				}
			}
		}

		return classes.substr(1);
	}

	if (typeof module !== 'undefined' && module.exports) {
		module.exports = classNames;
	} else if (typeof define === 'function' && typeof define.amd === 'object' && define.amd){
		// AMD. Register as an anonymous module.
		define(function () {
			return classNames;
		});
	} else {
		window.classNames = classNames;
	}

}());

},{}],103:[function(require,module,exports){
'use strict';
var canUseDOM = require('../util/inDOM');
var off = function off() {};

if (canUseDOM) {

  off = (function () {

    if (document.addEventListener) return function (node, eventName, handler, capture) {
      return node.removeEventListener(eventName, handler, capture || false);
    };else if (document.attachEvent) return function (node, eventName, handler) {
      return node.detachEvent('on' + eventName, handler);
    };
  })();
}

module.exports = off;
},{"../util/inDOM":122}],104:[function(require,module,exports){
'use strict';
var canUseDOM = require('../util/inDOM');
var on = function on() {};

if (canUseDOM) {
  on = (function () {

    if (document.addEventListener) return function (node, eventName, handler, capture) {
      return node.addEventListener(eventName, handler, capture || false);
    };else if (document.attachEvent) return function (node, eventName, handler) {
      return node.attachEvent('on' + eventName, handler);
    };
  })();
}

module.exports = on;
},{"../util/inDOM":122}],105:[function(require,module,exports){
"use strict";

exports.__esModule = true;
exports["default"] = ownerDocument;

function ownerDocument(node) {
  return node && node.ownerDocument || document;
}

module.exports = exports["default"];
},{}],106:[function(require,module,exports){
'use strict';
var canUseDOM = require('../util/inDOM');

var contains = (function () {
  var root = canUseDOM && document.documentElement;

  return root && root.contains ? function (context, node) {
    return context.contains(node);
  } : root && root.compareDocumentPosition ? function (context, node) {
    return context === node || !!(context.compareDocumentPosition(node) & 16);
  } : function (context, node) {
    if (node) do {
      if (node === context) return true;
    } while (node = node.parentNode);

    return false;
  };
})();

module.exports = contains;
},{"../util/inDOM":122}],107:[function(require,module,exports){
'use strict';

module.exports = function getWindow(node) {
  return node === node.window ? node : node.nodeType === 9 ? node.defaultView || node.parentWindow : false;
};
},{}],108:[function(require,module,exports){
'use strict';
var contains = require('./contains'),
    getWindow = require('./isWindow'),
    ownerDocument = require('../ownerDocument');

module.exports = function offset(node) {
  var doc = ownerDocument(node),
      win = getWindow(doc),
      docElem = doc && doc.documentElement,
      box = { top: 0, left: 0, height: 0, width: 0 };

  if (!doc) return;

  // Make sure it's not a disconnected DOM node
  if (!contains(docElem, node)) return box;

  if (node.getBoundingClientRect !== undefined) box = node.getBoundingClientRect();

  if (box.width || box.height) {

    box = {
      top: box.top + (win.pageYOffset || docElem.scrollTop) - (docElem.clientTop || 0),
      left: box.left + (win.pageXOffset || docElem.scrollLeft) - (docElem.clientLeft || 0),
      width: (box.width == null ? node.offsetWidth : box.width) || 0,
      height: (box.height == null ? node.offsetHeight : box.height) || 0
    };
  }

  return box;
};
},{"../ownerDocument":105,"./contains":106,"./isWindow":107}],109:[function(require,module,exports){
'use strict';

var babelHelpers = require('../util/babelHelpers.js');

exports.__esModule = true;
exports['default'] = offsetParent;

var _ownerDocument = require('../ownerDocument');

var _ownerDocument2 = babelHelpers.interopRequireDefault(_ownerDocument);

var _style = require('../style');

var _style2 = babelHelpers.interopRequireDefault(_style);

function nodeName(node) {
  return node.nodeName && node.nodeName.toLowerCase();
}

function offsetParent(node) {
  var doc = (0, _ownerDocument2['default'])(node),
      offsetParent = node && node.offsetParent;

  while (offsetParent && nodeName(node) !== 'html' && (0, _style2['default'])(offsetParent, 'position') === 'static') {
    offsetParent = offsetParent.offsetParent;
  }

  return offsetParent || doc.documentElement;
}

module.exports = exports['default'];
},{"../ownerDocument":105,"../style":114,"../util/babelHelpers.js":117}],110:[function(require,module,exports){
'use strict';

var babelHelpers = require('../util/babelHelpers.js');

exports.__esModule = true;
exports['default'] = position;

var _offset = require('./offset');

var _offset2 = babelHelpers.interopRequireDefault(_offset);

var _offsetParent = require('./offsetParent');

var _offsetParent2 = babelHelpers.interopRequireDefault(_offsetParent);

var _scrollTop = require('./scrollTop');

var _scrollTop2 = babelHelpers.interopRequireDefault(_scrollTop);

var _scrollLeft = require('./scrollLeft');

var _scrollLeft2 = babelHelpers.interopRequireDefault(_scrollLeft);

var _style = require('../style');

var _style2 = babelHelpers.interopRequireDefault(_style);

function nodeName(node) {
  return node.nodeName && node.nodeName.toLowerCase();
}

function position(node, offsetParent) {
  var parentOffset = { top: 0, left: 0 },
      offset;

  // Fixed elements are offset from window (parentOffset = {top:0, left: 0},
  // because it is its only offset parent
  if ((0, _style2['default'])(node, 'position') === 'fixed') {
    offset = node.getBoundingClientRect();
  } else {
    offsetParent = offsetParent || (0, _offsetParent2['default'])(node);
    offset = (0, _offset2['default'])(node);

    if (nodeName(offsetParent) !== 'html') parentOffset = (0, _offset2['default'])(offsetParent);

    parentOffset.top += parseInt((0, _style2['default'])(offsetParent, 'borderTopWidth'), 10) - (0, _scrollTop2['default'])(offsetParent) || 0;
    parentOffset.left += parseInt((0, _style2['default'])(offsetParent, 'borderLeftWidth'), 10) - (0, _scrollLeft2['default'])(offsetParent) || 0;
  }

  // Subtract parent offsets and node margins
  return babelHelpers._extends({}, offset, {
    top: offset.top - parentOffset.top - (parseInt((0, _style2['default'])(node, 'marginTop'), 10) || 0),
    left: offset.left - parentOffset.left - (parseInt((0, _style2['default'])(node, 'marginLeft'), 10) || 0)
  });
}

module.exports = exports['default'];
},{"../style":114,"../util/babelHelpers.js":117,"./offset":108,"./offsetParent":109,"./scrollLeft":111,"./scrollTop":112}],111:[function(require,module,exports){
'use strict';
var getWindow = require('./isWindow');

module.exports = function scrollTop(node, val) {
  var win = getWindow(node);

  if (val === undefined) return win ? 'pageXOffset' in win ? win.pageXOffset : win.document.documentElement.scrollLeft : node.scrollLeft;

  if (win) win.scrollTo(val, 'pageYOffset' in win ? win.pageYOffset : win.document.documentElement.scrollTop);else node.scrollLeft = val;
};
},{"./isWindow":107}],112:[function(require,module,exports){
'use strict';
var getWindow = require('./isWindow');

module.exports = function scrollTop(node, val) {
  var win = getWindow(node);

  if (val === undefined) return win ? 'pageYOffset' in win ? win.pageYOffset : win.document.documentElement.scrollTop : node.scrollTop;

  if (win) win.scrollTo('pageXOffset' in win ? win.pageXOffset : win.document.documentElement.scrollLeft, val);else node.scrollTop = val;
};
},{"./isWindow":107}],113:[function(require,module,exports){
'use strict';

var babelHelpers = require('../util/babelHelpers.js');

var _utilCamelizeStyle = require('../util/camelizeStyle');

var _utilCamelizeStyle2 = babelHelpers.interopRequireDefault(_utilCamelizeStyle);

var rposition = /^(top|right|bottom|left)$/;
var rnumnonpx = /^([+-]?(?:\d*\.|)\d+(?:[eE][+-]?\d+|))(?!px)[a-z%]+$/i;

module.exports = function _getComputedStyle(node) {
  if (!node) throw new TypeError('No Element passed to `getComputedStyle()`');
  var doc = node.ownerDocument;

  return 'defaultView' in doc ? doc.defaultView.opener ? node.ownerDocument.defaultView.getComputedStyle(node, null) : window.getComputedStyle(node, null) : { //ie 8 "magic" from: https://github.com/jquery/jquery/blob/1.11-stable/src/css/curCSS.js#L72
    getPropertyValue: function getPropertyValue(prop) {
      var style = node.style;

      prop = (0, _utilCamelizeStyle2['default'])(prop);

      if (prop == 'float') prop = 'styleFloat';

      var current = node.currentStyle[prop] || null;

      if (current == null && style && style[prop]) current = style[prop];

      if (rnumnonpx.test(current) && !rposition.test(prop)) {
        // Remember the original values
        var left = style.left;
        var runStyle = node.runtimeStyle;
        var rsLeft = runStyle && runStyle.left;

        // Put in the new values to get a computed value out
        if (rsLeft) runStyle.left = node.currentStyle.left;

        style.left = prop === 'fontSize' ? '1em' : current;
        current = style.pixelLeft + 'px';

        // Revert the changed values
        style.left = left;
        if (rsLeft) runStyle.left = rsLeft;
      }

      return current;
    }
  };
};
},{"../util/babelHelpers.js":117,"../util/camelizeStyle":119}],114:[function(require,module,exports){
'use strict';

var camelize = require('../util/camelizeStyle'),
    hyphenate = require('../util/hyphenateStyle'),
    _getComputedStyle = require('./getComputedStyle'),
    removeStyle = require('./removeStyle');

var has = Object.prototype.hasOwnProperty;

module.exports = function style(node, property, value) {
  var css = '',
      props = property;

  if (typeof property === 'string') {

    if (value === undefined) return node.style[camelize(property)] || _getComputedStyle(node).getPropertyValue(hyphenate(property));else (props = {})[property] = value;
  }

  for (var key in props) if (has.call(props, key)) {
    !props[key] && props[key] !== 0 ? removeStyle(node, hyphenate(key)) : css += hyphenate(key) + ':' + props[key] + ';';
  }

  node.style.cssText += ';' + css;
};
},{"../util/camelizeStyle":119,"../util/hyphenateStyle":121,"./getComputedStyle":113,"./removeStyle":115}],115:[function(require,module,exports){
'use strict';

module.exports = function removeStyle(node, key) {
  return 'removeProperty' in node.style ? node.style.removeProperty(key) : node.style.removeAttribute(key);
};
},{}],116:[function(require,module,exports){
'use strict';
var canUseDOM = require('../util/inDOM');

var has = Object.prototype.hasOwnProperty,
    transform = 'transform',
    transition = {},
    transitionTiming,
    transitionDuration,
    transitionProperty,
    transitionDelay;

if (canUseDOM) {
  transition = getTransitionProperties();

  transform = transition.prefix + transform;

  transitionProperty = transition.prefix + 'transition-property';
  transitionDuration = transition.prefix + 'transition-duration';
  transitionDelay = transition.prefix + 'transition-delay';
  transitionTiming = transition.prefix + 'transition-timing-function';
}

module.exports = {
  transform: transform,
  end: transition.end,
  property: transitionProperty,
  timing: transitionTiming,
  delay: transitionDelay,
  duration: transitionDuration
};

function getTransitionProperties() {
  var endEvent,
      prefix = '',
      transitions = {
    O: 'otransitionend',
    Moz: 'transitionend',
    Webkit: 'webkitTransitionEnd',
    ms: 'MSTransitionEnd'
  };

  var element = document.createElement('div');

  for (var vendor in transitions) if (has.call(transitions, vendor)) {
    if (element.style[vendor + 'TransitionProperty'] !== undefined) {
      prefix = '-' + vendor.toLowerCase() + '-';
      endEvent = transitions[vendor];
      break;
    }
  }

  if (!endEvent && element.style.transitionProperty !== undefined) endEvent = 'transitionend';

  return { end: endEvent, prefix: prefix };
}
},{"../util/inDOM":122}],117:[function(require,module,exports){
(function (root, factory) {
  if (typeof define === "function" && define.amd) {
    define(["exports"], factory);
  } else if (typeof exports === "object") {
    factory(exports);
  } else {
    factory(root.babelHelpers = {});
  }
})(this, function (global) {
  var babelHelpers = global;

  babelHelpers.interopRequireDefault = function (obj) {
    return obj && obj.__esModule ? obj : {
      "default": obj
    };
  };

  babelHelpers._extends = Object.assign || function (target) {
    for (var i = 1; i < arguments.length; i++) {
      var source = arguments[i];

      for (var key in source) {
        if (Object.prototype.hasOwnProperty.call(source, key)) {
          target[key] = source[key];
        }
      }
    }

    return target;
  };
})
},{}],118:[function(require,module,exports){
"use strict";

var rHyphen = /-(.)/g;

module.exports = function camelize(string) {
  return string.replace(rHyphen, function (_, chr) {
    return chr.toUpperCase();
  });
};
},{}],119:[function(require,module,exports){
/**
 * Copyright 2014-2015, Facebook, Inc.
 * All rights reserved.
 * https://github.com/facebook/react/blob/2aeb8a2a6beb00617a4217f7f8284924fa2ad819/src/vendor/core/camelizeStyleName.js
 */

'use strict';
var camelize = require('./camelize');
var msPattern = /^-ms-/;

module.exports = function camelizeStyleName(string) {
  return camelize(string.replace(msPattern, 'ms-'));
};
},{"./camelize":118}],120:[function(require,module,exports){
'use strict';

var rUpper = /([A-Z])/g;

module.exports = function hyphenate(string) {
  return string.replace(rUpper, '-$1').toLowerCase();
};
},{}],121:[function(require,module,exports){
/**
 * Copyright 2013-2014, Facebook, Inc.
 * All rights reserved.
 * https://github.com/facebook/react/blob/2aeb8a2a6beb00617a4217f7f8284924fa2ad819/src/vendor/core/hyphenateStyleName.js
 */

"use strict";

var hyphenate = require("./hyphenate");
var msPattern = /^ms-/;

module.exports = function hyphenateStyleName(string) {
  return hyphenate(string).replace(msPattern, "-ms-");
};
},{"./hyphenate":120}],122:[function(require,module,exports){
'use strict';
module.exports = !!(typeof window !== 'undefined' && window.document && window.document.createElement);
},{}],123:[function(require,module,exports){
'use strict';

exports.__esModule = true;

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }

function _objectWithoutProperties(obj, keys) { var target = {}; for (var i in obj) { if (keys.indexOf(i) >= 0) continue; if (!Object.prototype.hasOwnProperty.call(obj, i)) continue; target[i] = obj[i]; } return target; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError('Cannot call a class as a function'); } }

function _inherits(subClass, superClass) { if (typeof superClass !== 'function' && superClass !== null) { throw new TypeError('Super expression must either be null or a function, not ' + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) subClass.__proto__ = superClass; }

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _Portal = require('./Portal');

var _Portal2 = _interopRequireDefault(_Portal);

var _Position = require('./Position');

var _Position2 = _interopRequireDefault(_Position);

var _RootCloseWrapper = require('./RootCloseWrapper');

var _RootCloseWrapper2 = _interopRequireDefault(_RootCloseWrapper);

var _reactPropTypesLibElementType = require('react-prop-types/lib/elementType');

var _reactPropTypesLibElementType2 = _interopRequireDefault(_reactPropTypesLibElementType);

/**
 * Built on top of `<Position/>` and `<Portal/>`, the overlay component is great for custom tooltip overlays.
 */

var Overlay = (function (_React$Component) {
  function Overlay(props, context) {
    _classCallCheck(this, Overlay);

    _React$Component.call(this, props, context);

    this.state = { exited: !props.show };
    this.onHiddenListener = this.handleHidden.bind(this);
  }

  _inherits(Overlay, _React$Component);

  Overlay.prototype.componentWillReceiveProps = function componentWillReceiveProps(nextProps) {
    if (nextProps.show) {
      this.setState({ exited: false });
    } else if (!nextProps.transition) {
      // Otherwise let handleHidden take care of marking exited.
      this.setState({ exited: true });
    }
  };

  Overlay.prototype.render = function render() {
    var _props = this.props;
    var container = _props.container;
    var containerPadding = _props.containerPadding;
    var target = _props.target;
    var placement = _props.placement;
    var rootClose = _props.rootClose;
    var children = _props.children;
    var Transition = _props.transition;

    var props = _objectWithoutProperties(_props, ['container', 'containerPadding', 'target', 'placement', 'rootClose', 'children', 'transition']);

    // Don't un-render the overlay while it's transitioning out.
    var mountOverlay = props.show || Transition && !this.state.exited;
    if (!mountOverlay) {
      // Don't bother showing anything if we don't have to.
      return null;
    }

    var child = children;

    // Position is be inner-most because it adds inline styles into the child,
    // which the other wrappers don't forward correctly.
    child = _react2['default'].createElement(
      _Position2['default'],
      { container: container, containerPadding: containerPadding, target: target, placement: placement },
      child
    );

    if (Transition) {
      var onExit = props.onExit;
      var onExiting = props.onExiting;
      var onEnter = props.onEnter;
      var onEntering = props.onEntering;
      var onEntered = props.onEntered;

      // This animates the child node by injecting props, so it must precede
      // anything that adds a wrapping div.
      child = _react2['default'].createElement(
        Transition,
        {
          'in': props.show,
          transitionAppear: true,
          onExit: onExit,
          onExiting: onExiting,
          onExited: this.onHiddenListener,
          onEnter: onEnter,
          onEntering: onEntering,
          onEntered: onEntered
        },
        child
      );
    }

    // This goes after everything else because it adds a wrapping div.
    if (rootClose) {
      child = _react2['default'].createElement(
        _RootCloseWrapper2['default'],
        { onRootClose: props.onHide },
        child
      );
    }

    return _react2['default'].createElement(
      _Portal2['default'],
      { container: container },
      child
    );
  };

  Overlay.prototype.handleHidden = function handleHidden() {
    this.setState({ exited: true });

    if (this.props.onExited) {
      var _props2;

      (_props2 = this.props).onExited.apply(_props2, arguments);
    }
  };

  return Overlay;
})(_react2['default'].Component);

Overlay.propTypes = _extends({}, _Portal2['default'].propTypes, _Position2['default'].propTypes, {
  /**
   * Set the visibility of the Overlay
   */
  show: _react2['default'].PropTypes.bool,
  /**
   * Specify whether the overlay should trigger onHide when the user clicks outside the overlay
   */
  rootClose: _react2['default'].PropTypes.bool,
  /**
   * A Callback fired by the Overlay when it wishes to be hidden.
   */
  onHide: _react2['default'].PropTypes.func,

  /**
   * A `<Transition/>` component used to animate the overlay changes visibility.
   */
  transition: _reactPropTypesLibElementType2['default'],

  /**
   * Callback fired before the Overlay transitions in
   */
  onEnter: _react2['default'].PropTypes.func,

  /**
   * Callback fired as the Overlay begins to transition in
   */
  onEntering: _react2['default'].PropTypes.func,

  /**
   * Callback fired after the Overlay finishes transitioning in
   */
  onEntered: _react2['default'].PropTypes.func,

  /**
   * Callback fired right before the Overlay transitions out
   */
  onExit: _react2['default'].PropTypes.func,

  /**
   * Callback fired as the Overlay begins to transition out
   */
  onExiting: _react2['default'].PropTypes.func,

  /**
   * Callback fired after the Overlay finishes transitioning out
   */
  onExited: _react2['default'].PropTypes.func
});

exports['default'] = Overlay;
module.exports = exports['default'];
},{"./Portal":124,"./Position":125,"./RootCloseWrapper":126,"react":undefined,"react-prop-types/lib/elementType":134}],124:[function(require,module,exports){
'use strict';

exports.__esModule = true;

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _reactPropTypesLibMountable = require('react-prop-types/lib/mountable');

var _reactPropTypesLibMountable2 = _interopRequireDefault(_reactPropTypesLibMountable);

var _utilsOwnerDocument = require('./utils/ownerDocument');

var _utilsOwnerDocument2 = _interopRequireDefault(_utilsOwnerDocument);

var _utilsGetContainer = require('./utils/getContainer');

var _utilsGetContainer2 = _interopRequireDefault(_utilsGetContainer);

/**
 * The `<Portal/>` component renders its children into a new "subtree" outside of current component hierarchy.
 * You can think of it as a declarative `appendChild()`, or jQuery's `$.fn.appendTo()`.
 * The children of `<Portal/>` component will be appended to the `container` specified.
 */
var Portal = _react2['default'].createClass({

  displayName: 'Portal',

  propTypes: {
    /**
     * A Node, Component instance, or function that returns either. The `container` will have the Portal children
     * appended to it.
     */
    container: _react2['default'].PropTypes.oneOfType([_reactPropTypesLibMountable2['default'], _react2['default'].PropTypes.func])
  },

  componentDidMount: function componentDidMount() {
    this._renderOverlay();
  },

  componentDidUpdate: function componentDidUpdate() {
    this._renderOverlay();
  },

  componentWillUnmount: function componentWillUnmount() {
    this._unrenderOverlay();
    this._unmountOverlayTarget();
  },

  _mountOverlayTarget: function _mountOverlayTarget() {
    if (!this._overlayTarget) {
      this._overlayTarget = document.createElement('div');
      this.getContainerDOMNode().appendChild(this._overlayTarget);
    }
  },

  _unmountOverlayTarget: function _unmountOverlayTarget() {
    if (this._overlayTarget) {
      this.getContainerDOMNode().removeChild(this._overlayTarget);
      this._overlayTarget = null;
    }
  },

  _renderOverlay: function _renderOverlay() {

    var overlay = !this.props.children ? null : _react2['default'].Children.only(this.props.children);

    // Save reference for future access.
    if (overlay !== null) {
      this._mountOverlayTarget();
      this._overlayInstance = _react2['default'].render(overlay, this._overlayTarget);
    } else {
      // Unrender if the component is null for transitions to null
      this._unrenderOverlay();
      this._unmountOverlayTarget();
    }
  },

  _unrenderOverlay: function _unrenderOverlay() {
    if (this._overlayTarget) {
      _react2['default'].unmountComponentAtNode(this._overlayTarget);
      this._overlayInstance = null;
    }
  },

  render: function render() {
    return null;
  },

  getOverlayDOMNode: function getOverlayDOMNode() {
    if (!this.isMounted()) {
      throw new Error('getOverlayDOMNode(): A component must be mounted to have a DOM node.');
    }

    if (this._overlayInstance) {
      if (this._overlayInstance.getWrappedDOMNode) {
        return this._overlayInstance.getWrappedDOMNode();
      } else {
        return _react2['default'].findDOMNode(this._overlayInstance);
      }
    }

    return null;
  },

  getContainerDOMNode: function getContainerDOMNode() {
    return _utilsGetContainer2['default'](this.props.container, _utilsOwnerDocument2['default'](this).body);
  }
});

exports['default'] = Portal;
module.exports = exports['default'];
},{"./utils/getContainer":130,"./utils/ownerDocument":132,"react":undefined,"react-prop-types/lib/mountable":135}],125:[function(require,module,exports){
'use strict';

exports.__esModule = true;

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }

function _objectWithoutProperties(obj, keys) { var target = {}; for (var i in obj) { if (keys.indexOf(i) >= 0) continue; if (!Object.prototype.hasOwnProperty.call(obj, i)) continue; target[i] = obj[i]; } return target; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError('Cannot call a class as a function'); } }

function _inherits(subClass, superClass) { if (typeof superClass !== 'function' && superClass !== null) { throw new TypeError('Super expression must either be null or a function, not ' + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) subClass.__proto__ = superClass; }

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _classnames = require('classnames');

var _classnames2 = _interopRequireDefault(_classnames);

var _utilsOwnerDocument = require('./utils/ownerDocument');

var _utilsOwnerDocument2 = _interopRequireDefault(_utilsOwnerDocument);

var _utilsGetContainer = require('./utils/getContainer');

var _utilsGetContainer2 = _interopRequireDefault(_utilsGetContainer);

var _utilsOverlayPositionUtils = require('./utils/overlayPositionUtils');

var _reactPropTypesLibMountable = require('react-prop-types/lib/mountable');

var _reactPropTypesLibMountable2 = _interopRequireDefault(_reactPropTypesLibMountable);

/**
 * The Position component calulates the corrdinates for its child, to
 * position it relative to a `target` component or node. Useful for creating callouts and tooltips,
 * the Position component injects a `style` props with `left` and `top` values for positioning your component.
 *
 * It also injects "arrow" `left`, and `top` values for styling callout arrows for giving your components
 * a sense of directionality.
 */

var Position = (function (_React$Component) {
  function Position(props, context) {
    _classCallCheck(this, Position);

    _React$Component.call(this, props, context);

    this.state = {
      positionLeft: null,
      positionTop: null,
      arrowOffsetLeft: null,
      arrowOffsetTop: null
    };

    this._needsFlush = false;
    this._lastTarget = null;
  }

  _inherits(Position, _React$Component);

  Position.prototype.componentDidMount = function componentDidMount() {
    this.updatePosition();
  };

  Position.prototype.componentWillReceiveProps = function componentWillReceiveProps() {
    this._needsFlush = true;
  };

  Position.prototype.componentDidUpdate = function componentDidUpdate(prevProps) {
    if (this._needsFlush) {
      this._needsFlush = false;
      this.updatePosition(prevProps.placement !== this.props.placement);
    }
  };

  Position.prototype.componentWillUnmount = function componentWillUnmount() {
    // Probably not necessary, but just in case holding a reference to the
    // target causes problems somewhere.
    this._lastTarget = null;
  };

  Position.prototype.render = function render() {
    var _props = this.props;
    var children = _props.children;
    var className = _props.className;

    var props = _objectWithoutProperties(_props, ['children', 'className']);

    var _state = this.state;
    var positionLeft = _state.positionLeft;
    var positionTop = _state.positionTop;

    var arrowPosition = _objectWithoutProperties(_state, ['positionLeft', 'positionTop']);

    var child = _react2['default'].Children.only(children);
    return _react.cloneElement(child, _extends({}, props, arrowPosition, {
      //do we need to also forward positionLeft and positionTop if they are set to style?
      positionLeft: positionLeft,
      positionTop: positionTop,
      className: _classnames2['default'](className, child.props.className),
      style: _extends({}, child.props.style, {
        left: positionLeft,
        top: positionTop
      })
    }));
  };

  Position.prototype.getTargetSafe = function getTargetSafe() {
    if (!this.props.target) {
      return null;
    }

    var target = this.props.target(this.props);
    if (!target) {
      // This is so we can just use === check below on all falsy targets.
      return null;
    }

    return target;
  };

  Position.prototype.updatePosition = function updatePosition(placementChanged) {
    var target = this.getTargetSafe();

    if (target === this._lastTarget && !placementChanged) {
      return;
    }

    this._lastTarget = target;

    if (!target) {
      this.setState({
        positionLeft: null,
        positionTop: null,
        arrowOffsetLeft: null,
        arrowOffsetTop: null
      });

      return;
    }

    var overlay = _react2['default'].findDOMNode(this);
    var container = _utilsGetContainer2['default'](this.props.container, _utilsOwnerDocument2['default'](this).body);

    this.setState(_utilsOverlayPositionUtils.calcOverlayPosition(this.props.placement, overlay, target, container, this.props.containerPadding));
  };

  return Position;
})(_react2['default'].Component);

Position.propTypes = {
  /**
   * Function mapping props to a DOM node the component is positioned next to
   */
  target: _react2['default'].PropTypes.func,
  /**
   * "offsetParent" of the component
   */
  container: _reactPropTypesLibMountable2['default'],
  /**
   * Minimum spacing in pixels between container border and component border
   */
  containerPadding: _react2['default'].PropTypes.number,
  /**
   * How to position the component relative to the target
   */
  placement: _react2['default'].PropTypes.oneOf(['top', 'right', 'bottom', 'left'])
};

Position.displayName = 'Position';

Position.defaultProps = {
  containerPadding: 0,
  placement: 'right'
};

exports['default'] = Position;
module.exports = exports['default'];
},{"./utils/getContainer":130,"./utils/overlayPositionUtils":131,"./utils/ownerDocument":132,"classnames":102,"react":undefined,"react-prop-types/lib/mountable":135}],126:[function(require,module,exports){
'use strict';

exports.__esModule = true;

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError('Cannot call a class as a function'); } }

function _inherits(subClass, superClass) { if (typeof superClass !== 'function' && superClass !== null) { throw new TypeError('Super expression must either be null or a function, not ' + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) subClass.__proto__ = superClass; }

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _utilsAddEventListener = require('./utils/addEventListener');

var _utilsAddEventListener2 = _interopRequireDefault(_utilsAddEventListener);

var _utilsCreateChainedFunction = require('./utils/createChainedFunction');

var _utilsCreateChainedFunction2 = _interopRequireDefault(_utilsCreateChainedFunction);

var _utilsOwnerDocument = require('./utils/ownerDocument');

var _utilsOwnerDocument2 = _interopRequireDefault(_utilsOwnerDocument);

// TODO: Consider using an ES6 symbol here, once we use babel-runtime.
var CLICK_WAS_INSIDE = '__click_was_inside';

function suppressRootClose(event) {
  // Tag the native event to prevent the root close logic on document click.
  // This seems safer than using event.nativeEvent.stopImmediatePropagation(),
  // which is only supported in IE >= 9.
  event.nativeEvent[CLICK_WAS_INSIDE] = true;
}

var RootCloseWrapper = (function (_React$Component) {
  function RootCloseWrapper(props) {
    _classCallCheck(this, RootCloseWrapper);

    _React$Component.call(this, props);

    this.handleDocumentClick = this.handleDocumentClick.bind(this);
    this.handleDocumentKeyUp = this.handleDocumentKeyUp.bind(this);
  }

  _inherits(RootCloseWrapper, _React$Component);

  RootCloseWrapper.prototype.bindRootCloseHandlers = function bindRootCloseHandlers() {
    var doc = _utilsOwnerDocument2['default'](this);

    this._onDocumentClickListener = _utilsAddEventListener2['default'](doc, 'click', this.handleDocumentClick);

    this._onDocumentKeyupListener = _utilsAddEventListener2['default'](doc, 'keyup', this.handleDocumentKeyUp);
  };

  RootCloseWrapper.prototype.handleDocumentClick = function handleDocumentClick(e) {
    // This is now the native event.
    if (e[CLICK_WAS_INSIDE]) {
      return;
    }

    this.props.onRootClose();
  };

  RootCloseWrapper.prototype.handleDocumentKeyUp = function handleDocumentKeyUp(e) {
    if (e.keyCode === 27) {
      this.props.onRootClose();
    }
  };

  RootCloseWrapper.prototype.unbindRootCloseHandlers = function unbindRootCloseHandlers() {
    if (this._onDocumentClickListener) {
      this._onDocumentClickListener.remove();
    }

    if (this._onDocumentKeyupListener) {
      this._onDocumentKeyupListener.remove();
    }
  };

  RootCloseWrapper.prototype.componentDidMount = function componentDidMount() {
    this.bindRootCloseHandlers();
  };

  RootCloseWrapper.prototype.render = function render() {
    var _props = this.props;
    var noWrap = _props.noWrap;
    var children = _props.children;

    var child = _react2['default'].Children.only(children);

    if (noWrap) {
      return _react2['default'].cloneElement(child, {
        onClick: _utilsCreateChainedFunction2['default'](suppressRootClose, child.props.onClick)
      });
    }

    // Wrap the child in a new element, so the child won't have to handle
    // potentially combining multiple onClick listeners.
    return _react2['default'].createElement(
      'div',
      { onClick: suppressRootClose },
      child
    );
  };

  RootCloseWrapper.prototype.getWrappedDOMNode = function getWrappedDOMNode() {
    // We can't use a ref to identify the wrapped child, since we might be
    // stealing the ref from the owner, but we know exactly the DOM structure
    // that will be rendered, so we can just do this to get the child's DOM
    // node for doing size calculations in OverlayMixin.
    var node = _react2['default'].findDOMNode(this);
    return this.props.noWrap ? node : node.firstChild;
  };

  RootCloseWrapper.prototype.componentWillUnmount = function componentWillUnmount() {
    this.unbindRootCloseHandlers();
  };

  return RootCloseWrapper;
})(_react2['default'].Component);

exports['default'] = RootCloseWrapper;

RootCloseWrapper.displayName = 'RootCloseWrapper';

RootCloseWrapper.propTypes = {
  onRootClose: _react2['default'].PropTypes.func.isRequired,

  /**
   * Passes the suppress click handler directly to the child component instead
   * of placing it on a wrapping div. Only use when you can be sure the child
   * properly handle the click event.
   */
  noWrap: _react2['default'].PropTypes.bool
};
module.exports = exports['default'];
},{"./utils/addEventListener":128,"./utils/createChainedFunction":129,"./utils/ownerDocument":132,"react":undefined}],127:[function(require,module,exports){
'use strict';

exports.__esModule = true;

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }

function _objectWithoutProperties(obj, keys) { var target = {}; for (var i in obj) { if (keys.indexOf(i) >= 0) continue; if (!Object.prototype.hasOwnProperty.call(obj, i)) continue; target[i] = obj[i]; } return target; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError('Cannot call a class as a function'); } }

function _inherits(subClass, superClass) { if (typeof superClass !== 'function' && superClass !== null) { throw new TypeError('Super expression must either be null or a function, not ' + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) subClass.__proto__ = superClass; }

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _domHelpersTransitionProperties = require('dom-helpers/transition/properties');

var _domHelpersTransitionProperties2 = _interopRequireDefault(_domHelpersTransitionProperties);

var _domHelpersEventsOn = require('dom-helpers/events/on');

var _domHelpersEventsOn2 = _interopRequireDefault(_domHelpersEventsOn);

var _classnames = require('classnames');

var _classnames2 = _interopRequireDefault(_classnames);

var transitionEndEvent = _domHelpersTransitionProperties2['default'].end;

var UNMOUNTED = 0;
exports.UNMOUNTED = UNMOUNTED;
var EXITED = 1;
exports.EXITED = EXITED;
var ENTERING = 2;
exports.ENTERING = ENTERING;
var ENTERED = 3;
exports.ENTERED = ENTERED;
var EXITING = 4;

exports.EXITING = EXITING;
/**
 * The Transition component lets you define and run css transitions with a simple declarative api.
 * It works similar to React's own [CSSTransitionGroup](http://facebook.github.io/react/docs/animation.html#high-level-api-reactcsstransitiongroup)
 * but is specifically optimized for transitioning a single child "in" or "out".
 *
 * You don't even need to use class based css transitions if you don't want to (but it is easiest).
 * The extensive set of lifecyle callbacks means you have control over
 * the transitioning now at each step of the way.
 */

var Transition = (function (_React$Component) {
  function Transition(props, context) {
    _classCallCheck(this, Transition);

    _React$Component.call(this, props, context);

    var initialStatus = undefined;
    if (props['in']) {
      // Start enter transition in componentDidMount.
      initialStatus = props.transitionAppear ? EXITED : ENTERED;
    } else {
      initialStatus = props.unmountOnExit ? UNMOUNTED : EXITED;
    }
    this.state = { status: initialStatus };

    this.nextCallback = null;
  }

  _inherits(Transition, _React$Component);

  Transition.prototype.componentDidMount = function componentDidMount() {
    if (this.props.transitionAppear && this.props['in']) {
      this.performEnter(this.props);
    }
  };

  Transition.prototype.componentWillReceiveProps = function componentWillReceiveProps(nextProps) {
    var status = this.state.status;
    if (nextProps['in']) {
      if (status === EXITING) {
        this.performEnter(nextProps);
      } else if (this.props.unmountOnExit) {
        if (status === UNMOUNTED) {
          // Start enter transition in componentDidUpdate.
          this.setState({ status: EXITED });
        }
      } else if (status === EXITED) {
        this.performEnter(nextProps);
      }

      // Otherwise we're already entering or entered.
    } else {
      if (status === ENTERING || status === ENTERED) {
        this.performExit(nextProps);
      }

      // Otherwise we're already exited or exiting.
    }
  };

  Transition.prototype.componentDidUpdate = function componentDidUpdate() {
    if (this.props.unmountOnExit && this.state.status === EXITED) {
      // EXITED is always a transitional state to either ENTERING or UNMOUNTED
      // when using unmountOnExit.
      if (this.props['in']) {
        this.performEnter(this.props);
      } else {
        this.setState({ status: UNMOUNTED });
      }
    }
  };

  Transition.prototype.componentWillUnmount = function componentWillUnmount() {
    this.cancelNextCallback();
  };

  Transition.prototype.performEnter = function performEnter(props) {
    var _this = this;

    this.cancelNextCallback();
    var node = _react2['default'].findDOMNode(this);

    // Not this.props, because we might be about to receive new props.
    props.onEnter(node);

    this.safeSetState({ status: ENTERING }, function () {
      _this.props.onEntering(node);

      _this.onTransitionEnd(node, function () {
        _this.safeSetState({ status: ENTERED }, function () {
          _this.props.onEntered(node);
        });
      });
    });
  };

  Transition.prototype.performExit = function performExit(props) {
    var _this2 = this;

    this.cancelNextCallback();
    var node = _react2['default'].findDOMNode(this);

    // Not this.props, because we might be about to receive new props.
    props.onExit(node);

    this.safeSetState({ status: EXITING }, function () {
      _this2.props.onExiting(node);

      _this2.onTransitionEnd(node, function () {
        _this2.safeSetState({ status: EXITED }, function () {
          _this2.props.onExited(node);
        });
      });
    });
  };

  Transition.prototype.cancelNextCallback = function cancelNextCallback() {
    if (this.nextCallback !== null) {
      this.nextCallback.cancel();
      this.nextCallback = null;
    }
  };

  Transition.prototype.safeSetState = function safeSetState(nextState, callback) {
    // This shouldn't be necessary, but there are weird race conditions with
    // setState callbacks and unmounting in testing, so always make sure that
    // we can cancel any pending setState callbacks after we unmount.
    this.setState(nextState, this.setNextCallback(callback));
  };

  Transition.prototype.setNextCallback = function setNextCallback(callback) {
    var _this3 = this;

    var active = true;

    this.nextCallback = function (event) {
      if (active) {
        active = false;
        _this3.nextCallback = null;

        callback(event);
      }
    };

    this.nextCallback.cancel = function () {
      active = false;
    };

    return this.nextCallback;
  };

  Transition.prototype.onTransitionEnd = function onTransitionEnd(node, handler) {
    this.setNextCallback(handler);

    if (node) {
      _domHelpersEventsOn2['default'](node, transitionEndEvent, this.nextCallback);
      setTimeout(this.nextCallback, this.props.timeout);
    } else {
      setTimeout(this.nextCallback, 0);
    }
  };

  Transition.prototype.render = function render() {
    var status = this.state.status;
    if (status === UNMOUNTED) {
      return null;
    }

    var _props = this.props;
    var children = _props.children;
    var className = _props.className;

    var childProps = _objectWithoutProperties(_props, ['children', 'className']);

    Object.keys(Transition.propTypes).forEach(function (key) {
      return delete childProps[key];
    });

    var transitionClassName = undefined;
    if (status === EXITED) {
      transitionClassName = this.props.exitedClassName;
    } else if (status === ENTERING) {
      transitionClassName = this.props.enteringClassName;
    } else if (status === ENTERED) {
      transitionClassName = this.props.enteredClassName;
    } else if (status === EXITING) {
      transitionClassName = this.props.exitingClassName;
    }

    var child = _react2['default'].Children.only(children);
    return _react2['default'].cloneElement(child, _extends({}, childProps, {
      className: _classnames2['default'](child.props.className, className, transitionClassName)
    }));
  };

  return Transition;
})(_react2['default'].Component);

Transition.propTypes = {
  /**
   * Show the component; triggers the enter or exit animation
   */
  'in': _react2['default'].PropTypes.bool,

  /**
   * Unmount the component (remove it from the DOM) when it is not shown
   */
  unmountOnExit: _react2['default'].PropTypes.bool,

  /**
   * Run the enter animation when the component mounts, if it is initially
   * shown
   */
  transitionAppear: _react2['default'].PropTypes.bool,

  /**
   * A Timeout for the animation, in milliseconds, to ensure that a node doesn't
   * transition indefinately if the browser transitionEnd events are
   * canceled or interrupted.
   *
   * By default this is set to a high number (5 seconds) as a failsafe. You should consider
   * setting this to the duration of your animation (or a bit above it).
   */
  timeout: _react2['default'].PropTypes.number,

  /**
   * CSS class or classes applied when the component is exited
   */
  exitedClassName: _react2['default'].PropTypes.string,
  /**
   * CSS class or classes applied while the component is exiting
   */
  exitingClassName: _react2['default'].PropTypes.string,
  /**
   * CSS class or classes applied when the component is entered
   */
  enteredClassName: _react2['default'].PropTypes.string,
  /**
   * CSS class or classes applied while the component is entering
   */
  enteringClassName: _react2['default'].PropTypes.string,

  /**
   * Callback fired before the "entering" classes are applied
   */
  onEnter: _react2['default'].PropTypes.func,
  /**
   * Callback fired after the "entering" classes are applied
   */
  onEntering: _react2['default'].PropTypes.func,
  /**
   * Callback fired after the "enter" classes are applied
   */
  onEntered: _react2['default'].PropTypes.func,
  /**
   * Callback fired before the "exiting" classes are applied
   */
  onExit: _react2['default'].PropTypes.func,
  /**
   * Callback fired after the "exiting" classes are applied
   */
  onExiting: _react2['default'].PropTypes.func,
  /**
   * Callback fired after the "exited" classes are applied
   */
  onExited: _react2['default'].PropTypes.func
};

// Name the function so it is clearer in the documentation
function noop() {}

Transition.displayName = 'Transition';

Transition.defaultProps = {
  'in': false,
  unmountOnExit: false,
  transitionAppear: false,

  timeout: 5000,

  onEnter: noop,
  onEntering: noop,
  onEntered: noop,

  onExit: noop,
  onExiting: noop,
  onExited: noop
};

exports['default'] = Transition;
},{"classnames":102,"dom-helpers/events/on":104,"dom-helpers/transition/properties":116,"react":undefined}],128:[function(require,module,exports){
'use strict';

exports.__esModule = true;

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }

var _domHelpersEventsOn = require('dom-helpers/events/on');

var _domHelpersEventsOn2 = _interopRequireDefault(_domHelpersEventsOn);

var _domHelpersEventsOff = require('dom-helpers/events/off');

var _domHelpersEventsOff2 = _interopRequireDefault(_domHelpersEventsOff);

exports['default'] = function (node, event, handler) {
  _domHelpersEventsOn2['default'](node, event, handler);
  return {
    remove: function remove() {
      _domHelpersEventsOff2['default'](node, event, handler);
    }
  };
};

module.exports = exports['default'];
},{"dom-helpers/events/off":103,"dom-helpers/events/on":104}],129:[function(require,module,exports){
/**
 * Safe chained function
 *
 * Will only create a new function if needed,
 * otherwise will pass back existing functions or null.
 *
 * @param {function} functions to chain
 * @returns {function|null}
 */
'use strict';

exports.__esModule = true;
function createChainedFunction() {
  for (var _len = arguments.length, funcs = Array(_len), _key = 0; _key < _len; _key++) {
    funcs[_key] = arguments[_key];
  }

  return funcs.filter(function (f) {
    return f != null;
  }).reduce(function (acc, f) {
    if (typeof f !== 'function') {
      throw new Error('Invalid Argument Type, must only provide functions, undefined, or null.');
    }

    if (acc === null) {
      return f;
    }

    return function chainedFunction() {
      for (var _len2 = arguments.length, args = Array(_len2), _key2 = 0; _key2 < _len2; _key2++) {
        args[_key2] = arguments[_key2];
      }

      acc.apply(this, args);
      f.apply(this, args);
    };
  }, null);
}

exports['default'] = createChainedFunction;
module.exports = exports['default'];
},{}],130:[function(require,module,exports){
'use strict';

exports.__esModule = true;
exports['default'] = getContainer;

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

function getContainer(container, defaultContainer) {
  container = typeof container === 'function' ? container() : container;
  return _react2['default'].findDOMNode(container) || defaultContainer;
}

module.exports = exports['default'];
},{"react":undefined}],131:[function(require,module,exports){
'use strict';

exports.__esModule = true;

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }

var _ownerDocument = require('./ownerDocument');

var _ownerDocument2 = _interopRequireDefault(_ownerDocument);

var _domHelpersQueryOffset = require('dom-helpers/query/offset');

var _domHelpersQueryOffset2 = _interopRequireDefault(_domHelpersQueryOffset);

var _domHelpersQueryPosition = require('dom-helpers/query/position');

var _domHelpersQueryPosition2 = _interopRequireDefault(_domHelpersQueryPosition);

var _domHelpersQueryScrollTop = require('dom-helpers/query/scrollTop');

var _domHelpersQueryScrollTop2 = _interopRequireDefault(_domHelpersQueryScrollTop);

var utils = {

  getContainerDimensions: function getContainerDimensions(containerNode) {
    var width = undefined,
        height = undefined,
        scroll = undefined;

    if (containerNode.tagName === 'BODY') {
      width = window.innerWidth;
      height = window.innerHeight;

      scroll = _domHelpersQueryScrollTop2['default'](_ownerDocument2['default'](containerNode).documentElement) || _domHelpersQueryScrollTop2['default'](containerNode);
    } else {
      var _getOffset = _domHelpersQueryOffset2['default'](containerNode);

      width = _getOffset.width;
      height = _getOffset.height;

      scroll = _domHelpersQueryScrollTop2['default'](containerNode);
    }

    return { width: width, height: height, scroll: scroll };
  },

  getPosition: function getPosition(target, container) {
    var offset = container.tagName === 'BODY' ? _domHelpersQueryOffset2['default'](target) : _domHelpersQueryPosition2['default'](target, container);

    return offset;
  },

  calcOverlayPosition: function calcOverlayPosition(placement, overlayNode, target, container, padding) {
    var childOffset = utils.getPosition(target, container);

    var _getOffset2 = _domHelpersQueryOffset2['default'](overlayNode);

    var overlayHeight = _getOffset2.height;
    var overlayWidth = _getOffset2.width;

    var positionLeft = undefined,
        positionTop = undefined,
        arrowOffsetLeft = undefined,
        arrowOffsetTop = undefined;

    if (placement === 'left' || placement === 'right') {
      positionTop = childOffset.top + (childOffset.height - overlayHeight) / 2;

      if (placement === 'left') {
        positionLeft = childOffset.left - overlayWidth;
      } else {
        positionLeft = childOffset.left + childOffset.width;
      }

      var topDelta = getTopDelta(positionTop, overlayHeight, container, padding);

      positionTop += topDelta;
      arrowOffsetTop = 50 * (1 - 2 * topDelta / overlayHeight) + '%';
      arrowOffsetLeft = void 0;
    } else if (placement === 'top' || placement === 'bottom') {
      positionLeft = childOffset.left + (childOffset.width - overlayWidth) / 2;

      if (placement === 'top') {
        positionTop = childOffset.top - overlayHeight;
      } else {
        positionTop = childOffset.top + childOffset.height;
      }

      var leftDelta = getLeftDelta(positionLeft, overlayWidth, container, padding);
      positionLeft += leftDelta;
      arrowOffsetLeft = 50 * (1 - 2 * leftDelta / overlayWidth) + '%';
      arrowOffsetTop = void 0;
    } else {
      throw new Error('calcOverlayPosition(): No such placement of "' + placement + '" found.');
    }

    return { positionLeft: positionLeft, positionTop: positionTop, arrowOffsetLeft: arrowOffsetLeft, arrowOffsetTop: arrowOffsetTop };
  }
};

function getTopDelta(top, overlayHeight, container, padding) {
  var containerDimensions = utils.getContainerDimensions(container);
  var containerScroll = containerDimensions.scroll;
  var containerHeight = containerDimensions.height;

  var topEdgeOffset = top - padding - containerScroll;
  var bottomEdgeOffset = top + padding - containerScroll + overlayHeight;

  if (topEdgeOffset < 0) {
    return -topEdgeOffset;
  } else if (bottomEdgeOffset > containerHeight) {
    return containerHeight - bottomEdgeOffset;
  } else {
    return 0;
  }
}

function getLeftDelta(left, overlayWidth, container, padding) {
  var containerDimensions = utils.getContainerDimensions(container);
  var containerWidth = containerDimensions.width;

  var leftEdgeOffset = left - padding;
  var rightEdgeOffset = left + padding + overlayWidth;

  if (leftEdgeOffset < 0) {
    return -leftEdgeOffset;
  } else if (rightEdgeOffset > containerWidth) {
    return containerWidth - rightEdgeOffset;
  } else {
    return 0;
  }
}
exports['default'] = utils;
module.exports = exports['default'];
},{"./ownerDocument":132,"dom-helpers/query/offset":108,"dom-helpers/query/position":110,"dom-helpers/query/scrollTop":112}],132:[function(require,module,exports){
'use strict';

exports.__esModule = true;

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _domHelpersOwnerDocument = require('dom-helpers/ownerDocument');

var _domHelpersOwnerDocument2 = _interopRequireDefault(_domHelpersOwnerDocument);

exports['default'] = function (componentOrElement) {
  return _domHelpersOwnerDocument2['default'](_react2['default'].findDOMNode(componentOrElement));
};

module.exports = exports['default'];
},{"dom-helpers/ownerDocument":105,"react":undefined}],133:[function(require,module,exports){
'use strict';

exports.__esModule = true;
exports.errMsg = errMsg;
exports.createChainableTypeChecker = createChainableTypeChecker;

function errMsg(props, propName, componentName, msgContinuation) {
  return 'Invalid prop \'' + propName + '\' of value \'' + props[propName] + '\'' + (' supplied to \'' + componentName + '\'' + msgContinuation);
}

/**
 * Create chain-able isRequired validator
 *
 * Largely copied directly from:
 *  https://github.com/facebook/react/blob/0.11-stable/src/core/ReactPropTypes.js#L94
 */

function createChainableTypeChecker(validate) {
  function checkType(isRequired, props, propName, componentName) {
    componentName = componentName || '<<anonymous>>';
    if (props[propName] == null) {
      if (isRequired) {
        return new Error('Required prop \'' + propName + '\' was not specified in \'' + componentName + '\'.');
      }
    } else {
      return validate(props, propName, componentName);
    }
  }

  var chainedCheckType = checkType.bind(null, false);
  chainedCheckType.isRequired = checkType.bind(null, true);

  return chainedCheckType;
}
},{}],134:[function(require,module,exports){
'use strict';

exports.__esModule = true;

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _common = require('./common');

/**
 * Checks whether a prop provides a type of element.
 *
 * The type of element can be provided in two forms:
 * - tag name (string)
 * - a return value of React.createClass(...)
 *
 * @param props
 * @param propName
 * @param componentName
 * @returns {Error|undefined}
 */

function validate(props, propName, componentName) {
  var errBeginning = _common.errMsg(props, propName, componentName, '. Expected an Element `type`');

  if (typeof props[propName] !== 'function') {
    if (_react2['default'].isValidElement(props[propName])) {
      return new Error(errBeginning + ', not an actual Element');
    }

    if (typeof props[propName] !== 'string') {
      return new Error(errBeginning + ' such as a tag name or return value of React.createClass(...)');
    }
  }
}

exports['default'] = _common.createChainableTypeChecker(validate);
module.exports = exports['default'];
},{"./common":133,"react":undefined}],135:[function(require,module,exports){
'use strict';

exports.__esModule = true;

var _common = require('./common');

/**
 * Checks whether a prop provides a DOM element
 *
 * The element can be provided in two forms:
 * - Directly passed
 * - Or passed an object that has a `render` method
 *
 * @param props
 * @param propName
 * @param componentName
 * @returns {Error|undefined}
 */

function validate(props, propName, componentName) {
  if (typeof props[propName] !== 'object' || typeof props[propName].render !== 'function' && props[propName].nodeType !== 1) {
    return new Error(_common.errMsg(props, propName, componentName, ', expected a DOM element or an object that has a `render` method'));
  }
}

exports['default'] = _common.createChainableTypeChecker(validate);
module.exports = exports['default'];
},{"./common":133}],136:[function(require,module,exports){
module.exports = require('./lib/console');

},{"./lib/console":137}],137:[function(require,module,exports){
var React = require('react');
var _ = require('lodash');
var Draggable = require('react-draggable');

var	debug = require('debug')('react-console');

var Console = React.createClass({displayName: "Console",
	getInitialState() {
		var props = this.props.log;
		var msg = _.isArray(props.messages) ? props.messages : props.messages ? [props.messages] : [];
		var doc = _.isArray(props.docs) ? props.docs : props.docs ? [props.docs] : [];
		var err = _.isArray(props.error) ? props.error : props.error ? [props.error] : [];
		
		this._cache;
		this._repeat = 1;
		this._sync = 0;
		this._ui = {}
		
		return { 
			log: msg.concat(doc,err),
			alive: props.alive,
			fullscreen: false
		};
	},
	componentWillReceiveProps(props) {
		debug('console received props', props)
		var _this = this;
		if(props.alive !== this.state.alive) {
			_this.setState({alive: props.alive});
		}
		
		var logs = props.log;
		
		// object
		var docs = _.isArray(logs.doc) ? logs.doc : logs.doc ? [logs.doc] : []
		_.each(docs, function(m) {
			var push = _this.state.log;
			try {
				var mm = JSON.stringify(m, null, 4);
			} catch(e) {
				var mm = 'doc not parsed'
			}
			if(mm && mm !== _this._cache) {
				push.push(React.createElement("pre", null, mm));
				_this.setState({log:  push });
				_this._repeat = 1;
			} else {
				_this._repeat++;
				_this._sync++;
				if(_this._repeat > 4 && _this._repeat % 5 === 0) {
					_this._sync--;
					push.push(React.createElement("pre", null, mm));
					push.push(React.createElement("div", {className: "repeatlog"}, "Log entry below repeated ", _this._repeat, " times"));
					_this.setState({log:  push });
				}
			}
				
			_this._cache = mm;
		});
		
		// error
		var error = _.isArray(logs.error) ? logs.error : logs.error ? [logs.error] : []
		_.each(error, function(m) {
			var push = _this.state.log;
			if(m && m !== _this._cache) {
				push.push(React.createElement("div", {className: "bg-danger", dangerouslySetInnerHTML: {__html:m}}));
				_this.setState({log:  push });
				_this._repeat = 1;
			} else {
				_this._repeat++;
				_this._sync++;
				if(_this._repeat > 4 && _this._repeat % 5 === 0) {
					_this._sync--;
					push.push(React.createElement("div", {className: "bg-danger", dangerouslySetInnerHTML: {__html:m}}));
					push.push(React.createElement("div", {className: "repeatlog"}, "Log entry below repeated ", _this._repeat, " times"));
					_this.setState({log:  push });
				}
			}
				
			_this._cache = m;
			
		});
		
		// standard message
		var msg = _.isArray(logs.message) ? logs.message : logs.message ? [logs.message] : []
		_.each(msg, function(m) {
			var push = _this.state.log;
			if(m && m !== _this._cache) {
				push.push(React.createElement("div", {dangerouslySetInnerHTML: {__html:m}}));
				_this.setState({log:  push });
				_this._repeat = 1;
			} else {
				_this._repeat++;
				_this._sync++;
				if(_this._repeat > 4 && _this._repeat % 5 === 0) {
					_this._sync--;
					push.push(React.createElement("div", {dangerouslySetInnerHTML: {__html:m}}));
					push.push(React.createElement("div", {className: "repeatlog"}, "Log entry below repeated  ", _this._repeat, " times"));
					_this.setState({log:  push });
				}
			}
			 
			_this._cache = m;
		});
		
		// clean up the log array 
		if(this.state.log.length > 500) {
			this.setState({ log: this.state.log.splice(0,200) });
		}
	},
    handleStop: function (event, ui) {
        this._ui = ui.position;
    },
	render() {
		
		var _this = this;
		if(_.isArray(this.state.log) && this.state.log.length > 0) {

			var rev = this.state.log;
			var num = _this._sync;
			var map = rev.map(function(entry, k) {
				num++;
				return (React.createElement("div", {className: "clearfix", key: k}, 
					React.createElement("div", {className: "col-sm-1"}, num), 
					React.createElement("div", {className: "col-sm-11"}, entry)
				));
			}).reverse();
		} else {
			var map = React.createElement("span", null)
		}
		
		var cmd = this.props.alive ? (React.createElement("div", {className: "col-sm-8 no-padding"}, React.createElement("form", {onSubmit: this.command}, React.createElement("div", {className: "col-sm-10 no-padding"}, 
				React.createElement("input", {type: "text", ref: "console", className: "form-control"})
				), 
				React.createElement("div", {className: "col-sm-2  no-padding"}, 
				React.createElement("a", {href: "#", onClick: this.command, className: "btn btn-sm btn-info "}, "Emit")
				)))) : React.createElement("div", {className: "col-sm-8 "}, React.createElement("span", null))
		
		var clear = this.props.alive ? 
				React.createElement("span", null, 
					React.createElement("a", {title: "Erase console", href: "#", onClick: this.clear, className: "btn btn-sm btn-danger pull-right"}, React.createElement("span", {className: "glyphicon glyphicon-erase"}))
				)
			: 
				React.createElement("span", null)
		
		var classy = " console no-padding ";
		var add = this._ui.top ? {start: {x: this._ui.left, y: this._ui.top}} : {};
		debug(add)
		if(!this.props.alive) {
			classy += ' close ';
			if(this._ui.top) add = {start: {x: 0, y: 0}};
		} else if(this.state.fullscreen) {
			classy += ' fullscreen '; 
			add = {start: {x: 0, y: 0}}; 
		}	
		
		var minimize = this.props.alive ? React.createElement("a", {title: "minimize console", href: "#", onClick: this.toggle, className: "btn btn-sm btn-default pull-right"}, React.createElement("span", {className: "glyphicon glyphicon-minus"})) : React.createElement("span", null)
		
		var maximize;
		var move = React.createElement("span", null);
		if((this.props.alive && !this.state.fullscreen) || (!this.props.alive && this.state.fullscreen)) {
			maximize = React.createElement("span", null, React.createElement("a", {title: "fullscreen", href: "#", onClick: this.screen, className: "btn btn-sm btn-default pull-right"}, React.createElement("span", {className: "glyphicon glyphicon-fullscreen"})))
			if(!this.props.fullscreen && this.props.alive) move = React.createElement("div", {title: "move console", className: "btn btn-sm btn-warning pull-right handle"}, React.createElement("span", {className: "handle glyphicon glyphicon-move"}));
		} else if(this.props.alive) {
			maximize = React.createElement("a", {title: "small window", href: "#", onClick: this.screen, className: "btn btn-sm btn-default pull-right"}, React.createElement("span", {className: "glyphicon glyphicon-modal-window"}))
			
		} else {
			maximize = React.createElement("a", {title: "View Console", href: "#", onClick: this.screen, className: "btn btn-sm btn-default pull-right"}, React.createElement("span", {className: "glyphicon glyphicon-modal-window"}))
		}
		
		return (React.createElement(Draggable, React.__spread({
                handle: ".handle", 
                zIndex: 1000, 
                moveOnStartChange: true, 
                onStop: this.handleStop}, 
                add, 
                {bounds: "parent"}), 
						React.createElement("div", {className: classy}, 
							cmd, 
							React.createElement("div", {className: "col-sm-4 no-padding"}, " ", move, React.createElement("span", {className: "pull-right"}, "   "), " ", minimize, React.createElement("span", {className: "pull-right"}, "   "), maximize, React.createElement("span", {className: "pull-right"}, "   "), clear, " "), 
							React.createElement("div", {className: "clearfix msg no-padding"}, 
								map	
							)	
						)
					
		));	
	},
	clear() {
		this._cache = false;
		this._repeat = 1;
		this._sync = 0;
		this.setState({log:[]});
	}, 
	command(e) {
		e.preventDefault();
		if(_.isFunction(this.props.command)) {
			this.props.command(this.refs.console.getDOMNode().value);
		}
		
	},
	toggle(e) {
		e.preventDefault();
		if(_.isFunction(this.props.toggle)) {
			this.props.toggle(e);
		}
	},
	screen(e) {
		e.preventDefault();
		if(!this.state.alive) {
			this.toggle(e);
		} else {
			this.setState({ fullscreen: !this.state.fullscreen });
			
		}
	},
	
	
});

module.exports = Console;

},{"debug":17,"lodash":undefined,"react":undefined,"react-draggable":138}],138:[function(require,module,exports){
module.exports = require('./lib/draggable');

},{"./lib/draggable":139}],139:[function(require,module,exports){
'use strict';

var React = require('react');
var emptyFunction = function(){};
var assign = require('object-assign');
var classNames = require('classnames');

//
// Helpers. See Element definition below this section.
//

function createUIEvent(draggable) {
  // State changes are often (but not always!) async. We want the latest value.
  var state = draggable._pendingState || draggable.state;
  return {
    node: draggable.getDOMNode(),
    position: {
      top: state.clientY,
      left: state.clientX
    }
  };
}

function canDragY(draggable) {
  return draggable.props.axis === 'both' ||
      draggable.props.axis === 'y';
}

function canDragX(draggable) {
  return draggable.props.axis === 'both' ||
      draggable.props.axis === 'x';
}

function isFunction(func) {
  return typeof func === 'function' || Object.prototype.toString.call(func) === '[object Function]';
}

// @credits https://gist.github.com/rogozhnikoff/a43cfed27c41e4e68cdc
function findInArray(array, callback) {
  for (var i = 0, length = array.length; i < length; i++) {
    if (callback.apply(callback, [array[i], i, array])) return array[i];
  }
}

function matchesSelector(el, selector) {
  var method = findInArray([
    'matches',
    'webkitMatchesSelector',
    'mozMatchesSelector',
    'msMatchesSelector',
    'oMatchesSelector'
  ], function(method){
    return isFunction(el[method]);
  });

  return el[method].call(el, selector);
}

/**
 * simple abstraction for dragging events names
 * */
var eventsFor = {
  touch: {
    start: 'touchstart',
    move: 'touchmove',
    end: 'touchend'
  },
  mouse: {
    start: 'mousedown',
    move: 'mousemove',
    end: 'mouseup'
  }
};

// Default to mouse events
var dragEventFor = eventsFor['mouse'];

/**
 * get {clientX, clientY} positions of control
 * */
function getControlPosition(e) {
  var position = (e.touches && e.touches[0]) || e;
  return {
    clientX: position.clientX,
    clientY: position.clientY
  };
}

function addEvent(el, event, handler) {
  if (!el) { return; }
  if (el.attachEvent) {
    el.attachEvent('on' + event, handler);
  } else if (el.addEventListener) {
    el.addEventListener(event, handler, true);
  } else {
    el['on' + event] = handler;
  }
}

function removeEvent(el, event, handler) {
  if (!el) { return; }
  if (el.detachEvent) {
    el.detachEvent('on' + event, handler);
  } else if (el.removeEventListener) {
    el.removeEventListener(event, handler, true);
  } else {
    el['on' + event] = null;
  }
}

function outerHeight(node) {
  // This is deliberately excluding margin for our calculations, since we are using
  // offsetTop which is including margin. See getBoundPosition
  var height = node.clientHeight;
  var computedStyle = window.getComputedStyle(node);
  height += int(computedStyle.borderTopWidth);
  height += int(computedStyle.borderBottomWidth);
  return height;
}

function outerWidth(node) {
  // This is deliberately excluding margin for our calculations, since we are using
  // offsetLeft which is including margin. See getBoundPosition
  var width = node.clientWidth;
  var computedStyle = window.getComputedStyle(node);
  width += int(computedStyle.borderLeftWidth);
  width += int(computedStyle.borderRightWidth);
  return width;
}
function innerHeight(node) {
  var height = node.clientHeight;
  var computedStyle = window.getComputedStyle(node);
  height -= int(computedStyle.paddingTop);
  height -= int(computedStyle.paddingBottom);
  return height;
}

function innerWidth(node) {
  var width = node.clientWidth;
  var computedStyle = window.getComputedStyle(node);
  width -= int(computedStyle.paddingLeft);
  width -= int(computedStyle.paddingRight);
  return width;
}

function isNum(num) {
  return typeof num === 'number' && !isNaN(num);
}

function int(a) {
  return parseInt(a, 10);
}

function getBoundPosition(draggable, clientX, clientY) {
  var bounds = JSON.parse(JSON.stringify(draggable.props.bounds));
  var node = draggable.getDOMNode();
  var parent = node.parentNode;

  if (bounds === 'parent') {
    var nodeStyle = window.getComputedStyle(node);
    var parentStyle = window.getComputedStyle(parent);
    // Compute bounds. This is a pain with padding and offsets but this gets it exactly right.
    bounds = {
      left: -node.offsetLeft + int(parentStyle.paddingLeft) +
            int(nodeStyle.borderLeftWidth) + int(nodeStyle.marginLeft),
      top: -node.offsetTop + int(parentStyle.paddingTop) +
            int(nodeStyle.borderTopWidth) + int(nodeStyle.marginTop),
      right: innerWidth(parent) - outerWidth(node) - node.offsetLeft,
      bottom: innerHeight(parent) - outerHeight(node) - node.offsetTop
    };
  }

  // Keep x and y below right and bottom limits...
  if (isNum(bounds.right)) clientX = Math.min(clientX, bounds.right);
  if (isNum(bounds.bottom)) clientY = Math.min(clientY, bounds.bottom);

  // But above left and top limits.
  if (isNum(bounds.left)) clientX = Math.max(clientX, bounds.left);
  if (isNum(bounds.top)) clientY = Math.max(clientY, bounds.top);

  return [clientX, clientY];
}

function snapToGrid(grid, pendingX, pendingY) {
  var x = Math.round(pendingX / grid[0]) * grid[0];
  var y = Math.round(pendingY / grid[1]) * grid[1];
  return [x, y];
}

// Useful for preventing blue highlights all over everything when dragging.
var userSelectStyle = ';user-select: none;-webkit-user-select:none;-moz-user-select:none;' +
  '-o-user-select:none;-ms-user-select:none;';

function addUserSelectStyles(draggable) {
  if (!draggable.props.enableUserSelectHack) return;
  var style = document.body.getAttribute('style') || '';
  document.body.setAttribute('style', style + userSelectStyle);
}

function removeUserSelectStyles(draggable) {
  if (!draggable.props.enableUserSelectHack) return;
  var style = document.body.getAttribute('style') || '';
  document.body.setAttribute('style', style.replace(userSelectStyle, ''));
}

function createCSSTransform(style) {
  // Replace unitless items with px
  var x = style.x + 'px';
  var y = style.y + 'px';
  return {
    transform: 'translate(' + x + ',' + y + ')',
    WebkitTransform: 'translate(' + x + ',' + y + ')',
    OTransform: 'translate(' + x + ',' + y + ')',
    msTransform: 'translate(' + x + ',' + y + ')',
    MozTransform: 'translate(' + x + ',' + y + ')'
  };
}


//
// End Helpers.
//

//
// Define <Draggable>
//

module.exports = React.createClass({
  displayName: 'Draggable',

  propTypes: {
    /**
     * `axis` determines which axis the draggable can move.
     *
     * 'both' allows movement horizontally and vertically.
     * 'x' limits movement to horizontal axis.
     * 'y' limits movement to vertical axis.
     *
     * Defaults to 'both'.
     */
    axis: React.PropTypes.oneOf(['both', 'x', 'y']),

    /**
     * `bounds` determines the range of movement available to the element.
     * Available values are:
     *
     * 'parent' restricts movement within the Draggable's parent node.
     *
     * Alternatively, pass an object with the following properties, all of which are optional:
     *
     * {left: LEFT_BOUND, right: RIGHT_BOUND, bottom: BOTTOM_BOUND, top: TOP_BOUND}
     *
     * All values are in px.
     *
     * Example:
     *
     * ```jsx
     *   var App = React.createClass({
     *       render: function () {
     *         return (
     *            <Draggable bounds={{right: 300, bottom: 300}}>
     *              <div>Content</div>
     *           </Draggable>
     *         );
     *       }
     *   });
     * ```
     */
    bounds: React.PropTypes.oneOfType([
      React.PropTypes.shape({
        left: React.PropTypes.Number,
        right: React.PropTypes.Number,
        top: React.PropTypes.Number,
        bottom: React.PropTypes.Number
      }),
      React.PropTypes.oneOf(['parent', false])
    ]),

    /**
     * By default, we add 'user-select:none' attributes to the document body
     * to prevent ugly text selection during drag. If this is causing problems
     * for your app, set this to `false`.
     */
    enableUserSelectHack: React.PropTypes.bool,

    /**
     * `handle` specifies a selector to be used as the handle that initiates drag.
     *
     * Example:
     *
     * ```jsx
     *   var App = React.createClass({
     *       render: function () {
     *         return (
     *            <Draggable handle=".handle">
     *              <div>
     *                  <div className="handle">Click me to drag</div>
     *                  <div>This is some other content</div>
     *              </div>
     *           </Draggable>
     *         );
     *       }
     *   });
     * ```
     */
    handle: React.PropTypes.string,

    /**
     * `cancel` specifies a selector to be used to prevent drag initialization.
     *
     * Example:
     *
     * ```jsx
     *   var App = React.createClass({
     *       render: function () {
     *           return(
     *               <Draggable cancel=".cancel">
     *                   <div>
     *                     <div className="cancel">You can't drag from here</div>
     *            <div>Dragging here works fine</div>
     *                   </div>
     *               </Draggable>
     *           );
     *       }
     *   });
     * ```
     */
    cancel: React.PropTypes.string,

    /**
     * `grid` specifies the x and y that dragging should snap to.
     *
     * Example:
     *
     * ```jsx
     *   var App = React.createClass({
     *       render: function () {
     *           return (
     *               <Draggable grid={[25, 25]}>
     *                   <div>I snap to a 25 x 25 grid</div>
     *               </Draggable>
     *           );
     *       }
     *   });
     * ```
     */
    grid: React.PropTypes.arrayOf(React.PropTypes.number),

    /**
     * `start` specifies the x and y that the dragged item should start at
     *
     * Example:
     *
     * ```jsx
     *      var App = React.createClass({
     *          render: function () {
     *              return (
     *                  <Draggable start={{x: 25, y: 25}}>
     *                      <div>I start with transformX: 25px and transformY: 25px;</div>
     *                  </Draggable>
     *              );
     *          }
     *      });
     * ```
     */
    start: React.PropTypes.shape({
      x: React.PropTypes.number,
      y: React.PropTypes.number
    }),

    /**
     * `moveOnStartChange`, if true (default false) will move the element if the `start`
     * property changes.
     */
    moveOnStartChange: React.PropTypes.bool,


    /**
     * `zIndex` specifies the zIndex to use while dragging.
     *
     * Example:
     *
     * ```jsx
     *   var App = React.createClass({
     *       render: function () {
     *           return (
     *               <Draggable zIndex={100}>
     *                   <div>I have a zIndex</div>
     *               </Draggable>
     *           );
     *       }
     *   });
     * ```
     */
    zIndex: React.PropTypes.number,

    /**
     * Called when dragging starts.
     * If this function returns the boolean false, dragging will be canceled.
     *
     * Example:
     *
     * ```js
     *  function (event, ui) {}
     * ```
     *
     * `event` is the Event that was triggered.
     * `ui` is an object:
     *
     * ```js
     *  {
     *    position: {top: 0, left: 0}
     *  }
     * ```
     */
    onStart: React.PropTypes.func,

    /**
     * Called while dragging.
     * If this function returns the boolean false, dragging will be canceled.
     *
     * Example:
     *
     * ```js
     *  function (event, ui) {}
     * ```
     *
     * `event` is the Event that was triggered.
     * `ui` is an object:
     *
     * ```js
     *  {
     *    position: {top: 0, left: 0}
     *  }
     * ```
     */
    onDrag: React.PropTypes.func,

    /**
     * Called when dragging stops.
     *
     * Example:
     *
     * ```js
     *  function (event, ui) {}
     * ```
     *
     * `event` is the Event that was triggered.
     * `ui` is an object:
     *
     * ```js
     *  {
     *    position: {top: 0, left: 0}
     *  }
     * ```
     */
    onStop: React.PropTypes.func,

    /**
     * A workaround option which can be passed if onMouseDown needs to be accessed,
     * since it'll always be blocked (due to that there's internal use of onMouseDown)
     */
    onMouseDown: React.PropTypes.func,
  },

  componentWillReceiveProps: function(newProps) {
    // React to changes in the 'start' param.
    if (newProps.moveOnStartChange && newProps.start) {
      this.setState(this.getInitialState(newProps));
    }
  },

  componentWillUnmount: function() {
    // Remove any leftover event handlers
    removeEvent(document, dragEventFor['move'], this.handleDrag);
    removeEvent(document, dragEventFor['end'], this.handleDragEnd);
    removeUserSelectStyles(this);
  },

  getDefaultProps: function () {
    return {
      axis: 'both',
      bounds: false,
      handle: null,
      cancel: null,
      grid: null,
      moveOnStartChange: false,
      start: {x: 0, y: 0},
      zIndex: NaN,
      enableUserSelectHack: true,
      onStart: emptyFunction,
      onDrag: emptyFunction,
      onStop: emptyFunction,
      onMouseDown: emptyFunction
    };
  },

  getInitialState: function (props) {
    // Handle call from CWRP
    props = props || this.props;
    return {
      // Whether or not we are currently dragging.
      dragging: false,

      // Offset between start top/left and mouse top/left while dragging.
      offsetX: 0, offsetY: 0,

      // Current transform x and y.
      clientX: props.start.x, clientY: props.start.y
    };
  },

  handleDragStart: function (e) {
    // Make it possible to attach event handlers on top of this one
    this.props.onMouseDown(e);

    // Short circuit if handle or cancel prop was provided and selector doesn't match
    if ((this.props.handle && !matchesSelector(e.target, this.props.handle)) ||
      (this.props.cancel && matchesSelector(e.target, this.props.cancel))) {
      return;
    }

    // Call event handler. If it returns explicit false, cancel.
    var shouldStart = this.props.onStart(e, createUIEvent(this));
    if (shouldStart === false) return;

    var dragPoint = getControlPosition(e);

    // Add a style to the body to disable user-select. This prevents text from
    // being selected all over the page.
    addUserSelectStyles(this);

    // Initiate dragging. Set the current x and y as offsets
    // so we know how much we've moved during the drag. This allows us
    // to drag elements around even if they have been moved, without issue.
    this.setState({
      dragging: true,
      offsetX: dragPoint.clientX - this.state.clientX,
      offsetY: dragPoint.clientY - this.state.clientY
    });


    // Add event handlers
    addEvent(document, dragEventFor['move'], this.handleDrag);
    addEvent(document, dragEventFor['end'], this.handleDragEnd);
  },

  handleDragEnd: function (e) {
    // Short circuit if not currently dragging
    if (!this.state.dragging) {
      return;
    }

    removeUserSelectStyles(this);

    // Turn off dragging
    this.setState({
      dragging: false
    });

    // Call event handler
    this.props.onStop(e, createUIEvent(this));

    // Remove event handlers
    removeEvent(document, dragEventFor['move'], this.handleDrag);
    removeEvent(document, dragEventFor['end'], this.handleDragEnd);
  },

  handleDrag: function (e) {
    var dragPoint = getControlPosition(e);

    // Calculate X and Y
    var clientX = dragPoint.clientX - this.state.offsetX;
    var clientY = dragPoint.clientY - this.state.offsetY;

    // Snap to grid if prop has been provided
    if (Array.isArray(this.props.grid)) {
      var coords = snapToGrid(this.props.grid, clientX, clientY);
      clientX = coords[0], clientY = coords[1];
    }

    if (this.props.bounds) {
      var pos = getBoundPosition(this, clientX, clientY);
      clientX = pos[0], clientY = pos[1];
    }

    // Call event handler. If it returns explicit false, cancel.
    var shouldUpdate = this.props.onDrag(e, createUIEvent(this));
    if (shouldUpdate === false) return this.handleDragEnd();

    // Update transform
    this.setState({
      clientX: clientX,
      clientY: clientY
    });
  },

  onMouseDown: function(ev) {
    // Prevent 'ghost click' which happens 300ms after touchstart if the event isn't cancelled.
    // We don't cancel the event on touchstart because of #37; we might want to make a scrollable item draggable.
    // More on ghost clicks: http://ariatemplates.com/blog/2014/05/ghost-clicks-in-mobile-browsers/
    if (dragEventFor == eventsFor['touch']) {
      return ev.preventDefault();
    }

    return this.handleDragStart.apply(this, arguments);
  },

  onTouchStart: function(ev) {
    // We're on a touch device now, so change the event handlers
    dragEventFor = eventsFor['touch'];

    return this.handleDragStart.apply(this, arguments);
  },

  // Intended for use by a parent component. Resets internal state on this component. Useful for
  // <Resizable> and other components in case this element is manually resized and start/moveOnStartChange
  // don't work for you.
  resetState: function() {
    this.setState({
      offsetX: 0, offsetY: 0, clientX: 0, clientY: 0
    });
  },

  render: function () {
    // Create style object. We extend from existing styles so we don't
    // remove anything already set (like background, color, etc).
    var childStyle = this.props.children.props.style || {};

    // Add a CSS transform to move the element around. This allows us to move the element around
    // without worrying about whether or not it is relatively or absolutely positioned.
    // If the item you are dragging already has a transform set, wrap it in a <span> so <Draggable>
    // has a clean slate.
    var transform = createCSSTransform({
      // Set left if horizontal drag is enabled
      x: canDragX(this) ?
        this.state.clientX :
        0,

      // Set top if vertical drag is enabled
      y: canDragY(this) ?
        this.state.clientY :
        0
    });

    // Workaround IE pointer events; see #51
    // https://github.com/mzabriskie/react-draggable/issues/51#issuecomment-103488278
    var touchHacks = {
      touchAction: 'none'
    };

    var style = assign({}, childStyle, transform, touchHacks);

    // Set zIndex if currently dragging and prop has been provided
    if (this.state.dragging && !isNaN(this.props.zIndex)) {
      style.zIndex = this.props.zIndex;
    }

    var className = classNames((this.props.children.props.className || ''), 'react-draggable', {
      'react-draggable-dragging': this.state.dragging,
      'react-draggable-dragged': this.state.dragged
    });

    // Reuse the child provided
    // This makes it flexible to use whatever element is wanted (div, ul, etc)
    return React.cloneElement(React.Children.only(this.props.children), {
      style: style,
      className: className,

      onMouseDown: this.onMouseDown,
      onTouchStart: this.onTouchStart,
      onMouseUp: this.handleDragEnd,
      onTouchEnd: this.handleDragEnd
    });
  }
});

},{"classnames":140,"object-assign":141,"react":undefined}],140:[function(require,module,exports){
/*!
  Copyright (c) 2015 Jed Watson.
  Licensed under the MIT License (MIT), see
  http://jedwatson.github.io/classnames
*/

function classNames() {
	var classes = '';
	var arg;

	for (var i = 0; i < arguments.length; i++) {
		arg = arguments[i];
		if (!arg) {
			continue;
		}

		if ('string' === typeof arg || 'number' === typeof arg) {
			classes += ' ' + arg;
		} else if (Object.prototype.toString.call(arg) === '[object Array]') {
			classes += ' ' + classNames.apply(null, arg);
		} else if ('object' === typeof arg) {
			for (var key in arg) {
				if (!arg.hasOwnProperty(key) || !arg[key]) {
					continue;
				}
				classes += ' ' + key;
			}
		}
	}
	return classes.substr(1);
}

// safely export classNames for node / browserify
if (typeof module !== 'undefined' && module.exports) {
	module.exports = classNames;
}

// safely export classNames for RequireJS
if (typeof define !== 'undefined' && define.amd) {
	define('classnames', [], function() {
		return classNames;
	});
}

},{}],141:[function(require,module,exports){
'use strict';

function ToObject(val) {
	if (val == null) {
		throw new TypeError('Object.assign cannot be called with null or undefined');
	}

	return Object(val);
}

module.exports = Object.assign || function (target, source) {
	var from;
	var keys;
	var to = ToObject(target);

	for (var s = 1; s < arguments.length; s++) {
		from = arguments[s];
		keys = Object.keys(Object(from));

		for (var i = 0; i < keys.length; i++) {
			to[keys[i]] = from[keys[i]];
		}
	}

	return to;
};

},{}],142:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, '__esModule', {
  value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _reactBootstrapLibButton = require('react-bootstrap/lib/Button');

var _reactBootstrapLibButton2 = _interopRequireDefault(_reactBootstrapLibButton);

var _LinkMixin = require('./LinkMixin');

var _LinkMixin2 = _interopRequireDefault(_LinkMixin);

var ButtonLink = _react2['default'].createClass({
  displayName: 'ButtonLink',

  mixins: [_LinkMixin2['default']],

  render: function render() {
    return _react2['default'].createElement(
      _reactBootstrapLibButton2['default'],
      _extends({}, this.getLinkProps(), { ref: 'button' }),
      this.props.children
    );
  }
});

exports['default'] = ButtonLink;
module.exports = exports['default'];
},{"./LinkMixin":143,"react":undefined,"react-bootstrap/lib/Button":47}],143:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, '__esModule', {
  value: true
});

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }

function _objectWithoutProperties(obj, keys) { var target = {}; for (var i in obj) { if (keys.indexOf(i) >= 0) continue; if (!Object.prototype.hasOwnProperty.call(obj, i)) continue; target[i] = obj[i]; } return target; }

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

function isLeftClickEvent(event) {
  return event.button === 0;
}

function isModifiedEvent(event) {
  return !!(event.metaKey || event.altKey || event.ctrlKey || event.shiftKey);
}

exports['default'] = {
  propTypes: {
    active: _react2['default'].PropTypes.bool,
    activeClassName: _react2['default'].PropTypes.string.isRequired,
    disabled: _react2['default'].PropTypes.bool,
    to: _react2['default'].PropTypes.string.isRequired,
    params: _react2['default'].PropTypes.object,
    query: _react2['default'].PropTypes.object,
    onClick: _react2['default'].PropTypes.func
  },
  contextTypes: {
    router: _react2['default'].PropTypes.func.isRequired
  },

  getDefaultProps: function getDefaultProps() {
    return {
      activeClassName: 'active'
    };
  },

  /**
   * Returns props except those used by this Mixin
   * Gets "active" from router if needed.
   * Gets the value of the "href" attribute to use on the DOM element.
   * Sets "onClick" to "handleRouteTo".
   */
  getLinkProps: function getLinkProps() {
    var _props = this.props;
    var to = _props.to;
    var params = _props.params;
    var query = _props.query;

    var props = _objectWithoutProperties(_props, ['to', 'params', 'query']);

    if (this.props.active === undefined) {
      props.active = this.context.router.isActive(to, params, query);
    }

    props.href = this.context.router.makeHref(to, params, query);

    props.onClick = this.handleRouteTo;

    return props;
  },

  handleRouteTo: function handleRouteTo(event) {
    var allowTransition = true;
    var clickResult = undefined;

    if (this.props.disabled) {
      event.preventDefault();
      return;
    }

    if (this.props.onClick) {
      clickResult = this.props.onClick(event);
    }

    if (isModifiedEvent(event) || !isLeftClickEvent(event)) {
      return;
    }

    if (clickResult === false || event.defaultPrevented === true) {
      allowTransition = false;
    }

    event.preventDefault();

    if (allowTransition) {
      this.context.router.transitionTo(this.props.to, this.props.params, this.props.query);
    }
  }
};
module.exports = exports['default'];
},{"react":undefined}],144:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, '__esModule', {
  value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _reactBootstrapLibListGroupItem = require('react-bootstrap/lib/ListGroupItem');

var _reactBootstrapLibListGroupItem2 = _interopRequireDefault(_reactBootstrapLibListGroupItem);

var _LinkMixin = require('./LinkMixin');

var _LinkMixin2 = _interopRequireDefault(_LinkMixin);

var LinkGroupItemLink = _react2['default'].createClass({
  displayName: 'LinkGroupItemLink',

  mixins: [_LinkMixin2['default']],

  render: function render() {
    return _react2['default'].createElement(
      _reactBootstrapLibListGroupItem2['default'],
      _extends({}, this.getLinkProps(), { ref: 'listGroupItem' }),
      this.props.children
    );
  }
});

exports['default'] = LinkGroupItemLink;
module.exports = exports['default'];
},{"./LinkMixin":143,"react":undefined,"react-bootstrap/lib/ListGroupItem":53}],145:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, '__esModule', {
  value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _reactBootstrapLibMenuItem = require('react-bootstrap/lib/MenuItem');

var _reactBootstrapLibMenuItem2 = _interopRequireDefault(_reactBootstrapLibMenuItem);

var _LinkMixin = require('./LinkMixin');

var _LinkMixin2 = _interopRequireDefault(_LinkMixin);

var MenuItemLink = _react2['default'].createClass({
  displayName: 'MenuItemLink',

  mixins: [_LinkMixin2['default']],

  render: function render() {
    var props = this.getLinkProps();
    delete props.onSelect; // this is done on the copy of this.props

    return _react2['default'].createElement(
      _reactBootstrapLibMenuItem2['default'],
      _extends({}, props, { ref: 'menuItem' }),
      this.props.children
    );
  }
});

exports['default'] = MenuItemLink;
module.exports = exports['default'];
},{"./LinkMixin":143,"react":undefined,"react-bootstrap/lib/MenuItem":54}],146:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, '__esModule', {
  value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _reactBootstrapLibNavItem = require('react-bootstrap/lib/NavItem');

var _reactBootstrapLibNavItem2 = _interopRequireDefault(_reactBootstrapLibNavItem);

var _LinkMixin = require('./LinkMixin');

var _LinkMixin2 = _interopRequireDefault(_LinkMixin);

var NavItemLink = _react2['default'].createClass({
  displayName: 'NavItemLink',

  mixins: [_LinkMixin2['default']],

  render: function render() {
    return _react2['default'].createElement(
      _reactBootstrapLibNavItem2['default'],
      _extends({}, this.getLinkProps(), { ref: 'navItem' }),
      this.props.children
    );
  }
});

exports['default'] = NavItemLink;
module.exports = exports['default'];
},{"./LinkMixin":143,"react":undefined,"react-bootstrap/lib/NavItem":55}],147:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, '__esModule', {
  value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _reactBootstrapLibPageItem = require('react-bootstrap/lib/PageItem');

var _reactBootstrapLibPageItem2 = _interopRequireDefault(_reactBootstrapLibPageItem);

var _LinkMixin = require('./LinkMixin');

var _LinkMixin2 = _interopRequireDefault(_LinkMixin);

var PageItemLink = _react2['default'].createClass({
  displayName: 'PageItemLink',

  mixins: [_LinkMixin2['default']],

  render: function render() {
    return _react2['default'].createElement(
      _reactBootstrapLibPageItem2['default'],
      _extends({}, this.getLinkProps(), { ref: 'pageItem' }),
      this.props.children
    );
  }
});

exports['default'] = PageItemLink;
module.exports = exports['default'];
},{"./LinkMixin":143,"react":undefined,"react-bootstrap/lib/PageItem":58}],148:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, '__esModule', {
  value: true
});

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _reactBootstrapLibOverlayTrigger = require('react-bootstrap/lib/OverlayTrigger');

var _reactBootstrapLibOverlayTrigger2 = _interopRequireDefault(_reactBootstrapLibOverlayTrigger);

exports['default'] = _reactBootstrapLibOverlayTrigger2['default'].withContext({
  router: _react2['default'].PropTypes.func
});
module.exports = exports['default'];
},{"react":undefined,"react-bootstrap/lib/OverlayTrigger":57}],149:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, '__esModule', {
  value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _reactBootstrapLibThumbnail = require('react-bootstrap/lib/Thumbnail');

var _reactBootstrapLibThumbnail2 = _interopRequireDefault(_reactBootstrapLibThumbnail);

var _LinkMixin = require('./LinkMixin');

var _LinkMixin2 = _interopRequireDefault(_LinkMixin);

var ThumbnailLink = _react2['default'].createClass({
  displayName: 'ThumbnailLink',

  mixins: [_LinkMixin2['default']],

  render: function render() {
    return _react2['default'].createElement(
      _reactBootstrapLibThumbnail2['default'],
      _extends({}, this.getLinkProps(), { ref: 'thumbnail' }),
      this.props.children
    );
  }
});

exports['default'] = ThumbnailLink;
module.exports = exports['default'];
},{"./LinkMixin":143,"react":undefined,"react-bootstrap/lib/Thumbnail":60}],150:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, '__esModule', {
  value: true
});

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }

var _ButtonLink2 = require('./ButtonLink');

var _ButtonLink3 = _interopRequireDefault(_ButtonLink2);

exports.ButtonLink = _ButtonLink3['default'];

var _ListGroupItemLink2 = require('./ListGroupItemLink');

var _ListGroupItemLink3 = _interopRequireDefault(_ListGroupItemLink2);

exports.ListGroupItemLink = _ListGroupItemLink3['default'];

var _MenuItemLink2 = require('./MenuItemLink');

var _MenuItemLink3 = _interopRequireDefault(_MenuItemLink2);

exports.MenuItemLink = _MenuItemLink3['default'];

var _NavItemLink2 = require('./NavItemLink');

var _NavItemLink3 = _interopRequireDefault(_NavItemLink2);

exports.NavItemLink = _NavItemLink3['default'];

var _PageItemLink2 = require('./PageItemLink');

var _PageItemLink3 = _interopRequireDefault(_PageItemLink2);

exports.PageItemLink = _PageItemLink3['default'];

var _RouterOverlayTrigger2 = require('./RouterOverlayTrigger');

var _RouterOverlayTrigger3 = _interopRequireDefault(_RouterOverlayTrigger2);

exports.RouterOverlayTrigger = _RouterOverlayTrigger3['default'];

var _ThumbnailLink2 = require('./ThumbnailLink');

var _ThumbnailLink3 = _interopRequireDefault(_ThumbnailLink2);

exports.ThumbnailLink = _ThumbnailLink3['default'];
},{"./ButtonLink":142,"./ListGroupItemLink":144,"./MenuItemLink":145,"./NavItemLink":146,"./PageItemLink":147,"./RouterOverlayTrigger":148,"./ThumbnailLink":149}],151:[function(require,module,exports){
/**
 * Copyright 2013-2015, Facebook, Inc.
 * All rights reserved.
 *
 * This source code is licensed under the BSD-style license found in the
 * LICENSE file in the root directory of this source tree. An additional grant
 * of patent rights can be found in the PATENTS file in the same directory.
 *
 * @providesModule emptyFunction
 */

function makeEmptyFunction(arg) {
  return function() {
    return arg;
  };
}

/**
 * This function accepts and discards inputs; it has no side effects. This is
 * primarily useful idiomatically for overridable function endpoints which
 * always need to be callable, since JS lacks a null-call idiom ala Cocoa.
 */
function emptyFunction() {}

emptyFunction.thatReturns = makeEmptyFunction;
emptyFunction.thatReturnsFalse = makeEmptyFunction(false);
emptyFunction.thatReturnsTrue = makeEmptyFunction(true);
emptyFunction.thatReturnsNull = makeEmptyFunction(null);
emptyFunction.thatReturnsThis = function() { return this; };
emptyFunction.thatReturnsArgument = function(arg) { return arg; };

module.exports = emptyFunction;

},{}],152:[function(require,module,exports){
(function (process){
/**
 * Copyright 2014-2015, Facebook, Inc.
 * All rights reserved.
 *
 * This source code is licensed under the BSD-style license found in the
 * LICENSE file in the root directory of this source tree. An additional grant
 * of patent rights can be found in the PATENTS file in the same directory.
 *
 * @providesModule warning
 */

"use strict";

var emptyFunction = require("./emptyFunction");

/**
 * Similar to invariant but only logs a warning if the condition is not met.
 * This can be used to log issues in development environments in critical
 * paths. Removing the logging code for production environments will keep the
 * same logic and follow the same code paths.
 */

var warning = emptyFunction;

if ("production" !== process.env.NODE_ENV) {
  warning = function(condition, format ) {for (var args=[],$__0=2,$__1=arguments.length;$__0<$__1;$__0++) args.push(arguments[$__0]);
    if (format === undefined) {
      throw new Error(
        '`warning(condition, format, ...args)` requires a warning ' +
        'message argument'
      );
    }

    if (format.length < 10 || /^[s\W]*$/.test(format)) {
      throw new Error(
        'The warning format should be able to uniquely identify this ' +
        'warning. Please, use a more descriptive format than: ' + format
      );
    }

    if (format.indexOf('Failed Composite propType: ') === 0) {
      return; // Ignore CompositeComponent proptype check.
    }

    if (!condition) {
      var argIndex = 0;
      var message = 'Warning: ' + format.replace(/%s/g, function()  {return args[argIndex++];});
      console.warn(message);
      try {
        // --- Welcome to debugging React ---
        // This error was thrown as a convenience so that you can use this stack
        // to find the callsite that caused this warning to fire.
        throw new Error(message);
      } catch(x) {}
    }
  };
}

module.exports = warning;

}).call(this,require('_process'))
},{"./emptyFunction":151,"_process":16}]},{},[1]);
