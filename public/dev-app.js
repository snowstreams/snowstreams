System
	.import('/config.js')
	.then(function() {
		System.import('/bundles/dependencies.js').catch(console.error.bind(console));
		System.import('/app').catch(console.error.bind(console));	

	})
	.catch(console.error.bind(console));

