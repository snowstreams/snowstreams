/**
 * Creates a upd broadcast from a source stream
 *
 * ####Example:
 *
 *     ss.stream.udp('hdudp', 'tv1', host, port, callback)
 *
 *     
 * @param {String} stream name
 * @param {String} source name
 * @param {String} host
 * @param {Int} port
 * @param {Function} callback
 * @api public
 */

var debug = require('debug')('snowstreams:stream:udp');
var _ = require('lodash');
var dgram = require('dgram');
var chunkingStreams = require('chunking-streams');
var SizeChunker = chunkingStreams.SizeChunker;


module.exports = function(ss) {
	
	return function udp(opts, callback) {
	
		var _this = this;
		var source = opts.source;
		var name = opts.name;
		var port = opts.port;
		var host = opts.host;
		var doc = opts.doc;
		var Channel = opts.Channel;
		_this.paused = false;
		
		if(!_.isFunction(callback)) {
			var callback = function(){}
		}
		if(ss.asset[doc._id]) {
			callback('asset exists');
			return _this;
		}
		if(!_.isString(host) ) {
			callback('host must be a String');
			return _this;
		}
		if(_.isNaN(parseFloat(port))) {
			callback('port must be a Number');
			return _this;
		} else {
			port = parseFloat(port);
		}
		if(!_.isString(host)) {
			callback('valid host required');
			return _this;
		}
		
		var client = dgram.createSocket('udp4');
		// Listen for messages from client
		client.on('message', function (message) {
			debug("Client: " + message.toString());
		});
		
		debug('add new Asset', name);
		var Asset = new ss.Asset(name, {
			doc: doc,
			host: host,
			port: port,
			source: source,
			client: client,
			log: [],
			asset: 'stream',
			type: 'udp',
			upd: true,
			options: opts,
			Channel: Channel
		});
		
		if(Channel) {
			debug('add new Asset to Channel ' + Channel.name);
			Channel.addAsset(name, Asset);
		}	
		
		// set streaming bit
		Asset.pushPlay(function(stream, callback) {
			if(this.doc && _.isFunction(this.doc.save)) {
				this.doc.streaming = true;
				this.doc.save();
			}
			ss.notify('status', ss.Listen.status());
			callback(null, stream);
		}.bind(Asset));
		
		Asset.pushStop(function(pass, cb) {
			
			debug('check for upd stream');
			this.doc.streaming = false;
			this.doc.save();
			if(ss.isReadableStream(this.client) && _.isFunction(this.client.end)) {
				debug('stop upd stream', this);
				this.client.end();
			}	
			ss.notify('status', ss.Listen.status());
			if(_.isFunction(cb)) {
				cb(null, pass);
			}
			
		}.bind(Asset));
		
		// set the final pipe with chunking-streams
		// splits the stream into 7 TSP instead of 16 TSP
		Asset.chunker = new SizeChunker({
			chunkSize: 188 * 7,
			flushTail: true
		});

		Asset.chunker.on('chunkStart', function(id, done) {
			// Create the client
			if(_this.paused) debug('chunker start ... PAUSED');
			done();
		});
		 
		Asset.chunker.on('chunkEnd', function(id, done) {
			if(_this.paused) debug('chunker end ... PAUSED');
			done();
		});
		 
		Asset.chunker.on('data', function(chunk) {
			Asset.client.send(chunk.data, 0, chunk.data.length, port, host);
		});
		
		Asset.pushPlay(function(stream, callback) {
			debug('start upd stream');
			if(ss.isReadableStream(stream)) {
				callback(null, stream.pipe(this.chunker));
			} else {
				callback('error reading stream for ' + Asset.name);
			}
		}.bind(Asset));
		
		debug('done with new udp stream ' + opts.name + '... ready for play action');
		
		callback(null, Asset);
		
		return this;
	}

}
