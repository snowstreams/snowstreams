/**
 * Creates a upd unicast from a multicast stream
 *
 * ####Example:
 *
 *     ss.stream.udpProxy('start', port, callback)
 *
 *     
 * @param {String} action
 * @param {Int} port
 * @param {Function} callback
 * @api public
 */

var debug = require('debug')('snowstreams:stream:sourceProxy');
var _ = require('lodash');
var stream = require('stream');
var keystone = require('keystone');


module.exports = function(ss) {
	
	return function sourceProxy(route, callback) {
	
		var _this = this;
		
		if(!_.isFunction(callback)) {
			var callback = function(){}
		}
		if(!_.isString(route)) {
			route = 'proxy';
		}
		
		ss.set('proxy', route);
		
		var routes = function(app) {
			debug('route: '+ route)		
			
			/* execute a named program */
			app.all('/' + route + "/program/:program", keystone.middleware.api, function(req, res) {
				debug('args:', req.query);			
				if(req.method.toLowerCase() == 'get') {
					var query = req.query;
				} else {
					var query = req.body;
				}
				
				var program = req.params.program;
				
				ss.Listen.runProgram({
					program, 
					query
				}, function(err, data) {
					if(err) {
						return res.apiError({err: err});
					} else {
						return res.apiResponse({
							name: program.name,
							pid: program.pid,
							command: program.command,
							type: program.type,
							respawn: program.respawn,
							restart: program.restart,
							options: program.options,
							log: program.log
						});
					} 
				});			
			});
			
			
			
			/* manage a named channel */
			app.get('/' + route + "/channel/:channel/:action", keystone.middleware.api, function(req, res) {
							
				var asset = 'Channel';
				var slug = req.params.channel;
				
				debug('channel action', req.params.action, asset, slug);		
				ss.Listen[req.params.action]({
					asset, 
					slug
				}, function(err, data) {
					if(err) {
						return res.apiError({err: err});
					} else {
						debug(asset + ' received response');
						return res.apiResponse({
							success: true,
							action: req.params.action,
							channel: slug
						});
					} 
				});
				
			});
			
			/* serve a named mpegts source over http */
			app.all('/' + route + "/:source", function(req, res, next) {
				debug('source route', req.params.source);
				if(req.params.source) {
					var stream = req.params.source;
					
					if(_.isObject(ss.sources[stream]) && ss.isReadableStream(ss.sources[stream].stream)) {
						
						var cl = ss.servers.keystone.proxies.push({ip: req.ip, source: stream});
						ss.openSocket.emit('status', ss.Listen.status());
						
						res.writeHead(206, { "Content-Type": 'video/mpegts' });
						ss.sources[stream].stream.pipe(res, {end:true})
						/*
						ss.sources[stream].stream.on('data', function(chunk) {
							res.write(chunk);
						});
						* */
						ss.sources[stream].stream.on('end', function() {
							//res.end();
							ss.servers.keystone.proxies.splice(cl - 1, 1);
							ss.openSocket.emit('status', ss.Listen.status());
						});
						req.connection.addListener('close', function () {
							ss.servers.keystone.proxies.splice(cl - 1, 1);
							ss.openSocket.emit('status', ss.Listen.status());
						});

					} else {
						debug('NO source');
						res.apiError('Source Unavailable');
					}
				} else {
					res.apiError();
				}
			});
			/* a named streams management and play source over http*/
			app.all('/' + route + "/stream/:stream1/:arg", function(req, res, next) {
				debug('stream route', req.params.stream1, ss.sources[stream]);
				if(req.params.stream1) {
					var stream = req.params.stream1;
					if(_.isObject(ss.streams[stream]) && ss.isReadableStream(ss.streams[stream].stream)) {
						
						var cl = ss.servers.keystone.proxies.push({ip: req.ip, stream: stream});
						ss.openSocket.emit('status', ss.Listen.status());
						
						res.writeHead(206, { "Content-Type": 'video/mpegts' });
						ss.streams[stream].stream.pipe(res, {end:true})
						/*
						ss.sources[stream].stream.on('data', function(chunk) {
							res.write(chunk);
						});
						* */
						ss.streams[stream].stream.on('end', function() {
							//res.end();
							ss.servers.keystone.proxies.splice(cl - 1, 1);
							ss.openSocket.emit('status', ss.Listen.status());
						});
						req.connection.addListener('close', function () {
							ss.servers.keystone.proxies.splice(cl - 1, 1);
							ss.openSocket.emit('status', ss.Listen.status());
						});

					} else {
						debug('NO stream');
						res.apiError();
					}
				} else {
					res.apiError();
				}
			});
			
			
			/* serve a named source over http using a user supplied content type*/
			app.all('/' + route + '/:ct/:source2', function(req, res, next) {
				debug('source custom content type route', req.params);
				if(req.params.source2) {
					var stream = req.params.source2;
					if(_.isObject(ss.sources[stream]) && ss.isReadableStream(ss.sources[stream].stream)) {
						
						var cl = ss.servers.keystone.proxies.push({ip: req.ip, source: stream});
						ss.openSocket.emit('status', ss.Listen.status());
						
						res.writeHead(206, { "Content-Type": 'video/' + req.params.ct });
						ss.sources[stream].stream.pipe(res, {end:true})
						ss.sources[stream].stream.on('end', function() {
							//res.end();
							ss.servers.keystone.proxies.splice(cl - 1, 1);
							ss.openSocket.emit('status', ss.Listen.status());
						});
						req.connection.addListener('close', function () {
							ss.servers.keystone.proxies.splice(cl - 1, 1);
							ss.openSocket.emit('status', ss.Listen.status());
						});

					} else {
						res.apiError();
						debug('NO source');
					}
				} else {
					res.apiError();
				}
			});
			
			return app;
		}
		
		ss.routes.push(routes);
		
		return this;
	}
}
