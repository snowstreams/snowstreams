/**
 * Creates a upd unicast from a multicast stream
 *
 * ####Example:
 *
 *     ss.stream.udpProxy('start', port, callback)
 *
 *     
 * @param {String} action
 * @param {Int} port
 * @param {Function} callback
 * @api public
 */

var debug = require('debug')('snowstreams:stream:udpProxy');
var _ = require('lodash');
var stream = require('stream');
var chunkingStreams = require('chunking-streams');
var SizeChunker = chunkingStreams.SizeChunker;


module.exports = function(ss) {
	
	return function udpProxy(opts, callback) {
	
		var _this = this;
		
		if(!_.isFunction(callback)) {
			var callback = function(){}
		}
		if(!_.isObject(opts)) {
			opts = {};
		}
		if(_.isNaN(parseFloat(opts.port))) {
			opts.port = 7001;
		} else {
			opts.port = parseFloat(opts.port);
		}
		if(!opts.name) {
			return callback('name required');
		}
		var routes = function(app) {
			/* serves main page */
			app.get("/:stream", function(req, res, next) {

				if(req.params.stream) {
					var stream = req.params.stream;
					if(_.isObject(ss.sources[stream]) && isReadableStream(ss.sources[stream].stream)) {
						
						var cl = ss.servers[opts.name].proxies.push({ip: req.ip, source: stream});
						res.writeHead(206, { "Content-Type": 'video/mpegts' });
						ss.sources[stream].stream.on('data', function(chunk) {
							res.write(chunk);
						});
						ss.sources[stream].stream.on('end', function() {
							res.end();
							ss.servers[opts.name].proxies.splice(cl - 1, 1);
						});
						req.connection.addListener('close', function () {
							ss.servers[opts.name].proxies.splice(cl - 1, 1);
						});

					} else {
						next();
					}
				} else {
					next();
				}
			});
			
			// override the default catch-all
			app.use(function(req, res, next) {
				var list = [];
				list.push('<div style="margin-top:10px;word-spacing:-.7px;"><b>Streams</b></div>');
				if(_.isObject(ss.streams))
					_.each(ss.streams, function(v) {
						list.push('<span style="word-spacing:-.7px;">' + v.type +  ' - ' + v.name +  ' - ' + v.host + ':' + v.port + '</span><br />');
					});
				if(_.isObject(ss.servers[opts.name].proxies))
					_.each(ss.servers[opts.name].proxies, function(v) {
						list.push('<span style="word-spacing:-.7px;">proxy - ' + v.source + ' - ' + v.ip + '</span><br />');
					});
				list.push('<div style="margin-top:10px;word-spacing:-.7px;"><b>Sources</b></div>');
				if(_.isObject(ss.sources))
					_.each(ss.sources, function(v) {
						var text = '<span style="word-spacing:-.7px;">' + v.type + ' - ' + v.name;
						if(v.pid) text += ' - PID: ' + v.pid  + '</span><br />'
						list.push(text);
					});
					
				list.push('<div style="margin-top:10px;word-spacing:-.7px;"><b>Servers</b></div>');
				if(_.isObject(ss.servers))
					_.each(ss.servers, function(v) {
						list.push('<span style="word-spacing:-.7px;">' + v.name + ' - ' + v.host  + ':' + v.port + '</span><br />');
					});
				var list2 = Object.keys(ss.servers[opts.name].sockets).length + ' open connections.';	
				var ll = '';
				_.each(list, function(l) {
					ll += l
				});		
				return res.status(200).send(ss.wrapHTML("Status","<p>"+list2+"</p><span  style='border-bottom:1px solid #aaa;'><div  style='padding-top:6px;'>" + ll + '<br /></div>'));
				
				
			});
			
			
			return app;
		}
		var options = {
			host: opts.host || false,
			port: opts.port || 10001,
			routes: routes
		}
		ss.createServer(opts.name,options);
		
		return this;
	}

	function isReadableStream(obj) {
		return obj instanceof stream.Stream &&
			typeof (obj._read === 'function') &&
			typeof (obj._readableState === 'object');
	}

}
