var	_ = require('lodash');
var	debug = require('debug')('snowstreams:lib:asset');
var async = require("async");

module.exports = function(ss) {
	function Asset(key, options) { 
		
		if (!(this instanceof Asset)) return new Asset(key, options);

		_.defaults(this, options);
		
		this.name = key;
		this.id = options.doc._id;
		this.Channel = options.Channel || false;
		
		// stop methods
		this._stop = [];
		// start methods
		this._start = [];
		// pause methods
		this._pause = [];
		// delete functions
		this._delete = [];
		
		ss.registerAsset(this);
		
		debug('new Asset ', this.name);
		
		// log
		this._log = [];
		
	}
	
	/**
	 * Actioners
	 * 
	 * */
	
	// crash
	Asset.prototype.crash = function(forced) {
		debug('Asset crashed... forced ' + forced);
		this.remove();
	}
	 
	// log
	Asset.prototype.log = function(entry) {
		if(entry) {
			return this._log.push(entry)
		} else {
			return this._log;	
		}
	}	 
	// Stop
	Asset.prototype.stop = function(passedValue, callback) {
		debug('run stop');
		return this.runAction('stop', passedValue, callback);
	}
	// Start
	Asset.prototype.start = function(passedValue, callback) {
		debug('run start');
		return this.runAction('start', passedValue, callback);
	}
	Asset.prototype.play = Asset.prototype.start;
	// Pause
	Asset.prototype.pause = function(passedValue, callback) {
		return this.runAction('pause', passedValue, callback);
	}
	// Remove
	Asset.prototype.remove = function(passedValue, callback) {
		debug('run remove');
		return this.runAction('stop', passedValue, () => {
			debug('delete Asset for good', this.name); 
			delete this;
			callback();
		});
	}

	/**
	 * Run an action
	 * 
	 * The UI stores asset trees by the parent doc._id by
	 * pushing stop functions to ss._stop[doc._id]
	 * 
	 * Programatically you should push your own stop functions
	 * -- var myAsset = new ss.Asset('myAsset');
	 * -- myAsset.pushStop(Function);
	 * -- myAsset.stop();
	 * 
	 * @api private
	 * 
	 * */

	Asset.prototype.runAction = function(action, passedValue, callback) {
		
		var actioner = this['_'+action];
		var _current = passedValue;
		
		debug('run action', action);	
		
		if(actioner.length > 0) {
					
			debug('run ' + this.name + ' - ' + this.asset + ' ' + actioner, actioner);
			
			// we have items in this method so run them
			async.forEachOfSeries(actioner, (fn, k, advance) => {
								
				if(fn instanceof Asset) {
					// run  Asset function
					fn.runAction(action, advance);
					
				} else if(_.isFunction(fn)) {
					
					debug('run function ' + k + ' for ' + this.name + ' - ' + this.asset, ss.isReadableStream(_current));
					fn(_current, (err, passedValue) => {
						debug('got result from function ' + k + ' for ' + this.name + ' - ' + this.asset, ss.isReadableStream(passedValue));
						_current = passedValue;
						advance();
					});
				
				} else {
					
					debug('skip');
					advance();
				
				}
				
			}, (err) => {
				debug('done with foreach methods for ' + this.name + ' - ' + this.asset);
				if(!_.isFunction(callback)) {
					return this;
				}
				
				callback(err, _current);
			});
			
		} else {
			
			debug('no ' + action + ' methods for ' + this.name + ' - ' + this.asset);
			if(!_.isFunction(callback)) {
				return this;
			}
			
			callback(err, _current);
		
		}
		
	}

	/**
	 * Adds a method to an action
	 * 
	 * The UI stores asset trees by the parent doc._id by
	 * pushing stop functions to ss._stop[doc._id]
	 * 
	 * Programatically you should push your own stop functions
	 * -- var myAsset = new ss.Asset('myAsset');
	 * -- myAsset.pushStop(Function);
	 * -- myAsset.pullStop(Function);
	 * 
	 * @api private
	 * 
	 * */

	Asset.prototype.pushMethod = function(actioner, who, callback) {
		
		debug('push method');
		
		if(!_.isFunction(callback)) {
			callback = ()=>{};
		}
		
		if(!_.isFunction(who)) {
			debug('push failed');
			return callback('first argument must be a function');
		} 	
		
		var index = actioner.indexOf(who);

		if(index > -1) {
			debug('Method exists');
			callback('Method exists');
		} else {
			actioner.push(who)
			callback(null, 'method added');
		}
		
	}

	/**
	 * Removes a method from an action
	 * 
	 * The UI stores asset trees by the parent doc._id by
	 * pushing stop functions to ss._stop[doc._id]
	 * 
	 * Programatically you should push your own stop functions
	 * -- var myAsset = new ss.Asset('myAsset');
	 * -- myAsset.pushStop(Function);
	 * -- myAsset.pullStop(Function);
	 * 
	 * @api private
	 *  
	 * */

	Asset.prototype.pullMethod = function(actioner, who, callback) {
		
		if(!_.isFunction(callback)) {
			callback = ()=>{};
		}
		
		if(!_.isFunction(who)) {
			debug('push failed');
			return callback('first argument must be a function');
		} 	
		
		var index = actioner.indexOf(who);
		
		if(index > -1) {
			_.pullAt(actioner, index);
			debug('Method removed');
			callback(null, 'Method removed');
		} else {
			callback('method not found');
			debug('method not found');
		}
		
	}

	/**
	 * Action Methods
	 * 
	 * Programatically you should push your own stop functions
	 * -- var myAsset = new ss.Asset('myAsset');
	 * -- myAsset.pushStop(Function);
	 * -- myAsset.pullStop(Function);
	 * 
	 * */
	// Stop
	Asset.prototype.pullStop = function(who, callback) {
		return this.pullMethod(this._stop, who, callback);
	}
	Asset.prototype.pushStop = function(who, callback) {
		return this.pushMethod(this._stop, who, callback);
	}
	// Start
	Asset.prototype.pullStart = function(who, callback) {
		return this.pullMethod(this._start, who, callback);
	}
	Asset.prototype.pushStart = function(who, callback) {
		return this.pushMethod(this._start, who, callback);
	}
	Asset.prototype.pushPlay = Asset.prototype.pushStart;
	Asset.prototype.pullPlay =Asset.prototype.pullStart;
	// Pause
	Asset.prototype.pullPause = function(who, callback) {
		return this.pullMethod(this._pause, who, callback);
	}
	Asset.prototype.pushPause = function(who, callback) {
		return this.pushMethod(this._pause, who, callback);
	}

	return Asset;
}
