var	_ = require('lodash');
var	debug = require('debug')('snowstreams:lib:core:socket');
var async = require("async");
var keystone = require('keystone');
var Listeners = require('./listeners');

function sockets() { 
	
	var exports = {};
	/**
	 * All exports are added to the snowstreams.prototype
	 */
	
	exports.listeners = Listeners;
	
	exports.socketRoutes = function(Live) {
		var ss = this;
		
		// create a new connection for open requests
		ss.openSocket =  Live.io.of('/snowstreams');
		ss.openSocket.on("disconnect", function(s) {
			debug('snowstreams socket disconnected');
		});
		ss.openSocket.on("connection", function(socket) {
			
			socket.on('status',function() {
				socket.emit('status',ss.Listen.status());
			});
			
			// auth
			socket.on('auth', ss.Listen.auth);
			
		});
		
		
		ss.authSocket = Live._live.namespace.lists
		// list connection is auth based
		ss.authSocket.on("connection", function(socket) {
			
			// console help
			socket.on('help',function(data) {
				debug('help emitted', 'send to', data.iden, data);
				if(data.iden) socket.emit(data.iden, {message: consoleHelp()});
				socket.emit('help',{message: consoleHelp()});
			});
			
			// start
			socket.on('start', ss.Listen.start);
			
			// stop
			socket.on('stop', ss.Listen.stop);
			
			// runProgram
			socket.on('runProgram', ss.Listen.runProgram);
			
			// manager
			socket.on('manager', ss.Listen.manager);
		
		});
		
	}
	
	return exports;
}

module.exports = sockets;


function consoleHelp() {
	return '<pre ><p ><span >helping fingers... help </span><i >h</i></p><h4 ><u >Manage Documents</u></h4><p ><b >List::action::key:value::anotherkey:value::doc::putdoc:last::andmimic:therest</b></p><p ><b>List Choices:</b> <span >Channel </span><i >C</i><span > | File </span><i >F</i><span > | Program </span><i >P</i><span > | ProgramArgument </span><i >A</i><span > | Source </span><i >I</i><span > | Stream </span><i >O</i><span > | User </span><i >U</i><span > </span></p><h5 >Example</h5><p ><b >Program::get::slug:streamable::populate:arguments updatedBy::exclude:__v</b></p><p ><b >I::update::id:9878y08790tg89::doc::name:webbier</b></p><p >** first letter must be uppercase **</p><p ><br ></p><p ></p><h4 ><u >Manage Assets</u></h4><p ><b >manager::type::action:value::search::slug:dvgrab</b></p><p ><b>Type Choices:</b> <span >channel </span><i >c</i><span > | file </span><i >f</i><span > | program </span><i >p</i><span > | source </span><i >i</i><span > | stream </span><i >o</i></p><h5 >Example</h5><p ><b >manager::channel::start:true::search::slug:tv</b></p><p ><b >manager::p::action:run::argument:0::channel:724::search::slug:change-channel</b></p><p >** first letter must be lowercase **</p><p ><br ></p><p ></p><h4 ><u >Chat</u></h4><p ><b >where::event::data</b></p><h5 >Example</h5><p ><b >wan::message::message:hello</b></p><p ><b >room::message::message:hi</b></p></pre>';
}
