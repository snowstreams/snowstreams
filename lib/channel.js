var	_ = require('lodash');
var	debug = require('debug')('snowstreams:lib:channel');
var async = require("async");
var Asset = require('./asset.js');
var hat = require('hat');

module.exports = function(ss) {
	function Channel(key, options) { 
		
		if (!(this instanceof Channel)) return new Channel(key, options);
		
		if(!_.isObject(options)) {
			options = {};
		}
		
		if(!_.isObject(options.doc)) {
			options.doc = {
				_id: hat()
			}
		}
		
		_.defaults(this, options);
		
		this.name = key;
		this.id = options.doc._id;
		
		// assets
		this.assets = {
			sources: {},
			streams: {}
		};
		this._assets = [];
		
		this._currentStream = false;
		
		ss.registerChannel(this);
		
		debug('new Channel ', this.name);
		
	}
	
	function getType(streamORsource) {
		var type = false;
		switch(streamORsource) {
			case "source":
			case "sources":
				type = 'sources';
				break;
			case "stream":
			case "streams":
				type = 'streams';
				break;
			default:
				type = streamORsource;
				break;
		}	
		return type;	
	}
	
	/**
	 * Add a source or stream asset
	 * 
	 * @api public
	 * 
	 * */

	Channel.prototype.addAsset = function(name, asset, callback) {	
		
		var type = getType(asset.asset);
		
		debug('Add Asset to Channel', name, type);
			
		this.assets[type][name] = asset;
		this._assets.push(asset);
		
		if(!_.isFunction(callback)) {
			return this;
		} else {
			return callback();
		}
		
	}
	Channel.prototype.add = Asset.prototype.addAsset;
	
	/**
	 * Remove a source or stream asset
	 * 
	 * @api public
	 * 
	 * */

	Channel.prototype.removeAsset = function(name, asset, callback) {
		
		var type = getType(asset.asset);
				
		return asset.stop(function() {
			delete this.assets[type][name];
			this._assets = _.without(this._assets, asset);
			
			if(!_.isFunction(callback)) {
				return this;
			} else {
				return callback();
			}

		});
		
	}
	
	/**
	 * Get a source or stream asset
	 * 
	 * @api public
	 * 
	 * */

	Channel.prototype.getAsset = function(name, streamORsource, callback) {
		
		var type = getType(streamORsource);
		
		var asset = this.assets[type][name];
		
		if(!_.isFunction(callback)) {
			return asset;
		} else {
			return callback(null, asset);
		}
		
	}
	Channel.prototype.get = Channel.prototype.getAsset;
	
	/**
	 * Actioners
	 * 
	 * */
	 
	// crash
	Channel.prototype.crash = function(Asset, forced) {
		if(Asset) {
			debug('Asset crashed... forced ' + forced);
			this.removeAsset(Asset.id, Asset);
			return;
		}
		debug('Channel crashed... forced ' + forced);
		this.remove();
	}
	// Stop
	Channel.prototype.stop = function(callback) {
		debug('run stop');
		return this.runAction('stop', callback);
	}
	// Start
	Channel.prototype.start = function(callback) {
		debug('run start');
		return this.runAction('start', callback);
	}
	Channel.prototype.play = Channel.prototype.start;
	// Pause
	Channel.prototype.pause = function(callback) {
		return this.runAction('pause', callback);
	}
	// Remove
	Channel.prototype.remove = function(callback) {
		debug('run remove');
		async.forEachOfSeries(
			this._assets,
			(asset, key, next) => {
				debug('remove asset ' + asset.name);
				asset.options.killChannel = false;
				ss.removeAsset(asset, (err, pass) => {
					
					next();
				});
			}, 
			(err) => {
				debug('delete Channel for good', this.name); 
				delete this;
			}
		);
	}

	/**
	 * Run an action
	 * 
	 * 
	 * @api private
	 * 
	 * */

	Channel.prototype.runAction = function(action, callback) {
		
		debug('run action ' + action);
		
		var channel = this;
		
		debug(this._assets.map((a)=>{ return { name:a.name, type:a.type, asset:a.asset}}));
		
		var done = (err) => {
			debug('done run action ' + action);
			if(_.isFunction(callback)) {
				callback(null, this._currentStream);
			}
		};

		var runForEach = (value, key, nextOne) => {
			
			debug('run ' + action + ' for Asset ' , value.name, value.asset, value.type);
			
			if(_.isFunction(value[action])) {
				
				value[action](this._currentStream, (err, passedValue) => {
					if(ss.isReadableStream(passedValue)) {
						this._currentStream = passedValue;
					}
					nextOne();
				})
			
			} else {
				
				debug('bad value');
				nextOne();
			
			}
		}
				
		async.forEachOfSeries(this._assets, runForEach, done);
		
	}

	/**
	 * Adds an Asset to the tree
	 * 
	 * -- var myChannel = new ss.Channel('cable 1');
	 * -- myChannel.pushMethod(Asset);
	 * 
	 * @api private
	 * 
	 * */

	Channel.prototype.pushMethod = function(actioner, who, callback) {
		
		debug('push method');
		
		if(!_.isFunction(callback)) {
			callback = ()=>{};
		}
		
		if(!_.isFunction(who)) {
			debug('push failed');
			return callback('first argument must be a function');
		} 	
		
		var index = actioner.indexOf(who);

		if(index > -1) {
			debug('Method exists');
			callback('Method exists');
		} else {
			actioner.push(who)
			callback(null, 'method added');
		}
		
	}

	/**
	 * Removes an Asset from the tree
	 * 
	 * -- var myChannel = new ss.Channel('cable 1');
	 * -- myChannel.pullMethod(Asset);
	 * 
	 * @api private
	 *  
	 * */

	Channel.prototype.pullMethod = function(actioner, who, callback) {
		
		if(!_.isFunction(callback)) {
			callback = ()=>{};
		}
		
		if(!_.isFunction(who)) {
			debug('push failed');
			return callback('first argument must be a function');
		} 	
		
		var index = actioner.indexOf(who);
		
		if(index > -1) {
			_.pullAt(actioner, index);
			debug('Method removed');
			callback(null, 'Method removed');
		} else {
			callback('method not found');
			debug('method not found');
		}
		
	}

	return Channel;
}
