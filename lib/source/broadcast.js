/**
 * Creates a broadcast using streams from other defined sources
 *
 * ####Example:
 *
 *     ss.source.broadcast({}, callback)
 *
 *     
 * @param {Object} opts
 * @param {Function} callback
 * @api public
 */

var debug = require('debug')('snowstreams:source:broadcast');
var _ = require('lodash');
// node streams
var stream = require('stream');
var Transform = stream.Transform;
var fs = require('fs');
var streamstream = require('stream-stream');
var queue = [];
var async = require('async');
// include the custom buffer duplex stream
var bufferStream = require('../node-streams/leaky');


module.exports = function(ss) {
	
	return function broadcast(opts, callback) {
	
		debug('broadcast');
		
		var _this = this;
		var ran = 0;
		if(!_.isFunction(callback)) {
			debug('no callback');
			var callback = function(){}
		}
		if(!_.isString(opts.name)) {
			callback('name must be a String');
			return _this;
		}
		if(ss.sources[opts.name]) {
			//callback('name in use');
			//return _this;
		}
		
		var Channel = opts.Channel;
		
		// this broadcast write stream should only end with explicit instructions
		// We could use a simple Passthrough, but with a Transform stream
		// maybe we can push filler source when there is not a current incoming stream
		var transform = Transform();
		transform._transform = function (data, encoding, callback) {
			this.cb = callback;
			if(data !== null) {
				this.push(data);
				callback();
			} else {
				debug('no data');
				
			}
		};
		
		// send back the broadcast info
		var Asset;
		sendBack();
		
		function sendBack() {
			Asset = new ss.Asset(opts.name, {
				doc: opts.doc,
				asset: 'source',
				type: 'broadcast',
				broadcast: true,
				sources: opts.source,
				source: false,
				stream: transform,
				current: false,
				childSources: [],
				childStreams: [],
				options: opts,
				Channel: Channel
				
			});
			
			if(Channel) {
				debug('add new Asset to Channel ' + Channel.name);
				Channel.addAsset(opts.name, Asset);
			}
			
			// set streaming bit
			Asset.pushPlay(function(stream, returnCallback) {
				debug('save broadcast as streaming');
				if(this.doc && _.isFunction(this.doc.save)) {
					this.doc.streaming = true;
					this.doc.save();
				}
				ss.notify('status', ss.Listen.status());
				returnCallback(null, stream);
			}.bind(Asset));
			
			Asset.pushStop(function(pass, returnCallback) {
				
				debug('check for broadcast');
				if(ss.isReadableStream(this.stream) && _.isFunction(this.stream.end)) {
					debug('stop broadcast');
					this.stream.end();
				}
				this.doc.streaming = false;
				this.doc.save();
				ss.notify('status', ss.Listen.status());
				if(_.isFunction(returnCallback)) {
					returnCallback(null, pass);
				}
				
			}.bind(Asset));
			
			ss.notify('status', ss.Listen.status());
			
			// run the sources and pipe into the broadcast
			Asset.pushPlay(function(stream, returnCallback) {
				debug('play broadcast', ss.isReadableStream(stream), this.name);
				if(ss.isReadableStream(stream)) {
					returnCallback(null, stream.pipe(this.stream));
				} else {
					returnCallback('a valid stream was not provided');
				}
			}.bind(Asset));
			
			callback(null, Asset);
				
		}
		
		function runSources(Asset, returnCallback) {
			async.forEachOf(Asset.sources, function(source, key, doNext) {
				debug('create stream and pipe next source in queue');
				// check to see if this asset has been stopped
				if(!ss.asset(Asset.id)) {
					return doNext('Process stopped by external means');
				}
				var next = function(sourceAsset) {
					debug('stream ' + sourceAsset.name + ' done... start next');
					if(sourceAsset) {
						delete sourceAsset;
					}
					ss.notify('status', ss.Listen.status());
					doNext();
				}
				Asset.current = source;
				addSource(Asset, next, returnCallback, function(stream){
					stream.pipe(transform);
				});
			}, function( err ){
				if( err ) {
					debug('A file failed to process');
					if(err) debug(err);
					returnCallback(err, Asset)
				} else {
					debug('All sources have been consumed by the stream successfully');
					// if we are looping start over
					if(opts.loop) {
						debug('Looping enabled... Stating over...');
						runSources(Asset, callback);
					} else {
						returnCallback(null, Asset)
					}
				}
				
				
			});
		}
		
		function writeit(stream) {
			//debug(stream)
			transform.write(stream);
			
		}
		// create each read stream and push to queue
		// callback should be the original exit function
		function addSource(Asset, end, maincb, advance) {
			
			var src = Asset.current;
			
			if(ss.isReadableStream(src)) {
				//queue.push(src);
				debug('is a stream... use it');
				advance(src);
				return;
			} else if(_.isString(src) && !ss.asset[src]) {
				ss.isFile(src, function(err, res) {
					if(!err && res) {
						Asset.file = src;
						
						ss.source.file({
							name: src,
							file: src,
							end,
							parentSource: opts.name,
							Asset
						}, function(err, source) {
							if(err || !source) {
								debug('' + src + ' did not produce a valid source');
								return maincb('' + src + ' did not produce a valid source');	
							}
							if(ss.isReadableStream(source.stream)) {
								debug('created source stream');
								//queue.push(source.stream);
								advance(source.stream);
								return;
							}
						});
						
					} else {
						return maincb('source ' + src + ' is not a valid file');
					}
				});
			} else if(_.isString(src)) {
				if(!ss.sources[src]) {
					return maincb('source ' + src + ' not found in list');
				}
				debug('is a source stream... use it');
				attachToSourceEmitter(src);
				//queue.push(ss.sources[src].stream);
				advance(ss.sources[src].stream);
				return;
			} else {	
				return maincb('each source must be a valid stream or file ' + src, false);
			}
		}	
		
		// attach to the sources emitter
		function attachToSourceEmitter(program) {
			ss.on('source.' + program, function(data) {
				if(data.action !== null) {
					var da = data.action;
					// the source restarted so reset the stream input
					if(da === 'restart') {
						debug(program + ' restarted');
						setTimeout(function(){
							//ss.sources[opts.source].stream.on('data', chunkit);
						}, 50);
						//ss.sources[program].stream.pipe(transform);
					}
				}
			});
		}
		
		return this;
	}
}
