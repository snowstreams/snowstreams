/**
 * Creates a source stream from a udp stream
 *
 * ####Example:
 *
 *     ss.source.udp({port:10000,host:'224.0.0.251'}, callback)
 *
 *     
 * @param {Object} opts
 * @param {Function} callback
 * @api public
 */

var debug = require('debug')('snowstreams:lib:source:file');
var _ = require('lodash');
// node streams
var fs = require('fs');  
// include the custom buffer duplex stream
var bufferStream = require('../node-streams/leaky');
var Transform = require('stream').Transform;
var ratelimit = require('ratelimit');
var ffmpeg = require('fluent-ffmpeg');
//ffmpeg.setFfmpegPath('avconv');
//ffmpeg.setFfprobePath('avprobe');

module.exports = function(ss) {

	return function file(opts, callback) {
	
		debug('process file');
		
		var _this = this;
		
		if(!_.isFunction(callback)) {
			debug('no callback in file');
			var callback = function(){}
		}
		if(!_.isString(opts.name)) {
			callback('name must be a String');
			return _this;
		}
		if(ss.sources[opts.name]) {
			callback('name in use');
			return _this;
		}
		if(!_.isString(opts.file) ) {
			callback('path must be a String');
			return _this;
		}
		if(!opts.Asset) {
			callback('Asset manager must be attached');
			return _this;
		}
		fs.lstat(opts.file, function(err, stats) {
			if (!err && stats.isFile()) {
				// Yes it is
			} else {
				callback('file must exist');
				return _this;
			}
		});
		
		var removeAsset = function () {
			delete ss.sources[opts.name];
			delete this.assets.sources[opts.name];
		}
		
		// create the new stream
		var transform = Transform();
		transform._transform = function (data, encoding, callback) {
			this.cb = callback;
			if(data !== null) {
				//debug("write chunk");
				this.push(data);
				callback();
			} else {
				debug('no file data');
				callback();
			}
		};
		
		var chunkit = function(chunk) {
			write();
			function write() {
				ok = transform.write(chunk);
				if(!ok) {
					//transform.once('drain', write);
				}
			}
		}
		
		// make sure you set the correct path to your video file
		var proc = ffmpeg(opts.file)
		.inputOptions('-re')
		.format("mpegts")
		.addOptions([
		  "-c copy",
		  "-bsf h264_mp4toannexb",
		  //"-movflags frag_keyframe+faststart",
		  "-strict experimental"
		])
		// setup event handlers
		.on('end', function() {
			debug('file has been converted succesfully');
			
			// remove the remove event from Asset
			opts.Asset.pullRemove(removeAsset);
				
			if(_.isFunction(opts.end)) {
				opts.end(opts.name);
			}
		})
		.on('error', function(err) {
			debug('an error happened: ' + err.message);
			
			// remove the remove event from Asset
			opts.Asset.pullRemove(removeAsset);
			
			if(_.isFunction(opts.end)) {
				opts.end();
			}
		})
		// save to stream
		.pipe(transform, {end:false}); //end = true, close output stream after writing
		
		// non-video or non fluent-ffmpeg stream
		// readable(opts.file);
		function readable(file) {
			var r  = fs.createReadStream(opts.file, { autoClose: false });
			ratelimit(r, 250 * 1024); //7 * 188   7200 * 8
			//r.pipe(transform);
			r.on('data', function(data) {
				//debug("send chunk");
				chunkit(data);
			});
			r.on('end', function() {
				if(opts.loop) {
					//debug('readable looping');
					return readable(file);
				}
				
				// remove the remove event from Asset
				opts.Asset.pullRemove(removeAsset);
				
				// run a callback event from another asset
				if(_.isFunction(opts.end)) {
					opts.end();
				}
				
				debug('file done streaming');
			});
		}
		
		var tsource = ss.sources[opts.name] = {
			name: opts.name,
			path: opts.file,
			stream: transform.pipe(new bufferStream()),
			_log: [],
			get log() {
				return this._log;
			},
			set log(entry) {
				return this._log.push(entry)
			},
			type: 'file',
			parentSource: opts.parentSource,
			parentStream: opts.parentStream
		}
		
		// add the file to the current Asset
		opts.Asset.addAsset(opts.name, 'source', tsource);
		
		// push a stop event for this asset onto the parent
		opts.Asset.pushStop(() => {
			debug('check file');
			if(ss.sources[opts.name]  && ss.isReadableStream(ss.sources[opts.name].stream) && _.isFunction(ss.sources[opts.name].stream.end)) {
				debug('stop file');
				ss.sources[opts.name].stream.end();
			}	
		});
		
		// register the remove event for stopping the parent asset
		opts.Asset.pushRemove(removeAsset);
		
		// register the emitter
		ss.emitterReg('source.' + opts.name);
		ss.talk('source.' + opts.name, { action: 'start'});
		
		// update status with the new source
		ss.openSocket.emit('status', ss.Listen.status());
				
		callback(null, tsource);
		return this;
	}
}


