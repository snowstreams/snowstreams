/**
 * Creates a stream from a program
 *
 * ####Example:
 *
 *     ss.source.program('tv1', 'dvgrab', ['-f', 'mpeg2', '-g', '0x94ccb9fffe5dc69a', '-' ], callback)
 *
 *     
 * @param {String} name
 * @param {String} program
 * @param {Array} args
 * @param {Function} callback
 * @api public
 */

var debug = require('debug')('snowstreams:source:program');
var _ = require('lodash');
var stream = require('stream');
var Pass = stream.PassThrough;
var spawn = require('child_process').spawn
var Respawn = require('respawn');
// include the custom buffer duplex stream
var bufferStream = require('../node-streams/leaky');
// snowstreams lib
var ratelimit = require('ratelimit');
var async = require('async');

module.exports = function(ss) {
	
	return function program(options, callback) {
		debug('program start');
		var _this = this;
		var ran = 0;
		var started = 0;
		var stopAsset;
		
		var opts = options
		
		var doc = opts.doc || {};
		opts.name = opts.name || doc.name;
		opts.id = doc._id;
		opts.program = opts.program || doc.program;
		opts.arg = opts.argument || doc.argument;
		opts.respawn = opts.respawn || doc.respawn;
		opts.restart = opts.restart || doc.restart;
		
		debug('program checks');
		
		var Channel = options.Channel;
		
		if(!_.isFunction(callback)) {
			var callback = function(){}
		}
		if(!_.isString(opts.name)) {
			callback('name must be a String');
			return _this;
		}
		if(ss.asset[opts.id]) {
			callback('asset exists');
			return _this;
		}
		if(!_.isString(opts.program) && !_.isObject(opts.program)) {
			callback('there must be a program ' + opts.program);
			return _this;
		}
		
		if(_.isObject(opts.program)) {
			opts.program = opts.program.program;
		}
		
		if(!_.isArray(opts.arg) && _.isObject(opts.arg)) {
			
			opts.arg = opts.arg.argument.split(' ').filter((v) => { return v !== '' });
		
		} else if (_.isString(opts.arg)) {
			
			opts.arg = opts.arg.split(' ').filter((v) => { return v !== '' });
		
		} 
		
		// do we have a full program argument line or is there a source
		if(_.isArray(opts.doc.source) && opts.doc.source.length > 0) {
			debug('run source', opts.doc.source)
			async.eachSeries(opts.doc.source, (source, key, proceed) => {
				if(source.type === 'file' && source.file) {
					// we have a single file to consume
					debug('single file for program', opts.doc.argument.argument);
					opts.arg = ss.createArgument(opts.doc.argument.argument, {
						'input': source.file
					});
					debug('argument', argument);
					useSource(opts);
					proceed();
					
				} else if(source.type === 'file' && source.files) {
					// we have multiple files to consume
					debug('multiple files for program');
					
				} else if(source.type === 'program') {
					// we have a program stream to consume
					debug('program for program');
					
				} else if(source.type === 'http') {
					// we have a http stream to consume
					debug('htpp stream for program');
					
				} else if(source.type === 'udp') {
					// we have a dgram stream to consume
					debug('udp stream for program');
					
				}
				
				
				
			}, (err) => {
				
			});
		} else if(_.isArray(opts.arg)) {
			
			debug('create program')
			// create Asset
			useSource(opts);
			
			// remove on program end
			if(!program.respawn) {
				var tFn = (command) => {
					debug('got emit for ' + opts.name, command);
					if(!_.isObject(command)) {
						command = {
							action: command
						}
					}
					if(command.action === 'stop') {
						debug('remove single run program Asset');
						ss.removeAsset(ss.asset(opts.id));
						ss.removeListener('source.' + opts.name, tFn)
					}
				}
				ss.on('source.' + opts.id, tFn);
			}
			
		} 

		function useSource(opts) {
			
			// create our new Asset
			var Asset = new ss.Asset(opts.name, {
				doc: opts.doc,
				pid: 0,
				spawn: false,
				stream: false,
				command: [opts.program].concat(opts.arg),
				asset: 'source',
				type: 'program',
				program: true,
				respawn: opts.respawn,
				options: opts,
				restart: opts.restart,
				Channel: Channel
			});
			
			if(Channel) {
				debug('add new Asset to Channel ' + Channel.name);
				Channel.addAsset(opts.name, Asset);
			}			
			
			// set streaming bit
			Asset.pushPlay(function(stream, cb) {
				
				debug('set as streaming for program ' + this.name);
				
				if(Asset.doc && _.isFunction(this.doc.save)) {
					this.doc.streaming = true;
					this.doc.save();
				}
				
				cb(null, stream);
				
			}.bind(Asset));
			
			// asset stop function
			var stopAsset = function(pass, cb) {
				
				debug('check for program stream');
				var Asset = this;
				
				if(_.isObject(Asset.monitor) && _.isFunction(Asset.monitor.stop)) {
					debug('stop program monitor');
					Asset.monitor.stop();
				}
				if(ss.isReadableStream(Asset.stream) && _.isFunction(Asset.stream.end)) {
					debug('stop program stream');
					Asset.stream.end();
				}	
				
				this.doc.streaming = false;
				this.doc.save();
				
				ss.notify('status', ss.Listen.status());
				
				debug('stop program done');
				
				if(_.isFunction(cb)) {
					cb(null, pass);
				}
				
			}.bind(Asset);
			
			debug('push stop to asset');
			Asset.pushStop(stopAsset);
			
			debug('program passed tests... creating stream');
			var endStream;
			
			if(opts.respawn) {	
				
				debug('runAgainAgain');
				
				Asset.pushPlay(function(stream, cb) {
					debug('runAgainAgain from Asset ' + this.name);
					runAgainAgain(stream, this, cb);
				}.bind(Asset));
				
				debug('run callback');
				
				callback(null, Asset);
			
			} else {
				
				debug('run once');
				
				Asset.pushPlay(function(stream, cb) {
					debug('runOnce from Asset ' + this.name);
					runOnce(stream, this, cb);
				}.bind(Asset));
				
				debug('run callback');
				
				callback(null, Asset);
			}
			
		}
		
		function runOnce(stream, Asset, cb) {
			
			var opts = Asset.options;
			debug(opts.program, opts.arg);
					
			var runProgram = Asset.spawn = spawn(opts.program, opts.arg);
			
			runProgram.stderr.on('data', function (data) {
				Asset.log = ('stderr: ' + data);
				if(opts.debug) debug('stderr: ' + data);
			});
			
			if(ss.isReadableStream(runProgram.stdout)) {
				
				debug(opts.program + ' produced a readable stream');
				
				//var limited = ratelimit(runProgram.stdout, 250 * 1024);
				Asset.log = (opts.program + ' produced a readable stream');
				Asset.stream = endStream = runProgram.stdout.pipe(new bufferStream({'highWaterMark':7, 'objectMode': true}));
				Asset.pid = runProgram.pid;
				
				// register the emitter
				ss.emitterReg('source.' + opts.id);
				
				debug('emit program action');
				ss.talk('source.' + opts.id, { action: 'start'});
				ss.notify('status', ss.Listen.status());
				
				runProgram.on('close', function (code) {
					debug('program quit ' + opts.program + ' ' + code);
					ss.talk('source.' + opts.id, { action: 'stop'});
				});
								
				cb(null, Asset.stream);
				return _this;
				
			} else {
				
				debug(opts.program + 'did not produce a readable stream');
				Asset.log = (opts.program + 'did not produce a readable stream');
				debug(runProgram);
				cb(opts.program + 'did not produce a readable stream', Asset);
				return _this;
				
			}
		}
		
		function runAgainAgain(stream, Asset, cb) {
			
			var opts = Asset.options;
			
			// generic passthrough stream to survive restart
			Asset.stream = new Pass();
			
			// respawn monitor
			debug('start monitor', Asset.command);
			var monitor = Asset.monitor = Respawn(Asset.command, {
				maxRestarts: opts.restarts || 10, // how many restarts are allowed within 60s or -1 for infinite restarts
				sleep: opts.sleep || 1000,  // time to sleep between restarts,
				kill: opts.kill || 30000, // wait 30s before force killing after stopping
			});
			monitor.start();
			monitor.on('stdout', function (data) {
				// spawn is not emitting so we hack the shit out of this
				if(ran === 0) {
					debug('spawned - in stdout')
					var spawned = monitor.child;
					spawned.stdout.pipe(new bufferStream({'highWaterMark':7, 'objectMode': true})).pipe(Asset.stream);
					Asset.pid = spawned.pid;
					Asset.spawn = spawned;
										
					// register the emitter
					ss.emitterReg('source.' + opts.id);
					ss.notify('status', ss.Listen.status());
					ran = 1;
					if(started === 1) ss.talk('source.' + opts.id, { action: 'restart'});
				}
				if(started === 0) {
					debug('return')
					cb(null, Asset.stream);
					started = 1;
					ss.talk('source.' + opts.id, { action: 'start'});
					return _this;
				}
			});
			monitor.on('spawn', function (process) {
				// this emits on a respawn
				debug('respawn from spawn event', ss.isReadableStream(process.stdout), ss.isReadableStream(Asset.stream));
				
				if(!ss.isReadableStream(process.stdout) || !ss.isReadableStream(Asset.stream)) {
					debug('we have an invalid stream');
					if(Channel) {
						Channel.crash(Asset);
					} else {
						Asset.crash();
					}
					return;
				}
				
				process.stdout.pipe(new bufferStream({'highWaterMark':7, 'objectMode': true})).pipe(Asset.stream);
				Asset.pid = process.pid;
				Asset.spawn = process;
				ss.notify('status', ss.Listen.status());
			});
			monitor.on('start', function () {
				//this doesnt emit for some reason
				debug('start');
			});
			monitor.on('stderr', function (data) {
				Asset.log = ('stderr: ' + data);
				debug('stderr: ' + data);
				Asset.log = (opts.name + ' stderr: ' + data);
				ss.talk('source error', { error: 'stderr: ' + data, stream: opts.name});
				ss.talk('source.' + opts.id, { error: 'stderr: ' + data, stream: opts.name});
				if(started === 0) {
					debug('return')
					cb('program stderr ' + data, Asset.stream);
					started = 1;
				}
			});

			monitor.on('stop', function (code) {
				Asset.log = ('program respawn stopped with code ' + code);
				debug('program respawn stopped with code ' + code);
				Asset.stream.end();
				ss.notify('status', ss.Listen.status());
				// ran = 0;
			});
			monitor.on('exit', function (code, signal) {
				debug('child process exited with code ' + code);
				if(started === 0) {
					debug('return')
					cb('child process exited with code ' + code, Asset);
					started = 1;
				}
				Asset.log = ('child process exited with code ' + code);
				ss.openSocket.emit('status', ss.Listen.status());
				// ran = 0;
			});
			monitor.on('crash', function () {
				debug('program respawn monitor crashed');
				Asset.log = ('program respawn monitor crashed');
				Asset.stream.end();
				ss.removeAsset(Asset);
				ss.openSocket.emit('status', ss.Listen.status());
				// ran = 0;
			});

		}
		
	}
	
	function runSources(opts, returnCallback) {
		async.forEachOf(opts.sources, function(source, key, doNext) {
			debug('create stream and pipe next source in queue');
			// check to see if this asset has been stopped
			if(!ss.asset(Asset.id)) {
				return doNext('Process stopped by external means');
			}
			var next = function(sourceAsset) {
				debug('stream ' + sourceAsset.name + ' done... start next');
				if(sourceAsset) {
					delete sourceAsset;
				}
				ss.notify('status', ss.Listen.status());
				doNext();
			}
			Asset.current = source;
			addSource(Asset, next, returnCallback, function(stream){
				stream.pipe(transform);
			});
		}, function( err ){
			if( err ) {
				debug('A file failed to process');
				if(err) debug(err);
				returnCallback(err, Asset)
			} else {
				debug('All sources have been consumed by the stream successfully');
				// if we are looping start over
				if(opts.loop) {
					debug('Looping enabled... Stating over...');
					runSources(Asset, callback);
				} else {
					returnCallback(null, Asset)
				}
			}
			
			
		});
	}
	
}
