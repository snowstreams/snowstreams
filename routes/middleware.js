/**
 * This file contains the common middleware used by your routes.
 * 
 * Extend or replace these functions as your application requires.
 * 
 * This structure is not enforced, and just a starting point. If
 * you have more middleware you may want to group it as separate
 * modules in your project's /lib directory.
 */

var _ = require('lodash'),
	querystring = require('querystring'),
	keystone = require('keystone');


/**
	Initialises the standard view locals
	
	The included layout depends on the navLinks array to generate
	the navigation in the header, you may wish to change this array
	or replace it with your own templates / logic.
*/

exports.initLocals = function(req, res, next) {
	
	var locals = res.locals;
	
	locals.user = req.user;
	
	var gethost=req.get('host').replace('http://','').split(':');
	locals.socketio = {port:keystone.get('socket port') || false, host:gethost[0] || false, ssl:keystone.get('socket ssl') || false}
	
	locals.csrf_token_value =  keystone.security.csrf.getToken(req, res);
	locals.csrf_token_key = keystone.security.csrf.TOKEN_KEY;
	
	next();
	
};


/**
	Fetches and clears the flashMessages before a view is rendered
*/

exports.flashMessages = function(req, res, next) {
	
	var flashMessages = {
		info: req.flash('info'),
		success: req.flash('success'),
		warning: req.flash('warning'),
		error: req.flash('error')
	};
	
	res.locals.messages = _.any(flashMessages, function(msgs) { return msgs.length; }) ? flashMessages : false;
	
	next();
	
};


/**
	Prevents people from accessing protected pages when they're not signed in
 */

exports.requireUser = function(req, res, next) {
	
	if (!req.user && keystone.get('auth')) {
		req.flash('error', 'Please sign in to access this page.');
		res.redirect('/keystone/signin');
	} else {
		next();
	}
	
};

/**
 * Middleware to initialise a custom API response... copied from keystone initAPI and altered
 *
 * Adds `res.apiResponse` and `res.apiError` methods.
 *
 * ####Example:
 *
 *     app.all('/api*', publicAPI);
 *
 * @param {app.request} req
 * @param {app.response} res
 * @param {function} next
 * @api public
 */

exports.publicAPI = function(req, res, next) {
	res.apiResponse = function(status) {
		//add the requesting url back to the response
		status.url=req.protocol + '://' + req.get('host') + req.originalUrl; 
		/* you can customize the response here using the status object.  dont overwrite your existing props. */
		
		/* add in the response with json */
		if (req.query.callback)
			res.jsonp(status);
		else
			res.json(status);
	};
	res.apiError = function(key, err, msg, code) {
		msg = msg || 'Error';
		key = key || 'unknown error';
		msg += ' (' + key + ')';
		if (keystone.get('logger')) {
			console.log(msg + (err ? ':' : ''));
			if (err) {
				console.log(err);
			}
		}
		res.status(code || 500);
		res.apiResponse({ error: key || 'error', detail: err });
	};
	next();
};

/**
	Inits the error handler functions into `req`
*/
