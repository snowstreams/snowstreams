var _ = require('lodash');
var keystone = require('keystone');
var middleware = require('./middleware');
var importRoutes = keystone.importer(__dirname);
var browserify = require('browserify-middleware');
var reactify = require('reactify');
var literalify = require('literalify');

// Common Middleware
keystone.pre('routes', middleware.initLocals);
keystone.pre('routes', keystone.middleware.api);
keystone.pre('render', middleware.flashMessages);

// Import Route Controllers
var routes = {
	views: importRoutes('./views')
};

// Setup Route Bindings
exports = module.exports = function(app) {
	
	// Views
	app.get('/', function(req, res) {
		res.redirect('/client');
	});
	/*app.get('/react/client', browserify('./client/app.js', {
		transform: [reactify]
	}));*/
	app.get('/client', routes.views.index);
	app.get('/client/*', routes.views.index);
	app.get('/testbed', middleware.requireUser,routes.views.testbed);
	
	// NOTE: To protect a route so that only admins can see it, use the requireUser middleware:
	// app.get('/protected', middleware.requireUser, routes.views.protected);
	
};
