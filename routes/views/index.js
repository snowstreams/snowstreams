var keystone = require('keystone');

exports = module.exports = function(req, res) {
	
	var view = new keystone.View(req, res),
		locals = res.locals;
	
	locals.socketPort = keystone.get('port');
	locals.socketHost = keystone.get('socketHost') || req.hostname;
	res.header('x-frame-options', '*');
	// Render the view
	view.render('index');
	
};
